<!DOCTYPE html>
<html style="margin: 0;padding: 0;">
<!--<![endif]-->
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <title></title><!--[if !mso]><!-->
    <meta content="IE=edge" http-equiv="X-UA-Compatible"><!--<![endif]-->
    <meta content="width=device-width" name="viewport">
    <style type="text/css">
        @media only screen and (min-width: 620px){.wrapper{min-width:600px !important}.wrapper h1{}.wrapper h1{font-size:64px !important;line-height:63px !important}.wrapper h2{}.wrapper h2{font-size:36px !important;line-height:43px !important}.wrapper h3{}.wrapper h3{font-size:20px !important;line-height:28px !important}.column{}.wrapper .size-8{font-size:8px !important;line-height:14px !important}.wrapper .size-9{font-size:9px !important;line-height:16px !important}.wrapper .size-10{font-size:10px !important;line-height:18px !important}.wrapper .size-11{font-size:11px !important;line-height:19px !important}.wrapper .size-12{font-size:12px !important;line-height:19px !important}.wrapper .size-13{font-size:13px !important;line-height:21px !important}.wrapper .size-14{font-size:14px !important;line-height:21px !important}.wrapper .size-15{font-size:15px !important;line-height:23px
        !important}.wrapper .size-16{font-size:16px !important;line-height:24px !important}.wrapper .size-17{font-size:17px !important;line-height:26px !important}.wrapper .size-18{font-size:18px !important;line-height:26px !important}.wrapper .size-20{font-size:20px !important;line-height:28px !important}.wrapper .size-22{font-size:22px !important;line-height:31px !important}.wrapper .size-24{font-size:24px !important;line-height:32px !important}.wrapper .size-26{font-size:26px !important;line-height:34px !important}.wrapper .size-28{font-size:28px !important;line-height:36px !important}.wrapper .size-30{font-size:30px !important;line-height:38px !important}.wrapper .size-32{font-size:32px !important;line-height:40px !important}.wrapper .size-34{font-size:34px !important;line-height:43px !important}.wrapper .size-36{font-size:36px !important;line-height:43px !important}.wrapper
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   .size-40{font-size:40px !important;line-height:47px !important}.wrapper .size-44{font-size:44px !important;line-height:50px !important}.wrapper .size-48{font-size:48px !important;line-height:54px !important}.wrapper .size-56{font-size:56px !important;line-height:60px !important}.wrapper .size-64{font-size:64px !important;line-height:63px !important}}
    </style>
    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
        }
        table {
            border-collapse: collapse;
            table-layout: fixed;
        }
        * {
            line-height: inherit;
        }
        [x-apple-data-detectors],
        [href^="tel"],
        [href^="sms"] {
            color: inherit !important;
            text-decoration: none !important;
        }
        .wrapper .footer__share-button a:hover,
        .wrapper .footer__share-button a:focus {
            color: #ffffff !important;
        }
        .btn a:hover,
        .btn a:focus,
        .footer__share-button a:hover,
        .footer__share-button a:focus,
        .email-footer__links a:hover,
        .email-footer__links a:focus {
            opacity: 0.8;
        }
        .preheader,
        .header,
        .layout,
        .column {
            transition: width 0.25s ease-in-out, max-width 0.25s ease-in-out;
        }
        .layout,
        div.header {
            max-width: 400px !important;
            -fallback-width: 95% !important;
            width: calc(100% - 20px) !important;
        }
        div.preheader {
            max-width: 360px !important;
            -fallback-width: 90% !important;
            width: calc(100% - 60px) !important;
        }
        .snippet,
        .webversion {
            Float: none !important;
        }
        .column {
            max-width: 400px !important;
            width: 100% !important;
        }
        .fixed-width.has-border {
            max-width: 402px !important;
        }
        .fixed-width.has-border .layout__inner {
            box-sizing: border-box;
        }
        .snippet,
        .webversion {
            width: 50% !important;
        }
        .ie .btn {
            width: 100%;
        }
        [owa] .column div,
        [owa] .column button {
            display: block !important;
        }
        .ie .column,
        [owa] .column,
        .ie .gutter,
        [owa] .gutter {
            display: table-cell;
            float: none !important;
            vertical-align: top;
        }
        .ie div.preheader,
        [owa] div.preheader,
        .ie .email-footer,
        [owa] .email-footer {
            max-width: 560px !important;
            width: 560px !important;
        }
        .ie .snippet,
        [owa] .snippet,
        .ie .webversion,
        [owa] .webversion {
            width: 280px !important;
        }
        .ie div.header,
        [owa] div.header,
        .ie .layout,
        [owa] .layout,
        .ie .one-col .column,
        [owa] .one-col .column {
            max-width: 600px !important;
            width: 600px !important;
        }
        .ie .fixed-width.has-border,
        [owa] .fixed-width.has-border,
        .ie .has-gutter.has-border,
        [owa] .has-gutter.has-border {
            max-width: 602px !important;
            width: 602px !important;
        }
        .ie .two-col .column,
        [owa] .two-col .column {
            max-width: 300px !important;
            width: 300px !important;
        }
        .ie .three-col .column,
        [owa] .three-col .column,
        .ie .narrow,
        [owa] .narrow {
            max-width: 200px !important;
            width: 200px !important;
        }
        .ie .wide,
        [owa] .wide {
            width: 400px !important;
        }
        .ie .two-col.has-gutter .column,
        [owa] .two-col.x_has-gutter .column {
            max-width: 290px !important;
            width: 290px !important;
        }
        .ie .three-col.has-gutter .column,
        [owa] .three-col.x_has-gutter .column,
        .ie .has-gutter .narrow,
        [owa] .has-gutter .narrow {
            max-width: 188px !important;
            width: 188px !important;
        }
        .ie .has-gutter .wide,
        [owa] .has-gutter .wide {
            max-width: 394px !important;
            width: 394px !important;
        }
        .ie .two-col.has-gutter.has-border .column,
        [owa] .two-col.x_has-gutter.x_has-border .column {
            max-width: 292px !important;
            width: 292px !important;
        }
        .ie .three-col.has-gutter.has-border .column,
        [owa] .three-col.x_has-gutter.x_has-border .column,
        .ie .has-gutter.has-border .narrow,
        [owa] .has-gutter.x_has-border .narrow {
            max-width: 190px !important;
            width: 190px !important;
        }
        .ie .has-gutter.has-border .wide,
        [owa] .has-gutter.x_has-border .wide {
            max-width: 396px !important;
            width: 396px !important;
        }
        .ie .fixed-width .layout__inner {
            border-left: 0 none white !important;
            border-right: 0 none white !important;
        }
        .ie .layout__edges {
            display: none;
        }
        .mso .layout__edges {
            font-size: 0;
        }
        .layout-fixed-width,
        .mso .layout-full-width {
            background-color: #ffffff;
        }
        @media only screen and (min-width: 620px) {
            .column,
            .gutter {
                display: table-cell;
                Float: none !important;
                vertical-align: top;
            }
            div.preheader,
            .email-footer {
                max-width: 560px !important;
                width: 560px !important;
            }
            .snippet,
            .webversion {
                width: 280px !important;
            }
            div.header,
            .layout,
            .one-col .column {
                max-width: 800px !important;
                width: 800px !important;
            }
            .fixed-width.has-border,
            .fixed-width.ecxhas-border,
            .has-gutter.has-border,
            .has-gutter.ecxhas-border {
                max-width: 602px !important;
                width: 602px !important;
            }
            .two-col .column {
                max-width: 300px !important;
                width: 300px !important;
            }
            .three-col .column,
            .column.narrow {
                max-width: 200px !important;
                width: 200px !important;
            }
            .column.wide {
                width: 400px !important;
            }
            .two-col.has-gutter .column,
            .two-col.ecxhas-gutter .column {
                max-width: 290px !important;
                width: 290px !important;
            }
            .three-col.has-gutter .column,
            .three-col.ecxhas-gutter .column,
            .has-gutter .narrow {
                max-width: 188px !important;
                width: 188px !important;
            }
            .has-gutter .wide {
                max-width: 394px !important;
                width: 394px !important;
            }
            .two-col.has-gutter.has-border .column,
            .two-col.ecxhas-gutter.ecxhas-border .column {
                max-width: 292px !important;
                width: 292px !important;
            }
            .three-col.has-gutter.has-border .column,
            .three-col.ecxhas-gutter.ecxhas-border .column,
            .has-gutter.has-border .narrow,
            .has-gutter.ecxhas-border .narrow {
                max-width: 190px !important;
                width: 190px !important;
            }
            .has-gutter.has-border .wide,
            .has-gutter.ecxhas-border .wide {
                max-width: 396px !important;
                width: 396px !important;
            }
        }
        @media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
            .fblike {
                background-image: url(https://i3.createsend1.com/static/eb/master/13-the-blueprint-3/images/fblike@2x.png) !important;
            }
            .tweet {
                background-image: url(https://i4.createsend1.com/static/eb/master/13-the-blueprint-3/images/tweet@2x.png) !important;
            }
            .linkedinshare {
                background-image: url(https://i6.createsend1.com/static/eb/master/13-the-blueprint-3/images/lishare@2x.png) !important;
            }
            .forwardtoafriend {
                background-image: url(https://i5.createsend1.com/static/eb/master/13-the-blueprint-3/images/forward@2x.png) !important;
            }
        }
        @media (max-width: 321px) {
            .fixed-width.has-border .layout__inner {
                border-width: 1px 0 !important;
            }
            .layout,
            .column {
                min-width: 320px !important;
                width: 320px !important;
            }
            .border {
                display: none;
            }
        }
        .mso div {
            border: 0 none white !important;
        }
        .mso .w560 .divider {
            Margin-left: 260px !important;
            Margin-right: 260px !important;
        }
        .mso .w360 .divider {
            Margin-left: 160px !important;
            Margin-right: 160px !important;
        }
        .mso .w260 .divider {
            Margin-left: 110px !important;
            Margin-right: 110px !important;
        }
        .mso .w160 .divider {
            Margin-left: 60px !important;
            Margin-right: 60px !important;
        }
        .mso .w354 .divider {
            Margin-left: 157px !important;
            Margin-right: 157px !important;
        }
        .mso .w250 .divider {
            Margin-left: 105px !important;
            Margin-right: 105px !important;
        }
        .mso .w148 .divider {
            Margin-left: 54px !important;
            Margin-right: 54px !important;
        }
        .mso .footer__share-button p {
            margin: 0;
        }
        .mso .size-8,
        .ie .size-8 {
            font-size: 8px !important;
            line-height: 14px !important;
        }
        .mso .size-9,
        .ie .size-9 {
            font-size: 9px !important;
            line-height: 16px !important;
        }
        .mso .size-10,
        .ie .size-10 {
            font-size: 10px !important;
            line-height: 18px !important;
        }
        .mso .size-11,
        .ie .size-11 {
            font-size: 11px !important;
            line-height: 19px !important;
        }
        .mso .size-12,
        .ie .size-12 {
            font-size: 12px !important;
            line-height: 19px !important;
        }
        .mso .size-13,
        .ie .size-13 {
            font-size: 13px !important;
            line-height: 21px !important;
        }
        .mso .size-14,
        .ie .size-14 {
            font-size: 14px !important;
            line-height: 21px !important;
        }
        .mso .size-15,
        .ie .size-15 {
            font-size: 15px !important;
            line-height: 23px !important;
        }
        .mso .size-16,
        .ie .size-16 {
            font-size: 16px !important;
            line-height: 24px !important;
        }
        .mso .size-17,
        .ie .size-17 {
            font-size: 17px !important;
            line-height: 26px !important;
        }
        .mso .size-18,
        .ie .size-18 {
            font-size: 18px !important;
            line-height: 26px !important;
        }
        .mso .size-20,
        .ie .size-20 {
            font-size: 20px !important;
            line-height: 28px !important;
        }
        .mso .size-22,
        .ie .size-22 {
            font-size: 22px !important;
            line-height: 31px !important;
        }
        .mso .size-24,
        .ie .size-24 {
            font-size: 24px !important;
            line-height: 32px !important;
        }
        .mso .size-26,
        .ie .size-26 {
            font-size: 26px !important;
            line-height: 34px !important;
        }
        .mso .size-28,
        .ie .size-28 {
            font-size: 28px !important;
            line-height: 36px !important;
        }
        .mso .size-30,
        .ie .size-30 {
            font-size: 30px !important;
            line-height: 38px !important;
        }
        .mso .size-32,
        .ie .size-32 {
            font-size: 32px !important;
            line-height: 40px !important;
        }
        .mso .size-34,
        .ie .size-34 {
            font-size: 34px !important;
            line-height: 43px !important;
        }
        .mso .size-36,
        .ie .size-36 {
            font-size: 36px !important;
            line-height: 43px !important;
        }
        .mso .size-40,
        .ie .size-40 {
            font-size: 40px !important;
            line-height: 47px !important;
        }
        .mso .size-44,
        .ie .size-44 {
            font-size: 44px !important;
            line-height: 50px !important;
        }
        .mso .size-48,
        .ie .size-48 {
            font-size: 48px !important;
            line-height: 54px !important;
        }
        .mso .size-56,
        .ie .size-56 {
            font-size: 56px !important;
            line-height: 60px !important;
        }
        .mso .size-64,
        .ie .size-64 {
            font-size: 64px !important;
            line-height: 63px !important;
        }
        .footer__share-button p {
            margin: 0;
        }

        .name_food{
            margin-top: 0;
            margin-bottom: 0;
            font-style: normal;
            font-weight: normal;
            color: #14161e;
            font-size: 60px;
            line-height: 58px;
            text-align: center;
            background-color:rgba(255, 110, 0, 0.85);
            padding: 30px;
            margin: 15px ;
            border-radius: 10px;
        }
        @media screen and (max-width: 480px) {
            body {

            .name_food{
                margin-top: 0;
                margin-bottom: 0;
                font-style: normal;
                font-weight: normal;
                color: #14161e;
                font-size: 60px;
                line-height: 58px;
                text-align: center;
                background-color:rgba(255, 110, 0, 0.85);
                padding: 30px;
                margin: 15px ;
                border-radius: 10px;
            }
        }
        }
    </style>
    <style type="text/css">
        body{background-color:#fff}.logo a:hover,.logo a:focus{color:#859bb1 !important}.mso .layout-has-border{border-top:1px solid #ccc;border-bottom:1px solid #ccc}.mso .layout-has-bottom-border{border-bottom:1px solid #ccc}.mso .border,.ie .border{background-color:#ccc}.mso h1,.ie h1{}.mso h1,.ie h1{font-size:64px !important;line-height:63px !important}.mso h2,.ie h2{}.mso h2,.ie h2{font-size:36px !important;line-height:43px !important}.mso h3,.ie h3{}.mso h3,.ie h3{font-size:20px !important;line-height:28px !important}.mso .layout__inner,.ie .layout__inner{}
    </style>
    <meta content="noindex,nofollow" name="robots">
    <meta content="20%" property="og:title">
    <link href="./a2102.createsend.com_files/social.min.css" media="screen,projection" rel="stylesheet" type="text/css">
    <style type="text/css">
        .fb_hidden{position:absolute;top:-10000px;z-index:10001}.fb_reposition{overflow:hidden;position:relative}.fb_invisible{display:none}.fb_reset{background:none;border:0;border-spacing:0;color:#000;cursor:auto;direction:ltr;font-family:"lucida grande", tahoma, verdana, arial, sans-serif;font-size:11px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;line-height:1;margin:0;overflow:visible;padding:0;text-align:left;text-decoration:none;text-indent:0;text-shadow:none;text-transform:none;visibility:visible;white-space:normal;word-spacing:normal}.fb_reset>div{overflow:hidden}.fb_link img{border:none}@keyframes fb_transform{from{opacity:0;transform:scale(.95)}to{opacity:1;transform:scale(1)}}.fb_animate{animation:fb_transform .3s forwards}
        .fb_dialog{background:rgba(82, 82, 82, .7);position:absolute;top:-10000px;z-index:10001}.fb_reset .fb_dialog_legacy{overflow:visible}.fb_dialog_advanced{padding:10px;-moz-border-radius:8px;-webkit-border-radius:8px;border-radius:8px}.fb_dialog_content{background:#fff;color:#333}.fb_dialog_close_icon{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 0 transparent;cursor:pointer;display:block;height:15px;position:absolute;right:18px;top:17px;width:15px}.fb_dialog_mobile .fb_dialog_close_icon{top:5px;left:5px;right:auto}.fb_dialog_padding{background-color:transparent;position:absolute;width:1px;z-index:-1}.fb_dialog_close_icon:hover{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 -15px transparent}.fb_dialog_close_icon:active{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 -30px transparent}.fb_dialog_loader{background-color:#f6f7f9;border:1px solid #606060;font-size:24px;padding:20px}.fb_dialog_top_left,.fb_dialog_top_right,.fb_dialog_bottom_left,.fb_dialog_bottom_right{height:10px;width:10px;overflow:hidden;position:absolute}.fb_dialog_top_left{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ye/r/8YeTNIlTZjm.png) no-repeat 0 0;left:-10px;top:-10px}.fb_dialog_top_right{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ye/r/8YeTNIlTZjm.png) no-repeat 0 -10px;right:-10px;top:-10px}.fb_dialog_bottom_left{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ye/r/8YeTNIlTZjm.png) no-repeat 0 -20px;bottom:-10px;left:-10px}.fb_dialog_bottom_right{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ye/r/8YeTNIlTZjm.png) no-repeat 0 -30px;right:-10px;bottom:-10px}.fb_dialog_vert_left,.fb_dialog_vert_right,.fb_dialog_horiz_top,.fb_dialog_horiz_bottom{position:absolute;background:#525252;filter:alpha(opacity=70);opacity:.7}.fb_dialog_vert_left,.fb_dialog_vert_right{width:10px;height:100%}.fb_dialog_vert_left{margin-left:-10px}.fb_dialog_vert_right{right:0;margin-right:-10px}.fb_dialog_horiz_top,.fb_dialog_horiz_bottom{width:100%;height:10px}.fb_dialog_horiz_top{margin-top:-10px}.fb_dialog_horiz_bottom{bottom:0;margin-bottom:-10px}.fb_dialog_iframe{line-height:0}.fb_dialog_content .dialog_title{background:#6d84b4;border:1px solid #365899;color:#fff;font-size:14px;font-weight:bold;margin:0}.fb_dialog_content .dialog_title>span{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yd/r/Cou7n-nqK52.gif) no-repeat 5px 50%;float:left;padding:5px 0 7px 26px}body.fb_hidden{-webkit-transform:none;height:100%;margin:0;overflow:visible;position:absolute;top:-10000px;left:0;width:100%}.fb_dialog.fb_dialog_mobile.loading{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ya/r/3rhSv5V8j3o.gif) white no-repeat 50% 50%;min-height:100%;min-width:100%;overflow:hidden;position:absolute;top:0;z-index:10001}.fb_dialog.fb_dialog_mobile.loading.centered{width:auto;height:auto;min-height:initial;min-width:initial;background:none}.fb_dialog.fb_dialog_mobile.loading.centered #fb_dialog_loader_spinner{width:100%}.fb_dialog.fb_dialog_mobile.loading.centered .fb_dialog_content{background:none}.loading.centered #fb_dialog_loader_close{color:#fff;display:block;padding-top:20px;clear:both;font-size:18px}#fb-root #fb_dialog_ipad_overlay{background:rgba(0, 0, 0, .45);position:absolute;bottom:0;left:0;right:0;top:0;width:100%;min-height:100%;z-index:10000}#fb-root #fb_dialog_ipad_overlay.hidden{display:none}.fb_dialog.fb_dialog_mobile.loading iframe{visibility:hidden}.fb_dialog_content .dialog_header{-webkit-box-shadow:white 0 1px 1px -1px inset;background:-webkit-gradient(linear, 0% 0%, 0% 100%, from(#738ABA), to(#2C4987));border-bottom:1px solid;border-color:#1d4088;color:#fff;font:14px Helvetica, sans-serif;font-weight:bold;text-overflow:ellipsis;text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0;vertical-align:middle;white-space:nowrap}.fb_dialog_content .dialog_header table{-webkit-font-smoothing:subpixel-antialiased;height:43px;width:100%}.fb_dialog_content .dialog_header td.header_left{font-size:12px;padding-left:5px;vertical-align:middle;width:60px}.fb_dialog_content .dialog_header td.header_right{font-size:12px;padding-right:5px;vertical-align:middle;width:60px}.fb_dialog_content .touchable_button{background:-webkit-gradient(linear, 0% 0%, 0% 100%, from(#4966A6), color-stop(.5, #355492), to(#2A4887));border:1px solid #29487d;-webkit-background-clip:padding-box;-webkit-border-radius:3px;-webkit-box-shadow:rgba(0, 0, 0, .117188) 0 1px 1px inset, rgba(255, 255, 255, .167969) 0 1px 0;display:inline-block;margin-top:3px;max-width:85px;line-height:18px;padding:4px 12px;position:relative}.fb_dialog_content .dialog_header .touchable_button input{border:none;background:none;color:#fff;font:12px Helvetica, sans-serif;font-weight:bold;margin:2px -12px;padding:2px 6px 3px 6px;text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0}.fb_dialog_content .dialog_header .header_center{color:#fff;font-size:16px;font-weight:bold;line-height:18px;text-align:center;vertical-align:middle}.fb_dialog_content .dialog_content{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/y9/r/jKEcVPZFk-2.gif) no-repeat 50% 50%;border:1px solid #555;border-bottom:0;border-top:0;height:150px}.fb_dialog_content .dialog_footer{background:#f6f7f9;border:1px solid #555;border-top-color:#ccc;height:40px}#fb_dialog_loader_close{float:left}.fb_dialog.fb_dialog_mobile .fb_dialog_close_button{text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0}.fb_dialog.fb_dialog_mobile .fb_dialog_close_icon{visibility:hidden}#fb_dialog_loader_spinner{animation:rotateSpinner 1.2s linear infinite;background-color:transparent;background-image:url(https://static.xx.fbcdn.net/rsrc.php/v3/yD/r/t-wz8gw1xG1.png);background-repeat:no-repeat;background-position:50% 50%;height:24px;width:24px}@keyframes rotateSpinner{0%{transform:rotate(0deg)}100%{transform:rotate(360deg)}}
        .fb_iframe_widget{display:inline-block;position:relative}.fb_iframe_widget span{display:inline-block;position:relative;text-align:justify}.fb_iframe_widget iframe{position:absolute}.fb_iframe_widget_fluid_desktop,.fb_iframe_widget_fluid_desktop span,.fb_iframe_widget_fluid_desktop iframe{max-width:100%}.fb_iframe_widget_fluid_desktop iframe{min-width:220px;position:relative}.fb_iframe_widget_lift{z-index:1}.fb_hide_iframes iframe{position:relative;left:-10000px}.fb_iframe_widget_loader{position:relative;display:inline-block}.fb_iframe_widget_fluid{display:inline}.fb_iframe_widget_fluid span{width:100%}.fb_iframe_widget_loader iframe{min-height:32px;z-index:2;zoom:1}.fb_iframe_widget_loader .FB_Loader{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/y9/r/jKEcVPZFk-2.gif) no-repeat;height:32px;width:32px;margin-left:-16px;position:absolute;left:50%;z-index:4}
    </style>
</head><!--[if mso]>
<body class="mso">
<![endif]-->
<!--[if !mso]><!-->
<body class="no-padding" style="margin: 0px; padding: 0px; text-size-adjust: 100%;">
<!--<![endif]-->
<table cellpadding="0" cellspacing="0" class="wrapper" role="presentation" style="border-collapse: collapse;table-layout: fixed;min-width: 320px;width: 100%;background-color: #fff;">
    <tbody>
    <tr>
        <td>
            <div role="banner">

                <div class="header" id="emb-email-header-container" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);">
                    <!--[if (mso)|(IE)]><table align="center" class="header" cellpadding="0" cellspacing="0" role="presentation"><tr><td style="width: 600px"><![endif]-->
                    <div align="center" class="logo emb-logo-margin-box" style="font-size: 26px;    font-family: Avenir,sans-serif;line-height: 32px;Margin-top: 6px;Margin-bottom: 20px;color: #c3ced9;font-family: Roboto,sans-serif;Margin-left: 20px;Margin-right: 20px;">
                        <div align="center" class="logo-center" id="emb-email-header"><img alt="" src="https://a2bliving.ie/img/a2b-living-logo-1488647975-2.jpg" style="display: block;height: auto;width: 100%;border: 0;max-width: 257px;" width="257"></div>
                    </div><!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                </div>
            </div>
            <div role="section">

                <div>
                    <p style=" display: block;
    margin: 0 auto; width: 100%;  max-width: 1134px;margin-top: 0;margin-bottom: 50px;font-style: normal;     font-family: Avenir,sans-serif;font-weight: normal;color: #14161e;font-size: 18px;line-height: 25px;text-align: center; background-color:rgba(255, 110, 0, 0.85);
                                border-radius: 10px;
                                padding: 10px
                                "><span style="color:#ffffff">Dear ${user_name},<br>We all love food. Enjoy good food with friends with our 20% discount!</span></p>


                    <a href="http://onelink.to/q56pzh">
                        <img src="https://a2bliving.ie/img/bg202.jpg" style="width: 100%;
                                                        max-width: 1134px;

    height: auto;
     display: block;
    margin: 0 auto;">
                    </a>

                </div>



            </div>
            <div style="background-color: #ff6d00;">
                <div class="layout one-col" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">
                    <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;">
                        <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" role="presentation"><tr class="layout-full-width" style="background-color: #14161e;"><td class="layout__edges">&nbsp;</td><td style="width: 600px" class="w560"><![endif]-->
                        <div class="column" style="max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);text-align: left;color: #656873;font-size: 14px;line-height: 21px;font-family: Avenir,sans-serif;">
                            <div style="Margin-left: 20px;Margin-right: 20px;">
                                <div style="line-height:52px;font-size:1px">
                                    &nbsp;
                                </div>
                            </div>
                            <div style="Margin-left: 20px;Margin-right: 20px;">
                                <h3 style="Margin-top: 0;Margin-bottom: 12px;font-style: normal;font-weight: normal;color: #ffffff;font-size: 17px;line-height: 26px;text-align: center;">Don't forget to visit our A2B Grocery shop as well! We have a vast range of products to serve your daily needs.</h3>
                            </div>
                            <div style="Margin-left: 20px;Margin-right: 20px;">
                                <div style="line-height:41px;font-size:1px">
                                    &nbsp;
                                </div>
                            </div>
                        </div><!--[if (mso)|(IE)]></td><td class="layout__edges">&nbsp;</td></tr></table><![endif]-->
                    </div>
                </div>
            </div>

            <div style="line-height:20px;font-size:70px;">
                &nbsp;
            </div>
            <div class="layout two-col fixed-width" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">
                <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;background-color: #ffffff;" emb-background-style>
                    <!--[if (mso)|(IE)]><table align="center" cellpadding="0" cellspacing="0" role="presentation"><tr class="layout-fixed-width" emb-background-style><td style="width: 300px" valign="top" class="w260"><![endif]-->
                    <div class="column" style="text-align: left;color: #60666d;font-size: 14px;line-height: 21px;font-family: sans-serif;Float: left;max-width: 320px;min-width: 300px; width: 320px;width: calc(12300px - 2000%);">

                        <div style="font-size: 12px;font-style: normal;font-weight: normal;" align="center">
                            <a style="text-decoration: underline;transition: opacity 0.1s ease-in;color: #00afd1;" href="https://play.google.com/store/apps/details?id=a2bliving.ie.a2b"><img style="border: 0;display: block;height: auto;width: 100%;max-width: 120px;" alt="" width="120" src="http://www.a2bliving.ie/img/email/android.png" /></a>
                        </div>

                    </div>
                    <!--[if (mso)|(IE)]></td><td style="width: 300px" valign="top" class="w260"><![endif]-->
                    <div class="column" style="text-align: left;color: #60666d;font-size: 14px;line-height: 21px;font-family: sans-serif;Float: left;max-width: 320px;min-width: 300px; width: 320px;width: calc(12300px - 2000%);">

                        <div style="font-size: 12px;font-style: normal;font-weight: normal;" align="center">
                            <a style="text-decoration: underline;transition: opacity 0.1s ease-in;color: #00afd1;" href="https://itunes.apple.com/us/app/a2b-living/id1174079302?mt=8"><img style="border: 0;display: block;height: auto;width: 100%;max-width: 120px;" alt="" width="120" src="http://www.a2bliving.ie/img/email/ios.png" /></a>
                        </div>

                    </div>
                    <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                </div>
            </div>
            <div role="contentinfo">
                <div class="layout email-footer" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">
                    <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;">
                        <!--[if (mso)|(IE)]><table align="center" cellpadding="0" cellspacing="0" role="presentation"><tr class="layout-email-footer"><td style="width: 400px;" valign="top" class="w360"><![endif]-->
                        <div class="column wide" style="text-align: left;font-size: 12px;line-height: 19px;color: #b8b8b8;font-family: Ubuntu,sans-serif;Float: left;max-width: 400px;min-width: 320px; width: 320px;width: calc(8000% - 47600px);">
                            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 10px;Margin-bottom: 10px;">
                                <table class="email-footer__links emb-web-links" style="border-collapse: collapse;table-layout: fixed;" role="presentation"><tbody><tr role="navigation">
                                    <td class="emb-web-links" style="padding: 0;width: 26px;"><a style="text-decoration: underline;transition: opacity 0.1s ease-in;color: #b8b8b8;" href="https://www.facebook.com/A2BLiving/?fref=ts"><img style="border: 0;" src="https://i8.createsend1.com/static/eb/master/13-the-blueprint-3/images/facebook.png" width="26" height="26" /></a></td>
                                </tr></tbody></table>
                                <div style="font-size: 12px;line-height: 19px;Margin-top: 20px;">
                                    <div>A2BLiving<br />
                                        wwww.a2bliving.ie</div>
                                </div>
                                <div style="font-size: 12px;line-height: 19px;Margin-top: 18px;">

                                </div>
                            </div>
                        </div>

                        <!--[if (mso)|(IE)]></td><td style="width: 200px;" valign="top" class="w160"><![endif]-->
                        <div class="column narrow" style="text-align: left;font-size: 12px;line-height: 19px;color: #b8b8b8;font-family: Ubuntu,sans-serif;Float: left;max-width: 320px;min-width: 200px; width: 320px;width: calc(72200px - 12000%);">
                            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 10px;Margin-bottom: 10px;">

                            </div>
                        </div>
                        <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                    </div>
                </div>
                <div class="layout one-col email-footer" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">
                    <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;">
                        <!--[if (mso)|(IE)]><table align="center" cellpadding="0" cellspacing="0" role="presentation"><tr class="layout-email-footer"><td style="width: 600px;" class="w560"><![endif]-->
                        <div class="column" style="text-align: left;font-size: 12px;line-height: 19px;color: #b8b8b8;font-family: Ubuntu,sans-serif;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);">
                            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 10px;Margin-bottom: 10px;">
                                <div style="font-size: 12px;line-height: 19px;">
                                    <!-- <unsubscribe style="text-decoration: underline;">Unsubscribe</unsubscribe> -->
                                </div>
                            </div>
                        </div>
                        <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                    </div>
                </div>
            </div>
            <div style="line-height:40px;font-size:40px;">
                &nbsp;
            </div>
            </div>
        </td>
    </tr>
    </tbody>
</table><img alt="" border="0" height="1" src="./a2102.createsend.com_files/o.gif" style="overflow: hidden;position: fixed;visibility: hidden !important;display: block !important;height: 1px !important;width: 1px !important;border: 0 !important;margin: 0 !important;padding: 0 !important;" width="1">

<div id="facebox" style="display:none;">
    <div class="popup">
        <div class="content"></div>
        <div id="closeBox">
            <a class="close" href="https://a2102.createsend.com/t/ViewEmail/d/291C533C844A970B/C67FD2F38AC4859C/?tx=0&amp;previewAll=1&amp;print=1#">Close</a>
        </div>
    </div>
</div>
</body>
</html>