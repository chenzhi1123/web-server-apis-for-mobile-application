/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

import com.avaje.ebean.annotation.Sql;
import play.db.ebean.Model;

import javax.persistence.Entity;
import java.io.Serializable;

/**
 * This class is for querying product description with RawSql, using Ebean class to retrieve data from
 * database. Table involved: ps_product_lang
 * */

@Entity
@Sql
public class DeviceTokenTemplate extends Model implements Serializable
{
    public String device_token;
    public String device_type;

    public DeviceTokenTemplate(String product_description) {
        this.device_token = device_token;
        this.device_type = device_type;
    }

    public DeviceTokenTemplate()
    {
    }

    @Override
    public String toString() {
        return "DeviceTokenTemplate{" +
                "device_token='" + device_token + '\'' +
                ", device_type='" + device_type + '\'' +
                '}';
    }
}