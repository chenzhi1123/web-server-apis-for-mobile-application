/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

/**
 * Created by 20060 on 2017/3/28.
 */
import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Sql
public class DiscountOrders {

    public int id_order;
    public int id_order_detail;
    public int product_id;
    public String product_name;
    public int id_tax_rules_group;
    public int product_quantity;
    public String tax_type = "";
    public java.math.BigDecimal total_price_tax_incl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal original_product_price = BigDecimal.valueOf(0.000000);  //不含税
    public java.math.BigDecimal total_product_discount = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal original_unit_price = BigDecimal.valueOf(0.000000);  //应该含税
    public Date invoice_date;
    public String product_reference;

    public DiscountOrders(int id_order, int id_order_detail, int product_id, String product_name, int id_tax_rules_group, int product_quantity, String tax_type, BigDecimal total_price_tax_incl, BigDecimal original_product_price, BigDecimal total_product_discount, BigDecimal original_unit_price,Date invoice_date, String product_reference) {
        this.id_order = id_order;
        this.id_order_detail = id_order_detail;
        this.product_id = product_id;
        this.product_name = product_name;
        this.id_tax_rules_group = id_tax_rules_group;
        this.product_quantity = product_quantity;
        this.tax_type = tax_type;
        this.total_price_tax_incl = total_price_tax_incl;
        this.original_product_price = original_product_price;
        this.total_product_discount = total_product_discount;
        this.original_unit_price = original_unit_price;
        this.invoice_date = invoice_date;
        this.product_reference = product_reference;
    }

    public DiscountOrders(){

    }

    @Override
    public String toString() {
        return "DiscountOrders{" +
                "id_order=" + id_order +
                ", id_order_detail=" + id_order_detail +
                ", product_id=" + product_id +
                ", product_name='" + product_name + '\'' +
                ", id_tax_rules_group=" + id_tax_rules_group +
                ", product_quantity=" + product_quantity +
                ", tax_type='" + tax_type + '\'' +
                ", total_price_tax_incl=" + total_price_tax_incl +
                ", original_product_price=" + original_product_price +
                ", total_product_discount=" + total_product_discount +
                ", original_unit_price=" + original_unit_price +
                ", invoice_date=" + invoice_date +
                ", product_reference='" + product_reference + '\'' +
                '}';
    }
}

