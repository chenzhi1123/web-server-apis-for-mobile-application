/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Sql
public class OrderWithSequence implements Serializable {
    public Integer id_order;
    public String order_reference;
    public Integer order_sequence;
    public Integer id_customer;
    public String payment_method;
    public String order_state;
    public String delivery_address;
    public java.math.BigDecimal total_discounts = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_paid = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_shipping = BigDecimal.valueOf(0.000000);
    public Date date_add;
    public Date delivery_date;


    public OrderWithSequence(Integer id_order, String order_reference, Integer order_sequence, Integer id_customer, String payment_method, String order_state, String delivery_address, BigDecimal total_discounts, BigDecimal total_paid, BigDecimal total_shipping, Date date_add, Date delivery_date) {
        this.id_order = id_order;
        this.order_reference = order_reference;
        this.order_sequence = order_sequence;
        this.id_customer = id_customer;
        this.payment_method = payment_method;
        this.order_state = order_state;
        this.delivery_address = delivery_address;
        this.total_discounts = total_discounts;
        this.total_paid = total_paid;
        this.total_shipping = total_shipping;
        this.date_add = date_add;
        this.delivery_date = delivery_date;
    }

    public OrderWithSequence(){}


    @Override
    public String toString() {
        return "OrderWithSequence{" +
                "id_order=" + id_order +
                ", order_reference='" + order_reference + '\'' +
                ", order_sequence=" + order_sequence +
                ", id_customer=" + id_customer +
                ", payment_method='" + payment_method + '\'' +
                ", order_state='" + order_state + '\'' +
                ", delivery_address='" + delivery_address + '\'' +
                ", total_discounts=" + total_discounts +
                ", total_paid=" + total_paid +
                ", total_shipping=" + total_shipping +
                ", date_add=" + date_add +
                ", delivery_date=" + delivery_date +
                '}';
    }
}
