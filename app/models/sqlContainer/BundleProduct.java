/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

/**
 * Created by Zhi Chen on 2017/11/09.
 */

import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;

@Entity
@Sql
public class BundleProduct {

    public int id_onsale_product;
    public int id_real_product;
    public int bought_quantity_onsale_product;
    public int basic_unit;
    public int bundle_number;

    public BundleProduct(int id_onsale_product, int id_real_product, int bought_quantity_onsale_product, int basic_unit, int bundle_number) {
        this.id_onsale_product = id_onsale_product;
        this.id_real_product = id_real_product;
        this.bought_quantity_onsale_product = bought_quantity_onsale_product;
        this.basic_unit = basic_unit;
        this.bundle_number = bundle_number;
    }

    public BundleProduct(){

    }

    @Override
    public String toString() {
        return "BundleProduct{" +
                "id_onsale_product=" + id_onsale_product +
                ", id_real_product=" + id_real_product +
                ", bought_quantity_onsale_product=" + bought_quantity_onsale_product +
                ", basic_unit=" + basic_unit +
                ", bundle_number=" + bundle_number +
                '}';
    }
}

