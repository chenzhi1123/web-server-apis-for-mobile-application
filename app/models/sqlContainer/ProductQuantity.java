/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import java.io.Serializable;
@Entity
@Sql
public class ProductQuantity implements Serializable {
   public Integer quantity;
}
