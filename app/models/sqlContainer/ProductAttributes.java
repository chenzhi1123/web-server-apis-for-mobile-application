/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.ArrayList;

@Entity
public class ProductAttributes implements Serializable{


    public ArrayList<ProductAttribute> productAttributes;

    public ProductAttributes(){

    }


    public ProductAttributes(ArrayList<ProductAttribute> productAttributes)
    {
        super();
        this.productAttributes = productAttributes;
    }

    @Override
    public String toString() {
        return "ProductAttributes{" +
                "productAttributes=" + productAttributes +
                '}';
    }
}