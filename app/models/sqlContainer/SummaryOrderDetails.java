/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

/**
 * Created by 20060 on 2017/3/28.
 */
import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Sql
public class SummaryOrderDetails {
   //ps_orders Table
    public Integer id_order;
    public String payment;
    public Integer id_customer;
    public Integer if_valid;
    public java.math.BigDecimal total_discounts = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_shipping = BigDecimal.valueOf(0.000000);
    public Date invoice_date;

    //ps_order_detail Table
    public Integer product_id;
    public java.math.BigDecimal total_price_tax_incl = BigDecimal.valueOf(0.000000); //含税， 含折扣
    public java.math.BigDecimal original_product_price = BigDecimal.valueOf(0.000000); //不含税
    public Integer id_tax_rules_group;
    public Integer product_quantity;

    //ps_drivers_orders
    public Integer id_cashier = 0;
    public Integer id_driver = 0;

    //ps_customers
    public String first_name = "";
    public String last_name = "";

    public SummaryOrderDetails(Integer id_order, String payment, Integer id_customer, Integer if_valid, BigDecimal total_discounts, BigDecimal total_shipping, Date invoice_date, Integer product_id, BigDecimal total_price_tax_incl, BigDecimal original_product_price, Integer id_tax_rules_group, Integer product_quantity, Integer id_cashier, Integer id_driver, String first_name, String last_name) {
        this.id_order = id_order;
        this.payment = payment;
        this.id_customer = id_customer;
        this.if_valid = if_valid;
        this.total_discounts = total_discounts;
        this.total_shipping = total_shipping;
        this.invoice_date = invoice_date;
        this.product_id = product_id;
        this.total_price_tax_incl = total_price_tax_incl;
        this.original_product_price = original_product_price;
        this.id_tax_rules_group = id_tax_rules_group;
        this.product_quantity = product_quantity;
        this.id_cashier = id_cashier;
        this.id_driver = id_driver;
        this.first_name = first_name;
        this.last_name = last_name;
    }

    public SummaryOrderDetails(){

    }

    @Override
    public String toString() {
        return "SummaryOrderDetails{" +
                "id_order=" + id_order +
                ", payment='" + payment + '\'' +
                ", id_customer=" + id_customer +
                ", if_valid=" + if_valid +
                ", total_discounts=" + total_discounts +
                ", total_shipping=" + total_shipping +
                ", invoice_date=" + invoice_date +
                ", product_id=" + product_id +
                ", total_price_tax_incl=" + total_price_tax_incl +
                ", original_product_price=" + original_product_price +
                ", id_tax_rules_group=" + id_tax_rules_group +
                ", product_quantity=" + product_quantity +
                ", id_cashier=" + id_cashier +
                ", id_driver=" + id_driver +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                '}';
    }
}

