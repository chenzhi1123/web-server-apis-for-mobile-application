/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
/**
 * This class is for querying product description with RawSql, using Ebean class to retrieve data from
 * database. Table involved: ps_product_lang
 * */

@Entity
@Sql
public class Description implements Serializable
{
    @Id
    public String product_description;
//    public void update (CustomerGroup customerGroup)
//    {
//        this.id_customer = customerGroup.id_customer;
//        this.id_group  = customerGroup.id_group;
//
//    }
    public Description(String product_description) {
        this.product_description = product_description;
    }

    public Description()
    {
    }

    @Override
    public String toString() {
        return "Description{" +
                "product_description='" + product_description + '\'' +
                '}';
    }

}