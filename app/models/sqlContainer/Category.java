/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

import com.avaje.ebean.annotation.Sql;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
/**
 * Template class for RawSQL use. query using SQL and parse the data with this class
 * @see controllers.CategoryAPI
 * */

@Entity
@Sql
public class Category extends Model implements Serializable
{
    @Id
    public int   id_category;
    public int   id_parent;
    public int   level_depth;
    public String name;
    public String link_rewrite;
    public int position;
    private Categories subCategory;

    public Category(int id_category, int id_parent, int level_depth, String name, String link_rewrite, int position, Categories subCategory) {
        this.id_category = id_category;
        this.id_parent = id_parent;
        this.level_depth = level_depth;
        this.name = name;
        this.link_rewrite = link_rewrite;
        this.position = position;
        this.subCategory = subCategory;
    }

    public Category()
    {
    }


    @Override
    public String toString() {
        return "Category{" +
                "id_category=" + id_category +
                ", id_parent=" + id_parent +
                ", level_depth=" + level_depth +
                ", name='" + name + '\'' +
                ", link_rewrite='" + link_rewrite + '\'' +
                ", position=" + position +
                ", subCategory=" + subCategory +
                '}';
    }

    @Override
    public void update(Object arg0)
    {
      super.update(arg0);
    }

    

    public static Model.Finder<String, Category> find = new Model.Finder<String, Category>(String.class, Category.class);

    
}