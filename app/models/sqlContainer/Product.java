/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

import com.avaje.ebean.annotation.Sql;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Entity
@Sql
public class Product extends Model implements Serializable
{
    @Id
    public int   id_product;
    public int   id_category;
    public int   position;
    public int   id_compare;
    public Date   date_add;
    public Date   date_upd;
    public int id_supplier;
    public int id_manufacturer;
    public int id_category_default;
    public int id_shop_default;
    public String reference;
    public int id_attachment;
    public int id_product_attribute;
    public Integer reduction_tax;
    public int quantity;
    public String id_attribute_reference;
    public String reduction_type;
    public float price;
    public java.math.BigDecimal reduction;
    public float weight;
    public float unit_price_impact;
    public int id_attribute;
    //to distinguish if it belongs to food product or shop
    public  String upc = null;
    //tax
    public Integer tax_id;
    public java.math.BigDecimal rate;
    public int id_tax_rules_group;
    //discount
    //text information about one product
    public int id_shop;
    public int id_lang;
    public String description;
    public String description_short;
    public String link_rewrite;
    public String name;
    public String available_now;
    public String available_later;

    //image_ID
    public Integer id_image;
    public ArrayList attributes = null;

    public Product(int id_product, int id_category, int position, int id_compare, Date date_add, Date date_upd, int id_supplier, int id_manufacturer, int id_category_default, int id_shop_default, String reference, int id_attachment, int id_product_attribute, Integer reduction_tax, int quantity, String id_attribute_reference, String reduction_type, float price, BigDecimal reduction, float weight, float unit_price_impact, int id_attribute, String upc, Integer tax_id, BigDecimal rate, int id_tax_rules_group ,int id_shop, int id_lang, String description, String description_short, String link_rewrite, String name, String available_now, String available_later, Integer id_image, ArrayList attributes) {
        this.id_product = id_product;
        this.id_category = id_category;
        this.position = position;
        this.id_compare = id_compare;
        this.date_add = date_add;
        this.date_upd = date_upd;
        this.id_supplier = id_supplier;
        this.id_manufacturer = id_manufacturer;
        this.id_category_default = id_category_default;
        this.id_shop_default = id_shop_default;
        this.reference = reference;
        this.id_attachment = id_attachment;
        this.id_product_attribute = id_product_attribute;
        this.reduction_tax = reduction_tax;
        this.quantity = quantity;
        this.id_attribute_reference = id_attribute_reference;
        this.reduction_type = reduction_type;
        this.price = price;
        this.reduction = reduction;
        this.weight = weight;
        this.unit_price_impact = unit_price_impact;
        this.id_attribute = id_attribute;
        this.upc = upc;
        this.tax_id = tax_id;
        this.rate = rate;
        this.id_tax_rules_group = id_tax_rules_group;
        this.id_shop = id_shop;
        this.id_lang = id_lang;
        this.description = description;
        this.description_short = description_short;
        this.link_rewrite = link_rewrite;
        this.name = name;
        this.available_now = available_now;
        this.available_later = available_later;
        this.id_image = id_image;
        this.attributes = attributes;
    }

    public Product()
    {

    }

    @Override
    public String toString() {
        return "Product{" +
                "id_product=" + id_product +
                ", id_category=" + id_category +
                ", position=" + position +
                ", id_compare=" + id_compare +
                ", date_add=" + date_add +
                ", date_upd=" + date_upd +
                ", id_supplier=" + id_supplier +
                ", id_manufacturer=" + id_manufacturer +
                ", id_category_default=" + id_category_default +
                ", id_shop_default=" + id_shop_default +
                ", reference='" + reference + '\'' +
                ", id_attachment=" + id_attachment +
                ", id_product_attribute=" + id_product_attribute +
                ", reduction_tax=" + reduction_tax +
                ", quantity=" + quantity +
                ", id_attribute_reference='" + id_attribute_reference + '\'' +
                ", reduction_type='" + reduction_type + '\'' +
                ", price=" + price +
                ", reduction=" + reduction +
                ", weight=" + weight +
                ", unit_price_impact=" + unit_price_impact +
                ", id_attribute=" + id_attribute +
                ", upc='" + upc + '\'' +
                ", tax_id=" + tax_id +
                ", rate=" + rate +
                ", id_tax_rules_group=" + id_tax_rules_group +
                ", id_shop=" + id_shop +
                ", id_lang=" + id_lang +
                ", description='" + description + '\'' +
                ", description_short='" + description_short + '\'' +
                ", link_rewrite='" + link_rewrite + '\'' +
                ", name='" + name + '\'' +
                ", available_now='" + available_now + '\'' +
                ", available_later='" + available_later + '\'' +
                ", id_image=" + id_image +
                ", attributes=" + attributes +
                '}';
    }


    public static Product findByProductId(Long id)
    {
        return find.where().eq("id_product", id).findUnique();
    }

    public static List<Product> findAll()
    {
        return find.all();
    }

    public static Model.Finder<String, Product> find = new Model.Finder<String, Product>(String.class, Product.class);

}