/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Sql
public class ReturnManagerOrder {

    public int id_order;
    public String order_reference;
    public int id_customer;
    public String payment;
    public java.math.BigDecimal total_products_wt = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_shipping = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_discounts = BigDecimal.valueOf(0.000000);
    public Date date_add;
    public String order_status;
    public String driver_name = "Not assigned completely";
    public int order_number;


    public ReturnManagerOrder(int id_order, String order_reference, int id_customer, String payment, BigDecimal total_products_wt, BigDecimal total_shipping, Date date_add, String order_status, String driver_name, int order_number, BigDecimal total_discounts) {
        this.id_order = id_order;
        this.order_reference = order_reference;
        this.id_customer = id_customer;
        this.payment = payment;
        this.total_products_wt = total_products_wt;
        this.total_shipping = total_shipping;
        this.date_add = date_add;
        this.order_status = order_status;
        this.driver_name = driver_name;
        this.order_number = order_number;
        this.total_discounts = total_discounts;
    }

    public ReturnManagerOrder(){}

    @Override
    public String toString() {
        return "ReturnManagerOrder{" +
                "id_order=" + id_order +
                ", order_reference='" + order_reference + '\'' +
                ", id_customer=" + id_customer +
                ", payment='" + payment + '\'' +
                ", total_products_wt=" + total_products_wt +
                ", total_shipping=" + total_shipping +
                ", total_discounts=" + total_discounts +
                ", date_add=" + date_add +
                ", order_status='" + order_status + '\'' +
                ", driver_name='" + driver_name + '\'' +
                ", order_number=" + order_number +
                '}';
    }
}
