/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

import com.avaje.ebean.annotation.Sql;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
@Entity
@Sql
public class ProductAttribute extends Model implements Serializable
{
    @Id
    public int   id_attribute;
    public int   id_attribute_group;
    public int   id_product_attribute;
    public String group_type;
    public String public_name;
    public String name;
    public float price;
    public int quantity;
    public int position;

    public ArrayList productAttributeImage;
    public ProductAttribute(int id_attribute, int id_attribute_group, int id_product_attribute, String group_type, String public_name, String name, float price, int quantity, int position, ArrayList productAttributeImage) {
        this.id_attribute = id_attribute;
        this.id_attribute_group = id_attribute_group;
        this.id_product_attribute = id_product_attribute;
        this.group_type = group_type;
        this.public_name = public_name;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.position = position;
        this.productAttributeImage = productAttributeImage;
    }

    public ProductAttribute(){}

    @Override
    public String toString() {
        return "ProductAttribute{" +
                "id_attribute=" + id_attribute +
                ", id_attribute_group=" + id_attribute_group +
                ", id_product_attribute=" + id_product_attribute +
                ", group_type='" + group_type + '\'' +
                ", public_name='" + public_name + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                ", position=" + position +
                ", productAttributeImage=" + productAttributeImage +
                '}';
    }

    public static ProductAttribute findByProductAttributeID(Long id)
    {
        return find.where().eq("id_attribute", id).findUnique();
    }

    public static List<ProductAttribute> findAll()
    {
        return find.all();
    }


    public static Model.Finder<String, ProductAttribute> find = new Model.Finder<String, ProductAttribute>(String.class, ProductAttribute.class);


}