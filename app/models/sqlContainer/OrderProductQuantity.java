/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@Sql
public class OrderProductQuantity implements Serializable {
   public Integer product_id;  //订单里的产品id
   public Integer bought_quantity; //这件产品被购买的数量


    public OrderProductQuantity(Integer product_id, Integer bought_quantity) {
        this.product_id = product_id;
        this.bought_quantity = bought_quantity;
    }

    @Override
    public String toString() {
        return "OrderProductQuantity{" +
                "product_id=" + product_id +
                ", bought_quantity=" + bought_quantity +
                '}';
    }
}
