/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;
/**
 * 给打印机发送的司机送单情况
 * */

import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.Date;

@Entity
@Sql
public class ReturnDriverOrders {
    public String driver_name = "";
    public String contact_number = "";
    public Integer order_number;
    public String working_status = "";
    public ArrayList<Integer> doing_orders = new ArrayList<>();
    public ArrayList<Integer> todo_orders = new ArrayList<>();
    public Date start_time;
    public Date delivered_time;

    public ReturnDriverOrders(String driver_name, String contact_number, Integer order_number, String working_status, ArrayList<Integer> doing_orders, ArrayList<Integer> todo_orders, Date start_time, Date delivered_time) {
        this.driver_name = driver_name;
        this.contact_number = contact_number;
        this.order_number = order_number;
        this.working_status = working_status;
        this.doing_orders = doing_orders;
        this.todo_orders = todo_orders;
        this.start_time = start_time;
        this.delivered_time = delivered_time;
    }

    public ReturnDriverOrders() {

    }

    @Override
    public String toString() {
        return "ReturnDriverOrders{" +
                "driver_name='" + driver_name + '\'' +
                ", contact_number='" + contact_number + '\'' +
                ", order_number=" + order_number +
                ", working_status='" + working_status + '\'' +
                ", doing_orders=" + doing_orders +
                ", todo_orders=" + todo_orders +
                ", start_time=" + start_time +
                ", delivered_time=" + delivered_time +
                '}';
    }
}
