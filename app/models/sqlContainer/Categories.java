/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.ArrayList;
/**
 * RawSQL and Ebean require a class for executing SQL commands.
 * */
@Entity
public class Categories implements Serializable{
  
  
  public ArrayList<Category> categories;

  public Categories(ArrayList<Category> categories)
  {
    super();
    this.categories = categories;
  }


  @Override
  public String toString()
  {
    return "Categories [categories=" + categories + "]";
  }

  
}