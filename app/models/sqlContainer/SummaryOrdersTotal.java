/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

/**
 * Created by 20060 on 2017/3/28.
 */

import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Sql
public class SummaryOrdersTotal {

    public int id_order;
    //ps_drivers_orders
    public int id_cashier;
    public int id_driver;
    public String payment = "";
    //ps_customers
    public String first_name = "";
    public String last_name = "";
    public int if_valid;
    public java.math.BigDecimal total_voucher_discounts = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal delviery_fee = BigDecimal.valueOf(0.000000);

    public int total_products_quantity;
    public java.math.BigDecimal total_products_sales = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_tax = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_product_discounts = BigDecimal.valueOf(0.000000);

    public Date invoice_date;



    public SummaryOrdersTotal(int id_order, int id_cashier, int id_driver, String payment, String first_name, String last_name, int if_valid, int total_products_quantity, BigDecimal total_products_sales, BigDecimal total_tax, BigDecimal total_product_discounts, BigDecimal total_voucher_discounts, BigDecimal delviery_fee, Date invoice_date) {
        this.id_order = id_order;
        this.id_cashier = id_cashier;
        this.id_driver = id_driver;
        this.payment = payment;
        this.first_name = first_name;
        this.last_name = last_name;
        this.if_valid = if_valid;
        this.total_products_quantity = total_products_quantity;
        this.total_products_sales = total_products_sales;
        this.total_tax = total_tax;
        this.total_product_discounts = total_product_discounts;
        this.total_voucher_discounts = total_voucher_discounts;
        this.delviery_fee = delviery_fee;
        this.invoice_date = invoice_date;
    }

    public SummaryOrdersTotal(){

    }

    @Override
    public String toString() {
        return "SummaryOrdersTotal{" +
                "id_order=" + id_order +
                ", id_cashier=" + id_cashier +
                ", id_driver=" + id_driver +
                ", payment='" + payment + '\'' +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", if_valid=" + if_valid +
                ", total_voucher_discounts=" + total_voucher_discounts +
                ", delviery_fee=" + delviery_fee +
                ", total_products_quantity=" + total_products_quantity +
                ", total_products_sales=" + total_products_sales +
                ", total_tax=" + total_tax +
                ", total_product_discounts=" + total_product_discounts +
                ", invoice_date=" + invoice_date +
                '}';
    }
}

