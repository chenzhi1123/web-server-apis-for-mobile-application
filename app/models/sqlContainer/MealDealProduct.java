/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;
/**
 * 给打印机发送的司机送单情况
 * */

import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;

@Entity
@Sql
public class MealDealProduct {
    public int id_product_shop;
    public String key_word = "";
    public int purchased_quantity;

    public MealDealProduct(int id_product_shop, String key_word, int purchased_quantity) {
        this.id_product_shop = id_product_shop;
        this.key_word = key_word;
        this.purchased_quantity = purchased_quantity;
    }

    public MealDealProduct() {

    }

    @Override
    public String toString() {
        return "MealDealProduct{" +
                "id_product_shop=" + id_product_shop +
                ", key_word='" + key_word + '\'' +
                ", purchased_quantity=" + purchased_quantity +
                '}';
    }
}
