/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import java.math.BigDecimal;
import java.util.ArrayList;
@Entity
@Sql
public class TotalReturnDriverOrder {
    public String driver_name = "";
    public String payment_method = "";
    public int total_numberof_online_order;
    public int total_numberof_cash_order;
    public java.math.BigDecimal total_shipping_online = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_shipping_cash = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_sale_online = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_sale_cash  = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal unpaid_amount  = BigDecimal.valueOf(0.000000);
    public int unpaid_number;
    public int marina_orders;
    public ArrayList<ReturnDriverOrder> driverOrders = new ArrayList<>();

    public TotalReturnDriverOrder(String driver_name, String payment_method, int total_numberof_online_order, int total_numberof_cash_order, BigDecimal total_shipping_online, BigDecimal total_shipping_cash, BigDecimal total_sale_online, BigDecimal total_sale_cash, BigDecimal unpaid_amount, int unpaid_number, ArrayList<ReturnDriverOrder> driverOrders, int marina_orders) {
        this.driver_name = driver_name;
        this.payment_method = payment_method;
        this.total_numberof_online_order = total_numberof_online_order;
        this.total_numberof_cash_order = total_numberof_cash_order;
        this.total_shipping_online = total_shipping_online;
        this.total_shipping_cash = total_shipping_cash;
        this.total_sale_online = total_sale_online;
        this.total_sale_cash = total_sale_cash;
        this.unpaid_amount = unpaid_amount;
        this.unpaid_number = unpaid_number;
        this.driverOrders = driverOrders;
        this.marina_orders = marina_orders;
    }

    public TotalReturnDriverOrder() {

    }

    @Override
    public String toString() {
        return "TotalReturnDriverOrder{" +
                "driver_name='" + driver_name + '\'' +
                ", payment_method='" + payment_method + '\'' +
                ", total_numberof_online_order=" + total_numberof_online_order +
                ", total_numberof_cash_order=" + total_numberof_cash_order +
                ", total_shipping_online=" + total_shipping_online +
                ", total_shipping_cash=" + total_shipping_cash +
                ", total_sale_online=" + total_sale_online +
                ", total_sale_cash=" + total_sale_cash +
                ", unpaid_amount=" + unpaid_amount +
                ", unpaid_number=" + unpaid_number +
                ", marina_orders=" + marina_orders +
                ", driverOrders=" + driverOrders +
                '}';
    }
}
