/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import java.math.BigDecimal;
@Entity
@Sql
public class BrandSummary {

    public String brand_name = "";
    public int number_of_piece;
    public java.math.BigDecimal total_sales = BigDecimal.valueOf(0.000000);

    public BrandSummary(String brand_name, int number_of_piece, BigDecimal total_sales) {
        this.brand_name = brand_name;
        this.number_of_piece = number_of_piece;
        this.total_sales = total_sales;
    }

    public BrandSummary() {

    }

    @Override
    public String toString() {
        return "BrandSummary{" +
                "brand_name='" + brand_name + '\'' +
                ", number_of_piece=" + number_of_piece +
                ", total_sales=" + total_sales +
                '}';
    }
}
