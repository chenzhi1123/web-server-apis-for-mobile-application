/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

import com.avaje.ebean.annotation.Sql;
import models.template.CodeValidateTool;

import javax.persistence.Entity;
import java.math.BigDecimal;
import java.util.ArrayList;

@Entity
@Sql
public class NewCodeValidation {
    public BigDecimal minimum_amount = BigDecimal.valueOf(0.00);
    public int minimum_amount_if_shipping;
    public int if_free_shipping;
    public BigDecimal reduction_percent = BigDecimal.valueOf(0.00);
    public BigDecimal reduction_amount = BigDecimal.valueOf(0.00);
    public int reduction_product;
    public int product_restriction; //new
    public int group_restriction;
    public int minimum_product_quantity_required = 0;
    public int if_valid = 0;
    public String promotion_name = "";
    public int quantity_per_user;
    public String description = "";
    public int id_cart_rule;
    public String type = ""; // new
    public ArrayList<CodeValidateTool> products_constraint;  //new


    public NewCodeValidation(BigDecimal minimum_amount, int minimum_amount_if_shipping, int if_free_shipping, BigDecimal reduction_percent, BigDecimal reduction_amount, int reduction_product, int product_restriction, int group_restriction, int minimum_product_quantity_required, int if_valid, String promotion_name, int quantity_per_user, String description, int id_cart_rule, String type) {
        this.minimum_amount = minimum_amount;
        this.minimum_amount_if_shipping = minimum_amount_if_shipping;
        this.if_free_shipping = if_free_shipping;
        this.reduction_percent = reduction_percent;
        this.reduction_amount = reduction_amount;
        this.reduction_product = reduction_product;
        this.product_restriction = product_restriction;
        this.group_restriction = group_restriction;
        this.minimum_product_quantity_required = minimum_product_quantity_required;
        this.if_valid = if_valid;
        this.promotion_name = promotion_name;
        this.quantity_per_user = quantity_per_user;
        this.description = description;
        this.id_cart_rule = id_cart_rule;
        this.type = type;
    }

    public NewCodeValidation() {

    }

    @Override
    public String toString() {
        return "NewCodeValidation{" +
                "minimum_amount=" + minimum_amount +
                ", minimum_amount_if_shipping=" + minimum_amount_if_shipping +
                ", if_free_shipping=" + if_free_shipping +
                ", reduction_percent=" + reduction_percent +
                ", reduction_amount=" + reduction_amount +
                ", reduction_product=" + reduction_product +
                ", product_restriction=" + product_restriction +
                ", group_restriction=" + group_restriction +
                ", minimum_product_quantity_required=" + minimum_product_quantity_required +
                ", if_valid=" + if_valid +
                ", promotion_name='" + promotion_name + '\'' +
                ", quantity_per_user=" + quantity_per_user +
                ", description='" + description + '\'' +
                ", id_cart_rule=" + id_cart_rule +
                ", type='" + type + '\'' +
                '}';
    }
}
