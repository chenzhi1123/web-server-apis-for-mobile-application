/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.template;

import java.math.BigDecimal;
/**
 * This class is the template for creating objects containing order information after the first time of request from
 * mobile client, while waiting for authentication from the third-party payment company, if that process leads to a
 * yes answer, an object of this class will be sent to API server again to continue with order placing process.
 * */
public class ReturnInformation {
    public int id_order;
    public int id_customer;
    public int id_address_delivery;
    public int id_address_invoice;
    public int if_paid;
    public int id_order_payment; //new


    public String reference;
    public String SHA;
    public String secure_key;
    public String invoice_address; //new
    public String delivery_address;//new

    public java.math.BigDecimal total_paid = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_paid_tax_incl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_paid_tax_excl =  BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_paid_real =  BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_products =  BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_products_wt = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_shipping = BigDecimal.valueOf(0.000000);

    public java.math.BigDecimal total_shipping_tax_excl =  BigDecimal.valueOf(0.000000);  //new
    public java.math.BigDecimal total_shipping_tax_incl = BigDecimal.valueOf(0.000000);   //new

    //new attributes for promotion
    public int id_cart_rule = 0;
    public String name = "";
    public BigDecimal value = BigDecimal.valueOf(0.00);
    public BigDecimal value_tax_excl = BigDecimal.valueOf(0.00);
    public int free_shipping = 0;
    public int mobile_type = 0;//手机类型

    public  ReturnInformation(){}

    public ReturnInformation(int id_order, int id_customer, int id_address_delivery, int id_address_invoice, int if_paid, int id_order_payment, String reference, String SHA, String secure_key, String invoice_address, String delivery_address, BigDecimal total_paid, BigDecimal total_paid_tax_incl, BigDecimal total_paid_tax_excl, BigDecimal total_paid_real, BigDecimal total_products, BigDecimal total_products_wt, BigDecimal total_shipping, BigDecimal total_shipping_tax_excl, BigDecimal total_shipping_tax_incl, int id_cart_rule, String name, BigDecimal value, BigDecimal value_tax_excl, int free_shipping, int mobile_type) {
        this.id_order = id_order;
        this.id_customer = id_customer;
        this.id_address_delivery = id_address_delivery;
        this.id_address_invoice = id_address_invoice;
        this.if_paid = if_paid;
        this.id_order_payment = id_order_payment;
        this.reference = reference;
        this.SHA = SHA;
        this.secure_key = secure_key;
        this.invoice_address = invoice_address;
        this.delivery_address = delivery_address;
        this.total_paid = total_paid;
        this.total_paid_tax_incl = total_paid_tax_incl;
        this.total_paid_tax_excl = total_paid_tax_excl;
        this.total_paid_real = total_paid_real;
        this.total_products = total_products;
        this.total_products_wt = total_products_wt;
        this.total_shipping = total_shipping;
        this.total_shipping_tax_excl = total_shipping_tax_excl;
        this.total_shipping_tax_incl = total_shipping_tax_incl;
        this.id_cart_rule = id_cart_rule;
        this.name = name;
        this.value = value;
        this.value_tax_excl = value_tax_excl;
        this.free_shipping = free_shipping;
        this.mobile_type = mobile_type;
    }

    @Override
    public String toString() {
        return "ReturnInformation{" +
                "id_order=" + id_order +
                ", id_customer=" + id_customer +
                ", id_address_delivery=" + id_address_delivery +
                ", id_address_invoice=" + id_address_invoice +
                ", if_paid=" + if_paid +
                ", id_order_payment=" + id_order_payment +
                ", reference='" + reference + '\'' +
                ", SHA='" + SHA + '\'' +
                ", secure_key='" + secure_key + '\'' +
                ", invoice_address='" + invoice_address + '\'' +
                ", delivery_address='" + delivery_address + '\'' +
                ", total_paid=" + total_paid +
                ", total_paid_tax_incl=" + total_paid_tax_incl +
                ", total_paid_tax_excl=" + total_paid_tax_excl +
                ", total_paid_real=" + total_paid_real +
                ", total_products=" + total_products +
                ", total_products_wt=" + total_products_wt +
                ", total_shipping=" + total_shipping +
                ", total_shipping_tax_excl=" + total_shipping_tax_excl +
                ", total_shipping_tax_incl=" + total_shipping_tax_incl +
                ", id_cart_rule=" + id_cart_rule +
                ", name='" + name + '\'' +
                ", value=" + value +
                ", value_tax_excl=" + value_tax_excl +
                ", free_shipping=" + free_shipping +
                ", mobile_type=" + mobile_type +
                '}';
    }
}
