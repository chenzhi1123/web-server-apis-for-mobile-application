/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.template;

import play.db.ebean.Model;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by Zhi Chen on 2016/10/17.
 */
public class TotalOrder extends Model {
    public int id_customer;
    public int id_address_delivery;
    public int id_address_invoice;
    public java.math.BigDecimal total_paid = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_paid_tax_incl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_paid_tax_excl =  BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_paid_real =  BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_products =  BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_products_wt = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_shipping = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_shipping_tax_incl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_shipping_tax_excl = BigDecimal.valueOf(0.000000);
    public int payment_method;
    public String invoice_address = "";
    public String delivery_address = "";
    public ArrayList<OrderProduct> products;
    public String secure_key;
    public String message;
    //new attributes for promotion
    public int id_cart_rule = 0;
    public String name = "";
    public BigDecimal value = BigDecimal.valueOf(0.00); //打折（含税）了多少钱
    public BigDecimal value_tax_excl = BigDecimal.valueOf(0.00); //打折（不含税）多少钱
    public int free_shipping = 0;
    public int mobile_type = 0;


    public TotalOrder()
    {

    }

    public TotalOrder(int id_customer, int id_address_delivery, int id_address_invoice, BigDecimal total_paid, BigDecimal total_paid_tax_incl, BigDecimal total_paid_tax_excl, BigDecimal total_paid_real, BigDecimal total_products, BigDecimal total_products_wt, BigDecimal total_shipping, BigDecimal total_shipping_tax_incl, BigDecimal total_shipping_tax_excl, int payment_method, String invoice_address, String delivery_address, ArrayList<OrderProduct> products, String secure_key, String message, int id_cart_rule, String name, BigDecimal value, BigDecimal value_tax_excl, int free_shipping, int mobile_type) {
        this.id_customer = id_customer;
        this.id_address_delivery = id_address_delivery;
        this.id_address_invoice = id_address_invoice;
        this.total_paid = total_paid;
        this.total_paid_tax_incl = total_paid_tax_incl;
        this.total_paid_tax_excl = total_paid_tax_excl;
        this.total_paid_real = total_paid_real;
        this.total_products = total_products;
        this.total_products_wt = total_products_wt;
        this.total_shipping = total_shipping;
        this.total_shipping_tax_incl = total_shipping_tax_incl;
        this.total_shipping_tax_excl = total_shipping_tax_excl;
        this.payment_method = payment_method;
        this.invoice_address = invoice_address;
        this.delivery_address = delivery_address;
        this.products = products;
        this.secure_key = secure_key;
        this.message = message;
        this.id_cart_rule = id_cart_rule;
        this.name = name;
        this.value = value;
        this.value_tax_excl = value_tax_excl;
        this.free_shipping = free_shipping;
        this.mobile_type = mobile_type;
    }

    @Override
    public String toString() {
        return "TotalOrder{" +
                "id_customer=" + id_customer +
                ", id_address_delivery=" + id_address_delivery +
                ", id_address_invoice=" + id_address_invoice +
                ", total_paid=" + total_paid +
                ", total_paid_tax_incl=" + total_paid_tax_incl +
                ", total_paid_tax_excl=" + total_paid_tax_excl +
                ", total_paid_real=" + total_paid_real +
                ", total_products=" + total_products +
                ", total_products_wt=" + total_products_wt +
                ", total_shipping=" + total_shipping +
                ", total_shipping_tax_incl=" + total_shipping_tax_incl +
                ", total_shipping_tax_excl=" + total_shipping_tax_excl +
                ", payment_method=" + payment_method +
                ", invoice_address='" + invoice_address + '\'' +
                ", delivery_address='" + delivery_address + '\'' +
                ", products=" + products +
                ", secure_key='" + secure_key + '\'' +
                ", message='" + message + '\'' +
                ", id_cart_rule=" + id_cart_rule +
                ", name='" + name + '\'' +
                ", value=" + value +
                ", value_tax_excl=" + value_tax_excl +
                ", free_shipping=" + free_shipping +
                ", mobile_type=" + mobile_type +
                '}';
    }
}
