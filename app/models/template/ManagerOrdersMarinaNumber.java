/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.template;

import models.sqlContainer.ReturnManagerOrder;

import java.util.ArrayList;

/**
 * 计算Marina订单情况
 * Created by 20060 on 2017/3/25.
 */
public class ManagerOrdersMarinaNumber {
    public ArrayList<ReturnManagerOrder> returnManagerOrders = new ArrayList<ReturnManagerOrder>();
    public int marinaOrders;
    public int marinaChargedOrders;

    public ManagerOrdersMarinaNumber(ArrayList<ReturnManagerOrder> returnManagerOrders, int marinaOrders, int marinaChargedOrders) {
        this.returnManagerOrders = returnManagerOrders;
        this.marinaOrders = marinaOrders;
        this.marinaChargedOrders = marinaChargedOrders;
    }

    public ManagerOrdersMarinaNumber(){

    }

    @Override
    public String toString() {
        return "ManagerOrdersMarinaNumber{" +
                "returnManagerOrders=" + returnManagerOrders +
                ", marinaOrders=" + marinaOrders +
                ", marinaChargedOrders=" + marinaChargedOrders +
                '}';
    }
}
