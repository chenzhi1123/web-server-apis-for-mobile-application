/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.template;

import java.math.BigDecimal;

/**
 * Created by Zhi Chen  on 2017/3/29.
 */
public class DeliverySummary {
    public int quantity;
    public java.math.BigDecimal total_shipping = BigDecimal.valueOf(0.000000);
    public String delivery_type = "";

    public DeliverySummary(int quantity, BigDecimal total_shipping, String delivery_type) {
        this.quantity = quantity;
        this.total_shipping = total_shipping;
        this.delivery_type = delivery_type;
    }

    public DeliverySummary(){}

    @Override
    public String toString() {
        return "DeliverySummary{" +
                "quantity=" + quantity +
                ", total_shipping=" + total_shipping +
                ", delivery_type='" + delivery_type + '\'' +
                '}';
    }
}

