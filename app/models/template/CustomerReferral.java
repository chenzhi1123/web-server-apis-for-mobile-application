/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.template;

/**
 * Created by Zhi Chen  on 2016/10/25.
 */
public class CustomerReferral {
    public int oldUser_id;
    public String oldUser_firstName;
    public String oldUser_lastName;
    public String oldUser_email;
    public String newUser_firstName;
    public String newUser_lastName;
    public String newUser_email;

    public CustomerReferral(){
    }

    public CustomerReferral(int oldUser_id, String oldUser_firstName, String oldUser_lastName, String oldUser_email, String newUser_firstName, String newUser_lastName, String newUser_email) {
        this.oldUser_id = oldUser_id;
        this.oldUser_firstName = oldUser_firstName;
        this.oldUser_lastName = oldUser_lastName;
        this.oldUser_email = oldUser_email;
        this.newUser_firstName = newUser_firstName;
        this.newUser_lastName = newUser_lastName;
        this.newUser_email = newUser_email;
    }

    @Override
    public String toString() {
        return "CustomerReferral{" +
                "oldUser_id=" + oldUser_id +
                ", oldUser_firstName='" + oldUser_firstName + '\'' +
                ", oldUser_lastName='" + oldUser_lastName + '\'' +
                ", oldUser_email='" + oldUser_email + '\'' +
                ", newUser_firstName='" + newUser_firstName + '\'' +
                ", newUser_lastName='" + newUser_lastName + '\'' +
                ", newUser_email='" + newUser_email + '\'' +
                '}';
    }
}
