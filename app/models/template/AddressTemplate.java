/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.template;

/**
 * This is a class for encapsulate information related to address, and create an object within ReturnOrder class attributes
 * @see com.avaje.ebean.bean.EntityBean
 * @see ReturnOrder
 * */
public class AddressTemplate {

    public String alias="";
    public String company = "";
    public String firstname= "";
    public String lastname= "";
    public String address1="";
    public String address2 ="";
    public String city="";
    public String phone="" ;


    public AddressTemplate()
    {

    }

    public AddressTemplate(String alias, String company, String firstname, String lastname, String address1, String address2, String city, String phone) {
        this.alias = alias;
        this.company = company;
        this.firstname = firstname;
        this.lastname = lastname;
        this.address1 = address1;
        this.address2 = address2;
        this.city = city;
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "AddressTemplate{" +
                "alias='" + alias + '\'' +
                ", company='" + company + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", city='" + city + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
