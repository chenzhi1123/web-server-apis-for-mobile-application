/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.template;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by Zhi Chen  on 2016/10/25.
 */
public class TotalPrintOrder {   //// TODO: 2017/3/20
    public int id_order;
    public int id_customer;
    public int order_sequence;
    public String reference;
    public String payment;
    public String message;
    public java.math.BigDecimal total_discounts_tax_incl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_discounts_tax_excl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_paid_real = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_paid_tax_excl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_products = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_products_wt = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_shipping = BigDecimal.valueOf(0.000000);
    public Date date_add;
    public AddressTemplate delivery_address;
    public List<TotalPrintProduct> products;
    public String current_state;


    public  TotalPrintOrder(){}

    public TotalPrintOrder(int id_order, int id_customer,int order_sequence, String reference, String payment, String message, BigDecimal total_discounts_tax_incl, BigDecimal total_discounts_tax_excl, BigDecimal total_paid_real, BigDecimal total_paid_tax_excl, BigDecimal total_products, BigDecimal total_products_wt, BigDecimal total_shipping, Date date_add, AddressTemplate delivery_address, List<TotalPrintProduct> products, String current_state) {
        this.id_order = id_order;
        this.id_customer = id_customer;
        this.order_sequence = order_sequence;
        this.reference = reference;
        this.payment = payment;
        this.message = message;
        this.total_discounts_tax_incl = total_discounts_tax_incl;
        this.total_discounts_tax_excl = total_discounts_tax_excl;
        this.total_paid_real = total_paid_real;
        this.total_paid_tax_excl = total_paid_tax_excl;
        this.total_products = total_products;
        this.total_products_wt = total_products_wt;
        this.total_shipping = total_shipping;
        this.date_add = date_add;
        this.delivery_address = delivery_address;
        this.products = products;
        this.current_state = current_state;
    }

    @Override
    public String toString() {
        return "TotalPrintOrder{" +
                "id_order=" + id_order +
                ", id_customer=" + id_customer +
                ", order_sequence=" + order_sequence +
                ", reference='" + reference + '\'' +
                ", payment='" + payment + '\'' +
                ", message='" + message + '\'' +
                ", total_discounts_tax_incl=" + total_discounts_tax_incl +
                ", total_discounts_tax_excl=" + total_discounts_tax_excl +
                ", total_paid_real=" + total_paid_real +
                ", total_paid_tax_excl=" + total_paid_tax_excl +
                ", total_products=" + total_products +
                ", total_products_wt=" + total_products_wt +
                ", total_shipping=" + total_shipping +
                ", date_add=" + date_add +
                ", delivery_address=" + delivery_address +
                ", products=" + products +
                ", current_state='" + current_state + '\'' +
                '}';
    }
}
