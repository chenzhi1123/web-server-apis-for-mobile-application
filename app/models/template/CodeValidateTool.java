package models.template;

/**
 * Created by Zhi Chen  on 2017/6/29.
 */
public class CodeValidateTool {
    public String type = "";
    public int item_id;

    public CodeValidateTool(String type, int item_id) {
        this.type = type;
        this.item_id = item_id;
    }

    @Override
    public String toString() {
        return "CodeValidateTool{" +
                "type='" + type + '\'' +
                ", item_id=" + item_id +
                '}';
    }
}
