/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.template;

import java.io.Serializable;

/**
 * DriverOrders class is with regard to a joint table, which records order-driver relation
 * Index keys are id_driver, id_order.
 * */

public class RequestDriverOrders implements Serializable
{
    public int id_order;
    public String order_reference = "";
    public int order_number;
    public String payment_method = "";
    public String driver_name = "";


    public RequestDriverOrders()
    {

    }

    public RequestDriverOrders(int id_order, String order_reference, int order_number, String payment_method, String driver_name) {
        this.id_order = id_order;
        this.order_reference = order_reference;
        this.order_number = order_number;
        this.payment_method = payment_method;
        this.driver_name = driver_name;
    }

}
