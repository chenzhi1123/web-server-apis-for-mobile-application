/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.template;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by Zhi Chen  on 2016/10/25.
 */
public class ReturnOrder {
    public int id_order;
    public String reference;
    public AddressTemplate delivery_address;
    public String current_state;
    public String payment;
    public java.math.BigDecimal total_paid_real = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_shipping = BigDecimal.valueOf(0.000000);
    public Date date_add;
    public List<ReturnProduct> products;


    public  ReturnOrder(){}

    public ReturnOrder(int id_order, String reference, AddressTemplate delivery_address, String current_state, String payment, BigDecimal total_paid_real, BigDecimal total_shipping, Date date_add, List<ReturnProduct> products) {
        this.id_order = id_order;
        this.reference = reference;
        this.delivery_address = delivery_address;
        this.current_state = current_state;
        this.payment = payment;
        this.total_paid_real = total_paid_real;
        this.total_shipping = total_shipping;
        this.date_add = date_add;
        this.products = products;
    }

    @Override
    public String toString() {
        return "ReturnOrder{" +
                "id_order=" + id_order +
                ", reference='" + reference + '\'' +
                ", delivery_address='" + delivery_address + '\'' +
                ", current_state='" + current_state + '\'' +
                ", payment='" + payment + '\'' +
                ", total_paid_real=" + total_paid_real +
                ", total_shipping=" + total_shipping +
                ", date_add=" + date_add +
                ", products=" + products +
                '}';
    }
}
