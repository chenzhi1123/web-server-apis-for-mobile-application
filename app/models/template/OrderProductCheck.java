/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.template;

import java.util.ArrayList;

/**
 * Created by Zhi Chen on 2017/11/20.
 */
public class OrderProductCheck {
    public int id_customer;
    public String device_type;
    public String device_token;
    public ArrayList<OrderProduct> products;

    public OrderProductCheck() {
    }

    public OrderProductCheck(int id_customer, String device_type, String device_token, ArrayList<OrderProduct> products) {
        this.id_customer = id_customer;
        this.device_type = device_type;
        this.device_token = device_token;
        this.products = products;
    }

    @Override
    public String toString() {
        return "OrderProductCheck{" +
                "id_customer=" + id_customer +
                ", device_type='" + device_type + '\'' +
                ", device_token='" + device_token + '\'' +
                ", products=" + products +
                '}';
    }
}
