/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.template;

import models.sqlContainer.NewCodeValidation;

import java.util.ArrayList;

/**
 * 把要修改的订单和订单里自带的打折信息合并在一个类里
 */
public class CombineOrderVoucher {
    public ArrayList<AmendTotalOrder> amendTotalOrder = new ArrayList<>();
    public ArrayList<NewCodeValidation> newCodeValidation = new ArrayList<>();

    public CombineOrderVoucher(ArrayList<AmendTotalOrder> amendTotalOrder, ArrayList<NewCodeValidation> newCodeValidation) {
        this.amendTotalOrder = amendTotalOrder;
        this.newCodeValidation = newCodeValidation;
    }

    public CombineOrderVoucher(){

    }

    @Override
    public String toString() {
        return "CombineOrderVoucher{" +
                "amendTotalOrder=" + amendTotalOrder +
                ", newCodeValidation=" + newCodeValidation +
                '}';
    }
}
