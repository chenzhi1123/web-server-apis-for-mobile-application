/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

@SuppressWarnings("serial")
@Entity
@Table(name = "ps_access_token")
public class AccessToken extends Model
{
    @Id
    @GeneratedValue
    public int id_access_token;
    public String access_token;


    public AccessToken()
    {

    }

    public AccessToken(String access_token) {
        this.access_token = access_token;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof AccessToken)
        {
            final AccessToken other = (AccessToken) obj;
            return Objects.equal(id_access_token, other.id_access_token)
                    && Objects.equal(access_token, other.access_token);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "AccessToken{" +
                "id_access_token=" + id_access_token +
                ", access_token='" + access_token + '\'' +
                '}';
    }

    public static String findById(int id_access_token)
    {
        return find.where().eq("id_access_token", id_access_token).findUnique().access_token;
    }

    public static List<AccessToken> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (AccessToken orderState : AccessToken.findAll())
        {
            orderState.delete();
        }
    }

    public static Finder<String, AccessToken> find = new Finder<String, AccessToken>(String.class, AccessToken.class);

}
