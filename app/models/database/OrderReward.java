/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;

//Driver class has the corresponding table in the database named "ps_order_reward"
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_order_reward")
public class OrderReward extends Model
{
    @Id
    public int id_order;
    @Id
    public int id_cart_rule;
    public String voucher_type;
    public Date date_add = new Date();


    public OrderReward()
    {

    }

    public OrderReward(int id_order, int id_cart_rule, String voucher_type, Date date_add) {
        this.id_order = id_order;
        this.id_cart_rule = id_cart_rule;
        this.voucher_type = voucher_type;
        this.date_add = date_add;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof OrderReward)
        {
            final OrderReward other = (OrderReward) obj;
            return Objects.equal(id_order, other.id_order)
                    &&Objects.equal(id_cart_rule, other.id_cart_rule)
                    &&Objects.equal(voucher_type, other.voucher_type)
                    &&Objects.equal(date_add, other.date_add);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "OrderReward{" +
                "id_order=" + id_order +
                ", id_cart_rule=" + id_cart_rule +
                ", voucher_type='" + voucher_type + '\'' +
                ", date_add=" + date_add +
                '}';
    }

    public static OrderReward findByName(String name)
    {
        return OrderReward.find.where().eq("name", name).findUnique();
    }

    public static OrderReward findById(int id_order)
    {
        return find.where().eq("id_order", id_order).findUnique();
    }


    /**
     * Check drivers list according to work state.
     * In the ps_drivers table, if_work column indicates working status.
     * @param if_work
     * */
    public  static List<OrderReward> findByState(int if_work) {
        return find.where().eq("if_work", if_work).orderBy("id_driver").findList();
    }

    public  static List<OrderReward> findByCartRule(int id_cart_rule) {
        return find.select("id_group").where().eq("id_cart_rule", id_cart_rule).findList();
    }

    public static List<OrderReward> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (OrderReward driver : OrderReward.findAll())
        {
            driver.delete();
        }
    }

    public static Finder<String, OrderReward> find = new Finder<String, OrderReward>(String.class, OrderReward.class);
}
