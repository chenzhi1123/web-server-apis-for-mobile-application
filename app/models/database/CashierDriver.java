/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;
/**
 * CashierDriver Class matches the table in the database, attributes names match corresponding column names
 * This class relates to the actions between Cashier and drivers
 *
 * Cashier pay back the delivery fee to drivers
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_cashier_driver_daily")
public class CashierDriver extends Model
{
    @Id
    @GeneratedValue
    public int id_cashier_driver;
    public int id_cashier;
    public int id_driver;
    public String name;
    public double delivery_fee_paid;
    public int number_of_onlinepayment;
    public Date date_add;


    public CashierDriver()
    {

    }

    public CashierDriver(int id_cashier, int id_driver, String name, double delivery_fee_paid, int number_of_onlinepayment, Date date_add) {
        this.id_cashier = id_cashier;
        this.id_driver = id_driver;
        this.name = name;
        this.delivery_fee_paid = delivery_fee_paid;
        this.number_of_onlinepayment = number_of_onlinepayment;
        this.date_add = date_add;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof CashierDriver)
        {
            final CashierDriver other = (CashierDriver) obj;
            return Objects.equal(id_driver, other.id_driver)
                    && Objects.equal(id_cashier_driver, other.id_cashier_driver)
                    && Objects.equal(id_cashier, other.id_cashier)
                    && Objects.equal(name, other.name)
                    && Objects.equal(delivery_fee_paid, other.delivery_fee_paid)
                    && Objects.equal(number_of_onlinepayment, other.number_of_onlinepayment)
                    && Objects.equal(date_add, other.date_add);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "CashierDriver{" +
                "id_cashier_driver=" + id_cashier_driver +
                ", id_cashier=" + id_cashier +
                ", id_driver=" + id_driver +
                ", name='" + name + '\'' +
                ", delivery_fee_paid=" + delivery_fee_paid +
                ", number_of_onlinepayment=" + number_of_onlinepayment +
                ", date_add=" + date_add +
                '}';
    }

    public static CashierDriver findByID(int id)
    {
        return find.where().eq("id_cashier_driver", id).findUnique();
    }

    //calculate all the compensations of a driver, in the last 14 hours there should be only one record
    public  static CashierDriver findByDriverID(int id_driver) {
        long nowMinus1Day = System.currentTimeMillis() - 1 * 14 * (1000 * 60 * 60);
        Date lastOneDay = new Date(nowMinus1Day);
        return find.where().eq("id_driver", id_driver).gt("date_add",lastOneDay).findUnique();
    }

    //find all the delivery fee paid to the drives from a cashier
    public  static List<CashierDriver> findByCashier(int id_cashier) {
        long nowMinus1Day = System.currentTimeMillis() - 1 * 14 * (1000 * 60 * 60);
        Date lastOneDay = new Date(nowMinus1Day);
        return find.where().eq("id_cashier", id_cashier).gt("date_add",lastOneDay).orderBy("date_add desc").findList();
    }

    public static List<CashierDriver> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (CashierDriver order : CashierDriver.findAll())
        {
            order.delete();
        }
    }

    public static Model.Finder<String, CashierDriver> find = new Model.Finder<String, CashierDriver>(String.class, CashierDriver.class);
}
