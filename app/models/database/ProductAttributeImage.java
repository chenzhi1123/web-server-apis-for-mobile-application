/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_product_attribute_image")
public class ProductAttributeImage extends Model
{
    @Id
    @GeneratedValue
    public int id_product_attribute;
    public int id_image;


    public ProductAttributeImage()
    {

    }

    public ProductAttributeImage(int id_image) {
        this.id_image = id_image;
    }
    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof ProductAttributeImage)
        {
            final ProductAttributeImage other = (ProductAttributeImage) obj;
            return Objects.equal(id_image, other.id_image);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "ProductAttributeImage{" +
                "id_product_attribute=" + id_product_attribute +
                ", id_image=" + id_image +
                '}';
    }

    public static ProductAttributeImage findByEmail(String email)

    {
        return ProductAttributeImage.find.where().eq("email", email).findUnique();
    }

    public static ArrayList<Integer> findById(int id)
    {
     List<ProductAttributeImage> imageList= find.where().eq("id_product_attribute", id).findList();
        int size = imageList.size();
        ArrayList<Integer> image_id = new ArrayList<>();
        for(int i = 0; i<size; i++){
         int this_imageID = imageList.get(i).id_image;
            image_id.add(this_imageID);
        }
     return image_id;
    }

    public static List<ProductAttributeImage> findAll()
    {
        return find.all();
    }



    public static Model.Finder<String, ProductAttributeImage> find = new Model.Finder<String, ProductAttributeImage>(String.class, ProductAttributeImage.class);

}
