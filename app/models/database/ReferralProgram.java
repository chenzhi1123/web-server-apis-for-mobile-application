/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;

//Driver class has the corresponding table in the database named "ps_drivers"
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_referralprogram")
public class ReferralProgram extends Model
{
    @Id
    @GeneratedValue
    public int id_referralprogram;
    public int id_sponsor;
    public String email;
    public String lastname;
    public String firstname;
    public int id_customer;
    public int id_cart_rule;
    public int id_cart_rule_sponsor;
    public Date date_add;
    public Date date_upd;




    public ReferralProgram()
    {

    }

    public ReferralProgram(int id_sponsor, String email, String lastname, String firstname, int id_customer, int id_cart_rule, int id_cart_rule_sponsor, Date date_add, Date date_upd) {
        this.id_sponsor = id_sponsor;
        this.email = email;
        this.lastname = lastname;
        this.firstname = firstname;
        this.id_customer = id_customer;
        this.id_cart_rule = id_cart_rule;
        this.id_cart_rule_sponsor = id_cart_rule_sponsor;
        this.date_add = date_add;
        this.date_upd = date_upd;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof ReferralProgram)
        {
            final ReferralProgram other = (ReferralProgram) obj;
            return Objects.equal(id_cart_rule, other.id_cart_rule)
                    && Objects.equal(id_customer, other.id_customer)
                    && Objects.equal(id_cart_rule_sponsor, other.id_cart_rule_sponsor)
                    && Objects.equal(id_referralprogram, other.id_referralprogram)
                    && Objects.equal(date_add, other.date_add)
                    && Objects.equal(date_upd, other.date_upd)
                    && Objects.equal(email, other.email)
                    && Objects.equal(lastname, other.lastname)
                    && Objects.equal(firstname, other.firstname);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "ReferralProgram{" +
                "id_referralprogram=" + id_referralprogram +
                ", id_sponsor=" + id_sponsor +
                ", email='" + email + '\'' +
                ", lastname='" + lastname + '\'' +
                ", firstname='" + firstname + '\'' +
                ", id_customer=" + id_customer +
                ", id_cart_rule=" + id_cart_rule +
                ", id_cart_rule_sponsor=" + id_cart_rule_sponsor +
                ", date_add=" + date_add +
                ", date_upd=" + date_upd +
                '}';
    }

    public static ReferralProgram findByEmail(String email)
    {
        return ReferralProgram.find.where().eq("email", email).findUnique();
    }

    public static ReferralProgram findBySponsor(int id_sponsor,String email)
    {
        return ReferralProgram.find.where().eq("id_sponsor", id_sponsor).eq("email", email).findUnique();
    }

    public static ReferralProgram findBySponsoredUser(int id_customer)
    {
        return ReferralProgram.find.where().eq("id_customer", id_customer).eq("id_cart_rule_sponsor", 0).findUnique();
    }

    public static List<ReferralProgram> getSponsoredUsers(int id_old_user)
    {
        return find.where().eq("id_sponsor", id_old_user).gt("id_customer",0).findList();
    }

    public static List<ReferralProgram> getPendingUsers(int id_old_user)
    {
        return find.where().eq("id_sponsor", id_old_user).eq("id_customer",0).findList();
    }

    public static int findByCode(String str){
        return find.where().eq("code",str).findUnique().id_cart_rule;
    }

    public static int getConstraintUser(String str){
        return find.where().eq("code",str).findUnique().id_customer;
    }


    /**
     * Check drivers list according to work state.
     * In the ps_drivers table, if_work column indicates working status.
     * @param if_work
     * */
    public  static List<ReferralProgram> findByState(int if_work) {
        return find.where().eq("if_work", if_work).orderBy("id_driver").findList();
    }

    public static List<ReferralProgram> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (ReferralProgram driver : ReferralProgram.findAll())
        {
            driver.delete();
        }
    }

    public static Finder<String, ReferralProgram> find = new Finder<String, ReferralProgram>(String.class, ReferralProgram.class);
}
