/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_order_payment")
public class OrderPayment extends Model
{
    @Id
    @GeneratedValue
    public int id_order_payment;
    public String order_reference = null;
    public int id_currency;
    public java.math.BigDecimal amount;
    public String payment_method;
    public java.math.BigDecimal conversion_rate = BigDecimal.valueOf(1.000000);
    public String transaction_id = null;
    public String card_number = null;
    public String card_brand = null;
    public String card_expiration = null;
    public String card_holder = null;
    public Date date_add;


    public OrderPayment()
    {

    }

    public OrderPayment(String order_reference, int id_currency, BigDecimal amount, String payment_method, BigDecimal conversion_rate, String transaction_id, String card_number, String card_brand, String card_expiration, String card_holder, Date date_add) {
        this.order_reference = order_reference;
        this.id_currency = id_currency;
        this.amount = amount;
        this.payment_method = payment_method;
        this.conversion_rate = conversion_rate;
        this.transaction_id = transaction_id;
        this.card_number = card_number;
        this.card_brand = card_brand;
        this.card_expiration = card_expiration;
        this.card_holder = card_holder;
        this.date_add = date_add;
    }

    public void update(BigDecimal amount){
        this.amount = amount;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof OrderPayment)
        {
            final OrderPayment other = (OrderPayment) obj;
            return Objects.equal(order_reference, other.order_reference)
                    && Objects.equal(id_currency, other.id_currency)
                    && Objects.equal(amount, other.amount)
                    && Objects.equal(payment_method, other.payment_method)
                    && Objects.equal(conversion_rate, other.conversion_rate)
                    && Objects.equal(transaction_id, other.transaction_id)
                    && Objects.equal(card_number, other.card_number)
                    && Objects.equal(card_brand, other.card_brand)
                    && Objects.equal(card_expiration, other.card_expiration)
                    && Objects.equal(card_holder, other.card_holder)
                    && Objects.equal(date_add, other.date_add);

        }
        else
        {
            return false;
        }
    }


    @Override
    public String toString() {
        return "OrderPayment{" +
                "id_order_payment=" + id_order_payment +
                ", order_reference='" + order_reference + '\'' +
                ", id_currency=" + id_currency +
                ", amount=" + amount +
                ", payment_method='" + payment_method + '\'' +
                ", conversion_rate=" + conversion_rate +
                ", transaction_id='" + transaction_id + '\'' +
                ", card_number='" + card_number + '\'' +
                ", card_brand='" + card_brand + '\'' +
                ", card_expiration='" + card_expiration + '\'' +
                ", card_holder='" + card_holder + '\'' +
                ", date_add=" + date_add +
                '}';
    }

    public static OrderPayment findByEmail(String email)

    {
        return OrderPayment.find.where().eq("email", email).findUnique();
    }

    public static OrderPayment findById(Long id)
    {
        return find.where().eq("id_order_payment", id).findUnique();
    }

    public static OrderPayment findByReference(String order_reference)
    {
        return find.where().eq("order_reference", order_reference).findUnique();
    }

    public static List<OrderPayment> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (OrderPayment orderPayment : OrderPayment.findAll())
        {
            orderPayment.delete();
        }
    }

    public static Model.Finder<String, OrderPayment> find = new Model.Finder<String, OrderPayment>(String.class, OrderPayment.class);

}
