/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;
//Address Class matches the table in the database, attributes names match corresponding column names
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_address")
public class Address extends Model
{
    @Id
    @GeneratedValue
    public int id_address;
    public int id_country;
    public Integer id_state;
    public int id_customer;
    public int id_manufacturer;
    public int id_supplier;
    public int id_warehouse;
    public String alias;
    public String company;
    public String firstname;
    public String lastname;
    public String address1;
    public String address2;
    public String postcode;
    public String city;
    public java.lang.String other;
    public String phone;
    public String phone_mobile;
    public String vat_number ;
    public String dni;
    public Date date_add;
    public Date date_upd;
    public int active = 1;
    public int deleted = 0;

    public Address()
    {

    }


    public Address(int id_country, Integer id_state, int id_customer, int id_manufacturer, int id_supplier, int id_warehouse, String alias, String company, String firstname, String lastname, String address1, String address2, String postcode, String city, String other, String phone, String phone_mobile, String vat_number, String dni, Date date_add, Date date_upd, int active, int deleted) {
        this.id_country = id_country;
        this.id_state = id_state;
        this.id_customer = id_customer;
        this.id_manufacturer = id_manufacturer;
        this.id_supplier = id_supplier;
        this.id_warehouse = id_warehouse;
        this.alias = alias;
        this.company = company;
        this.firstname = firstname;
        this.lastname = lastname;
        this.address1 = address1;
        this.address2 = address2;
        this.postcode = postcode;
        this.city = city;
        this.other = other;
        this.phone = phone;
        this.phone_mobile = phone_mobile;
        this.vat_number = vat_number;
        this.dni = dni;
        this.date_add = date_add;
        this.date_upd = date_upd;
        this.active = active;
        this.deleted = deleted;
    }

    public void update(Address address)
    {
        this.id_country = address.id_country;
        this.id_state = address.id_state;
        this.id_customer = address.id_customer;
        this.id_manufacturer = address.id_manufacturer;
        this.id_supplier = address.id_supplier;
        this.id_warehouse = address.id_warehouse;
        this.alias = address.alias;
        this.company = address.company;
        this.firstname = address.firstname;
        this.lastname = address.lastname;
        this.address1 = address.address1;
        this.address2 = address.address2;
        this.postcode = address.postcode;
        this.city = address.city;
        this.other = address.other;
        this.phone = address.phone;
        this.phone_mobile = address.phone_mobile;
        this.vat_number = address.vat_number;
        this.dni = address.dni;
        this.date_add = address.date_add;
        this.date_upd = address.date_upd;
        this.active = address.active;
        this.deleted = address.deleted;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof Address)
        {   final Address other = (Address) obj;
            return Objects.equal(id_country, other.id_country)
                    && Objects.equal(id_state, other.id_state)
                    && Objects.equal(id_customer, other.id_customer)
                    && Objects.equal(id_manufacturer, other.id_manufacturer)
                    && Objects.equal(id_supplier, other.id_supplier)
                    && Objects.equal(id_warehouse, other.id_warehouse)
                    && Objects.equal(alias, other.alias)
                    && Objects.equal(lastname, other.lastname)
                    && Objects.equal(firstname, other.firstname)
                    && Objects.equal(company, other.company)
                    && Objects.equal(address1, other.address1)
                    && Objects.equal(address2, other.address2)
                    && Objects.equal(postcode, other.postcode)
                    && Objects.equal(city, other.city)
                    && Objects.equal(other, other.other)
                    && Objects.equal(phone, other.phone)
                    && Objects.equal(phone_mobile, other.phone_mobile)
                    && Objects.equal(vat_number, other.vat_number)
                    && Objects.equal(dni, other.dni)
                    && Objects.equal(date_add, other.date_add)
                    && Objects.equal(date_upd, other.date_upd)
                    && Objects.equal(active, other.active)
                    && Objects.equal(deleted, other.deleted)
                    && Objects.equal(date_add, other.date_add)&& Objects.equal(date_upd, other.date_upd);
        }
        else
        {
            return false;
        }
    }


    @Override
    public String toString() {
        return "Address{" +
                "id_address=" + id_address +
                ", id_country=" + id_country +
                ", id_state=" + id_state +
                ", id_customer=" + id_customer +
                ", id_manufacturer=" + id_manufacturer +
                ", id_supplier=" + id_supplier +
                ", id_warehouse=" + id_warehouse +
                ", alias='" + alias + '\'' +
                ", company='" + company + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", postcode='" + postcode + '\'' +
                ", city='" + city + '\'' +
                ", other='" + other + '\'' +
                ", phone='" + phone + '\'' +
                ", phone_mobile='" + phone_mobile + '\'' +
                ", vat_number='" + vat_number + '\'' +
                ", dni='" + dni + '\'' +
                ", date_add=" + date_add +
                ", date_upd=" + date_upd +
                ", active=" + active +
                ", deleted=" + deleted +
                '}';
    }


    //Get all the addresses of a specific customer, also search for its alias name
    public static Address findByAlias(int customer_id, String alias)

    {
        return Address.find.where().eq("id_customer", customer_id).eq("alias", alias).eq("deleted", 0).findUnique();
    }

    public static Address findByAliasPhone(int id_customer,String phoneNumber, String alias)

    {
        return Address.find.where().eq("id_customer", id_customer).eq("phone", phoneNumber).eq("alias", alias).eq("deleted", 0).findUnique();
    }


    public static Address findById(int id)
    {
        return find.where().eq("id_address", id).findUnique();
    }

    public static List<Address> findAll()
    {
        return find.all();
    }

    public static List<Address> findCustomerAddress(long id)
    {
        return find.where().eq("id_customer", id).eq("active", 1).eq("deleted", 0).findList();
    }

    public static List<Address> findCustomerAddressByNumber(int phone)
    {
        int length = String.valueOf(phone).length();
       String number = "%" + phone + "%";
       if (length == 5 || length == 6){
           return find.where().like("phone", number).eq("active", 1).eq("deleted", 0).setMaxRows(10).findList();
       } else if (length > 6 && length < 13) {
        return find.where().like("phone", number).eq("active", 1).eq("deleted", 0).findList();
       } else {
           return null;
       }

    }

    public static void deleteAll()
    {
        for (Address address : Address.findAll())
        {
            address.delete();
        }
    }

    public static Model.Finder<String, Address> find = new Model.Finder<String, Address>(String.class, Address.class);
}
