/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
/**
 * CashierDriver Class matches the table in the database, attributes names match corresponding column names
 * This class relates to the actions between Cashier and drivers
 *
 * Cashier pay back the delivery fee to drivers
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_order_print_history")
public class OrderPrintHistory extends Model
{
    @Id
    @GeneratedValue
    public int id_order_print;

    @Column(name = "id_order")
    public int id_order;

    @Column(name = "order_sequence")
    public int order_sequence;

    @Column(name = "date_add")
    public Date date_add;


    public OrderPrintHistory()
    {

    }

    public OrderPrintHistory(int id_order, int order_sequence, Date date_add) {
        this.id_order = id_order;
        this.order_sequence = order_sequence;
        this.date_add = date_add;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof OrderPrintHistory)
        {
            final OrderPrintHistory other = (OrderPrintHistory) obj;
            return Objects.equal(id_order_print, other.id_order_print)
                    && Objects.equal(id_order, other.id_order)
                    && Objects.equal(order_sequence, other.order_sequence)
                    && Objects.equal(date_add, other.date_add);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "OrderPrintHistory{" +
                "id_order_print=" + id_order_print +
                ", id_order=" + id_order +
                ", order_sequence=" + order_sequence +
                ", date_add=" + date_add +
                '}';
    }

    public static List<OrderPrintHistory> getLastHour(){
        long lastOneHour = System.currentTimeMillis() - 1000 * 60 * 60;
        Date one_hour = new Date(lastOneHour);
        return find.where().gt("date_add",one_hour).findList();
    }

    public static OrderPrintHistory findById(int id)
    {
        return find.where().eq("id_order", id).findUnique();
    }

    public static List<OrderPrintHistory> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (OrderPrintHistory order : OrderPrintHistory.findAll())
        {
            order.delete();
        }
    }

    public static Model.Finder<String, OrderPrintHistory> find = new Model.Finder<String, OrderPrintHistory>(String.class, OrderPrintHistory.class);
}
