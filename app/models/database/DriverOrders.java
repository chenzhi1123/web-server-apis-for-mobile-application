/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
/**
 * DriverOrders class is with regard to a joint table, which records order-driver relation
 * Index keys are id_driver, id_order.
 * */
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_drivers_orders")
public class DriverOrders extends Model
{
    @Id
    @GeneratedValue
    @Version
    @Column(name = "id_driver_order")
    public int id_driver_order;

    @Column(name = "id_driver")
    public int id_driver;        // driver who deliveries this order

    @Column(name = "driver_name")
    public String driver_name;

    @Column(name = "id_order")
    public int id_order;

    @Column(name = "order_reference")
    public String order_reference;

    @Column(name = "is_active")
    public int is_active = 0;

    @Column(name = "date_add")
    public Date date_add;

    @Column(name = "order_number")
    public int order_number;    // sequential number of the order in one day

    @Column(name = "id_cashier")
    public int id_cashier = 0;  // cashier who allocates this order

    @Column(name = "if_paid")
    public int if_paid = 0;      // if this order is paid by the driver returning

    @Column(name = "paid_cashier")
    public int paid_cashier = 0; // cashier who is paid by the driver

    @Column(name = "if_delivered")
    public int if_delivered = 0; // cashier who is paid by the driver NEW


    @Column(name = "start_time")
    public Date start_time = new Date(0);

    @Column(name = "delivered_time")
    public Date delivered_time = new Date(0);

    //new   if_paid,  cashier_id


    public DriverOrders()
    {

    }

    public DriverOrders(int id_driver, String driver_name, int id_order, String order_reference, int is_active, Date date_add, int order_number, int id_cashier, int if_paid, int paid_cashier, int if_delivered, Date start_time, Date delivered_time) {
        this.id_driver = id_driver;
        this.driver_name = driver_name;
        this.id_order = id_order;
        this.order_reference = order_reference;
        this.is_active = is_active;
        this.date_add = date_add;
        this.order_number = order_number;
        this.id_cashier = id_cashier;
        this.if_paid = if_paid;
        this.paid_cashier = paid_cashier;
        this.if_delivered = if_delivered;
        this.start_time = start_time;
        this.delivered_time = delivered_time;
    }

    public void update(int if_delivered, Date delivered_time){
        this.if_delivered = if_delivered;
        this.delivered_time = delivered_time;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof DriverOrders)
        {
            final DriverOrders other = (DriverOrders) obj;
            return Objects.equal(id_driver, other.id_driver)
                    && Objects.equal(id_driver_order, other.id_driver_order)
                    && Objects.equal(id_order, other.id_order)
                    && Objects.equal(order_reference, other.order_reference)
                    && Objects.equal(driver_name, other.driver_name)
                    && Objects.equal(is_active, other.is_active)
                    && Objects.equal(date_add, other.date_add)
                    && Objects.equal(order_number, other.order_number)
                    && Objects.equal(id_cashier, other.id_cashier)
                    && Objects.equal(if_paid, other.if_paid)
                    && Objects.equal(if_delivered, other.if_delivered)
                    && Objects.equal(start_time, other.start_time)
                    && Objects.equal(delivered_time, other.delivered_time)
                    && Objects.equal(paid_cashier, other.paid_cashier);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "DriverOrders{" +
                "id_driver_order=" + id_driver_order +
                ", id_driver=" + id_driver +
                ", driver_name='" + driver_name + '\'' +
                ", id_order=" + id_order +
                ", order_reference='" + order_reference + '\'' +
                ", is_active=" + is_active +
                ", date_add=" + date_add +
                ", order_number=" + order_number +
                ", id_cashier=" + id_cashier +
                ", if_paid=" + if_paid +
                ", paid_cashier=" + paid_cashier +
                ", if_delivered=" + if_delivered +
                ", start_time=" + start_time +
                ", delivered_time=" + delivered_time +
                '}';
    }

    public static DriverOrders findByOrderId(int id)
    {
        return find.where().eq("id_order", id).findUnique();
    }

    public static DriverOrders findByBoth(int id_driver, int id_order)
    {
        return find.where().eq("id_driver", id_driver).eq("id_order", id_order).findUnique();
    }

    public static DriverOrders findByReference(String reference) {return find.where().eq("order_reference", reference).findUnique();}

    /** Find all the orders of one deliver
     *@param id_driver
     * */
    public  static List<DriverOrders> findByDriver(int id_driver) {
        return find.where().eq("id_driver", id_driver).orderBy("date_add desc").findList();
    }


    /**
     * Find all orders,in charge of one certain cashier, along with driver info.
     * @param id_cashier
     * @param id_driver
     * @param if_paid
     * */
    public  static List<DriverOrders> workOfDriver(int id_driver, int id_cashier, int if_paid) {
        long nowMinus1Day = System.currentTimeMillis() - 1 * 12 * (1000 * 60 * 60);//12小时以内
        Date lastOneDay = new Date(nowMinus1Day);
        return find.where().eq("id_driver", id_driver).eq("id_cashier", id_cashier).eq("if_paid", if_paid).gt("date_add",lastOneDay).orderBy("date_add desc").findList();
    }

    public  static List<DriverOrders> unpaidByDriver(int id_driver) {
        long nowMinus1Day = System.currentTimeMillis() - 1 * 12 * (1000 * 60 * 60);
        Date lastOneDay = new Date(nowMinus1Day);
        return find.where().eq("id_driver", id_driver).eq("if_paid", 0).gt("date_add",lastOneDay).orderBy("date_add desc").findList();
    }

    public  static List<DriverOrders> driverUnpaidOrders() {  //给manager 使用
        long nowMinus1Day = System.currentTimeMillis() - 1 * 12 * (1000 * 60 * 60);
        Date lastOneDay = new Date(nowMinus1Day);
        return find.where().eq("if_paid", 0).gt("date_add",lastOneDay).orderBy("id_driver").findList();
    }

    public static List<DriverOrders> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (DriverOrders order : DriverOrders.findAll())
        {
            order.delete();
        }
    }

    public static void delete(int id_order)
    {
        find.where().eq("id_order", id_order).findUnique().delete();
    }

    public static Model.Finder<String, DriverOrders> find = new Model.Finder<String, DriverOrders>(String.class, DriverOrders.class);

}
