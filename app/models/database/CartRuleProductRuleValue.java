/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

//Driver class has the corresponding table in the database named "ps_drivers"
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_cart_rule_product_rule_value")
public class CartRuleProductRuleValue extends Model
{
    @Id
    public int id_product_rule;
    @Id
    public int id_item;


    public CartRuleProductRuleValue()
    {

    }

    public CartRuleProductRuleValue(int id_product_rule, int id_item) {
        this.id_product_rule = id_product_rule;
        this.id_item = id_item;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof CartRuleProductRuleValue)
        {
            final CartRuleProductRuleValue other = (CartRuleProductRuleValue) obj;
            return Objects.equal(id_product_rule, other.id_product_rule)
                    &&Objects.equal(id_item, other.id_item);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "CartRuleProductRuleValue{" +
                "id_product_rule=" + id_product_rule +
                ", id_item=" + id_item +
                '}';
    }

    public static CartRuleProductRuleValue findByName(String name)
    {
        return CartRuleProductRuleValue.find.where().eq("name", name).findUnique();
    }

    public static CartRuleProductRuleValue findById(int id)
    {
        return find.where().eq("id_driver", id).findUnique();
    }


    /**
     * Check drivers list according to work state.
     * In the ps_drivers table, if_work column indicates working status.
     * @param if_work
     * */
    public  static List<CartRuleProductRuleValue> findByState(int if_work) {
        return find.where().eq("if_work", if_work).orderBy("id_driver").findList();
    }

    public  static List<CartRuleProductRuleValue> findByCartRule(int id_cart_rule) {
        return find.select("id_group").where().eq("id_cart_rule", id_cart_rule).findList();
    }

    public static List<CartRuleProductRuleValue> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (CartRuleProductRuleValue driver : CartRuleProductRuleValue.findAll())
        {
            driver.delete();
        }
    }

    public static Finder<String, CartRuleProductRuleValue> find = new Finder<String, CartRuleProductRuleValue>(String.class, CartRuleProductRuleValue.class);
}
