/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_message")
public class OrderMessage extends Model
{
    @Id
    @GeneratedValue
    @Column(name = "id_message")
    public int id_message;

    @Column(name = "id_cart")
    public int id_cart;

    @Column(name = "id_customer")
    public int id_customer;

    @Column(name = "id_employee")
    public int id_employee;

    @Column(name = "id_order")
    public int id_order;

    @Column(name = "message")
    public String message;

    @Column(name = "private")
    public int if_private = 0;

    @Column(name = "date_add")
    public Date date_add;




    public OrderMessage()
    {

    }

    public OrderMessage(int id_cart, int id_customer, int id_employee, int id_order, String message, int if_private, Date date_add) {
        this.id_cart = id_cart;
        this.id_customer = id_customer;
        this.id_employee = id_employee;
        this.id_order = id_order;
        this.message = message;
        this.if_private = if_private;
        this.date_add = date_add;
    }

    public void update(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof OrderMessage)
        {
            final OrderMessage other = (OrderMessage) obj;
            return Objects.equal(id_cart, other.id_cart)
                    && Objects.equal(id_customer, other.id_customer)
                    && Objects.equal(id_employee, other.id_employee)
                    && Objects.equal(id_order, other.id_order)
                    && Objects.equal(message, other.message)
                    && Objects.equal(if_private, other.if_private)
                    && Objects.equal(date_add, other.date_add);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "OrderMessage{" +
                "id_message=" + id_message +
                ", id_cart=" + id_cart +
                ", id_customer=" + id_customer +
                ", id_employee=" + id_employee +
                ", id_order=" + id_order +
                ", message='" + message + '\'' +
                ", if_private=" + if_private +
                ", date_add=" + date_add +
                '}';
    }

    public static OrderMessage findByEmail(String email)

    {
        return OrderMessage.find.where().eq("email", email).findUnique();
    }

    public static OrderMessage findById(int id)
    {
        return find.where().eq("id_message", id).findUnique();
    }

    public static OrderMessage findByOrder(int order_id, int customer_id)
    {
        return find.where().eq("id_order", order_id).eq("id_customer", customer_id).eq("if_private",0).findUnique();
    }

    public static OrderMessage findByID(int id_order){
        return find.where().eq("id_order", id_order).eq("if_private",0).findUnique();  //if_private = 0 是客户自己的留言
    }

    public static List<OrderMessage> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (OrderMessage orderMessage : OrderMessage.findAll())
        {
            orderMessage.delete();
        }
    }

    public static Model.Finder<String, OrderMessage> find = new Model.Finder<String, OrderMessage>(String.class, OrderMessage.class);

}
