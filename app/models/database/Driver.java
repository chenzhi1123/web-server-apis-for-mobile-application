/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
//Driver class has the corresponding table in the database named "ps_drivers"
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_drivers")
public class Driver extends Model
{
    @Id
    @GeneratedValue
    @Version
    public int id_driver;
    public String name;
    public int if_work;
    public Date date_add;
    public Date last_login;
    public Date last_logout;
    public String contact_number;
    public String password;
    public Date app_last_login;
    public Date app_last_logout;
    public int if_company_car;

    public Driver(){

    }

    public Driver(String name, int if_work, Date date_add, Date last_login, Date last_logout, String contact_number, String password, Date app_last_login, Date app_last_logout, int if_company_car) {
        this.name = name;
        this.if_work = if_work;
        this.date_add = date_add;
        this.last_login = last_login;
        this.last_logout = last_logout;
        this.contact_number = contact_number;
        this.password = password;
        this.app_last_login = app_last_login;
        this.app_last_logout = app_last_logout;
        this.if_company_car = if_company_car;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof Driver)
        {
            final Driver other = (Driver) obj;
            return Objects.equal(id_driver, other.id_driver)
                    && Objects.equal(name, other.name)
                    && Objects.equal(if_work, other.if_work)
                    && Objects.equal(date_add, other.date_add)
                    && Objects.equal(last_login, other.last_login)
                    && Objects.equal(last_logout, other.last_logout)
                    && Objects.equal(password, other.password)
                    && Objects.equal(app_last_login, other.app_last_login)
                    && Objects.equal(app_last_logout, other.app_last_logout)
                    && Objects.equal(if_company_car, other.if_company_car)
                    && Objects.equal(contact_number, other.contact_number);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "Driver{" +
                "id_driver=" + id_driver +
                ", name='" + name + '\'' +
                ", if_work=" + if_work +
                ", date_add=" + date_add +
                ", last_login=" + last_login +
                ", last_logout=" + last_logout +
                ", contact_number='" + contact_number + '\'' +
                ", password='" + password + '\'' +
                ", app_last_login=" + app_last_login +
                ", app_last_logout=" + app_last_logout +
                ", if_company_car=" + if_company_car +
                '}';
    }

    public void update(int if_work, Date last_logout) {
        this.if_work = if_work;
        this.last_logout = last_logout;
    }

    public static Driver findByName(String name)
    {
        return Driver.find.where().eq("name", name).findUnique();
    }

    public static Driver findByPhone(String contact_number)
    {
        return Driver.find.where().eq("contact_number", contact_number).findUnique();
    }

    public static Driver findById(int id)
    {
        return find.where().eq("id_driver", id).findUnique();
    }

    public static String getDriverName(int id_driver){
        return find.where().eq("id_driver", id_driver).findUnique().name;
    }

    /**
     * Check drivers list according to work state.
     * In the ps_drivers table, if_work column indicates working status.
     * @param if_work
     * */
    public  static List<Driver> findByState(int if_work) {
        return find.where().eq("if_work", if_work).orderBy("id_driver").findList();
    }

    public  static List<Driver> findWorkingDrivers() {
        return find.where().eq("if_work", 1).orderBy("id_driver").findList();
    }

    public static List<Driver> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (Driver driver : Driver.findAll())
        {
            driver.delete();
        }
    }

    public static Model.Finder<String, Driver> find = new Model.Finder<String, Driver>(String.class, Driver.class);
}
