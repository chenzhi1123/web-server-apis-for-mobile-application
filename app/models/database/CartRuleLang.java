/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

//Driver class has the corresponding table in the database named "ps_drivers"
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_cart_rule_lang")
public class CartRuleLang extends Model
{
    @Id
    public int id_cart_rule;
    @Id
    public int id_lang;
    public String name;


    public CartRuleLang()
    {

    }

    public CartRuleLang(int id_cart_rule, int id_lang, String name) {
        this.id_cart_rule = id_cart_rule;
        this.id_lang = id_lang;
        this.name = name;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof CartRuleLang)
        {
            final CartRuleLang other = (CartRuleLang) obj;
            return Objects.equal(id_cart_rule, other.id_cart_rule)
                    &&Objects.equal(id_lang, other.id_lang)
                    &&Objects.equal(name, other.name);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "CartRuleLang{" +
                "id_cart_rule=" + id_cart_rule +
                ", id_lang=" + id_lang +
                ", name='" + name + '\'' +
                '}';
    }

    public static CartRuleLang findByName(String name)
    {
        return CartRuleLang.find.where().eq("name", name).findUnique();
    }

    public static CartRuleLang findById(int id)
    {
        return find.where().eq("id_driver", id).findUnique();
    }


    /**
     * Check drivers list according to work state.
     * In the ps_drivers table, if_work column indicates working status.
     * @param if_work
     * */
    public  static List<CartRuleLang> findByState(int if_work) {
        return find.where().eq("if_work", if_work).orderBy("id_driver").findList();
    }

    public  static List<CartRuleLang> findByCartRule(int id_cart_rule) {
        return find.select("id_group").where().eq("id_cart_rule", id_cart_rule).findList();
    }

    public static List<CartRuleLang> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (CartRuleLang driver : CartRuleLang.findAll())
        {
            driver.delete();
        }
    }

    public static Finder<String, CartRuleLang> find = new Finder<String, CartRuleLang>(String.class, CartRuleLang.class);
}
