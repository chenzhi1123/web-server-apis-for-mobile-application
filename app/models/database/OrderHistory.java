/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_order_history")
public class OrderHistory extends Model
{
    @Id
    @GeneratedValue
    public int id_order_history;
    public int id_employee;
    public int id_order;
    public int id_order_state;
    public Date date_add;

    public OrderHistory()
    {

    }

    public OrderHistory(int id_employee, int id_order, int id_order_state, Date date_add) {
        this.id_employee = id_employee;
        this.id_order = id_order;
        this.id_order_state = id_order_state;
        this.date_add = date_add;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof OrderHistory)
        {
            final OrderHistory other = (OrderHistory) obj;
            return Objects.equal(id_employee, other.id_employee)
                    && Objects.equal(id_order, other.id_order)
                    && Objects.equal(id_order_history, other.id_order_history)
                    && Objects.equal(date_add, other.date_add);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "OrderHistory{" +
                "id_order_history=" + id_order_history +
                ", id_employee=" + id_employee +
                ", id_order=" + id_order +
                ", id_order_state=" + id_order_state +
                ", date_add=" + date_add +
                '}';
    }

    public static OrderHistory findByEmail(String email)

    {
        return OrderHistory.find.where().eq("email", email).findUnique();
    }

    public static OrderHistory findById(int id)
    {
        return find.where().eq("id_order", id).findUnique();
    }

    public static List<OrderHistory> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (OrderHistory orderHistory : OrderHistory.findAll())
        {
            orderHistory.delete();
        }
    }

    public static Model.Finder<String, OrderHistory> find = new Model.Finder<String, OrderHistory>(String.class, OrderHistory.class);

}
