/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;
import util.MD5;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
/**
 * Customer Class matches the table in the database, attributes names match corresponding column names
 * Table: ps_customer
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_customer")
public class Customer extends Model
{
  @Id
  @GeneratedValue
  public int id_customer;
  public int id_shop_group;
  public int id_shop;
  public int id_gender = 0;
  public int id_default_group;
  public int id_lang;
  public int id_risk;
  public String company;
  public String siret;
  public String ape;
  public String firstname;
  public String lastname;
  public String email;
  public String passwd;
  public String last_passwd_gen;
//@Temporal(TemporalType.TIMESTAMP)
  public Date newsletter_date_add;
  public Date birthday;
  public int newsletter;
//user registration option box
  public String ip_registration_newsletter = null;
  public int optin;
  public String website;
  public float outstanding_allow_amount = 0;
  public int show_public_prices = 0;
  public int max_payment_days = 0;
  public String secure_key;
  public String note;
    
  public int active = 1;
  public int is_guest = 0;
  public int deleted = 0;
  public Date date_add;
  @Version
  @Column(name = "date_upd")
  public Date date_upd;



  public Customer()
  {

  }


 

  public Customer(int id_shop_group, int id_shop, int id_gender, int id_default_group, int id_lang, int id_risk,
    String company, String siret, String ape, String firstname, String lastname, String email, String passwd,
    String last_passwd_gen, Date newsletter_date_add, Date birthday, int newsletter, String ip_registration_newsletter,
    int optin, String website, float outstanding_allow_amount, int show_public_prices, int max_payment_days,
    String secure_key, String note, int active, int is_guest, int deleted, Date date_add, Date date_upd)
{
  super();
  this.id_shop_group = id_shop_group;
  this.id_shop = id_shop;
  this.id_gender = id_gender;
  this.id_default_group = id_default_group;
  this.id_lang = id_lang;
  this.id_risk = id_risk;
  this.company = company;
  this.siret = siret;
  this.ape = ape;
  this.firstname = firstname;
  this.lastname = lastname;
  this.email = email;
  this.passwd = passwd;
  this.last_passwd_gen = last_passwd_gen;
  this.newsletter_date_add = newsletter_date_add;
  this.birthday = birthday;
  this.newsletter = newsletter;
  this.ip_registration_newsletter = ip_registration_newsletter;
  this.optin = optin;
  this.website = website;
  this.outstanding_allow_amount = outstanding_allow_amount;
  this.show_public_prices = show_public_prices;
  this.max_payment_days = max_payment_days;
  this.secure_key = secure_key;
  this.note = note;
  this.active = active;
  this.is_guest = is_guest;
  this.deleted = deleted;
  this.date_add = date_add;
  this.date_upd = date_upd;
}




  public void update(Date new_date)
  {
     this.date_upd = new_date;
}


  public void update(Customer customer)
  {
    this.note = customer.note;
    this.id_gender = customer.id_gender;
    this.id_lang = customer.id_lang;
    this.lastname = customer.lastname;
    this.firstname = customer.firstname;
    this.birthday = customer.birthday;
    this.email = customer.email;
    this.newsletter = customer.newsletter;
    this.ip_registration_newsletter = customer.ip_registration_newsletter;
    this.newsletter_date_add = customer.newsletter_date_add;
    this.optin = customer.optin;
    this.website = customer.website;
    this.company = customer.company;
    this.siret = customer.siret;
    this.ape = customer.ape;
    this.outstanding_allow_amount = customer.outstanding_allow_amount;
    this.show_public_prices = customer.show_public_prices;
    this.id_risk = customer.id_risk;
    this.max_payment_days = customer.max_payment_days;
    this.passwd = customer.passwd;
    this.last_passwd_gen = customer.last_passwd_gen;
    this.active = customer.active;
    this.is_guest = customer.is_guest;
    this.deleted = customer.deleted;
    this.date_upd = customer.date_upd;
  }

  @Override
  public boolean equals(final Object obj)
  {
    if (obj instanceof Customer)
    {
      final Customer other = (Customer) obj;
      return Objects.equal(id_shop, other.id_shop) && Objects.equal(id_shop_group, other.id_shop_group)
          && Objects.equal(secure_key, other.secure_key) && Objects.equal(note, other.note)
          && Objects.equal(id_gender, other.id_gender)&& Objects.equal(id_default_group, other.id_default_group)
          && Objects.equal(id_lang, other.id_lang)
          && Objects.equal(lastname, other.lastname)&& Objects.equal(firstname, other.firstname)&& Objects.equal(birthday, other.birthday)
          && Objects.equal(email, other.email)&& Objects.equal(newsletter, other.newsletter)&& Objects.equal(ip_registration_newsletter, other.ip_registration_newsletter)
          && Objects.equal(newsletter_date_add, other.newsletter_date_add)&& Objects.equal(optin, other.optin)&& Objects.equal(website, other.website)
          && Objects.equal(company, other.company)&& Objects.equal(siret, other.siret)&& Objects.equal(ape, other.ape)
          && Objects.equal(outstanding_allow_amount, other.outstanding_allow_amount)&& Objects.equal(show_public_prices, other.show_public_prices)&& Objects.equal(id_risk, other.id_risk)
          && Objects.equal(max_payment_days, other.max_payment_days)&& Objects.equal(passwd, other.passwd)&& Objects.equal(last_passwd_gen, other.last_passwd_gen)
          && Objects.equal(active, other.active)&& Objects.equal(is_guest, other.is_guest)&& Objects.equal(deleted, other.deleted)
          && Objects.equal(date_add, other.date_add)&& Objects.equal(date_upd, other.date_upd);


    }
    else
    {
      return false;
    }
  }
  @Override
  public String toString()
  {
    return "Customer [id_customer=" + id_customer + ", id_shop_group=" + id_shop_group + ", id_shop=" + id_shop
        + ", id_gender=" + id_gender + ", id_default_group=" + id_default_group + ", id_lang=" + id_lang + ", id_risk="
        + id_risk + ", company=" + company + ", siret=" + siret + ", ape=" + ape + ", firstname=" + firstname
        + ", lastname=" + lastname + ", email=" + email + ", passwd=" + passwd + ", last_passwd_gen=" + last_passwd_gen
        + ", birthday=" + birthday + ", newsletter=" + newsletter + ", ip_registration_newsletter="
        + ip_registration_newsletter + ", newsletter_date_add=" + newsletter_date_add + ", optin=" + optin
        + ", website=" + website + ", outstanding_allow_amount=" + outstanding_allow_amount + ", show_public_prices="
        + show_public_prices + ", max_payment_days=" + max_payment_days + ", secure_key=" + secure_key + ", note="
        + note + ", active=" + active + ", is_guest=" + is_guest + ", deleted=" + deleted + ", date_add=" + date_add
        + ", date_upd=" + date_upd + "]";
  }



  public static Customer findByEmail(String email)
  {
    return Customer.find.where().eq("email", email).eq("active", 1).eq("deleted", 0).eq("is_guest", 0).findUnique();
  }

  public static Customer findById(Long id)
  {
    return find.where().eq("id_customer", id).findUnique();
  }

  public static Customer findById(int id)
    {
        return find.where().eq("id_customer", id).eq("active", 1).eq("deleted", 0).findUnique();
    }

  public static int getID(String email) {

    return Customer.find.where().eq("email", email).findUnique().id_customer;
  }

  public static Customer findBySecure(String secure_key){return find.where().eq("secure_key", secure_key).eq("active", 1).eq("deleted", 0).findUnique();}

  public static Customer findByAuthentication(int id_customer,String secure_key) {
    return find.where().eq("id_customer", id_customer).eq("secure_key", secure_key).eq("active", 1).eq("deleted", 0).findUnique();
  }

  public static List<Customer> findAll()
  {
    return find.all();
  }

  public static void deleteAll()
  {
    for (Customer customer : Customer.findAll())
    {
      customer.delete();
    }
  }

  public static Model.Finder<String, Customer> find = new Model.Finder<String, Customer>(String.class, Customer.class);
  
//Authentification
  public static Customer authenticate(String email, String password) {
      String newpass = MD5.encrypt(password);
      Customer customer =  Customer.findByEmail(email);
      if (customer != null && customer.passwd.equals(newpass)) {
          return customer;
      } else {
          System.out.println(email);
          return null;
      }
  }

  /**
   * Use a current customer's info, filter something and create the rest of the info back to client end.
   * @see controllers.Application
   * */
  public static Customer setNewCustomer (Customer customer){
  //return the customer's info
    customer.id_risk= 0;
    customer.passwd = "";
    customer.last_passwd_gen = "";

    customer.newsletter_date_add = null;
    customer.website = "";
    customer.outstanding_allow_amount = 0;
    customer.show_public_prices = 0;
    customer.max_payment_days = 0;

     customer.note = "";
     customer.date_upd = null;
     customer.date_add= null;
     customer.passwd = "";
    Customer newCustomer = customer;

    return newCustomer;

}
}
