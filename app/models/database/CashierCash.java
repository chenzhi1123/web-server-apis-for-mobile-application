/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;

/**
 * Created by Zhi Chen on 2017/1/15.
 * Connected with the table in the database named "ps_cashier_driver_daily"
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_cashier_final_cash")
public class CashierCash extends Model{
    @Id
    @GeneratedValue
    public int id_cashier_cash;
    public int id_cashier;
    public double final_cash;
    public Date date_add;

    public CashierCash(){

    }

    public CashierCash(int id_cashier, double final_cash, Date date_add) {
        this.id_cashier = id_cashier;
        this.final_cash = final_cash;
        this.date_add = date_add;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof CashierCash)
        {
            final CashierCash other = (CashierCash) obj;
            return Objects.equal(id_cashier_cash, other.id_cashier_cash)
                    && Objects.equal(id_cashier, other.id_cashier)
                    && Objects.equal(final_cash, other.final_cash)
                    && Objects.equal(date_add, other.date_add);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "CashierCash{" +
                "id_cashier_cash=" + id_cashier_cash +
                ", id_cashier=" + id_cashier +
                ", final_cash=" + final_cash +
                ", date_add=" + date_add +
                '}';
    }


    /**
     * Query one record in the database, Ebean finder method instead of SQL queries
     * */
    public static CashierCash findById(int id)
    {
        return find.where().eq("id_cashier_cash", id).findUnique();
    }

    public static CashierCash findByCashier(int id_cashier)
    {
        long nowMinus1Day = System.currentTimeMillis() - 1 * 14 * (1000 * 60 * 60);
        Date lastOneDay = new Date(nowMinus1Day);
        return find.where().eq("id_cashier",id_cashier).gt("date_add",lastOneDay).findUnique();
    }


    public static List<CashierCash> findCashiers()
    {
        long nowMinus1Day = System.currentTimeMillis() - 1 * 14 * (1000 * 60 * 60);
        Date lastOneDay = new Date(nowMinus1Day);
        return find.where().gt("date_add",lastOneDay).findList();
    }

    public static List<CashierCash> findAll()
    {
        return find.all();
    }


    public static void deleteAll()
    {
        for (CashierCash order : CashierCash.findAll())
        {
            order.delete();
        }
    }

    public static Model.Finder<String, CashierCash> find = new Model.Finder<String, CashierCash>(String.class, CashierCash.class);

}
