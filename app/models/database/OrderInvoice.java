/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_order_invoice")
public class OrderInvoice extends Model
{
    @Id
    @GeneratedValue
    public int id_order_invoice;
    public int id_order;
    public int number;
    public int delivery_number;
    public Date delivery_date = null;
    public java.math.BigDecimal total_discount_tax_excl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_discount_tax_incl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_paid_tax_excl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_paid_tax_incl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_products = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_products_wt = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_shipping_tax_excl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_shipping_tax_incl = BigDecimal.valueOf(0.000000);
    public int shipping_tax_computation_method;
    public java.math.BigDecimal total_wrapping_tax_excl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_wrapping_tax_incl = BigDecimal.valueOf(0.000000);
    public String shop_address = "";
    public String invoice_address = "";
    public String delivery_address = "";
    public String note = "";
    public Date date_add;


    public OrderInvoice()
    {

    }

    public OrderInvoice(int id_order, int number, int delivery_number, Date delivery_date, BigDecimal total_discount_tax_excl, BigDecimal total_discount_tax_incl, BigDecimal total_paid_tax_excl, BigDecimal total_paid_tax_incl, BigDecimal total_products, BigDecimal total_products_wt, BigDecimal total_shipping_tax_excl, BigDecimal total_shipping_tax_incl, int shipping_tax_computation_method, BigDecimal total_wrapping_tax_excl, BigDecimal total_wrapping_tax_incl, String shop_address, String invoice_address, String delivery_address, String note, Date date_add) {
        this.id_order = id_order;
        this.number = number;
        this.delivery_number = delivery_number;
        this.delivery_date = delivery_date;
        this.total_discount_tax_excl = total_discount_tax_excl;
        this.total_discount_tax_incl = total_discount_tax_incl;
        this.total_paid_tax_excl = total_paid_tax_excl;
        this.total_paid_tax_incl = total_paid_tax_incl;
        this.total_products = total_products;
        this.total_products_wt = total_products_wt;
        this.total_shipping_tax_excl = total_shipping_tax_excl;
        this.total_shipping_tax_incl = total_shipping_tax_incl;
        this.shipping_tax_computation_method = shipping_tax_computation_method;
        this.total_wrapping_tax_excl = total_wrapping_tax_excl;
        this.total_wrapping_tax_incl = total_wrapping_tax_incl;
        this.shop_address = shop_address;
        this.invoice_address = invoice_address;
        this.delivery_address = delivery_address;
        this.note = note;
        this.date_add = date_add;
    }

    public void update(BigDecimal total_discount_tax_excl, BigDecimal total_discount_tax_incl, BigDecimal total_paid_tax_excl, BigDecimal total_paid_tax_incl, BigDecimal total_products, BigDecimal total_products_wt, BigDecimal total_shipping_tax_excl, BigDecimal total_shipping_tax_incl, String invoice_address, String delivery_address){
        this.total_discount_tax_excl = total_discount_tax_excl;
        this.total_discount_tax_incl = total_discount_tax_incl;
        this.total_paid_tax_excl = total_paid_tax_excl;
        this.total_paid_tax_incl = total_paid_tax_incl;
        this.total_products = total_products;
        this.total_products_wt = total_products_wt;
        this.total_shipping_tax_excl = total_shipping_tax_excl;
        this.total_shipping_tax_incl = total_shipping_tax_incl;
        this.invoice_address = invoice_address;
        this.delivery_address = delivery_address;
    }
    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof OrderInvoice)
        {
            final OrderInvoice other = (OrderInvoice) obj;
            return Objects.equal(id_order, other.id_order)
                    && Objects.equal(number, other.number)
                    && Objects.equal(delivery_number, other.delivery_number)
                    && Objects.equal(delivery_date, other.delivery_date)
                    && Objects.equal(total_discount_tax_excl, other.total_discount_tax_excl)
                    && Objects.equal(total_discount_tax_incl, other.total_discount_tax_incl)
                    && Objects.equal(total_paid_tax_excl, other.total_paid_tax_excl)
                    && Objects.equal(total_paid_tax_incl, other.total_paid_tax_incl)
                    && Objects.equal(total_products, other.total_products)
                    && Objects.equal(total_products_wt, other.total_products_wt)
                    && Objects.equal(total_shipping_tax_excl, other.total_shipping_tax_excl)
                    && Objects.equal(total_shipping_tax_incl, other.total_shipping_tax_incl)
                    && Objects.equal(shipping_tax_computation_method, other.shipping_tax_computation_method)
                    && Objects.equal(total_wrapping_tax_excl, other.total_wrapping_tax_excl)
                    && Objects.equal(total_wrapping_tax_incl, other.total_wrapping_tax_incl)
                    && Objects.equal(shop_address, other.shop_address)
                    && Objects.equal(invoice_address, other.invoice_address)
                    && Objects.equal(delivery_address, other.delivery_address)
                    && Objects.equal(note, other.note)
                    && Objects.equal(date_add, other.date_add);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "OrderInvoice{" +
                "id_order_invoice=" + id_order_invoice +
                ", id_order=" + id_order +
                ", number=" + number +
                ", delivery_number=" + delivery_number +
                ", delivery_date=" + delivery_date +
                ", total_discount_tax_excl=" + total_discount_tax_excl +
                ", total_discount_tax_incl=" + total_discount_tax_incl +
                ", total_paid_tax_excl=" + total_paid_tax_excl +
                ", total_paid_tax_incl=" + total_paid_tax_incl +
                ", total_products=" + total_products +
                ", total_products_wt=" + total_products_wt +
                ", total_shipping_tax_excl=" + total_shipping_tax_excl +
                ", total_shipping_tax_incl=" + total_shipping_tax_incl +
                ", shipping_tax_computation_method=" + shipping_tax_computation_method +
                ", total_wrapping_tax_excl=" + total_wrapping_tax_excl +
                ", total_wrapping_tax_incl=" + total_wrapping_tax_incl +
                ", shop_address='" + shop_address + '\'' +
                ", invoice_address='" + invoice_address + '\'' +
                ", delivery_address='" + delivery_address + '\'' +
                ", note='" + note + '\'' +
                ", date_add=" + date_add +
                '}';
    }

    public static OrderInvoice findByEmail(String email)

    {
        return OrderInvoice.find.where().eq("email", email).findUnique();
    }

    public static OrderInvoice findById(Long id)
    {
        return find.where().eq("id_order_invoice", id).findUnique();
    }

    public static OrderInvoice findByOrder(int id)
    {
        return find.where().eq("id_order", id).findUnique();
    }

    public static List<OrderInvoice> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (OrderInvoice orderInvoice : OrderInvoice.findAll())
        {
            orderInvoice.delete();
        }
    }

    public static Model.Finder<String, OrderInvoice> find = new Model.Finder<String, OrderInvoice>(String.class, OrderInvoice.class);
}
