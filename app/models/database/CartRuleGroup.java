/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;
//Driver class has the corresponding table in the database named "ps_drivers"
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_cart_rule_group")
public class CartRuleGroup extends Model
{
    @Id
    @GeneratedValue
    public int id_cart_rule;
    public int id_group;

    public CartRuleGroup()
    {

    }

    public CartRuleGroup(int id_group) {
        this.id_group = id_group;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof CartRuleGroup)
        {
            final CartRuleGroup other = (CartRuleGroup) obj;
            return Objects.equal(id_cart_rule, other.id_cart_rule);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "CartRuleGroup{" +
                "id_cart_rule=" + id_cart_rule +
                ", id_group=" + id_group +
                '}';
    }

    public static CartRuleGroup findByName(String name)

    {
        return CartRuleGroup.find.where().eq("name", name).findUnique();
    }

    public static CartRuleGroup findById(int id)
    {
        return find.where().eq("id_driver", id).findUnique();
    }


    /**
     * Check drivers list according to work state.
     * In the ps_drivers table, if_work column indicates working status.
     * @param if_work
     * */
    public  static List<CartRuleGroup> findByState(int if_work) {
        return find.where().eq("if_work", if_work).orderBy("id_driver").findList();
    }

    public  static List<CartRuleGroup> findByCartRule(int id_cart_rule) {
        return find.select("id_group").where().eq("id_cart_rule", id_cart_rule).findList();
    }

    public static List<CartRuleGroup> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (CartRuleGroup driver : CartRuleGroup.findAll())
        {
            driver.delete();
        }
    }

    public static Model.Finder<String, CartRuleGroup> find = new Model.Finder<String, CartRuleGroup>(String.class, CartRuleGroup.class);
}
