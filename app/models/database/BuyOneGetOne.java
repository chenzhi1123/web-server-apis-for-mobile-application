/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

//Mapping to the tables in the database, (Ebean), attributes correspond to columns in the table
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_product_buyone_getone")
public class BuyOneGetOne extends Model
{
    @Id
    @GeneratedValue
    public int id_bogo;
    public int id_onsale_product;
    public int id_real_product;
    public Date start_date;
    public Date end_date;
    public Date date_add;
    public Date date_upd;
    public int basic_unit;
    public int bundle_number;
    public String admin_name = "";
    public int if_valid;


    public BuyOneGetOne() {
    }

    public BuyOneGetOne(int id_onsale_product, int id_real_product, Date start_date, Date end_date, Date date_add, Date date_upd, int basic_unit, int bundle_number, String admin_name, int if_valid) {
        this.id_onsale_product = id_onsale_product;
        this.id_real_product = id_real_product;
        this.start_date = start_date;
        this.end_date = end_date;
        this.date_add = date_add;
        this.date_upd = date_upd;
        this.basic_unit = basic_unit;
        this.bundle_number = bundle_number;
        this.admin_name = admin_name;
        this.if_valid = if_valid;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof BuyOneGetOne)
        {
            final BuyOneGetOne other = (BuyOneGetOne) obj;
            return Objects.equal(id_bogo, other.id_bogo)
                    && Objects.equal(id_onsale_product, other.id_onsale_product)
                    && Objects.equal(id_real_product, other.id_real_product)
                    && Objects.equal(start_date, other.start_date)
                    && Objects.equal(end_date, other.end_date)
                    && Objects.equal(date_add, other.date_add)
                    && Objects.equal(date_upd, other.date_upd)
                    && Objects.equal(basic_unit, other.basic_unit)
                    && Objects.equal(admin_name, other.admin_name)
                    && Objects.equal(if_valid, other.if_valid)
                    && Objects.equal(bundle_number, other.bundle_number);

        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "BuyOneGetOne{" +
                "id_bogo=" + id_bogo +
                ", id_onsale_product=" + id_onsale_product +
                ", id_real_product=" + id_real_product +
                ", start_date=" + start_date +
                ", end_date=" + end_date +
                ", date_add=" + date_add +
                ", date_upd=" + date_upd +
                ", basic_unit=" + basic_unit +
                ", bundle_number=" + bundle_number +
                ", admin_name='" + admin_name + '\'' +
                ", if_valid=" + if_valid +
                '}';
    }

    public static List<BuyOneGetOne> findBySaleProductId(int id)
    {
        long current_timestamp = System.currentTimeMillis();  //current time
        Date current_time = new Date(current_timestamp);
        return find.where().eq("id_onsale_product", id).gt("end_date",current_time).lt("start_date",current_time).eq("if_valid", 1).findList();
    }

    public static List<BuyOneGetOne> findByOriginalProduct(int id)
    {
        long current_timestamp = System.currentTimeMillis();  //current time
        Date current_time = new Date(current_timestamp);
        return find.where().eq("id_real_product", id).gt("end_date",current_time).lt("start_date",current_time).eq("if_valid", 1).findList();
    }

    public static List<BuyOneGetOne> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (BuyOneGetOne stockAvailable : BuyOneGetOne.findAll())
        {
            stockAvailable.delete();
        }
    }

    public static Finder<String, BuyOneGetOne> find = new Finder<String, BuyOneGetOne>(String.class, BuyOneGetOne.class);

}

