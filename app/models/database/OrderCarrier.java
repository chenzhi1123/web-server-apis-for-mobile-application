/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_order_carrier")
public class OrderCarrier extends Model
{
    @Id
    @GeneratedValue
    public int id_order_carrier;
    public int id_order;
    public int id_carrier;
    public int id_order_invoice;
    public java.math.BigDecimal weight;
    public java.math.BigDecimal shipping_cost_tax_excl;
    public java.math.BigDecimal shipping_cost_tax_incl;
    public String tracking_number;
    public Date date_add;

    public OrderCarrier()
    {

    }

    public OrderCarrier(int id_order, int id_carrier, int id_order_invoice, BigDecimal weight, BigDecimal shipping_cost_tax_excl, BigDecimal shipping_cost_tax_incl, String tracking_number, Date date_add) {
        this.id_order = id_order;
        this.id_carrier = id_carrier;
        this.id_order_invoice = id_order_invoice;
        this.weight = weight;
        this.shipping_cost_tax_excl = shipping_cost_tax_excl;
        this.shipping_cost_tax_incl = shipping_cost_tax_incl;
        this.tracking_number = tracking_number;
        this.date_add = date_add;
    }

    public void update(BigDecimal shipping_cost_tax_excl, BigDecimal shipping_cost_tax_incl){
        this.shipping_cost_tax_excl = shipping_cost_tax_excl;
        this.shipping_cost_tax_incl = shipping_cost_tax_incl;
    }
    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof Order)
        {
            final OrderCarrier other = (OrderCarrier) obj;
            return Objects.equal(id_order, other.id_order)
                    && Objects.equal(id_carrier, other.id_carrier)
                    && Objects.equal(id_order_invoice, other.id_order_invoice)
                    && Objects.equal(weight, other.weight)
                    && Objects.equal(shipping_cost_tax_excl, other.shipping_cost_tax_excl)
                    && Objects.equal(shipping_cost_tax_incl, other.shipping_cost_tax_incl)
                    && Objects.equal(tracking_number, other.tracking_number)
                    && Objects.equal(date_add, other.date_add);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "OrderCarrier{" +
                "id_order_carrier=" + id_order_carrier +
                ", id_order=" + id_order +
                ", id_carrier=" + id_carrier +
                ", id_order_invoice=" + id_order_invoice +
                ", weight=" + weight +
                ", shipping_cost_tax_excl=" + shipping_cost_tax_excl +
                ", shipping_cost_tax_incl=" + shipping_cost_tax_incl +
                ", tracking_number='" + tracking_number + '\'' +
                ", date_add=" + date_add +
                '}';
    }

    public static OrderCarrier findByEmail(String email)

    {
        return OrderCarrier.find.where().eq("email", email).findUnique();
    }

    public static OrderCarrier findById(Long id)
    {
        return find.where().eq("id_order_carrier", id).findUnique();
    }

    public static OrderCarrier findByOrder(int id)
    {
        return find.where().eq("id_order", id).findUnique();
    }

    public static List<OrderCarrier> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (OrderCarrier orderCarrier : OrderCarrier.findAll())
        {
            orderCarrier.delete();
        }
    }

    public static Model.Finder<String, OrderCarrier> find = new Model.Finder<String, OrderCarrier>(String.class, OrderCarrier.class);

}
