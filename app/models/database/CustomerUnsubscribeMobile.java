/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;
//Address Class matches the table in the database, attributes names match corresponding column names
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_customer_sms_unsubscribe")
public class CustomerUnsubscribeMobile extends Model
{
    @Id
    @GeneratedValue
    public int id_customer_sms_unsubscribe;
    public String phone_number;

    public CustomerUnsubscribeMobile()
    {

    }

    public CustomerUnsubscribeMobile(String phone_number) {
        this.phone_number = phone_number;
    }


    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof CustomerUnsubscribeMobile)
        {   final CustomerUnsubscribeMobile other = (CustomerUnsubscribeMobile) obj;
            return Objects.equal(id_customer_sms_unsubscribe, other.id_customer_sms_unsubscribe)
                    && Objects.equal(phone_number, other.phone_number);
        }
        else
        {
            return false;
        }
    }


    @Override
    public String toString() {
        return "CustomerUnsubscribeMobile{" +
                "id_customer_sms_unsubscribe=" + id_customer_sms_unsubscribe +
                ", phone_number='" + phone_number + '\'' +
                '}';
    }

    //Get all the addresses of a specific customer, also search for its alias name
    public static CustomerUnsubscribeMobile findByAlias(int customer_id, String alias)

    {
        return CustomerUnsubscribeMobile.find.where().eq("id_customer", customer_id).eq("alias", alias).eq("deleted", 0).findUnique();
    }

    public static CustomerUnsubscribeMobile findByPhone(String phone_number)
    {
        return CustomerUnsubscribeMobile.find.where().eq("phone_number", phone_number).findUnique();
    }


    public static CustomerUnsubscribeMobile findById(int id)
    {
        return find.where().eq("id_address", id).findUnique();
    }

    public static List<CustomerUnsubscribeMobile> findAll()
    {
        return find.all();
    }

    public static List<CustomerUnsubscribeMobile> findCustomerAddress(long id)
    {
        return find.where().eq("id_customer", id).eq("active", 1).eq("deleted", 0).findList();
    }

    public static List<CustomerUnsubscribeMobile> findCustomerAddressByNumber(int phone)
    {
        int length = String.valueOf(phone).length();
        String number = "%" + phone + "%";
        if (length == 5 || length == 6){
            return find.where().like("phone", number).eq("active", 1).eq("deleted", 0).setMaxRows(10).findList();
        } else if (length > 6 && length < 13) {
            return find.where().like("phone", number).eq("active", 1).eq("deleted", 0).findList();
        } else {
            return null;
        }

    }

    public static void deleteAll()
    {
        for (CustomerUnsubscribeMobile address : CustomerUnsubscribeMobile.findAll())
        {
            address.delete();
        }
    }

    public static Model.Finder<String, CustomerUnsubscribeMobile> find = new Model.Finder<String, CustomerUnsubscribeMobile>(String.class, CustomerUnsubscribeMobile.class);
}
