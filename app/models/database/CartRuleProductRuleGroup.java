/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

//Driver class has the corresponding table in the database named "ps_drivers"
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_cart_rule_product_rule_group")
public class CartRuleProductRuleGroup extends Model
{
    @Id
    @GeneratedValue
    public int id_product_rule_group;
    public int id_cart_rule;
    public int quantity = 1;


    public CartRuleProductRuleGroup()
    {

    }

    public CartRuleProductRuleGroup(int id_cart_rule, int quantity) {
        this.id_cart_rule = id_cart_rule;
        this.quantity = quantity;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof CartRuleProductRuleGroup)
        {
            final CartRuleProductRuleGroup other = (CartRuleProductRuleGroup) obj;
            return Objects.equal(id_cart_rule, other.id_cart_rule)
                    &&Objects.equal(id_product_rule_group, other.id_product_rule_group)
                    &&Objects.equal(quantity, other.quantity);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "CartRuleProductRuleGroup{" +
                "id_product_rule_group=" + id_product_rule_group +
                ", id_cart_rule=" + id_cart_rule +
                ", quantity=" + quantity +
                '}';
    }

    public static CartRuleProductRuleGroup findByName(String name)
    {
        return CartRuleProductRuleGroup.find.where().eq("name", name).findUnique();
    }

    public static CartRuleProductRuleGroup findById(int id)
    {
        return find.where().eq("id_driver", id).findUnique();
    }


    /**
     * Check drivers list according to work state.
     * In the ps_drivers table, if_work column indicates working status.
     * @param if_work
     * */
    public  static List<CartRuleProductRuleGroup> findByState(int if_work) {
        return find.where().eq("if_work", if_work).orderBy("id_driver").findList();
    }

    public  static List<CartRuleProductRuleGroup> findByCartRule(int id_cart_rule) {
        return find.select("id_group").where().eq("id_cart_rule", id_cart_rule).findList();
    }

    public static List<CartRuleProductRuleGroup> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (CartRuleProductRuleGroup driver : CartRuleProductRuleGroup.findAll())
        {
            driver.delete();
        }
    }

    public static Finder<String, CartRuleProductRuleGroup> find = new Finder<String, CartRuleProductRuleGroup>(String.class, CartRuleProductRuleGroup.class);
}
