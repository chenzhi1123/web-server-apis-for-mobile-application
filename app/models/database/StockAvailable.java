/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.List;
//Mapping to the tables in the database, (Ebean), attributes correspond to columns in the table
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_stock_available")
public class StockAvailable extends Model
{
    @Id
    @GeneratedValue
    public int id_stock_available;
    public int id_product;
    public int id_product_attribute;
    public int id_shop;
    public int id_shop_group;
    public int quantity;
    public int depends_on_stock;
    public int out_of_stock;


    public StockAvailable() {
    }

    public StockAvailable(int id_product, int id_product_attribute, int id_shop, int id_shop_group, int quantity, int depends_on_stock, int out_of_stock) {
        this.id_product = id_product;
        this.id_product_attribute = id_product_attribute;
        this.id_shop = id_shop;
        this.id_shop_group = id_shop_group;
        this.quantity = quantity;
        this.depends_on_stock = depends_on_stock;
        this.out_of_stock = out_of_stock;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof StockAvailable)
        {
            final StockAvailable other = (StockAvailable) obj;
            return Objects.equal(id_product, other.id_product)
                    && Objects.equal(id_product_attribute, other.id_product_attribute)
                    && Objects.equal(id_shop, other.id_shop)
                    && Objects.equal(id_shop_group, other.id_shop_group)
                    && Objects.equal(quantity, other.quantity)
                    && Objects.equal(depends_on_stock, other.depends_on_stock)
                    && Objects.equal(out_of_stock, other.out_of_stock);

        }
        else
        {
            return false;
        }
    }


    @Override
    public String toString() {
        return "StockAvailable{" +
                "id_stock_available=" + id_stock_available +
                ", id_product=" + id_product +
                ", id_product_attribute=" + id_product_attribute +
                ", id_shop=" + id_shop +
                ", id_shop_group=" + id_shop_group +
                ", quantity=" + quantity +
                ", depends_on_stock=" + depends_on_stock +
                ", out_of_stock=" + out_of_stock +
                '}';
    }

    public static StockAvailable findByEmail(String email)

    {
        return StockAvailable.find.where().eq("email", email).findUnique();
    }

    public static StockAvailable findById(Long id)
    {
        return find.where().eq("id_order", id).findUnique();
    }

    public static  StockAvailable findByAttribute(int id_product, int id_product_attribute)
    {
        return StockAvailable.find.where().eq("id_product", id_product).eq("id_product_attribute", id_product_attribute).findUnique();
    }

    public static List<StockAvailable> findAll()
    {
        return find.all();
    }

    /**if product_attribute_id = 0, the number is the total quantity of that product*
     *@param product_id
     * @param quantity
     */
     public synchronized static void changeQuantity(int product_id, int quantity) {

        StockAvailable stock = StockAvailable.findByAttribute(product_id,0);
        if (stock != null){
            stock.quantity -= quantity;
            stock.save();
        }
        System.out.println("Quantity changed");

    }
    public static void deleteAll()
    {
        for (StockAvailable stockAvailable : StockAvailable.findAll())
        {
            stockAvailable.delete();
        }
    }

    public static Model.Finder<String, StockAvailable> find = new Model.Finder<String, StockAvailable>(String.class, StockAvailable.class);

}

