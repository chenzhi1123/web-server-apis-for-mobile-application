/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

//Driver class has the corresponding table in the database named "ps_drivers"
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_cart_rule_product_rule")
public class CartRuleProductRule extends Model
{
    @Id
    @GeneratedValue
    public int id_product_rule;
    public int id_product_rule_group;
    public String type;


    public CartRuleProductRule()
    {

    }

    public CartRuleProductRule(int id_product_rule_group, String type) {
        this.id_product_rule_group = id_product_rule_group;
        this.type = type;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof CartRuleProductRule)
        {
            final CartRuleProductRule other = (CartRuleProductRule) obj;
            return Objects.equal(id_product_rule, other.id_product_rule)
                    &&Objects.equal(id_product_rule_group, other.id_product_rule_group)
                    &&Objects.equal(type, other.type);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "CartRuleProductRule{" +
                "id_product_rule=" + id_product_rule +
                ", id_product_rule_group=" + id_product_rule_group +
                ", type='" + type + '\'' +
                '}';
    }

    public static CartRuleProductRule findByName(String name)
    {
        return CartRuleProductRule.find.where().eq("name", name).findUnique();
    }

    public static CartRuleProductRule findById(int id)
    {
        return find.where().eq("id_driver", id).findUnique();
    }


    /**
     * Check drivers list according to work state.
     * In the ps_drivers table, if_work column indicates working status.
     * @param if_work
     * */
    public  static List<CartRuleProductRule> findByState(int if_work) {
        return find.where().eq("if_work", if_work).orderBy("id_driver").findList();
    }

    public  static List<CartRuleProductRule> findByCartRule(int id_cart_rule) {
        return find.select("id_group").where().eq("id_cart_rule", id_cart_rule).findList();
    }

    public static List<CartRuleProductRule> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (CartRuleProductRule driver : CartRuleProductRule.findAll())
        {
            driver.delete();
        }
    }

    public static Finder<String, CartRuleProductRule> find = new Finder<String, CartRuleProductRule>(String.class, CartRuleProductRule.class);
}
