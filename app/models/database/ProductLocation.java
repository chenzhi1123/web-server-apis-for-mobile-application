/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;
/**
 * Order Class matches table ps_orders, attributes corresponds to columns in the table
 * */
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_product_location")
public class ProductLocation extends Model
{
    @Id
    @GeneratedValue
    public int id_product_location;
    public int id_product;
    public String zone;
    public String layer;
    public String part;
    public int position_number;

    public ProductLocation()
    {

    }

    public ProductLocation(int id_product, String zone, String layer, String part, int position_number) {
        this.id_product = id_product;
        this.zone = zone;
        this.layer = layer;
        this.part = part;
        this.position_number = position_number;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof ProductLocation)
        {
            final ProductLocation other = (ProductLocation) obj;
            return Objects.equal(id_product, other.id_product)
                    && Objects.equal(zone, other.zone)
                    && Objects.equal(layer, other.layer)
                    && Objects.equal(part, other.part)
                    && Objects.equal(position_number, other.position_number);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "ProductLocation{" +
                "id_product_location=" + id_product_location +
                ", id_product=" + id_product +
                ", zone='" + zone + '\'' +
                ", layer='" + layer + '\'' +
                ", position_number='" + position_number + '\'' +
                '}';
    }

    public static ProductLocation findByEmail(String email)

    {
        return ProductLocation.find.where().eq("email", email).findUnique();
    }

    public static ProductLocation findById(int id)
    {
        return find.where().eq("id_product_location", id).findUnique();
    }

    public static ProductLocation findByProduct(int id_product)
    {
        return find.where().eq("id_product", id_product).findUnique();
    }


    public static ProductLocation findByReference(String reference) {return find.where().eq("reference", reference).findUnique();}



    public static List<ProductLocation> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (ProductLocation order : ProductLocation.findAll())
        {
            order.delete();
        }
    }

    public static Model.Finder<String, ProductLocation> find = new Model.Finder<String, ProductLocation>(String.class, ProductLocation.class);

}
