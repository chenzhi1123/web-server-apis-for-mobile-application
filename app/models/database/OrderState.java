/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_order_state_lang")
public class OrderState extends Model
{
    @Id
    @GeneratedValue
    public int id_order_state;
    public int id_lang;
    public String name;
    public String template;


    public OrderState()
    {

    }

    public OrderState(int id_lang, String name, String template) {
        this.id_lang = id_lang;
        this.name = name;
        this.template = template;
    }
    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof OrderState)
        {
            final OrderState other = (OrderState) obj;
            return Objects.equal(id_lang, other.id_lang)
                    && Objects.equal(name, other.name)
                    && Objects.equal(template, other.template);

        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "OrderState{" +
                "id_order_state=" + id_order_state +
                ", id_lang=" + id_lang +
                ", name='" + name + '\'' +
                ", template='" + template + '\'' +
                '}';
    }


    public static String findById(int id)
    {
        return find.where().eq("id_order_state", id).findUnique().name;
    }

    public static List<OrderState> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (OrderState orderState : OrderState.findAll())
        {
            orderState.delete();
        }
    }

    public static Model.Finder<String, OrderState> find = new Model.Finder<String, OrderState>(String.class, OrderState.class);

}
