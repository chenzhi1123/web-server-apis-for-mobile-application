/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_order_invoice_payment")
public class OrderInvoicePayment extends Model
{
    @Id
    @GeneratedValue
    public int id_order_invoice;
    public int id_order_payment;
    public int id_order;

    public OrderInvoicePayment()
    {

    }

    public OrderInvoicePayment(int id_order_invoice, int id_order_payment, int id_order) {
        this.id_order_invoice = id_order_invoice;

        this.id_order_payment = id_order_payment;

        this.id_order = id_order;
    }
    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof OrderInvoicePayment)
        {
            final OrderInvoicePayment other = (OrderInvoicePayment) obj;
            return Objects.equal(id_order, other.id_order)
                    && Objects.equal(id_order_invoice, other.id_order_invoice)
                    && Objects.equal(id_order_payment, other.id_order_payment);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "OrderInvoicePayment{" +
                "id_order_invoice=" + id_order_invoice +
                ", id_order_payment=" + id_order_payment +
                ", id_order=" + id_order +
                '}';
    }

    public static OrderInvoicePayment findByOrder(int id_order)

    {
        return OrderInvoicePayment.find.where().eq("id_order", id_order).findUnique();
    }

    public static OrderInvoicePayment findById(Long id)
    {
        return find.where().eq("id_order_invoice", id).findUnique();
    }

    public static List<OrderInvoicePayment> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (OrderInvoicePayment orderInvoicePayment : OrderInvoicePayment.findAll())
        {
            orderInvoicePayment.delete();
        }
    }

    public static Model.Finder<String, OrderInvoicePayment> find = new Model.Finder<String, OrderInvoicePayment>(String.class, OrderInvoicePayment.class);
}
