/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
/**
 * Customer Class matches the table in the database, attributes names match corresponding column names
 * Table: ps_customer
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_customer_device_token")
public class CustomerDeviceToken extends Model
{
    @Id
    @GeneratedValue
    public int id_customer_device_token;
    public int id_customer;
    public String device_token;
    public String device_type;
    public Date last_access_time;

    public CustomerDeviceToken()
    {

    }

    public CustomerDeviceToken(int id_customer, String device_token, String device_type, Date last_access_time) {
        this.id_customer = id_customer;
        this.device_token = device_token;
        this.device_type = device_type;
        this.last_access_time = last_access_time;
    }


    public void update(CustomerDeviceToken customerDeviceToken)
    {
        this.id_customer = customerDeviceToken.id_customer;
        this.device_token = customerDeviceToken.device_token;
        this.device_type = customerDeviceToken.device_type;
        this.last_access_time = customerDeviceToken.last_access_time;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof CustomerDeviceToken)
        {
            final CustomerDeviceToken other = (CustomerDeviceToken) obj;
            return Objects.equal(id_customer, other.id_customer) && Objects.equal(device_token, other.device_token)
                    && Objects.equal(device_type, other.device_type) && Objects.equal(last_access_time, other.last_access_time);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "CustomerDeviceToken{" +
                "id_customer_device_token=" + id_customer_device_token +
                ", id_customer=" + id_customer +
                ", device_token='" + device_token + '\'' +
                ", device_type='" + device_type + '\'' +
                ", last_access_time=" + last_access_time +
                '}';
    }

    public static CustomerDeviceToken findByToken(int id_customer,String device_token)
    {
        return find.where().eq("id_customer", id_customer).eq("device_token", device_token).findUnique();
    }

    public static CustomerDeviceToken findById(Long id)
    {
        return find.where().eq("id_customer", id).findUnique();
    }

    public static CustomerDeviceToken findById(int id)
    {
        return find.where().eq("id_customer", id).eq("active", 1).eq("deleted", 0).findUnique();
    }

    public static int getID(String email) {

        return CustomerDeviceToken.find.where().eq("email", email).findUnique().id_customer;
    }


    public static CustomerDeviceToken findBySecure(String secure_key){return find.where().eq("secure_key", secure_key).eq("active", 1).eq("deleted", 0).findUnique();}

    public static List<CustomerDeviceToken> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (CustomerDeviceToken customer : CustomerDeviceToken.findAll())
        {
            customer.delete();
        }
    }

    public static Model.Finder<String, CustomerDeviceToken> find = new Model.Finder<String, CustomerDeviceToken>(String.class, CustomerDeviceToken.class);

}
