/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.List;
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_order_invoice_tax")
public class OrderInvoiceTax extends Model
{
    @Id
    @GeneratedValue
    public int id_order_invoice;
    public String type;
    public int id_tax;
    public java.math.BigDecimal amount = BigDecimal.valueOf(0.000000);

    public OrderInvoiceTax()
    {

    }

    public OrderInvoiceTax(int id_order_invoice, String type, int id_tax, BigDecimal amount) {
        this.type = type;
        this.id_order_invoice = id_order_invoice;
        this.id_tax = id_tax;
        this.amount = amount;
    }
    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof OrderInvoiceTax)
        {
            final OrderInvoiceTax other = (OrderInvoiceTax) obj;
            return Objects.equal(id_order_invoice, other.id_order_invoice)
                    && Objects.equal(type, other.type)
                    && Objects.equal(id_tax, other.id_tax)
                    && Objects.equal(amount, other.amount);

        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "OrderInvoiceTax{" +
                "id_order_invoice=" + id_order_invoice +
                ", type='" + type + '\'' +
                ", id_tax=" + id_tax +
                ", amount=" + amount +
                '}';
    }

    public static OrderInvoiceTax findByInvoice(int id_order_invoice)

    {
        return OrderInvoiceTax.find.where().eq("id_order_invoice", id_order_invoice).findUnique();
    }

    public static OrderInvoiceTax findById(Long id)
    {
        return find.where().eq("id_order_invoice", id).findUnique();
    }

    public static List<OrderInvoiceTax> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (OrderInvoiceTax orderInvoiceTax : OrderInvoiceTax.findAll())
        {
            orderInvoiceTax.delete();
        }
    }
    public static Model.Finder<String, OrderInvoiceTax> find = new Model.Finder<String, OrderInvoiceTax>(String.class, OrderInvoiceTax.class);

}
