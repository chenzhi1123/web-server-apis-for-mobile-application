/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.List;
// ps_order_cart_rule corresponds class OrderCartRule
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_order_cart_rule")
public class OrderCartRule extends Model
{
    @Id
    @GeneratedValue
    public int id_order_cart_rule;
    public int id_order;
    public int id_cart_rule;
    public int id_order_invoice;
    public String name;
    public BigDecimal value = BigDecimal.valueOf(0.00);
    public BigDecimal value_tax_excl = BigDecimal.valueOf(0.00);
    public int free_shipping;


    public OrderCartRule()
    {

    }

    public OrderCartRule(int id_order, int id_cart_rule, int id_order_invoice, String name, BigDecimal value, BigDecimal value_tax_excl, int free_shipping) {
        this.id_order = id_order;
        this.id_cart_rule = id_cart_rule;
        this.id_order_invoice = id_order_invoice;
        this.name = name;
        this.value = value;
        this.value_tax_excl = value_tax_excl;
        this.free_shipping = free_shipping;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof OrderCartRule)
        {
            final OrderCartRule other = (OrderCartRule) obj;
            return Objects.equal(id_order, other.id_order)
                    && Objects.equal(id_cart_rule, other.id_cart_rule)
                    && Objects.equal(id_order_cart_rule, other.id_order_cart_rule)
                    && Objects.equal(name, other.name)
                    && Objects.equal(value, other.value)
                    && Objects.equal(value_tax_excl, other.value_tax_excl)
                    && Objects.equal(free_shipping, other.free_shipping);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "OrderCartRule{" +
                "id_order_cart_rule=" + id_order_cart_rule +
                ", id_order=" + id_order +
                ", id_cart_rule=" + id_cart_rule +
                ", id_order_invoice=" + id_order_invoice +
                ", name='" + name + '\'' +
                ", value=" + value +
                ", value_tax_excl=" + value_tax_excl +
                ", free_shipping=" + free_shipping +
                '}';
    }

    public static OrderCartRule findByName(String name)

    {
        return OrderCartRule.find.where().eq("name", name).findUnique();
    }

    public static OrderCartRule findById(int id)
    {
        return find.where().eq("id_driver", id).findUnique();
    }

    public static List<OrderCartRule> findByOrder(int id_order)
    {
        return find.where().eq("id_order", id_order).findList();
    }


    public  static List findByCartRule(int id_cart_rule) {
        return find.fetch("id_group").where().eq("id_cart_rule", id_cart_rule).findList();
    }

    public static List<OrderCartRule> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (OrderCartRule driver : OrderCartRule.findAll())
        {
            driver.delete();
        }
    }

    public static Model.Finder<String, OrderCartRule> find = new Model.Finder<String, OrderCartRule>(String.class, OrderCartRule.class);
}
