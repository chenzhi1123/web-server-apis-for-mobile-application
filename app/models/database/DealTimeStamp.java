/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
/**
 * Order Class matches table ps_orders, attributes corresponds to columns in the table
 * */
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_deal_api_timestamp")
public class DealTimeStamp extends Model
{
    @Id
    @GeneratedValue
    @Version
    public int id_deal_api_timestamp;
    public Date last_access_time;

    public DealTimeStamp()
    {

    }

    public DealTimeStamp(Date last_access_time) {
        this.last_access_time = last_access_time;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof DealTimeStamp)
        {
            final DealTimeStamp other = (DealTimeStamp) obj;
            return Objects.equal(id_deal_api_timestamp, other.id_deal_api_timestamp)
                    && Objects.equal(last_access_time, other.last_access_time);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "DealTimeStamp{" +
                "id_deal_api_timestamp=" + id_deal_api_timestamp +
                ", last_access_time=" + last_access_time +
                '}';
    }

    public static DealTimeStamp findByEmail(String email)

    {
        return DealTimeStamp.find.where().eq("email", email).findUnique();
    }

    public static DealTimeStamp findById(int id)
    {
        return find.where().eq("id_deal_api_timestamp", id).findUnique();
    }

    public static DealTimeStamp findByProduct(int id_product)
    {
        return find.where().eq("id_product", id_product).findUnique();
    }


    public static DealTimeStamp findByReference(String reference) {return find.where().eq("reference", reference).findUnique();}



    public static List<DealTimeStamp> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (DealTimeStamp order : DealTimeStamp.findAll())
        {
            order.delete();
        }
    }

    public static Model.Finder<String, DealTimeStamp> find = new Model.Finder<String, DealTimeStamp>(String.class, DealTimeStamp.class);

}
