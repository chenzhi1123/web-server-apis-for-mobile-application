/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

@SuppressWarnings("serial")
@Entity
@Table(name = "ps_customer_browser_token")
public class CustomerBrowserToken extends Model
{
    @Id
    @GeneratedValue
    public int id_customer_browser_token;
    public String endpoint;
    public String p256dh;
    public String auth;


    public CustomerBrowserToken()
    {

    }

    public CustomerBrowserToken(String endpoint, String p256dh, String auth) {
        this.endpoint = endpoint;
        this.p256dh = p256dh;
        this.auth = auth;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof CustomerBrowserToken)
        {
            final CustomerBrowserToken other = (CustomerBrowserToken) obj;
            return Objects.equal(id_customer_browser_token, other.id_customer_browser_token)
                    && Objects.equal(p256dh, other.p256dh)
                    && Objects.equal(endpoint, other.endpoint)
                    && Objects.equal(auth, other.auth);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "CustomerBrowserToken{" +
                "id_customer_browser_token=" + id_customer_browser_token +
                ", endpoint='" + endpoint + '\'' +
                ", p256dh='" + p256dh + '\'' +
                ", auth='" + auth + '\'' +
                '}';
    }

    //通过浏览器端口值来判断是否存在记录
    public static CustomerBrowserToken findByEndpoint(String endpoint)
    {
        return find.where().eq("endpoint", endpoint).findUnique();
    }

    public static List<CustomerBrowserToken> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (CustomerBrowserToken orderState : CustomerBrowserToken.findAll())
        {
            orderState.delete();
        }
    }

    public static Finder<String, CustomerBrowserToken> find = new Finder<String, CustomerBrowserToken>(String.class, CustomerBrowserToken.class);

}
