/*
 * Copyright (c) 2017. Zhi Chen.
 * AttributeAPI is used for searching all attributes of one product.
 * For food products like Pizza, Chinese and Asian, the attributes are as a list.
 * For shop product the result is normally a "null" object
 */

package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import models.sqlContainer.ProductAttribute;
import models.database.ProductAttributeImage;
import models.sqlContainer.ProductAttributes;
import play.cache.Cache;
import play.mvc.Controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AttributeAPI extends Controller {
     /**
     * Get all attributes of a certain product, using RAWSQL package
     * @param productID
     * @return ArrayList<ProductAttribute>
     * */
    public static ArrayList<ProductAttribute> getProductAttributes(long productID, BigDecimal reduction_percent) {
        //If there is nothing int the cache, add a new record into cache, using Cache utility, key-value hash-map
        if (Cache.get("allAttributesOfProduct"+ productID) == null) {
            String sql =
                    "select distinct ps_layered_product_attribute.id_attribute, ps_attribute_group.id_attribute_group, ps_attribute_group.group_type, ps_attribute_group_lang.public_name, ps_attribute_lang.name, ps_product_attribute.price, ps_product_attribute_combination.id_product_attribute, ps_attribute.position\n" +
                            "\n" +
                            "from ps_layered_product_attribute, ps_attribute_group, ps_attribute_group_lang, ps_attribute_lang, ps_product_attribute, ps_product_attribute_combination, ps_attribute, ps_stock_available\n" +
                            "\n" +
                            " where ps_layered_product_attribute.id_product = \n" + productID +
                            " and ps_layered_product_attribute.id_shop = 1\n" +
                            "\n" +
                            " and ps_attribute.id_attribute = ps_layered_product_attribute.id_attribute\n" +
                            " and ps_layered_product_attribute.id_attribute = ps_attribute_lang.id_attribute\n" +
                            " and ps_attribute_lang.id_attribute = ps_product_attribute_combination.id_attribute\n" +
                            "\n" +
                            " and ps_layered_product_attribute.id_attribute_group = ps_attribute_group.id_attribute_group\n" +
                            " and ps_attribute_group.id_attribute_group = ps_attribute_group_lang.id_attribute_group\n" +
                            " and ps_product_attribute_combination.id_product_attribute = ps_stock_available.id_product_attribute\n" +
                            " and ps_stock_available.quantity > 0\n" +
                            " and ps_attribute_group.id_attribute_group != 6\n" +
                            " and ps_attribute_group.id_attribute_group != 19\n" +
                            "\n" +
                            " and ps_layered_product_attribute.id_product = ps_product_attribute.id_product\n" +
                            "\n" +
                            " and ps_product_attribute.id_product_attribute = ps_product_attribute_combination.id_product_attribute\n" +
                            "\n" +
                            "order by ps_attribute_group.position, ps_attribute.position";
            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_layered_product_attribute.id_attribute", "id_attribute")
                            .columnMapping("ps_attribute_group.id_attribute_group", "id_attribute_group")
                            .columnMapping("ps_attribute_group.group_type", "group_type")
                            .columnMapping("ps_attribute_group_lang.public_name", "public_name")
                            .columnMapping("ps_attribute_lang.name", "name")
                            .columnMapping("ps_product_attribute.price", "price")
                            .columnMapping("ps_product_attribute_combination.id_product_attribute", "id_product_attribute")
                            .columnMapping("ps_attribute.position", "position")
                            .create();
            com.avaje.ebean.Query<ProductAttribute> query = Ebean.find(ProductAttribute.class);
            query.setRawSql(rawSql);
            List<ProductAttribute> list = query.findList();
            int number = list.size();
            ArrayList<ProductAttribute> attributes = new ArrayList<ProductAttribute>();
            attributes.addAll(list);
//            ProductAttribute[] all_attributes = new ProductAttribute[list.size()];
//            list.toArray(all_attributes);
            //new thread
//            Runnable forloop = new Runnable() {
//                @Override
//                public synchronized void run() {
                    //change the attribute name from either "Extra Specification" or "SP" into "textarea" to comply the usage of AttributeWizard module in Prestashop
//                    for (int i = 0; i < number; i++) {
//                        try {
//                            if (  attributes.get(i).id_attribute == 38) {
//                                  attributes.get(i).group_type = "textarea";
//                            }
//                        } catch (Exception e) {
//                            System.out.println("no such attribute");
//                        }
//                        if (attributes.get(i).id_attribute_group ==15){
//                        attributes.get(i).price = (float) (attributes.get(i).price * (1.0 - reduction_percent.floatValue()));     //pizza topping 打折
//                        }
//                        int product_attribute = attributes.get(i).id_product_attribute;
//                        attributes.get(i).productAttributeImage = ProductAttributeImage.findById(product_attribute);
//                    }
            for(ProductAttribute this_one:attributes){
                try {
                    if (  this_one.id_attribute == 38) {
                        this_one.group_type = "textarea";
                    }
                } catch (Exception e) {
                    System.out.println("no such attribute");
                }
                if (this_one.id_attribute_group ==15){
                    this_one.price = (float) (this_one.price * (1.0 - reduction_percent.floatValue()));     //pizza topping 打折
                }
                int product_attribute = this_one.id_product_attribute;
                this_one.productAttributeImage = ProductAttributeImage.findById(product_attribute);
            }

//                }
//            };
//            new Thread(forloop).start();
            //thread end
            ProductAttributes a = new ProductAttributes(attributes);
            //set attributes cache time for 1 day on the server. Drawback: not real time attribute, one hour lag until clear the cache
            Cache.set("allAttributesOfProduct" + productID, a, 60 * 60 * 24);
            return attributes;
        }else{
            //retrieve the record of the product from Cache HashMap
//            ArrayList<ProductAttribute> attributes = (ArrayList<ProductAttribute>) Cache.get("allAttributesOfProduct"+productID);
            ProductAttributes a = (ProductAttributes)Cache.get("allAttributesOfProduct"+productID);
            ArrayList<ProductAttribute> attributes = a.productAttributes;
            return attributes;
        }
    }

    public static ArrayList<ProductAttribute> setNewCache(long productID, BigDecimal reduction_percent) {
        Cache.remove("allAttributesOfProduct" + productID);//先清除attribute缓存
            String sql =
                    "select distinct ps_layered_product_attribute.id_attribute, ps_attribute_group.id_attribute_group, ps_attribute_group.group_type, ps_attribute_group_lang.public_name, ps_attribute_lang.name, ps_product_attribute.price, ps_product_attribute_combination.id_product_attribute, ps_attribute.position\n" +
                            "\n" +
                            "from ps_layered_product_attribute, ps_attribute_group, ps_attribute_group_lang, ps_attribute_lang, ps_product_attribute, ps_product_attribute_combination, ps_attribute, ps_stock_available\n" +
                            "\n" +
                            " where ps_layered_product_attribute.id_product = \n" + productID +
                            " and ps_layered_product_attribute.id_shop = 1\n" +
                            "\n" +
                            " and ps_attribute.id_attribute = ps_layered_product_attribute.id_attribute\n" +
                            " and ps_layered_product_attribute.id_attribute = ps_attribute_lang.id_attribute\n" +
                            " and ps_attribute_lang.id_attribute = ps_product_attribute_combination.id_attribute\n" +
                            "\n" +
                            " and ps_layered_product_attribute.id_attribute_group = ps_attribute_group.id_attribute_group\n" +
                            " and ps_attribute_group.id_attribute_group = ps_attribute_group_lang.id_attribute_group\n" +
                            " and ps_product_attribute_combination.id_product_attribute = ps_stock_available.id_product_attribute\n" +
                            " and ps_stock_available.quantity > 0\n" +
                            " and ps_attribute_group.id_attribute_group != 6\n" +
                            " and ps_attribute_group.id_attribute_group != 19\n" +
                            "\n" +
                            " and ps_layered_product_attribute.id_product = ps_product_attribute.id_product\n" +
                            "\n" +
                            " and ps_product_attribute.id_product_attribute = ps_product_attribute_combination.id_product_attribute\n" +
                            "\n" +
                            "order by ps_attribute_group.position, ps_attribute.position";
            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_layered_product_attribute.id_attribute", "id_attribute")
                            .columnMapping("ps_attribute_group.id_attribute_group", "id_attribute_group")
                            .columnMapping("ps_attribute_group.group_type", "group_type")
                            .columnMapping("ps_attribute_group_lang.public_name", "public_name")
                            .columnMapping("ps_attribute_lang.name", "name")
                            .columnMapping("ps_product_attribute.price", "price")
                            .columnMapping("ps_product_attribute_combination.id_product_attribute", "id_product_attribute")
                            .columnMapping("ps_attribute.position", "position")
                            .create();
            com.avaje.ebean.Query<ProductAttribute> query = Ebean.find(ProductAttribute.class);
            query.setRawSql(rawSql);
            List<ProductAttribute> list = query.findList();
            int number = list.size();
            ArrayList<ProductAttribute> attributes = new ArrayList<ProductAttribute>();
            attributes.addAll(list);
            for(ProductAttribute this_one:attributes){
                try {
                    if (  this_one.id_attribute == 38) { //特殊情况的属性
                        this_one.group_type = "textarea";
                    }
                } catch (Exception e) {
                    System.out.println("no such attribute");
                }
                if (this_one.id_attribute_group ==15){
                    this_one.price = (float) (this_one.price * (1.0 - reduction_percent.floatValue()));     //pizza topping 打折
                }
                int product_attribute = this_one.id_product_attribute;
                this_one.productAttributeImage = ProductAttributeImage.findById(product_attribute);
            }

            ProductAttributes a = new ProductAttributes(attributes);
            //set attributes cache time for 1 day on the server. Drawback: not real time attribute, one hour lag until clear the cache
            Cache.set("allAttributesOfProduct" + productID, a, 60 * 60 * 24);
        return attributes;
    }
}
