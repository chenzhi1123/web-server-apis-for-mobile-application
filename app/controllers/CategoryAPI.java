/*
 * Copyright (c) 2017. Zhi Chen.
 * CategoryAPI is used to search all categories, either all category items
 * with individual level depth and parent_id, or query each specific category
 * with one ID.
 */

package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import models.sqlContainer.Categories;
import models.sqlContainer.Category;
import play.cache.Cache;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.ArrayList;
import java.util.List;

import static parsers.JsonParser.renderCategories;

public class CategoryAPI extends Controller {
    /**
     * Get all categories and each level depth in the Categories "binary tree"
     * Mobile side will retrieve the tree and make it into a hierarchical category
     * structure
     * */
    public static Result getAllCategory(){
        response().setHeader("Connection","close");
        if (Cache.get("allCategories") == null){
        String sql =
                "SELECT distinct ps_category.id_category, ps_category.id_parent, ps_category.level_depth, ps_category_lang.name, ps_category_lang.link_rewrite,ps_category_shop.position FROM ps_category, ps_category_lang, ps_category_shop WHERE ps_category.active = 1 AND ps_category.id_category = ps_category_lang.id_category AND ps_category.id_category = ps_category_shop.id_category AND ps_category_shop.id_shop = 1 ORDER BY ps_category_shop.position";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_category.id_category", "id_category")
                        .columnMapping("ps_category.id_parent", "id_parent")
                        .columnMapping("ps_category.level_depth", "level_depth")
                        .columnMapping("ps_category_lang.name", "name")
                        .columnMapping("ps_category_lang.link_rewrite", "link_rewrite")
                        .columnMapping("ps_category_shop.position", "position")
                        .create();
        com.avaje.ebean.Query<Category> query = Ebean.find(Category.class);
        query.setRawSql(rawSql);
        List<Category> list = query.findList();
        ArrayList<Category> categories = new ArrayList<>();
        categories.addAll(list);
        Categories c = new Categories(categories);
            // set cache time as 1 day
        Cache.set("allCategories", c, 60 * 60 * 24);
            System.out.println("this is not cache");
            return ok(renderCategories(c));
        }else {
            Categories c = (Categories)Cache.get("allCategories");
            System.out.println("Cache used");
            return ok(renderCategories(c));
        }
    }

    /**
     * Return the subcategory of one certain category
     * @param shopID
     * @param categoryID (father category)
     * */
    public static Result getCategory(long shopID, long categoryID) {
        response().setHeader("Connection","close");
        String sql =
                "SELECT distinct ps_category.id_category,ps_category_lang.name" +
                        " FROM  ps_category, ps_category_lang" +
                        " WHERE ps_category_lang.id_shop= " + shopID +
                        " AND " +
                        "ps_category.id_parent= " + categoryID +
                        " AND " + " ps_category.id_category = ps_category_lang.id_category" +
                        " ORDER BY id_category ";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_category.id_category", "id_category")
                        .columnMapping("ps_category_lang.name", "name")
                        .create();
        com.avaje.ebean.Query<Category> query = Ebean.find(Category.class);
        query.setRawSql(rawSql);
        List<Category> list = query.findList();
        ArrayList<Category> categories = new ArrayList<>();
        categories.addAll(list);
        System.out.println(categories.getClass());
        System.out.println(categories);
        Categories c = new Categories(categories);
        return ok(renderCategories(c));
    }
}
