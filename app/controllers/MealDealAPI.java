package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import models.database.DealTimeStamp;
import models.sqlContainer.BundleProduct;
import models.sqlContainer.MealDealProduct;
import play.data.validation.Constraints;
import play.db.ebean.Transactional;
import play.mvc.Controller;
import play.mvc.Result;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static controllers.OrderAPI.change_product_quantity;

//import static parsers.JsonParser.renderProduct;

public class MealDealAPI extends Controller
{
    public static class SearchBox{
        public String deal_product_id = "";
        public String id_product_attribute ="";
        public String id_product_shop = "";
        public String key_word ="";
    }

    public static class OnSalePerson{
        @Constraints.Required
        public String staff_name;
    }

    public static class AddStockForm{
        @Constraints.Required
        public int quantity;       //position_number
        @Constraints.Required
        public String expiry_date;  //zone

        public String if_on_sale;   //layer
        @Constraints.Required
        public String person_add;   //part
    }

    /**
     * 打印系统每隔一段时间传过来HTTP请求，更新数据库数据
     * */
    @Transactional
    public static Result upDateTimestamp() {  //更新库存数量
        response().setHeader("Connection","close");
        Date current_time = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timestamp_1 = sdf.format(current_time);
        DealTimeStamp last_time = DealTimeStamp.findById(1);
        Date last_access_time = last_time.last_access_time;
        String timestamp_2 = sdf.format(last_access_time);
        last_time.last_access_time = current_time;
        last_time.save();
        //更新最近一个时间段内的Meal_deal 产品
            String sql =
                    "select ps_deal_sale_table.id_product_shop, ps_deal_sale_table.key_word,sum(ps_order_detail.product_quantity)\n" +
                            "from ps_orders, ps_order_detail, ps_deal_sale_table\n" +
                            "where ps_orders.id_order = ps_order_detail.id_order\n" +
                            "and ps_orders.invoice_date >= " + "'" + timestamp_2+"'" + "\n" +
                            "and ps_orders.invoice_date < " + "'" + timestamp_1 + "'" + "\n" +
                            "and ps_orders.valid = 1\n" + //只寻找没有被删除的订单
                            "and ps_order_detail.product_id = ps_deal_sale_table.deal_product_id\n" +
                            "and  ps_order_detail.product_name like CONCAT('%', ps_deal_sale_table.key_word, '%')\n" +
                            "group by ps_deal_sale_table.id_product_shop";

            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_deal_sale_table.id_product_shop", "id_product_shop")
                            .columnMapping("ps_deal_sale_table.key_word", "key_word")
                            .columnMapping("sum(ps_order_detail.product_quantity)", "purchased_quantity")
                            .create();

            com.avaje.ebean.Query<MealDealProduct> query = Ebean.find(MealDealProduct.class);
            query.setRawSql(rawSql);
            List<MealDealProduct> list = query.findList();
            if(list.size() >0){
                for(MealDealProduct this_one: list){
                   int bought_quantity = this_one.purchased_quantity;
                    change_product_quantity(this_one.id_product_shop,bought_quantity,0);
                }
            }
            //更新最近一个时间段内的Buy One Get One 产品
            String sql_bogo =
                    "select ps_product_buyone_getone.id_onsale_product, ps_product_buyone_getone.id_real_product,sum(ps_order_detail.product_quantity), ps_product_buyone_getone.basic_unit, ps_product_buyone_getone.bundle_number\n" +
                            "from ps_orders, ps_order_detail, ps_product_buyone_getone\n" +
                            "where ps_orders.id_order = ps_order_detail.id_order\n" +
                            "and ps_orders.invoice_date >= " + "'" + timestamp_2+"'" + "\n" +
                            "and ps_orders.invoice_date < " + "'" + timestamp_1 + "'" + "\n" +
                            "and ps_orders.valid = 1\n" +
                            "and ps_order_detail.product_id = ps_product_buyone_getone.id_onsale_product\n" +
                            "and ps_product_buyone_getone.start_date < Now()\n" +
                            "and ps_product_buyone_getone.end_date > Now()\n" +
                            "group by ps_product_buyone_getone.id_onsale_product";

            RawSql rawSql_bogo =
                    RawSqlBuilder
                            .parse(sql_bogo)
                            .columnMapping("ps_product_buyone_getone.id_onsale_product", "id_onsale_product")
                            .columnMapping("ps_product_buyone_getone.id_real_product", "id_real_product")
                            .columnMapping("sum(ps_order_detail.product_quantity)", "bought_quantity_onsale_product")
                            .columnMapping("ps_product_buyone_getone.basic_unit", "basic_unit")
                            .columnMapping("ps_product_buyone_getone.bundle_number", "bundle_number")
                            .create();
            com.avaje.ebean.Query<BundleProduct> query_bogo = Ebean.find(BundleProduct.class);
            query_bogo.setRawSql(rawSql_bogo);
            List<BundleProduct> list_bogo = query_bogo.findList();
            if(list_bogo.size() >0){   //减去已经购买到的优惠产品数量
                for(BundleProduct this_one: list_bogo){
                    int real_bought_number = (this_one.bought_quantity_onsale_product/this_one.basic_unit)* this_one.bundle_number;
                    change_product_quantity(this_one.id_real_product,real_bought_number,0);
                }
            }
            return ok("Quantity updated");
        }
    }