/*
 * Copyright (c) 2017. Zhi Chen.
 * Application Controller is used for user to register and login,
 * through HTTP request. For convenience, Login process uses Play
 * Form feature, while Register process uses DynamicForm utility
 */

package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import com.avaje.ebean.SqlUpdate;
import com.avaje.ebean.annotation.Transactional;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.database.*;
import models.sqlContainer.ProductQuantity;
import models.template.CustomerReferral;
import play.cache.Cache;
import play.data.DynamicForm;
import play.data.Form;
import play.data.validation.Constraints.Email;
import play.data.validation.Constraints.Required;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import util.MD5;
import util.TimeStamp;
import util.http.SendHttp;
import util.sendEmail.InviteEmail;
import util.sendEmail.RenewTheLastVoucher;
import util.sendEmail.SendRewardEmail;
import views.html.getToken;
import views.html.index;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Random;

import static controllers.ProductAPI.reGenerateCache;
import static parsers.JsonParser.*;
import static util.http.HttpRequest.getReferralUrl;
import static util.sendEmail.EmailSender.sendEmailToCustomer;

public class Application extends Controller {
    /**
     * API Test page
     * Always valid to check if the api is working smoothly
     * @return customer info/ view_page for index
     * */
    public static Result index() throws InterruptedException {
        //check the existance of email address in the session
        response().setHeader("Connection","close");
        String email = session("email");
        Customer customer = Customer.findByEmail(email);
        if (email != null) {
            return customer==null? notFound() : ok(renderCustomer(customer));
        } else {
            return ok(index.render("please log in"));
        }
    }
    /**
     * Customer Registration class
     * Compulsory information at registration
     *@deprecated
     **/
    public static class Registration{
        public String company;
        @Required
        public String firstname;
        @Required
        public String lastname;
        @Email
        public String email;
        @Required
        public String passwd;
        public String birthday;
        public int newsletter;

    }

    /**
     * Mobile users register action
     * This API receives a registration form from HTTP request
     * */
    @Transactional
    public static Result postRegister() throws Exception {
        response().setHeader("Connection","close");
        DynamicForm customerForm = Form.form().bindFromRequest();
//     Form<Registration> customerForm = Form.form(Registration.class).bindFromRequest();
//     String model = customerForm.get("birthday");
        String a = request().remoteAddress();
        String email_address = customerForm.get("email");
        //Check if the email is already taken
        if (Customer.findByEmail(email_address)!= null){
            return status(409,"This email is already taken");
        }
        //if it is a new email, allow to register
        else{
            String firstName =   customerForm.get("firstname");
            if(firstName == null){
                firstName = "";
            }
            String lastName  =   customerForm.get("lastname");
            if(lastName == null){
                lastName = "";
            }

            String last_passwd_gen= TimeStamp.getDateTime();
            int num_newsletter;
            int genderNumber;
            String ip_registration_newsletter = customerForm.get("ip_registration_newsletter");

            if (ip_registration_newsletter != null && ip_registration_newsletter.equals("1")){
                ip_registration_newsletter = a;
            }else{
                ip_registration_newsletter = null;
            }

            if (customerForm.get("newsletter").equals("1")){
                num_newsletter = 1;
            }else{
                num_newsletter = 0;
            }

            if (customerForm.get("id_gender").equals("")){
                genderNumber=0;
            }else{
                genderNumber = Integer.parseInt(customerForm.get("id_gender"));
            }

            Date  newsletter_start = new Date();
            Date newsletter_date_add;

            if (num_newsletter==0){
                newsletter_date_add = null;
            }else{
                newsletter_date_add = newsletter_start;
            }
            String birthday_date;
            Date myDate;
            if (customerForm.get("birthday").equals("") || customerForm.get("birthday").equals("0")){
                myDate = new Date(0);
            }  //生日
            else{
                birthday_date = customerForm.get("birthday");
                try{
                    myDate=new Date(Long.valueOf(birthday_date));
                }catch(Exception e){
                    myDate=new Date(0);
                }
            }
            String company;
            try{
                company  = customerForm.get("company");
                if(company.contains("A2BLiving")){
                    company ="";
                }
            }catch(Exception e){
                company= "";
            }

            String reference_email;
            try{
                reference_email  = customerForm.get("recommendation_email"); //拿到推荐人的邮箱，发送邮件给用户
            }catch(Exception e){
                reference_email= "";
            }
            Customer customer = new Customer(
                    1,
                    1,
                    genderNumber,
                    3,
                    1,
                    0,
                    company,
                    null,
                    null,
                    firstName,
                    lastName,
                    email_address,
                    MD5.encrypt(customerForm.get("passwd")),
                    last_passwd_gen,
                    newsletter_date_add,
                    myDate,
                    num_newsletter,
                    ip_registration_newsletter,
                    0,
                    null,
                    0,
                    0,
                    0,
                    MD5.getSecureKey(),
                    null,
                    1,
                    0,
                    0,
                    newsletter_start,
                    newsletter_start);
            customer.save();
            System.out.println("successfully registered");
            Customer newCus = Customer.findByEmail(email_address);
            int id_customer = newCus.id_customer;
            CustomerGroup newRecord = new CustomerGroup(id_customer,3);    // id_group TODO later change it to 4
            newRecord.save();
            System.out.println("id_group generated");
            final String finalFirstName = firstName;
            final String final_reference_email = reference_email;  //推荐人邮件
            Runnable sendEmail = new Runnable() {
                @Override
                public void run() {
                    //给新用户发送Voucher
                    halfPriceVoucher(finalFirstName,email_address,id_customer);
                    String facebook_email = final_reference_email.replaceAll("@",".facebook@"); //加上facebook
                    Customer recommand_user = Customer.findByEmail(final_reference_email);
                    Customer recommand_user_facebook = Customer.findByEmail(facebook_email);
                    if(recommand_user!=null || recommand_user_facebook !=null ){   //如果是一个有效推荐人(已经存在，并且已经买过东西)， 否则没有用
                        int recommand_user_id = 0;
                        int total_order_history = 0;
                        // 推荐人存在三种情况： 1,a2bliving 用户； 2，facebook 用户； 3,两个都存在的用户
                        if(recommand_user != null && recommand_user_facebook == null){ //1,a2bliving 用户
                            recommand_user_id = recommand_user.id_customer;
                            String sql =
                                    "SELECT ps_orders.id_order \n" +
                                            "FROM ps_orders\n" +
                                            "where valid = 1\n" +
                                            "and id_customer = " + recommand_user_id;
                            RawSql rawSql =
                                    RawSqlBuilder
                                            .parse(sql)
                                            .columnMapping("ps_orders.id_order", "quantity")
                                            .create();
                            com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
                            query.setRawSql(rawSql);
                            List<ProductQuantity> list = query.findList();
                            total_order_history = list.size();
                        }else if(recommand_user == null && recommand_user_facebook != null) { //2,facebook 用户
                            recommand_user_id = recommand_user_facebook.id_customer;
                            String sql =
                                    "SELECT ps_orders.id_order \n" +
                                            "FROM ps_orders\n" +
                                            "where valid = 1\n" +
                                            "and id_customer = " + recommand_user_id;
                            RawSql rawSql =
                                    RawSqlBuilder
                                            .parse(sql)
                                            .columnMapping("ps_orders.id_order", "quantity")
                                            .create();
                            com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
                            query.setRawSql(rawSql);
                            List<ProductQuantity> list = query.findList();
                            total_order_history = list.size();
                        }else if(recommand_user != null && recommand_user_facebook != null) { //3,两个都存在的用户
                            recommand_user_id = recommand_user.id_customer; //以a2b账号为主
                            String sql =
                                    "SELECT ps_orders.id_order \n" +
                                            "FROM ps_orders\n" +
                                            "where valid = 1\n" +
                                            "and id_customer = " + recommand_user_id + " or id_customer = " + recommand_user_facebook.id_customer;
                            RawSql rawSql =
                                    RawSqlBuilder
                                            .parse(sql)
                                            .columnMapping("ps_orders.id_order", "quantity")
                                            .create();
                            com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
                            query.setRawSql(rawSql);
                            List<ProductQuantity> list = query.findList();
                            total_order_history = list.size();
                        }
//
                        if(total_order_history > 0) {  //如果推荐用户有过成功购买记录
                            ReferralProgram existing_record = ReferralProgram.findBySponsor(recommand_user_id, email_address);
                            if (existing_record != null) { //已经接受过这个用户邀请，现在来提交用户注册
                                Date today = new Date();
                                existing_record.id_customer = id_customer;
                                existing_record.date_upd = today;
                                existing_record.save();
                            } else {
                                //该推荐用户可能是线下邀请。然后现在来注册
                                //需要排除填写错推荐人的情况
                                ReferralProgram if_exist = ReferralProgram.findByEmail(email_address);
                                if (if_exist == null) {
                                    Date today = new Date();
                                    ReferralProgram new_referral = new ReferralProgram(recommand_user_id, email_address, "", finalFirstName, id_customer, 0, 0, today, today);
                                    new_referral.save();
                                }
                            }
                        }
                    }
                }
            };
            new Thread(sendEmail).start();
            return status(201,"registered");
        }
    }

    /**
     * Facebook account login/register+login, 登陆，或者注册然后直接登陆
     * This API receives a form from HTTP request, containing information about a customer from facebook
     * */
    @Transactional
    public static Result facebookLogin() throws Exception { //todo:提醒手机端传递正确key-value
        response().setHeader("Connection","close");
        DynamicForm customerForm = Form.form().bindFromRequest();
//     从手机端获取来自facebook的信息, 会获取一个facebook token
        String facebook_token;
        facebook_token = customerForm.get("fb_access_token");
        if(facebook_token == null){
            facebook_token = "sorry";
        }
        if(facebook_token.contains("sorry")){
            return badRequest("No Facebook Token");
        }else {
//     String model = customerForm.get("birthday");
        String a = request().remoteAddress();
        String email_address = customerForm.get("email");
        String facebook_email = email_address.replaceAll("@",".facebook@"); //发邮件的时候应该避免facebook关键字，否则发送不到用户真实邮箱
        //Check if the email is already taken
        Customer if_exist_facebook = Customer.findByEmail(facebook_email);
        Customer if_exist = Customer.findByEmail(email_address);
        if (if_exist_facebook != null){  //之前已经用facebook登陆过了
            //已经存在该用户，转换成为facebook 注册用户
            //// TODO: 2017/6/24 需要进行facebook 身份验证
            Customer customer = if_exist_facebook;
            Customer newOne = Customer.setNewCustomer(customer);
            System.out.println("Login successfully " + "Facebook User: "+ facebook_email);
            return  status(200,renderCustomer(newOne)); // 顺利用facebook登陆
        }else { //准备注册facebook 账户
                String reference_email;
                reference_email = customerForm.get("recommendation_email"); //拿到推荐人的邮箱，发送邮件给用户
                if(reference_email ==null){
                    reference_email = "";
                }
            if(if_exist == null && reference_email.equals("")){ //这个用户是新用户，并且还没有填写过推荐人， 返回请求，填写推荐人邮箱
                return status(202,"new user on A2B living");
            }else{
                //if it is a new email, allow to register
                String firstName = customerForm.get("firstname");
                if (firstName == null) {
                    firstName = "";
                }
                String lastName = customerForm.get("lastname");
                if (lastName == null) {
                    lastName = "";
                }
                String last_passwd_gen = TimeStamp.getDateTime();
                int num_newsletter;
                int genderNumber;
                String ip_registration_newsletter = customerForm.get("ip_registration_newsletter");
                if (ip_registration_newsletter != null && ip_registration_newsletter.equals("1")) {
                    ip_registration_newsletter = a;
                } else {
                    ip_registration_newsletter = null;
                }
                if (customerForm.get("newsletter").equals("1")) {
                    num_newsletter = 1;
                } else {
                    num_newsletter = 0;
                }
                if (customerForm.get("id_gender").equals("")) {
                    genderNumber = 0;
                } else {
                    genderNumber = Integer.parseInt(customerForm.get("id_gender"));
                }

                Date newsletter_start = new Date();
                Date newsletter_date_add;

                if (num_newsletter == 0) {
                    newsletter_date_add = null;
                } else {
                    newsletter_date_add = newsletter_start;
                }
                String birthday_date;
                Date myDate;
                if (customerForm.get("birthday").equals("") || customerForm.get("birthday").equals("0")) {
                    myDate = new Date(0);
                }  //生日
                else {
                    birthday_date = customerForm.get("birthday");
                    try {
                        myDate = new Date(Long.valueOf(birthday_date));
                    } catch (Exception e) {
                        myDate = new Date(0);
                    }
                }
                String company;
                try {
                    company = customerForm.get("company");
                    if (company.contains("A2BLiving")) {
                        company = "";
                    }
                } catch (Exception e) {
                    company = "";
                }
                Customer customer = new Customer(
                        1,
                        1,
                        genderNumber,
                        3,
                        1,
                        0,
                        company,
                        null,
                        null,
                        firstName,
                        lastName,
                        facebook_email,
                        MD5.encrypt("a2bliving"),
                        last_passwd_gen,
                        newsletter_date_add,
                        myDate,
                        num_newsletter,
                        ip_registration_newsletter,
                        0,
                        null,
                        0,
                        0,
                        0,
                        MD5.getSecureKey(),
                        null,
                        1,
                        0,
                        0,
                        newsletter_start,
                        newsletter_start);
                customer.save();
                System.out.println("successfully registered from Facebook");
                Customer newCus = Customer.findByEmail(facebook_email);
                int id_customer = newCus.id_customer;
                CustomerGroup newRecord = new CustomerGroup(id_customer, 3);    // id_group TODO later change it to 4
                newRecord.save();
                System.out.println("id_group generated");
                if(if_exist ==null){ //只有真正的a2bliving新用户才能收到voucher
                    final String finalFirstName = firstName;
                    final String final_reference_email = reference_email;  //推荐人邮件
                    Runnable sendEmail = new Runnable() {
                        @Override
                        public void run() {
                             //给真正的新用户发送email
                            halfPriceVoucher(finalFirstName,email_address,id_customer);
                            String facebook_email = final_reference_email.replaceAll("@",".facebook@"); //加上facebook
                            Customer recommand_user = Customer.findByEmail(final_reference_email);
                            Customer recommand_user_facebook = Customer.findByEmail(facebook_email);
                            if(recommand_user!=null || recommand_user_facebook !=null ){   //如果是一个有效推荐人(已经存在，并且已经买过东西)， 否则没有用
                                int recommand_user_id = 0;
                                int total_order_history = 0;
                                // 推荐人存在三种情况： 1,a2bliving 用户； 2，facebook 用户； 3,两个都存在的用户
                                if(recommand_user != null && recommand_user_facebook == null){ //1,a2bliving 用户
                                    recommand_user_id = recommand_user.id_customer;
                                    String sql =
                                            "SELECT ps_orders.id_order \n" +
                                                    "FROM ps_orders\n" +
                                                    "where valid = 1\n" +
                                                    "and id_customer = " + recommand_user_id;
                                    RawSql rawSql =
                                            RawSqlBuilder
                                                    .parse(sql)
                                                    .columnMapping("ps_orders.id_order", "quantity")
                                                    .create();
                                    com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
                                    query.setRawSql(rawSql);
                                    List<ProductQuantity> list = query.findList();
                                    total_order_history = list.size();
                                }else if(recommand_user == null && recommand_user_facebook != null) { //2,facebook 用户
                                    recommand_user_id = recommand_user_facebook.id_customer;
                                    String sql =
                                            "SELECT ps_orders.id_order \n" +
                                                    "FROM ps_orders\n" +
                                                    "where valid = 1\n" +
                                                    "and id_customer = " + recommand_user_id;
                                    RawSql rawSql =
                                            RawSqlBuilder
                                                    .parse(sql)
                                                    .columnMapping("ps_orders.id_order", "quantity")
                                                    .create();
                                    com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
                                    query.setRawSql(rawSql);
                                    List<ProductQuantity> list = query.findList();
                                    total_order_history = list.size();
                                }else if(recommand_user != null && recommand_user_facebook != null) { //3,两个都存在的用户
                                    recommand_user_id = recommand_user.id_customer; //以a2b账号为主
                                    String sql =
                                            "SELECT ps_orders.id_order \n" +
                                                    "FROM ps_orders\n" +
                                                    "where valid = 1\n" +
                                                    "and id_customer = " + recommand_user_id + " or id_customer = " + recommand_user_facebook.id_customer;
                                    RawSql rawSql =
                                            RawSqlBuilder
                                                    .parse(sql)
                                                    .columnMapping("ps_orders.id_order", "quantity")
                                                    .create();
                                    com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
                                    query.setRawSql(rawSql);
                                    List<ProductQuantity> list = query.findList();
                                    total_order_history = list.size();
                                }
                                if(total_order_history > 0) {  //如果推荐用户有过成功购买记录
                                    ReferralProgram existing_record = ReferralProgram.findBySponsor(recommand_user_id, email_address);
                                    if (existing_record != null) { //已经接受过这个用户邀请，现在来提交用户注册
                                        Date today = new Date();
                                        existing_record.id_customer = id_customer;
                                        existing_record.date_upd = today;
                                        existing_record.save();
                                    } else {
                                        //该推荐用户可能是线下邀请。然后现在来注册
                                        //需要排除填写错推荐人的情况
                                        ReferralProgram if_exist = ReferralProgram.findByEmail(email_address);
                                        if (if_exist == null) {
                                            Date today = new Date();
                                            ReferralProgram new_referral = new ReferralProgram(recommand_user_id, email_address, "", finalFirstName, id_customer, 0, 0, today, today);
                                            new_referral.save();
                                        }
                                    }
                                }
                            }
                        }
                    };
                    new Thread(sendEmail).start();
                }
                Customer new_customer = Customer.setNewCustomer(newCus);
                System.out.println("Register as a new" + "Facebook User: "+ facebook_email);
                return  status(200,renderCustomer(new_customer)); // 顺利用facebook登陆
                }
            }
        }
    }
    /**
     * Customer Login template information
     * @email
     * @passwd
     * */
    public static class Login {
        @Email
        public String email;
        @Required
        public String passwd;

        public String validate() throws Exception {
            if (Customer.authenticate(email, passwd) == null) {
                return "Invalid user or password";
            } else{
                return null; }
        }

    }

    /**
     * Mobile users login action
     * This API receives a Login Form information
     * */
    public static Result postLogin() throws Exception {
        response().setHeader("Connection","close");
        Form<Login> customerForm = Form.form(Login.class).bindFromRequest();
        if (customerForm.hasErrors()) {
            ///Play framework Form features, hasError() method will make the form valid
            System.out.println("failed login");
            return badRequest("Wrong user/password");
        } else {
            String email = customerForm.get().email;
            session().clear();
            session("email",email);
            Customer customer = Customer.findByEmail(email);
//            int customer_id = customer.id_customer;
//            String secure_key = customer.secure_key;
//            String combine =  "secure_key=" + secure_key + "&"+"id_customer=" + customer_id;
            // Better with Try Catch
            //update the last login time of a customer
//            new Thread(new ThreadController(combine)).start();
            //create a new object via Class method setNewCustomer, filter unnecessary information
            if (customer != null) {
                Customer newOne = Customer.setNewCustomer(customer);
                System.out.println("Login successfully " + "User: "+ email);
                return  customer==null? notFound() : ok(renderCustomer(newOne));

            }else {
                return notFound("Invalid user");
            }

        }
    }
    /**
     * Update the last visit time of a customer
     * @param email
     * @deprecated
     * */
    public static void updateDate(String email) {
        Customer new_one = Customer.findByEmail(email);
        if (new_one != null){
            new_one.date_upd = new Date();
            new_one.save();
        }
    }

    /**
     * Login with the param of email
     * @param email
     * @deprecated
     * */
    @Transactional
    public static Result newLogin(String email){
        response().setHeader("Connection","close");
        DynamicForm user_info = Form.form().bindFromRequest();
        String customer_email = user_info.get("email");
        String password = user_info.get("passwd");
        if(email.equals(customer_email)){
            if (Customer.authenticate(customer_email, password) == null){
                return badRequest("Wrong user/password");
            }else{
                session().clear();
                session("email",customer_email);
                int customer_id = Customer.getID(email);
                Customer customer = Customer.findById(customer_id);
                customer.date_upd =new Date();
                customer.save();
                Customer newOne = Customer.setNewCustomer(customer);
                return  customer==null? notFound() : ok(renderCustomer(newOne));
            }
        }
        else{
            return badRequest("Wrong network request");
        }
    }

    public static Result clearCache() {
        response().setHeader("Connection","close");
        //clear and regenerate
        for(int i = 1; i <550; i++){
            reGenerateCache(i);
            System.out.println("Cache cleared------------------"+ i);
        }
        return ok("cache cleared and regenerated");
    }

    public static Result justClearCache() {
        response().setHeader("Connection","close");
        //clear and regenerate
        for(int i = 1; i <1000; i++){  //种类上线扩展到1000
            Cache.remove("allProductsOfCategory" + i);
            Cache.remove("allProductsOfCategoryV2" + i);
            System.out.println("Cache cleared------------------"+ i);
        }
        return ok("cache cleared");
    }

    public static Result singleClearCache(int category_id) {
        response().setHeader("Connection","close");
//        Cache.remove("allProductsOfCategory" + category_id);
        reGenerateCache(category_id);
//        System.out.println("Cache generated!!!!!!");
        return ok("cache cleared under this category");
    }


    /**
     * 获取device_token 的同时，检验购物车产品数量
     * */
    public static Result saveDeviceToken() {
        response().setHeader("Connection","close");
        DynamicForm user_info = Form.form().bindFromRequest();
        //从网络请求里面提取出 购买产品的信息
        String id_customer = user_info.get("id_customer");
        Integer customer_id;
        try{
            customer_id = Integer.valueOf(id_customer);
        }catch (Exception e){
            return ok("this cannot be added");
        }
        String device_token = user_info.get("device_token");
        if(device_token ==null){
            device_token = "";
        }
        String device_type = user_info.get("device_type");
        if(device_type == null){
            device_type = "";
        }
        Date last_access = new Date();
        //是否已经存在
        CustomerDeviceToken customerDeviceToken =CustomerDeviceToken.findByToken(customer_id,device_token);
        if(customerDeviceToken!=null){  //已经存在
            customerDeviceToken.last_access_time = last_access;
            customerDeviceToken.save();
        }else {
            CustomerDeviceToken new_one = new CustomerDeviceToken(
                    customer_id,
                    device_token,
                    device_type,
                    last_access
            );
            new_one.save();
        }
        return ok("Device token added");
    }

    //存储退订的手机用户
    public static Result addNewUnsubscribe(String phone) {
        response().setHeader("Connection","close");
        String phone_number = "";
        if (phone != null){
            phone_number = phone;
        }
        CustomerUnsubscribeMobile this_one = CustomerUnsubscribeMobile.findByPhone(phone_number);
        if(this_one == null){
            CustomerUnsubscribeMobile new_record = new CustomerUnsubscribeMobile(phone_number);
            new_record.save();
            System.out.println("Added a new mobile number to unsubscribe");
        }
        return ok("new record added");
    }

    //打开邀请朋友界面，查看是否符合作为邀请者的条件， 至少成功购买一单
    public static Result tryToInvite(int id_customer) {
        response().setHeader("Connection","close");
        try {
            String sql =
                    "SELECT ps_orders.id_order \n" +
                            "FROM ps_orders\n" +
                            "where valid = 1\n" +
                            "and id_customer = " + id_customer;
            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_orders.id_order", "quantity")
                            .create();
            com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
            query.setRawSql(rawSql);
            List<ProductQuantity> list = query.findList();
            if(list.size() > 0 ){
                return ok("qualified user");
            }else {
                return notFound("To become a sponsor, you need to have completed at least 1 order.");
            }
        }catch (Exception e){
            return badRequest("Invalid request");
        }
    }


    //邀请朋友，发送email 给未注册过的用户
    public static Result inviteFriends() {
        response().setHeader("Connection","close");
        try {
            CustomerReferral customerReferral = renderRequestCustomerReferral(request().body().asJson().toString());
            //得到用户提交的推荐新用户表格
            String new_user_email = customerReferral.newUser_email;
            ReferralProgram this_one = ReferralProgram.findByEmail(new_user_email);
            Customer if_exist = Customer.findByEmail(new_user_email);
            if(this_one != null || if_exist !=null){
                return badRequest("This user with email:" + new_user_email+" was already sponsored.");
            }else {
                Date today = new Date();
                //生成新的纪录，referralProgram 表格里，所有关于用户的记录都为0
                ReferralProgram new_one = new ReferralProgram(
                        customerReferral.oldUser_id,
                        customerReferral.newUser_email,
                        "",
                        customerReferral.newUser_firstName,
                        0,
                        0,
                        0,
                        today,
                        today);
                new_one.save();
                int id_referral_program = new_one.id_referralprogram;
                String plaintext = id_referral_program + "|" + customerReferral.newUser_email + "|";
                String encoded_plaintext = getReferralUrl(plaintext);
                String final_url = "https://www.a2bliving.ie/cork/index.php?controller=authentication&create_account=1&sponsor=" +
                        encoded_plaintext +
                        "&back=my-account";
                //准备发送给新用户邮箱
                String old_user_email = customerReferral.oldUser_email;
                if(old_user_email.contains("facebook")){
                    customerReferral.oldUser_email  = old_user_email.replaceAll(".facebook@","@"); //把目标邮箱的facebook替换掉
                }
                InviteEmail.inviteNewUser(customerReferral,final_url);
                return ok("Email successfully sent");
            }
        }catch (Exception e){
            return badRequest("Invalid request");
        }
    }

    public static Result testEncoded() {
        response().setHeader("Connection","close");
        String final_result = getReferralUrl("7|zchen.a2bliving@gmail.com|");
        return ok(final_result);
    }

    //查看已经发送请求却还没回应的用户
    public static Result checkPendingUsers(int id_customer, String secure_key) {
        response().setHeader("Connection","close");
        try{
            Customer if_exist = Customer.findByAuthentication(id_customer,secure_key);
           if(if_exist!=null){ //身份验证生效
               List<ReferralProgram> pending_users = ReferralProgram.getPendingUsers(id_customer);
               return ok(renderCategories(pending_users));
           }else{
               return notFound("Invalid user request");
           }
        }catch (Exception e){
            return badRequest("Un-authenticated user");
        }
    }

    //查看所有推荐过的用户
    public static Result checkAllSponsored(int id_customer, String secure_key) {
        response().setHeader("Connection","close");
        try{
            Customer if_exist = Customer.findByAuthentication(id_customer,secure_key);
            if(if_exist!=null){ //身份验证生效
                List<ReferralProgram> sponsored_users = ReferralProgram.getSponsoredUsers(id_customer);
                return ok(renderCategories(sponsored_users));
            }else{
                return notFound("Invalid user request");
            }
        }catch (Exception e){
            return badRequest("Un-authenticated user");
        }
    }

    //重新发送email 给未注册过的用户
    public static Result reSendEmail() {
        response().setHeader("Connection","close");
        CustomerReferral customerReferral = renderRequestCustomerReferral(request().body().asJson().toString());
        try {
            //得到用户提交的推荐新用户表格
            String new_user_email = customerReferral.newUser_email;
            String old_user_email = customerReferral.oldUser_email;
            ReferralProgram this_one = ReferralProgram.findByEmail(new_user_email);
            Customer if_exist = Customer.findByEmail(old_user_email);
            if(this_one == null || if_exist ==null){
                return notFound("Not a valid user");
            }else {
                int id_referral_program = this_one.id_referralprogram;
                String plaintext = id_referral_program + "|" + customerReferral.newUser_email + "|";
                String encoded_plaintext = getReferralUrl(plaintext);
                String final_url = "https://www.a2bliving.ie/cork/index.php?controller=authentication&create_account=1&sponsor=" +
                        encoded_plaintext +
                        "&back=my-account";
                //准备发送给新用户邮箱
                InviteEmail.inviteNewUser(customerReferral,final_url);
                return ok("Email successfully sent");
            }
        }catch (Exception e){
            return badRequest("Invalid request");
        }
    }

    public static String getRandomVoucher() {
        StringBuffer buffer = new StringBuffer("0123456789abcdefghijklmnopqrstuvwxyz");
        StringBuffer reference = new StringBuffer();
        Random random = new Random();
        int range = buffer.length();
        for (int i = 0; i < 9; i++) {
            reference.append(buffer.charAt(random.nextInt(range)));
        }
        return reference.toString();
    }

    //生成一万个Voucher, 价值5欧，没有最低消费，只限food产品，不绑定用户
    //制作12 个司机voucher
    @Transactional
    public static Result generateVoucher() {
        response().setHeader("Connection","close");
        for(int i =0; i<12; i++) {
            String voucher_code;
            do {
                voucher_code = getRandomVoucher();
            } while (CartRule.findByVoucher(voucher_code) != null);
            Date today = new Date();
            long nowAdd6Months = System.currentTimeMillis() + Long.parseLong("518400000");
            Date after_6_month = new Date(nowAdd6Months);
            //ps_cart_rule
            CartRule new_cart_rule = new CartRule(0, today, after_6_month, "Good Driver Deserves Nice Rewards", 1, 1, 1, 1, voucher_code, BigDecimal.valueOf(0.00),
                    1, 1, 0, 0, 0, 0, 1, 1, 0, 0, BigDecimal.valueOf(0.00), BigDecimal.valueOf(15.00), 1, 1, 0, 0, 0, 0, 1, today, today); //// TODO: 2017/6/29
            new_cart_rule.save();
            int id_cart_rule = new_cart_rule.id_cart_rule;
            //ps_cart_rule_lang
            CartRuleLang new_cart_lang = new CartRuleLang(id_cart_rule, 1, "Driver Credits");
            new_cart_lang.save();
            //ps_cart_rule_product_rule_group
            CartRuleProductRuleGroup new_cart_rule_product_rule_group = new CartRuleProductRuleGroup(id_cart_rule, 1);
            new_cart_rule_product_rule_group.save();
            int id_product_rule_group = new_cart_rule_product_rule_group.id_product_rule_group;
            //ps_cart_rule_product_rule
            CartRuleProductRule new_cart_rule_product_rule = new CartRuleProductRule(id_product_rule_group, "manufacturers");
            new_cart_rule_product_rule.save();
            int id_product_rule = new_cart_rule_product_rule.id_product_rule;
            //ps_cart_rule_product_rule_value
            CartRuleProductRuleValue new_cart_rule_product_rule_value = new CartRuleProductRuleValue(id_product_rule, 2);
            new_cart_rule_product_rule_value.save();
            System.out.println("New Voucher Generated-----------------------"+ (i+1));
        }
        return ok("Driver Credits created");
    }


    //生成Voucher,50%折扣，没有最低消费，只限food产品，id_manufacturer,不绑定用户
    @Transactional
    public static Result generatePercentVoucher() {
        response().setHeader("Connection","close");
        String voucher_code;
        do{
            voucher_code = getRandomVoucher();
        }while (CartRule.findByVoucher(voucher_code) != null);
        Date today = new Date();
        long nowAdd6Months = System.currentTimeMillis() + Long.parseLong("15768000000");
        Date after_6_month = new Date(nowAdd6Months);
        //ps_cart_rule
        CartRule new_cart_rule = new CartRule(0,today,after_6_month,"",1,1,1,0,voucher_code, BigDecimal.valueOf(0.00),
                1,1,0,0,0,0,1,1,0,0,BigDecimal.valueOf(50.00), BigDecimal.valueOf(0.00),0,1,-2,0,0,0,1,today,today);
        new_cart_rule.save();
        int id_cart_rule = new_cart_rule.id_cart_rule;
        //ps_cart_rule_lang
        CartRuleLang new_cart_lang = new CartRuleLang(id_cart_rule,1,"50% Off");
        new_cart_lang.save();
        //ps_cart_rule_product_rule_group
        CartRuleProductRuleGroup new_cart_rule_product_rule_group = new CartRuleProductRuleGroup(id_cart_rule,1);
        new_cart_rule_product_rule_group.save();
        int id_product_rule_group = new_cart_rule_product_rule_group.id_product_rule_group;
        //ps_cart_rule_product_rule
        CartRuleProductRule new_cart_rule_product_rule = new CartRuleProductRule(id_product_rule_group,"manufacturers");
        new_cart_rule_product_rule.save();
        int id_product_rule = new_cart_rule_product_rule.id_product_rule;
        //ps_cart_rule_product_rule_value
        CartRuleProductRuleValue new_cart_rule_product_rule_value = new CartRuleProductRuleValue(id_product_rule,2);
        new_cart_rule_product_rule_value.save();
        return ok("50% Off Voucher Generated");
    }

    //50% Off 折扣券，分配给某个用户
    /**
     * id_customer
     * */
    public static synchronized void halfPriceVoucher(String firstName, String emailAddress,int id_customer)  {
        String voucher_code;
        do{
            voucher_code = getRandomVoucher();
        }while (CartRule.findByVoucher(voucher_code) != null);
        Date today = new Date();
        long nowAdd6Months = System.currentTimeMillis() + Long.parseLong("15768000000");
        long yesterday = System.currentTimeMillis() - Long.parseLong("86400000");
        Date after_6_month = new Date(nowAdd6Months);
        Date one_day_before = new Date(yesterday);
        //ps_cart_rule
        CartRule new_cart_rule = new CartRule(id_customer,one_day_before,after_6_month,"",1,1,1,0,voucher_code, BigDecimal.valueOf(0.00),
                1,1,0,0,0,0,1,1,0,0,BigDecimal.valueOf(50.00), BigDecimal.valueOf(0.00),0,1,-2,0,0,0,1,today,today);
        new_cart_rule.save();
        int id_cart_rule = new_cart_rule.id_cart_rule;
        //ps_cart_rule_lang
        CartRuleLang new_cart_lang = new CartRuleLang(id_cart_rule,1,"New Customer 50% Off");
        new_cart_lang.save();
        //ps_cart_rule_product_rule_group
        CartRuleProductRuleGroup new_cart_rule_product_rule_group = new CartRuleProductRuleGroup(id_cart_rule,1);
        new_cart_rule_product_rule_group.save();
        int id_product_rule_group = new_cart_rule_product_rule_group.id_product_rule_group;
        //ps_cart_rule_product_rule
        CartRuleProductRule new_cart_rule_product_rule = new CartRuleProductRule(id_product_rule_group,"manufacturers");
        new_cart_rule_product_rule.save();
        int id_product_rule = new_cart_rule_product_rule.id_product_rule;
        //ps_cart_rule_product_rule_value
        CartRuleProductRuleValue new_cart_rule_product_rule_value = new CartRuleProductRuleValue(id_product_rule,2);
        new_cart_rule_product_rule_value.save();
        sendEmailToCustomer(firstName,emailAddress,voucher_code);
        System.out.println("50% Off Voucher Generated");
    }

    //学生8折优惠券
    @Transactional
    public static synchronized void voucherForStudent(String voucher) {
        String voucher_code = voucher;
        Date today = new Date();
        long nowAdd6Months = System.currentTimeMillis() + Long.parseLong("27734400000"); //截至时间为2018年7月31日
        Date after_6_month = new Date(nowAdd6Months);
        //ps_cart_rule
        CartRule new_cart_rule = new CartRule(0,today,after_6_month,"",999,999,1,0,voucher_code, BigDecimal.valueOf(0.00),
                1,1,0,0,0,0,1,1,0,0,BigDecimal.valueOf(20.00), BigDecimal.valueOf(0.00),0,1,-2,0,0,0,1,today,today);
        new_cart_rule.save();
        int id_cart_rule = new_cart_rule.id_cart_rule;
        //ps_cart_rule_lang
        CartRuleLang new_cart_lang = new CartRuleLang(id_cart_rule,1,"Student Discount");
        new_cart_lang.save();
        //ps_cart_rule_product_rule_group
        CartRuleProductRuleGroup new_cart_rule_product_rule_group = new CartRuleProductRuleGroup(id_cart_rule,1);
        new_cart_rule_product_rule_group.save();
        int id_product_rule_group = new_cart_rule_product_rule_group.id_product_rule_group;
        //ps_cart_rule_product_rule
        CartRuleProductRule new_cart_rule_product_rule = new CartRuleProductRule(id_product_rule_group,"manufacturers");
        new_cart_rule_product_rule.save();
        int id_product_rule = new_cart_rule_product_rule.id_product_rule;
        //ps_cart_rule_product_rule_value
        CartRuleProductRuleValue new_cart_rule_product_rule_value = new CartRuleProductRuleValue(id_product_rule,2);
        new_cart_rule_product_rule_value.save();
        System.out.println("Voucher Code: " + voucher_code + " has been generated");
    }

    //打印系统传过来请求，普通用户点单，给用户发送 下一次 5欧元 奖励voucher， 不绑定用户，限定shop 产品, 不能跟其他合用
    public static Result sendRewardToUser(int id_order, int id_customer){
        response().setHeader("Connection","close");
        try {
            //如果是电话点单，id_customer = 149 或者 102， 不产生 voucher, 也不发送邮件
            if(id_customer != 149 && id_customer != 102) {
                Customer this_customer = Customer.findById(id_customer);
                OrderReward if_exist = OrderReward.findById(id_order); //是否已经发送过奖励
                if (this_customer != null && if_exist == null) {   //存在用户，并且这以订单之前没有送过奖励
                    String first_name = this_customer.firstname;
                    String last_name = this_customer.lastname;
                    String email = this_customer.email;
                    if(email.contains("facebook")){
                        email = email.replaceAll(".facebook@","@"); //把目标邮箱的facebook替换掉
                    }
                    CustomerReferral new_customer_referral = new CustomerReferral(0, "",
                            "", "", first_name, last_name, email);
                    String voucher_code;
                    do {
                        voucher_code = getRandomVoucher();
                    } while (CartRule.findByVoucher(voucher_code) != null);
                    Date today = new Date();
                    long nowAdd6Months = System.currentTimeMillis() + Long.parseLong("15768000000");
                    Date after_6_month = new Date(nowAdd6Months);
                    //ps_cart_rule
                    CartRule new_cart_rule = new CartRule(id_customer, today, after_6_month, "", 1, 1, 1, 0, voucher_code, BigDecimal.valueOf(0.00),
                            1, 1, 0, 0, 0, 0, 1, 1, 0, 0, BigDecimal.valueOf(0.00), BigDecimal.valueOf(5.00), 1, 1, 0, 0, 0, 0, 1, today, today); //// TODO: 2017/6/29
                    new_cart_rule.save();
                    int id_cart_rule = new_cart_rule.id_cart_rule;
                    //ps_cart_rule_lang
                    CartRuleLang new_cart_lang = new CartRuleLang(id_cart_rule, 1, "5 Euro Off");
                    new_cart_lang.save();
                    //ps_cart_rule_product_rule_group
                    CartRuleProductRuleGroup new_cart_rule_product_rule_group = new CartRuleProductRuleGroup(id_cart_rule, 1);
                    new_cart_rule_product_rule_group.save();
                    int id_product_rule_group = new_cart_rule_product_rule_group.id_product_rule_group;
                    //ps_cart_rule_product_rule
                    CartRuleProductRule new_cart_rule_product_rule = new CartRuleProductRule(id_product_rule_group, "manufacturers");
                    new_cart_rule_product_rule.save();
                    int id_product_rule = new_cart_rule_product_rule.id_product_rule;
                    //ps_cart_rule_product_rule_value
                    CartRuleProductRuleValue new_cart_rule_product_rule_value = new CartRuleProductRuleValue(id_product_rule, 2);
                    new_cart_rule_product_rule_value.save();
                    SendRewardEmail.sendRewardToUser(new_customer_referral, voucher_code); //发送email

                    OrderReward new_record = new OrderReward(id_order, id_cart_rule,"5 Euro Off",today);
                    new_record.save(); //记录新的奖励记录, 包括 voucher 信息，订单信息， 添加时间
                }
            }
        }catch (Exception e){
            return badRequest("Invalid request");
        }
        return ok("Reward Email has been sent");
    }

    //为旧的voucher 产生新的voucher, 并且发邮件告诉用户  todo: 生成新voucher (继续带有Partial use 属性)
    public synchronized static void sendRenewalVoucher(int id_customer, BigDecimal discount_amount_left, String last_voucher_name, BigDecimal last_voucher_value, BigDecimal last_voucher_spent){  //给用户发送新的voucher，来续用之前的voucher
        try {
            //如果是电话点单，id_customer = 149 或者 102， 不产生 voucher, 也不发送邮件
            if(id_customer != 149 && id_customer != 102) {
                Customer this_customer = Customer.findById(id_customer);
                if (this_customer != null) {//存在用户
                    String first_name = this_customer.firstname;
                    String last_name = this_customer.lastname;
                    String email = this_customer.email;
                    if(email.contains("facebook")){
                        email = email.replaceAll(".facebook@","@"); //把目标邮箱的facebook替换掉
                    }
                    CustomerReferral new_customer_referral = new CustomerReferral(0, "",
                            "", "", first_name, last_name, email);
                    String voucher_code;
                    do {
                        voucher_code = getRandomVoucher();
                    } while (CartRule.findByVoucher(voucher_code) != null);
                    Date today = new Date();
                    long nowAdd6Months = System.currentTimeMillis() + Long.parseLong("15768000000");
                    Date after_6_month = new Date(nowAdd6Months);
                    //ps_cart_rule
                    CartRule new_cart_rule = new CartRule(id_customer, today, after_6_month, "", 1, 1, 1, 1, voucher_code, BigDecimal.valueOf(0.00),
                            1, 1, 0, 0, 0, 0, 1, 1, 0, 0, BigDecimal.valueOf(0.00), discount_amount_left, 1, 1, 0, 0, 0, 0, 1, today, today); //// TODO: 2017/6/29
                    new_cart_rule.save();
                    int id_cart_rule = new_cart_rule.id_cart_rule;
                    //ps_cart_rule_lang
                    CartRuleLang new_cart_lang = new CartRuleLang(id_cart_rule, 1, "Renewal Voucher");
                    new_cart_lang.save();
                    //ps_cart_rule_product_rule_group
                    CartRuleProductRuleGroup new_cart_rule_product_rule_group = new CartRuleProductRuleGroup(id_cart_rule, 1);
                    new_cart_rule_product_rule_group.save();
                    int id_product_rule_group = new_cart_rule_product_rule_group.id_product_rule_group;
                    //ps_cart_rule_product_rule
                    CartRuleProductRule new_cart_rule_product_rule = new CartRuleProductRule(id_product_rule_group, "manufacturers");
                    new_cart_rule_product_rule.save();
                    int id_product_rule = new_cart_rule_product_rule.id_product_rule;
                    //ps_cart_rule_product_rule_value
                    CartRuleProductRuleValue new_cart_rule_product_rule_value = new CartRuleProductRuleValue(id_product_rule, 2);
                    new_cart_rule_product_rule_value.save();
                    RenewTheLastVoucher.sendRenewalVoucherToUser(new_customer_referral,last_voucher_name, last_voucher_value, last_voucher_spent, discount_amount_left,voucher_code); //发送email, 包含内容： 上一单用了一个价值为xxx 的 voucher, 已经用掉xx元， 还剩xx元，新voucher名字叫做xxx
                }
            }
        }catch (Exception e){
            System.out.println("Invalid request");
        }
        System.out.println("Renewal Voucher Email has been sent");
    }

    public static Result seeAllOrderReward(){ //给推荐人发送50% off 的折扣券， 绑定用户，只运用给food产品
        response().setHeader("Connection","close");
        List<OrderReward> all_record = OrderReward.findAll();
        return ok(renderCategories(all_record));
    }

    public static Result optionNotificationPage() {  //接受OPTIONS 请求
//        Http.RequestBody body = request().body();
//        System.out.println(body);
        response().setHeader(CONTENT_TYPE, "application/json");//允许数值传递
        response().setHeader(ACCESS_CONTROL_ALLOW_ORIGIN, "*");//允许跨域访问， 返回服务器域名, * 表示所有域
        response().setHeader(ACCESS_CONTROL_ALLOW_HEADERS, "Content-Type");//允许跨域访问
        response().setHeader("Connection","close");
        System.out.println("Get a receive from OPTIONS request!");
        return ok(getToken.render("Welcome aboard"));//登陆界面
    }

    public static Result receiveNotification() {  //来自网页服务器的POST请求
        response().setHeader("Connection","close");
        String body = request().body().asJson().toString();
        System.out.println(body);
        ObjectNode data_content = Json.newObject();
        try {
            data_content = new ObjectMapper().readValue(body, ObjectNode.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //第一步解析
        String object_subscription = data_content.findValue("subscription").toString();
        //第二步解析
        try {
            data_content = new ObjectMapper().readValue(object_subscription, ObjectNode.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //第三步解析
        String end_point = data_content.findValue("endpoint").textValue();
        String keys_object = data_content.findValue("keys").toString();
        try {
            data_content = new ObjectMapper().readValue(keys_object, ObjectNode.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //第四步解析
        String p256dh = data_content.findValue("p256dh").textValue();
        String auth = data_content.findValue("auth").textValue();
        //存入数据库
        //先判断是否存在
        CustomerBrowserToken customerBrowserToken = CustomerBrowserToken.findByEndpoint(end_point);
        if(customerBrowserToken == null){ //只有空值的情况下才储存
            String sql = "INSERT INTO ps_customer_browser_token (endpoint, p256dh, auth) " +
                    "VALUES (:endpoint, :p256dh, :auth)";
            SqlUpdate insert = Ebean.createSqlUpdate(sql);
            insert.setParameter("endpoint",end_point);
            insert.setParameter("p256dh",p256dh);
            insert.setParameter("auth",auth);
            insert.execute();
            System.out.println("New record saved in the database");
        }
        response().setHeader(CONTENT_TYPE, "application/json");//允许数值传递
        response().setHeader(ACCESS_CONTROL_ALLOW_ORIGIN, "*");//允许跨域访问， 返回服务器域名，* 表示所有域
        response().setHeader(ACCESS_CONTROL_ALLOW_HEADERS, "Content-Type");//允许跨域访问
        System.out.println("Get a receive! from POST request");
        System.out.println("Finally SUCCESSFUL!!!!");
        return ok("congratulations");//登陆界面
    }

    public static Result notificationPage() {  //Play 服务器接到POST 请求,给浏览器发送通知
        response().setHeader("Connection","close");
        String body = request().body().asJson().toString();
        if(body !=null){
            ObjectNode data_content = Json.newObject();
            try {
                data_content = new ObjectMapper().readValue(body, ObjectNode.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
            String data = data_content.findValue("data").textValue();
            String access_token = data_content.findValue("access_token").textValue();
            if(null!= access_token && null != data){ //关键内容都不为空
                String original_token = AccessToken.findById(1);
                if(original_token ==null){
                    original_token = "";
                }else {
                    original_token = original_token.toString();
                }
                    if(original_token.equals(access_token)){
                        //准备多线程操作
                        Runnable runnable = new Runnable() {
                            @Override
                            public void run() {
                                SendHttp.browserNotification(data);  //传递给前端页面
                            }
                        };
                        Thread thread = new Thread(runnable);
                        thread.start();  //for testing later
                        System.out.println(data);
                        return ok("sending http request to Users");
                    }else{
                        System.out.println("Not Valid token");
                        return notFound("Not Valid token");
                    }
            }else {
                System.out.println("Important message missing");
                return badRequest("Remember to send your token together");
            }
        }else {
            System.out.println("Not Valid contents");
            return badRequest("Not valid contents");
        }
    }
}
