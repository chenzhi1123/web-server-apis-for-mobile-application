/*
 * Copyright (c) 2017. Zhi Chen.
 * AddressAPI is used for actions related to user's addresses
 * CRUD to the database
 */

package controllers;

import models.database.Address;
import models.database.Customer;
import models.database.Order;
import models.database.OrderInvoice;
import models.template.AddressTemplate;
import play.data.DynamicForm;
import play.data.Form;
import play.db.ebean.Transactional;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.Date;
import java.util.List;

import static models.database.Address.findById;
import static parsers.JsonParser.renderAddressTemplate;
import static parsers.JsonParser.renderCategories;


public class AddressAPI extends Controller{

    /**
     * Get all the addresses of one user
     * @param id
     * */
    public static Result getAddresses(long id) {
        response().setHeader("Connection","close");
       List<Address> all_address = Address.findCustomerAddress(id);
       return ok(renderCategories(all_address));
    }

    public static Result getAuthenticatedAddresses(long id, String secure_key) {
        response().setHeader("Connection","close");
        try{
            int id_customer = (int) id;
            Customer if_exist = Customer.findByAuthentication(id_customer,secure_key);
            if(if_exist!=null){ //身份验证生效
                List<Address> all_address = Address.findCustomerAddress(id);
                return ok(renderCategories(all_address));
            }else{
                return notFound("Invalid user request");
            }

        }catch (Exception e){
            return badRequest("Un-authenticated user");
        }

    }

    /**
     * Get a specific address in the database
     * @param id
     * @return address
     * */
    public static Result getOneAddress(int id) {
        response().setHeader("Connection","close");
       Address address = findById(id);
        return ok(renderCategories(address));
    }

    /**
     * Android mobile add an address for one customer
     * @param customer_id
     * @return 409 or 201
     * */
    public static Result addAddress(int customer_id) throws Exception {
        response().setHeader("Connection","close");
        String request = request().body().asJson().toString();
        AddressTemplate addressTemplate = renderAddressTemplate(request);

        String alias = addressTemplate.alias;
        if (alias == null){
            alias = "";
        }

        String phone = addressTemplate.phone;
        if (phone == null){
            phone = "";
        }

        if (alias.length() >30){
            return status(404,"Alias too long");
        }else if (Address.findByAlias(customer_id,alias)!= null){
            return status(409,"this alias is already taken");
        }else if (addressTemplate.company.length() > 60){
            return status(404,"Company name too long");
        }else if (addressTemplate.firstname.length() > 32 || addressTemplate.lastname.length() > 32){
            return status(404,"Name too long");
        }else if (addressTemplate.address1.length() > 128 || addressTemplate.address2.length() > 128){
            return status(404,"Address too long");
        }else if (addressTemplate.city.length() > 64){
            return status(404,"City too long");
        }else if (addressTemplate.phone.length() > 32){
            return status(404,"Phone too long");
        }else{
            String company = addressTemplate.company;
            String first_name =  addressTemplate.firstname;
            String last_name = addressTemplate.lastname;
            String address1 = addressTemplate.address1;
            String address2 = addressTemplate.address2;
//            String city  =  addressTemplate.city;
            if(company == null){
                company = "";
            }else if (company.contains("A2BLiving")){
                company = "";
            }
            if(first_name ==null){
                first_name = "";
            }
            if (last_name ==null){
                last_name = "";
            }
            if (address1 ==null){
                address1 = "";
            }
            if (address2 == null){
                address2 = "";
            }
//            if(city == null){
//                city = "";
//            }
            Date  date_add = new Date();
            Address address = new Address(
                    26,
                    0,
                    customer_id,
                    0,
                    0,
                    0,
                    alias,
                    company,
                    first_name,
                    last_name,
                    address1,
                    address2,
                    "",
                    "Cork",
                    "",
                    phone,
                    "",
                    "",
                    "",
                    date_add,
                    date_add,
                    1,
                    0);
            address.save();
            System.out.println("new address added");
            System.out.println(alias);
            System.out.println(addressTemplate.alias.getClass().getSimpleName());
            return status(201,"new address added");
        }
    }
    //更改一个订单的 地址， 添加临时新地址
    @Transactional
    public static Result changeAddress(int order_id) throws Exception {
        response().setHeader("Connection","close");
        String request = request().body().asJson().toString(); //body
        AddressTemplate addressTemplate = renderAddressTemplate(request);
        Order this_order = Order.findById(order_id);
        if (this_order != null) {
            int customer_id = this_order.id_customer;
            String alias = addressTemplate.alias;
            if (alias == null) {
                alias = "";
            }

            String phone = addressTemplate.phone;
            if (phone == null) {
                phone = "";
            }

            if (alias.length() > 30) {
                return status(404, "Alias too long");
            } else if (Address.findByAlias(customer_id, alias) != null) {
                return status(409, "this alias is already taken");
            } else {
                String company = addressTemplate.company;
                String first_name = addressTemplate.firstname;
                String last_name = addressTemplate.lastname;
                String address1 = addressTemplate.address1;
                String address2 = addressTemplate.address2;
//            String city  =  addressTemplate.city;
                if (company == null) {
                    company = "";
                } else if (company.contains("A2BLiving")) {
                    company = "";
                }
                if (first_name == null) {
                    first_name = "";
                }
                if (last_name == null) {
                    last_name = "";
                }
                if (address1 == null) {
                    address1 = "";
                }
                if (address2 == null) {
                    address2 = "";
                }
//            if(city == null){
//                city = "";
//            }
                Date date_add = new Date();
                Address address = new Address(
                        26,
                        0,
                        customer_id,
                        0,
                        0,
                        0,
                        alias,
                        company,
                        first_name,
                        last_name,
                        address1,
                        address2,
                        "",
                        "Cork",
                        "",
                        phone,
                        "",
                        "",
                        "",
                        date_add,
                        date_add,
                        1,
                        0);
                address.save();
                int address_id = address.id_address;
                this_order.id_address_delivery = address_id;
                this_order.id_address_invoice = address_id;
                this_order.save();      //save the new delivery number
                System.out.println("new address added");
                System.out.println(alias);
                OrderInvoice current_invoice = OrderInvoice.findByOrder(order_id);
                current_invoice.delivery_address = first_name + "<br/>" + last_name + "<br/>" + address1+ "<br/>" + address2+ "<br/>" + phone;
                current_invoice.invoice_address = current_invoice.delivery_address;
                current_invoice.save();
                return status(201, "new address added");
            }
        }else {
            return notFound("No such Order");
        }
    }


    /**
     * Android pad add one dress for a customer
     *@param customer_id 前台的ID
     *@return 409 or 200
     * */
    public static Result addAddressPad(int customer_id) throws Exception {
        response().setHeader("Connection","close");
        String request = request().body().asJson().toString();
        AddressTemplate addressTemplate = renderAddressTemplate(request);
        String alias = addressTemplate.alias;
        if (alias == null){
            alias = "";
        }

        if (alias.length()>32){
            alias = alias.substring(0,30);
        }

        String phone = addressTemplate.phone;
        if (phone == null){
            phone = "";
        }
        if (Address.findByAliasPhone(customer_id,phone,alias)!= null){
            return status(409,"this alias is already taken");
        }else{
            String company = addressTemplate.company;
            String first_name =  addressTemplate.firstname;
            String last_name = addressTemplate.lastname;
            String address1 = addressTemplate.address1;
            String address2 = addressTemplate.address2;
//            String city  =  addressTemplate.city;
            if(company == null){
                company = "";
            }
            if(first_name ==null){
                first_name = "";
            }
            if (last_name ==null){
                last_name = "";
            }
            if (address1 ==null){
                address1 = "";
            }
            if (address2 == null){
                address2 = "";
            }
//            if(city == null){
//                city = "Cork";
//            }
            Date  date_add = new Date();
            Address address = new Address(
                    26,
                    0,
                    customer_id,
                    0,
                    0,
                    0,
                    alias,
                    company,
                    first_name,
                    last_name,
                    address1,
                    address2,
                    "",
                    "Cork",
                    "",
                    phone,
                    "",
                    "",
                    "",
                    date_add,
                    date_add,
                    1,
                    0);
            address.save();
            Address just_now = Address.findByAliasPhone(customer_id,phone,alias);
            System.out.println("new address added");
            System.out.println(alias);
//            System.out.println(addressTemplate.alias.getClass().getSimpleName());
            return ok(renderCategories(just_now)); }
    }

    /**
     * Android mobile update an address of one certain user
     * @param address_id Address ID in the database
     * @param customer_id Customer ID
     * @return 409/404/201
     * */
    public static Result updateAddress(int address_id, int customer_id) throws Exception {
        response().setHeader("Connection","close");
        String request = request().body().asJson().toString();
        AddressTemplate addressTemplate = renderAddressTemplate(request);
        String alias = addressTemplate.alias;
        if (alias.length()>32){
            alias = alias.substring(0,30);
        }

        Address new_address = Address.findByAlias(customer_id,alias);
        if (new_address != null && new_address.id_address != address_id){
            System.out.println("Invalid Alias");
           return status(409,"this alias is already taken");
        }else{

            Address address = findById(address_id);
            if (address == null){
                System.out.println("Alias");
                return status(404,"former address doesn't exist");
             }
            else{
            Date  date_add = new Date();
                address.alias = alias;
                address.company = addressTemplate.company;
                address.firstname = addressTemplate.firstname;
                address.lastname = addressTemplate.lastname;
                address.address1 = addressTemplate.address1;
                address.address2 = addressTemplate.address2;
                address.city = addressTemplate.city;
                address.phone = addressTemplate.phone;
                address.date_upd = date_add;
                address.save();
            System.out.println("new address updated");
            System.out.println(alias);
            return status(201,"new address updated"); }
        }
    }

    /**
     * IOS mobile update the address of one certain user
     * @param customer_id
     * @param address_id
     * @return 409/404/201
     * */
    public static Result updateNewAddress(int address_id, int customer_id) throws Exception {
        response().setHeader("Connection","close");
        DynamicForm addressForm = Form.form().bindFromRequest();
        java.lang.String alias = addressForm.get("alias");
        if (alias.length()>32){
            alias = alias.substring(0,30);
        }

        if (Address.findByAlias(customer_id,alias)!= null){
            return status(409,"this alias is already taken");

        }
        else{
            Address address = findById(address_id);
            if (address == null){
                return status(404,"former address doesn't exist");
            }
            else{
            Date  date_add = new Date();
                address.alias = alias;
                address.company = "";
                address.firstname = addressForm.get("firstname");
                address.lastname = addressForm.get("lastname");
                address.address1 = addressForm.get("address1");
                address.address2 = addressForm.get("address2");
                address.city = addressForm.get("city");
                address.phone = addressForm.get("phone");
                address.date_upd = date_add;
                address.save();
            System.out.println("address updated ");
            System.out.println(alias);
            return status(201,"new address added"); }
        }
    }


    /**
     * IOS mobile add a new Address
     * @param customer_id
     * @return 409/201
     * */
    public static Result addNewAddress(int customer_id) throws Exception {
        response().setHeader("Connection","close");
        DynamicForm addressForm = Form.form().bindFromRequest();
        java.lang.String alias = addressForm.get("alias");
        if (alias.length()>32){
            alias = alias.substring(0,30);
        }

        if (Address.findByAlias(customer_id,alias)!= null){
            return status(409,"this alias is already taken");

        }
        else{
            Date  date_add = new Date();
            Address address = new Address(
                    26,
                    0,
                    customer_id,
                    0,
                    0,
                    0,
                    alias,
                    "",
                    addressForm.get("firstname"),
                    addressForm.get("lastname"),
                    addressForm.get("address1"),
                    addressForm.get("address2"),
                    "",
                    "Cork",
                    "",
                    addressForm.get("phone"),
                    "",
                    "",
                    "",
                    date_add,
                    date_add,
                    1,
                    0);
            address.save();
            System.out.println("new address added");
            System.out.println(alias);
            System.out.println(addressForm.get("firstname").getClass().getSimpleName());
            return status(201,"new address added"); }
    }

    /**
     * Delete one Address
     * @param address_id
     * @param customer_id
     * @return 404/400/200
     * */
    public static Result deleteAddress(int address_id, int customer_id){
        response().setHeader("Connection","close");
        Address address = findById(address_id);
        if (address == null ){
            return status(404,"there is no such address");
        }else if (address.id_customer != customer_id) {
            return status(400,"You are not the right user");
        }else{
        address.deleted = 1;
        address.save();
            System.out.println("deleted successfully");
        return ok("address deleted");
        }
    }


    /**
     *Get all the addresses of a phone number
     *@param phone
     *@return List<Address>
     */
    public static Result getAddressesByPhone(int phone) {
        response().setHeader("Connection","close");
        List<Address> all_address = Address.findCustomerAddressByNumber(phone);
        System.out.println(Integer.toString(phone).length());
        return ok(renderCategories(all_address));
    }

}