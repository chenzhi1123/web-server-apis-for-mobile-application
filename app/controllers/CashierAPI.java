/*
 * Copyright (c) 2017. Zhi Chen.
 * CashierAPI is used for cashier to hae a summary of daily sales.
 * Also Cashier uses apis to add record of a driver's cash payment issues
 */

package controllers;


import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import com.avaje.ebean.annotation.Transactional;
import models.database.CashierCash;
import models.database.CashierDriver;
import models.sqlContainer.TotalReturnDriverOrder;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static parsers.JsonParser.renderCategories;

public class CashierAPI extends Controller {
    /**
     * Calculate the total sales amount of a cashier in the last 14 hours
     * @param id_cashier
     * @return Total cash(product prices only), total order numbers
     * */
    public static TotalReturnDriverOrder getOrdersOfCashier(int id_cashier) {  //总价 cash, 只有cash 订单会存在 if_paid = 1, paid_cashier !=0
        String sql =
                "select sum(total_paid_real) as total_amount_sale, sum(total_shipping) as total_shipping_fee, count(*) as number_of_order\n" +
                        "\n" +
                        "from ps_orders, ps_drivers_orders\n" +
                        "\n" +
                        "where ps_orders.id_order = ps_drivers_orders.id_order\n" +
                        "\n" +
                        "and ps_orders.reference = ps_drivers_orders.order_reference\n" +
                        "\n" +
                        "and ps_orders.valid = 1\n" +  //valid orders only
                        "\n" +
                        "and ps_drivers_orders.if_paid = 1\n" +
                        "\n" +
                        "and ps_drivers_orders.paid_cashier =" + id_cashier+ "\n" +
                        "\n" +
                        "and ps_drivers_orders.date_add > DATE_SUB(NOW(), INTERVAL 14 HOUR)";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("sum(total_paid_real)", "total_sale_cash")
                        .columnMapping("sum(total_shipping)", "total_shipping_online")
                        .columnMapping("count(*)", "total_numberof_cash_order")
                        .create();

        com.avaje.ebean.Query<TotalReturnDriverOrder> query = Ebean.find(TotalReturnDriverOrder.class);
        query.setRawSql(rawSql);
        List<TotalReturnDriverOrder> list = query.findList();
//        int size = list.size();
//        for (int i =0; i<size; i++){
//            list.get(i).total_sale_cash = list.get(i).total_sale_cash.subtract(list.get(i).total_shipping_online);
//        }
        ArrayList<TotalReturnDriverOrder> totalReturnDriverOrders = new ArrayList<TotalReturnDriverOrder>(); //空数组
        totalReturnDriverOrders.addAll(list);
        TotalReturnDriverOrder final_record = totalReturnDriverOrders.get(0);
        return final_record;
    }


    /**
     * Calculate the total sales amount of a cashier in the last 14 hours
     * @param id_cashier
     * @return Total cash(product prices only), total order numbers
     * */
    public static TotalReturnDriverOrder getExceptionOrders(int id_cashier) {  //网站使用 Marina 订单， cash_on_delivery
        String sql =
                "select sum(total_shipping) as total_shipping_fee, count(*) as number_of_order\n" +
                        "\n" +
                        "from ps_orders, ps_drivers_orders\n" +
                        "\n" +
                        "where ps_orders.id_order = ps_drivers_orders.id_order\n" +
                        "\n" +
                        "and ps_orders.reference = ps_drivers_orders.order_reference\n" +
                        "\n" +
                        "and ps_orders.valid = 1\n" +  //valid orders only
                        "\n" +
                        "and ps_orders.total_discounts = 2.000000\n" +  //Marina Park 如果使用两个voucher, 就会少算Marina 单
                        "\n" +
                        "and ps_orders.total_shipping = 2.000000\n" +
                        "\n" +
                        "and ps_drivers_orders.if_paid = 1\n" +
                        "\n" +
                        "and ps_drivers_orders.paid_cashier =" + id_cashier+ "\n" +
                        "\n" +
                        "and ps_drivers_orders.date_add > DATE_SUB(NOW(), INTERVAL 14 HOUR)";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("sum(total_shipping)", "total_shipping_online")
                        .columnMapping("count(*)", "marina_orders")    //特殊的Marina订单
                        .create();

        com.avaje.ebean.Query<TotalReturnDriverOrder> query = Ebean.find(TotalReturnDriverOrder.class);
        query.setRawSql(rawSql);
        TotalReturnDriverOrder exceptionOrders = query.findUnique();
        if (exceptionOrders==null || exceptionOrders.total_shipping_online ==null){
            exceptionOrders = new TotalReturnDriverOrder();
        }
        return exceptionOrders;
    }

   /**
    * Return all the delivery from online-payment orders back to the driver
    * Add one record into the Cashier_driver relational table. One driver can
    * only have one record per day for the final cash paid summary.
    * @param id_driver
    * */
    @Transactional
    public static Result payBackToDriver(int id_driver) throws Exception {
        response().setHeader("Connection","close");
        DynamicForm cashier_driver = Form.form().bindFromRequest();
        int id_cashier = Integer.parseInt(cashier_driver.get("id_cashier"));
        String delivery_fee_paid = cashier_driver.get("total_shipping_online");
        if (delivery_fee_paid == null){
            delivery_fee_paid = "";
        }
        String name = cashier_driver.get("name");
        if (name == null){
            name = "";
        }
        double delivery_fee_online =  Double.parseDouble(delivery_fee_paid);
        int number_of_onlinepayment = Integer.parseInt(cashier_driver.get("number_of_onlinepayment"));
        Date date_add = new Date();
        CashierDriver a = CashierDriver.findByDriverID(id_driver);
        if (a != null){
            return status(409,"This driver was already paid");
        }else{
            //Add one record into the Cashier Driver Table
            CashierDriver cashierDriver = new CashierDriver(
                    id_cashier,
                    id_driver,
                    name,
                    delivery_fee_online,
                    number_of_onlinepayment,
                    date_add
            );
            cashierDriver.save();
         return ok("successfully paid to the driver fees");
        }
    }

    /**
     * Query one record of a driver, about how much delivery fee paid to that driver in total
     * @param id (driver_id)
     * @return record from database
     * */
    @Transactional
    public static Result getRecord(int id) throws Exception {
        response().setHeader("Connection","close");
        CashierDriver one = CashierDriver.findByID(id);
        return ok(renderCategories(one));
    }

    // test CashierCash table
    public static Result seeCash(int id) throws Exception {
        response().setHeader("Connection","close");
        CashierCash one = CashierCash.findById(id);
        return ok(renderCategories(one));
    }

   /**
    * Check if the driver is already paid by the cashier
    * For Pad Cashier App
    * @param id_driver
    * @return 0 or 1(status 200)
    * */
    public static Result checkIfPaid(int id_driver) throws Exception {
        response().setHeader("Connection","close");
       CashierDriver recent_one = CashierDriver.findByDriverID(id_driver);
      if (recent_one != null){
          return ok("false");
      }
        else{
          return ok("true");
      }
    }

}
