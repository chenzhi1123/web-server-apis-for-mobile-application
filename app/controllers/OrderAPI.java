/*
 * Copyright (c) 2017. Zhi Chen.
 * OrderAPI is for the process of customers making orders.
 * Actions like adding records in a series of tables
 * in the database,as well as updating the certain attribute
 * numbers, generating new reference,etc.
 */

package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import com.avaje.ebean.SqlUpdate;
import flexjson.JSONSerializer;
import models.database.*;
import models.sqlContainer.*;
import models.template.*;
import play.Logger;
import play.cache.Cache;
import play.data.DynamicForm;
import play.data.Form;
import play.db.ebean.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import util.sendEmail.CongratulationsVoucherEmail;

import java.math.BigDecimal;
import java.util.*;

import static controllers.Application.getRandomVoucher;
import static controllers.Application.sendRenewalVoucher;
import static controllers.DriverAPI.getSequenceNumber;
import static controllers.ProductAPI.getProductName;
import static controllers.ProductAPI.removeMealDealProduct;
import static models.database.DatabaseProduct.updateOneRecord;
import static parsers.JsonParser.*;
import static util.http.SendHttp.deviceNotification;

public class OrderAPI extends Controller {
    /**
     * Test Methods, to see if Java class has successfully connected database tables via Ebean Model
     **/
    public static Result getOneOrder(int id) {
        Order order = Order.findById(id);
        return ok(renderCategories(order));
    }
    public static Result getOneOrderCarrier(long id) {
        OrderCarrier orderCarrier = OrderCarrier.findById(id);
        return ok(renderCategories(orderCarrier));
    }
    public static Result getOneOrderDetail(int id) {
        OrderDetail orderDetail = OrderDetail.findById(id);
        return ok(renderCategories(orderDetail));
    }
    public static Result getOneOrderDetailTax(int id) {
        OrderDetailTax orderDetailTax = OrderDetailTax.findById(id);
        return ok(renderCategories(orderDetailTax));
    }
    public static Result getOneOrderHistory(int id){
        OrderHistory orderHistory = OrderHistory.findById(id);
        return ok(renderCategories(orderHistory));
    }
    public static Result getOneOrderInvoice(long id){
        OrderInvoice orderInvoice = OrderInvoice.findById(id);
        return ok(renderCategories(orderInvoice));
    }
    public static Result getOneOrderInvoicePayment(long id){
        OrderInvoicePayment orderInvoicePayment = OrderInvoicePayment.findById(id);
        return ok(renderCategories(orderInvoicePayment));
    }
    /**
     * Test Method, to see if Java class has successfully connected database tables via Ebean Model
     * @deprecated
     **/
    public static Result getOneOrderInvoiceTax(long id) {
    OrderInvoiceTax orderInvoiceTax = OrderInvoiceTax.findById(id);
    return ok(renderCategories(orderInvoiceTax));
    }
    public static Result getOneOrderPayment(long id) {
        OrderPayment orderPayment = OrderPayment.findById(id);
        return ok(renderCategories(orderPayment));
    }
    public static Result getOneOrderMessage(int id) {

        OrderMessage orderMessage = OrderMessage.findById(id);

        return ok(renderCategories(orderMessage));
    }
    /**
     * Mobile side checks out and adds one order
     * This method contains a series of actions to the database,
     * which involves many tables. at least 9 tables will be affected,
     * meanwhile 9 records will be added in a sequential order.
     * @param customer_id
     * @see Order
     * @see OrderHistory
     * @see OrderPayment
     * @see OrderInvoice
     * @see OrderCarrier
     * @see OrderDetail
     * @see OrderDetailTax
     * @see StockAvailable
     * @see OrderMessage
     * @see ReturnInformation  this is a template for order information sent
     * back to the mobile client in case the order is in online payment process
     * */
    @Transactional
    public static Result addOneOrder(int customer_id) throws Exception {
        response().setHeader("Connection","close");
        TotalOrder totalOrder = renderTotalOrder(request().body().asJson().toString());
        //需要在传递的信息中加上手机类型，以便发送notification
        Integer mobile_type = totalOrder.mobile_type;  //手机类型，用来发送Notification
        if(mobile_type == null){
            mobile_type = 0;
        }
        // promotion
        int id_cart_rule = totalOrder.id_cart_rule;
        String name = totalOrder.name;
        BigDecimal value = totalOrder.value;
        BigDecimal value_tax_excl = totalOrder.value_tax_excl;
        int free_shipping = totalOrder.free_shipping;
        String order_message = totalOrder.message;
        ArrayList<OrderProduct> new_products = totalOrder.products;
        int number_of_product = new_products.size();
        List<String> payment_method = new ArrayList<>();  //fixed String list for two payment methods
        payment_method.add("Online Payment");
        payment_method.add("Cash on delivery (COD)");
        List<String> payment_module = new ArrayList<>();  //fixed String list for two payment modules
        payment_module.add("ogone");
        payment_module.add("cashondelivery");
        String shop_address = "A2B Living<br />Marina Commercial Park<br />Cork,<br />+353 (0)21 4947888";
        Customer a = Customer.findById(totalOrder.id_customer);
        Customer b = Customer.findBySecure(totalOrder.secure_key);
        if (customer_id == totalOrder.id_customer && a != null && b != null && a.equals(b)) {  //是不是同一个用户发送请求
            //Start of flash sale products
            //判断购买产品是否超过30个。 如果超过，就不能下单；前提条件是这张单里面有1368号产品
            //先看这一单是否有1368 号产品
            int if_has_special_pizza=0;
            for(OrderProduct each_product: new_products){
                if(each_product.product_id == 1368){ //抢购披萨的产品ID todo: 1368
                    if_has_special_pizza = 1;  //如果订单里面有抢购披萨，就进行判断
                }
            }
            if(if_has_special_pizza ==1){ //如果这一单里面存在抢购披萨，就要进行下一步判断
                Boolean if_exceed_max;
                if_exceed_max = if_exceed_pizza();
                if(if_exceed_max == false){//超过了上限
                    return status(409, "All special pizzas are sold out");
                }
            }
            //end of flash sale products
                int payment_type = totalOrder.payment_method;
                String order_reference;
                int order_status = 0;
                do {
                    order_reference = getRandomString();
                } while (Order.findByReference(order_reference) != null);
                int valid_number = 0;
                /* if the order is paid by card, the payment method code should be 0, and the order status code should be 11 */
                if (payment_type == 0) {
                    order_status = 11;
                }
                /* if it is cash-on-delivery order, the payment method code should be 1, and the order status code should be 3 */
                else if (payment_type == 1) {
                    order_status = 3;
                    valid_number = 1;
                }
                Date nullDaTe = new Date(0);
                Date date_add = new Date();
                /* First table affected, ps_orders, a new record will be generated */
                Order order = new Order(
                        order_reference,
                        1,
                        1,
                        9, //a2b delivery， 以后就要用另外一个选项 collection 来决定订单是配送还是到店取
                        1,
                        customer_id,
                        0,
                        1,
                        totalOrder.id_address_delivery,
                        totalOrder.id_address_invoice,
                        order_status, //order status code, 11 or 3
                        totalOrder.secure_key,
                        payment_method.get(payment_type),
                        BigDecimal.valueOf(1.000000),
                        payment_module.get(payment_type),
                        0,
                        0,
                        "",
                        0,
                        "",
                        value,
                        value,
                        value_tax_excl,
                        totalOrder.total_paid,
                        totalOrder.total_paid_tax_incl,
                        totalOrder.total_paid_tax_excl,
                        totalOrder.total_paid_real,
                        totalOrder.total_products,
                        totalOrder.total_products_wt,
                        totalOrder.total_shipping,
                        totalOrder.total_shipping_tax_incl,
                        totalOrder.total_shipping_tax_excl,
                        BigDecimal.valueOf(0.000),
                        BigDecimal.valueOf(0.000000),
                        BigDecimal.valueOf(0.000000),
                        BigDecimal.valueOf(0.000000),
                        2,
                        2,
                        0,
                        0,
                        nullDaTe,
                        nullDaTe,
                        valid_number,
                        date_add,
                        date_add
                );
                order.save();
                Order just_added = Order.findByReference(order_reference);
                int id_order = just_added.id_order;
                /*
                The second period, add new records in table
                ps_order_history, ps_order_invoice, ps_order_payment, and ps_message
                */
                OrderHistory orderHistory = new OrderHistory(         //ps_order_history
                        0,
                        id_order,
                        order_status,
                        date_add
                );
                orderHistory.save();
                System.out.println("orderHistory.save(); " + System.currentTimeMillis());

                OrderPayment orderPayment = new OrderPayment(       //ps_order_payment
                        order_reference,
                        1,
                        totalOrder.total_paid,
                        payment_method.get(payment_type),
                        BigDecimal.valueOf(1.000000),
                        "",
                        "",
                        "",
                        "",
                        "",
                        date_add
                );
                orderPayment.save();
                System.out.println("orderPayment.save(); " + System.currentTimeMillis());
                OrderPayment new_one = OrderPayment.findByReference(order_reference);
                int id_order_payment = new_one.id_order_payment;
                OrderInvoice orderInvoice = new OrderInvoice(    //ps_order_invoice
                        id_order,
                        0,
                        0,
                        date_add,
                        value_tax_excl,
                        value,
                        totalOrder.total_paid_tax_excl,
                        totalOrder.total_paid_tax_incl,
                        totalOrder.total_products,
                        totalOrder.total_products_wt,
                        totalOrder.total_shipping_tax_excl,
                        totalOrder.total_shipping_tax_incl,
                        0,
                        BigDecimal.valueOf(0.000000),
                        BigDecimal.valueOf(0.000000),
                        shop_address,
                        totalOrder.invoice_address,
                        totalOrder.delivery_address,
                        "",
                        date_add
                );
                /* create a new record in the table of ps_order_invoice only when the order status is 3 */
                if (order_status == 3) {
                    orderInvoice.save();
                    System.out.println("orderInvoice.save(); " + System.currentTimeMillis());
                    OrderInvoice new_added_invoice = OrderInvoice.findByOrder(id_order);
                    int id_order_invoice = new_added_invoice.id_order_invoice;
                    new_added_invoice.number = id_order_invoice;
                    new_added_invoice.delivery_number = id_order_invoice;
                    new_added_invoice.save();
                    System.out.println("new_added_invoice.save(); " + System.currentTimeMillis());
                    just_added.invoice_number = id_order_invoice;
                    just_added.delivery_number = id_order_invoice;
                    just_added.invoice_date = date_add;
                    just_added.delivery_date = date_add;
                    just_added.save();
                    System.out.println("just_added.save(); " + System.currentTimeMillis());
                    /*
                    The third period, generate records in the following tables in the database,
                    meanwhile reduce the quantity of each product bought
                    */
                    OrderCarrier orderCarrier = new OrderCarrier(                                     // ps_order_carrier
                            id_order,
                            9, //carrier = 9
                            id_order_invoice,
                            BigDecimal.valueOf(0.000000),
                            totalOrder.total_shipping_tax_excl,
                            totalOrder.total_shipping_tax_incl,
                            "",
                            date_add
                    );
                    orderCarrier.save();

                    OrderInvoicePayment orderInvoicePayment = new OrderInvoicePayment(              //ps_order_invoice_payment
                            id_order_invoice,
                            id_order_payment,
                            id_order
                    );
                    orderInvoicePayment.save();

                    OrderInvoiceTax orderInvoiceTax = new OrderInvoiceTax(                          //ps_order_invoice_tax
                            id_order_invoice,
                            "shipping",
                            1,
                            BigDecimal.valueOf(0.000000)
                    );
                    orderInvoiceTax.save();
                   /* create a thread to process order_details adding action */
                    Integer finalMobile_type = mobile_type;
                    Runnable forloop1 = new Runnable() {
                        @Override
                        public synchronized void run() {  //锁住线程，按顺序执行
                            //更改每个被购买产品的数量
                            for (int i = 0; i < number_of_product; i++) {
                                OrderProduct orderProduct = new_products.get(i);
                                String product_reference = orderProduct.product_reference;
                                if (product_reference == null) {
                                    product_reference = "";
                                }
                                BigDecimal price = getWholeSale(orderProduct.product_id);
                                OrderDetail orderDetail = new OrderDetail(                       //ps_order_detail
                                        id_order,
                                        id_order_invoice,
                                        0,
                                        1,
                                        orderProduct.product_id,
                                        orderProduct.product_attribute_id,
                                        orderProduct.product_name,
                                        orderProduct.product_quantity,
                                        orderProduct.product_quantity,
                                        0,
                                        0,
                                        0,
                                        orderProduct.product_price,
                                        orderProduct.reduction_percent,
                                        orderProduct.reduction_amount,
                                        orderProduct.reduction_amount_tax_incl,
                                        orderProduct.reduction_amount_tax_excl,
                                        BigDecimal.valueOf(0.00),
                                        BigDecimal.valueOf(0.000000),
                                        "",
                                        orderProduct.product_upc,
                                        product_reference,
                                        "",
                                        BigDecimal.valueOf(0.000000),
                                        orderProduct.id_tax_rules_group,
                                        0,
                                        "",
                                        BigDecimal.valueOf(0.000),
                                        BigDecimal.valueOf(0.000000),
                                        BigDecimal.valueOf(0.000),
                                        0,
                                        "",
                                        0,
                                        new Date(0),
                                        orderProduct.total_price_tax_incl,
                                        orderProduct.total_price_tax_excl,
                                        orderProduct.unit_price_tax_incl,
                                        orderProduct.unit_price_tax_excl,
                                        BigDecimal.valueOf(0.000000),
                                        BigDecimal.valueOf(0.000000),
                                        BigDecimal.valueOf(0.000000),
                                        orderProduct.product_price,
                                        price
                                );
                                orderDetail.save();
                                int id_order_detail = orderDetail.id_order_detail;

                                OrderDetailTax orderDetailTax = new OrderDetailTax(                   //ps_order_detail_tax
                                        id_order_detail,
                                        orderProduct.id_tax,
                                        orderProduct.tax_unit_amount,
                                        orderProduct.tax_total_amount
                                );
                                orderDetailTax.save();
                                //get the current quantity of the product and then reduce the quantity by the number bought in the order
                                change_product_quantity(orderProduct.product_id, orderProduct.product_quantity, 0);
                            }
                            // promotion code actions
                            if (id_cart_rule != 0) {
                                //新 Voucher 数量控制，包括多次使用
                                change_voucher_quantity(customer_id,id_order,id_cart_rule,id_order_invoice,name,value,value_tax_excl,free_shipping);
                            }
                            //准备发送Notification
                            deviceNotification(customer_id, finalMobile_type);
                            sendEmailToSponsor(customer_id);//发送邮件
                        }
                    };
                    new Thread(forloop1).start();
                }else if (order_status == 11){  //pay online
                    if (order_message != null) {  //进入消息判断， 有存在消息才行
                        if(order_message.startsWith("Today") || order_message.startsWith("Deliver ASAP<br/>") || order_message.startsWith("Tomorrow")){ //判断出是苹果用户
                            return status(409, "Sorry, IOS online payment is not supported at the moment, please try cash on delivery.");
                        }
                    }
                    /*
                    if the status code is 11, the record in table order_invoice will not be created
                    currently the id_order_invoice will be 0 in the database until the record is generated
                    */
                    OrderCarrier orderCarrier = new OrderCarrier(                               //ps_order_carrier
                            id_order,
                            9,
                            0, //id_order_invoice
                            BigDecimal.valueOf(0.000000),
                            totalOrder.total_shipping_tax_excl,
                            totalOrder.total_shipping_tax_incl,
                            "",
                            date_add
                    );
                    orderCarrier.save();
                    /* Create a new thread for order_details, and order_detail_tax */
                    Runnable forloop2 = new Runnable() {
                        @Override
                        public synchronized void run() {
                            for (int i = 0; i < number_of_product; i++) {
                                OrderProduct orderProduct = new_products.get(i);
                                String product_reference = orderProduct.product_reference;
                                if (product_reference == null) {
                                    product_reference = "";
                                }
                                BigDecimal price = getWholeSale(orderProduct.product_id);
                                OrderDetail orderDetail = new OrderDetail(                       //ps_order_details
                                        id_order,
                                        0, //id_order_invoice
                                        0,
                                        1,
                                        orderProduct.product_id,
                                        orderProduct.product_attribute_id,
                                        orderProduct.product_name,
                                        orderProduct.product_quantity,
                                        orderProduct.product_quantity,
                                        0,
                                        0,
                                        0,
                                        orderProduct.product_price,
                                        orderProduct.reduction_percent,
                                        orderProduct.reduction_amount,
                                        orderProduct.reduction_amount_tax_incl,
                                        orderProduct.reduction_amount_tax_excl,
                                        BigDecimal.valueOf(0.00),
                                        BigDecimal.valueOf(0.000000),
                                        "",
                                        orderProduct.product_upc,
                                        product_reference,
                                        "",
                                        BigDecimal.valueOf(0.000000),
                                        orderProduct.id_tax_rules_group,
                                        0,
                                        "",
                                        BigDecimal.valueOf(0.000),
                                        BigDecimal.valueOf(0.000000),
                                        BigDecimal.valueOf(0.000),
                                        0,
                                        "",
                                        0,
                                        new Date(0),
                                        orderProduct.total_price_tax_incl,
                                        orderProduct.total_price_tax_excl,
                                        orderProduct.unit_price_tax_incl,
                                        orderProduct.unit_price_tax_excl,
                                        BigDecimal.valueOf(0.000000),
                                        BigDecimal.valueOf(0.000000),
                                        BigDecimal.valueOf(0.000000),
                                        orderProduct.product_price,
                                        price
                                );
                                orderDetail.save();
                                int id_order_detail = orderDetail.id_order_detail;

                                OrderDetailTax orderDetailTax = new OrderDetailTax(                    //ps_order_detail_tax
                                        id_order_detail,
                                        orderProduct.id_tax,
                                        orderProduct.tax_unit_amount,
                                        orderProduct.tax_total_amount
                                );
                                orderDetailTax.save();
                                // 在线付款不减去数量 第一次请求的时候
                            }
                        }
                    };
                    new Thread(forloop2).start();
                }
                /*
                Return some of the order information to Client side for further use
                For online-payment methods, it requires authentication from a third
                party, and some records will be generated after the authentication
                */
                ReturnInformation return_info = new ReturnInformation(
                        id_order,
                        customer_id,
                        totalOrder.id_address_delivery,
                        totalOrder.id_address_invoice,
                        0,
                        id_order_payment,
                        order_reference,
                        "",
                        totalOrder.secure_key,
                        totalOrder.invoice_address,
                        totalOrder.delivery_address,
                        totalOrder.total_paid,
                        totalOrder.total_paid_tax_incl,
                        totalOrder.total_paid_tax_excl,
                        totalOrder.total_paid_real,
                        totalOrder.total_products,
                        totalOrder.total_products_wt,
                        totalOrder.total_shipping,
                        totalOrder.total_shipping_tax_excl,
                        totalOrder.total_shipping_tax_incl,
                        id_cart_rule,
                        name,
                        value,
                        value_tax_excl,
                        free_shipping,
                        mobile_type
                );
                /* Either conditional process will lead to the return of general information of this order(if the order contains a message, add one record in ps_message) */
                if (order_message != null) {
                    if (!order_message.equals("")) {
                        OrderMessage message = new OrderMessage(
                                0,
                                customer_id,
                                0,
                                id_order,
                                order_message,
                                0, //if_private
                                date_add
                        );
                        message.save();
                    }
                }
                if(order_status == 11){ //只保存在线付款的返回值
                Logger.info("New order paid with card:-------->" + return_info);
                }
                return status(201, renderCategories(return_info));
        }else {
            return status(409, "Invalid user, you cannot order");
        }
    }

    /**
     * 把产品不可见，设置产品id_product，active = 0
     * 涉及表格： ps_product, ps_product_shop
     * */
    public static void setOffSale(int id_product){
        String dml = "UPDATE ps_product\n" +
                "SET active = :active, indexed = :indexed\n" +
                "WHERE id_product = :id_product";
        SqlUpdate update = Ebean.createSqlUpdate(dml)
                .setParameter("active", 0)
                .setParameter("indexed", 0)
                .setParameter("id_product", id_product);
        update.execute();

        String dml_2 = "UPDATE ps_product_shop\n" +
                "SET active = :active, indexed = :indexed\n" +
                "WHERE id_product = :id_product";
        SqlUpdate update_2 = Ebean.createSqlUpdate(dml_2)
                .setParameter("active", 0)
                .setParameter("indexed", 0)
                .setParameter("id_product", id_product);
        update_2.execute();
    }
   /**
    * 特殊时期用来监控 限量出售的产品
    * */
    public static Boolean if_exceed_pizza(){
        String sql =
                "select sum(ps_order_detail.product_quantity)\n" +
                        "from ps_orders, ps_order_detail\n" +
                        "where ps_orders.id_order  = ps_order_detail.id_order \n" +
                        "and ps_orders.valid = 1\n" +
                        "and ps_order_detail.product_id  = 1368\n" + //todo:测试产品，真正产品为1368
                        "and ps_orders.invoice_date > DATE_SUB(NOW(), INTERVAL 12 HOUR)";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("sum(ps_order_detail.product_quantity)", "quantity")
                        .create();
        com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
        query.setRawSql(rawSql);
        ProductQuantity current_number = query.findUnique();
        Integer current_total = current_number.quantity;//避免非空  值为0 的时候代表还没有卖出一件, 或者有可能不存在这件产品（一般不会出现）
        if(current_total ==null){  //如果为空，则为0
            current_total = 0;
        }
        if(current_total < 35 && current_total>=0){  //卖出总量还没达到上限
            return true;
        }else{  //卖出总量高于上限，进行产品下架，缓存清除，数量减为0
            //做一些事情，把第1368号产品禁用掉
            //检测这件产品是否已经被禁用掉，如果没有，就要disable 产品，清除缓存；
            String sql_active =
                    "select ps_product.active\n" +
                            "from ps_product\n" +
                            "where id_product = 1368";  //todo 1368
            RawSql rawSql_active =
                    RawSqlBuilder
                            .parse(sql_active)
                            .columnMapping("ps_product.active", "quantity")
                            .create();
            com.avaje.ebean.Query<ProductQuantity> query_active = Ebean.find(ProductQuantity.class);
            query_active.setRawSql(rawSql_active);
            ProductQuantity if_active = query_active.findUnique();
            Integer current_active = if_active.quantity;//避免非空  值为0 的时候代表已经被disabled过了
            if(current_active ==null){  //如果为空，则为0
                current_active = 0;
            }
            //如果已经被禁用掉，就不需要做任何事情；
            if(current_active ==1){  //还没有被禁用掉
            //禁用这件产品，并且把cache 清空，用于更新手机页面
                setOffSale(1368); //todo 1368
                for(int i = 1; i <1000; i++){//种类上限扩展到1000
                    Cache.remove("allProductsOfCategory" + i);
                    System.out.println("Cache cleared------------------"+ i);
                }
            }
            return false;
        }
    }

    @Transactional
    public static synchronized void change_product_quantity(int bought_product_id, int change_quantity, int add_or_remove){
        System.out.println("++++++++++++++++++++Start to change quantity+++++++++++++++++++++++");
//        int nbThreads =  Thread.getAllStackTraces().keySet().size(); 查看所有正在用的线程数
//        System.out.println("all threads: "+ nbThreads);
        StockAvailable stockAvailable = StockAvailable.findByAttribute(bought_product_id,0);  //meal_deal 产品不会卖完，里面附带的shop产品会被卖完
        if(stockAvailable !=null){
            if(add_or_remove ==0){ //减去
                System.out.println("Product____"+ bought_product_id + "____Quantity changed");
                //当发现减下去数量之后，产品数量会小于0的时候，直接让数量保持为0
                int current_quantity = stockAvailable.quantity;
                int new_quantity = current_quantity-change_quantity;
                if(new_quantity <= 0){
                    stockAvailable.quantity = 0; //当前产品数量为0
                }else{
                    stockAvailable.quantity = new_quantity;
                }
                stockAvailable.save();
                if(new_quantity <=0) {
                    //先判断是不是参与meal_deal产品销售
                    ArrayList<Integer> id_product_attributes = DealProductsTable.findByShopProduct(bought_product_id);
                    if(id_product_attributes.size()>0){  //存在这是meal_deal 正在卖的shop产品
                        removeMealDealProduct(bought_product_id,id_product_attributes);  //产品下架
                    }
                    //进行判断是否参与买一送一活动
                    List<BuyOneGetOne> if_exist_original_product = BuyOneGetOne.findByOriginalProduct(bought_product_id);
                    List<BuyOneGetOne> if_exist_sales_product = BuyOneGetOne.findBySaleProductId(bought_product_id);
                    if (if_exist_original_product.size() > 0) { //这件产品参与买一送一，数量已经为0
                      // 把买一送一那件产品正在做活动的产品数量变成0
                        for(BuyOneGetOne this_sales_product: if_exist_original_product){
                            int sales_product_id = this_sales_product.id_onsale_product;
                            StockAvailable sales_product_stock = StockAvailable.findByAttribute(sales_product_id,0);
                            int sales_product_quantity = sales_product_stock.quantity;
                            if(sales_product_quantity !=0){
                                sales_product_stock.quantity = 0;
                                sales_product_stock.save();
                                //产品下架
                                updateOneRecord(sales_product_id,0,0);
                            }
                        }
                    }else if (if_exist_sales_product.size() > 0) { //这件产品是买一送一产品，数量已经卖完了
                      // 查看每件原产品数量是不是已经被改成0
                        for(BuyOneGetOne this_original_product: if_exist_sales_product){
                            int original_product_id = this_original_product.id_real_product;
                            StockAvailable original_product_stock = StockAvailable.findByAttribute(original_product_id,0);
                            int original_product_quantity = original_product_stock.quantity;
                            if(original_product_quantity !=0){
                                original_product_stock.quantity = 0;
                                original_product_stock.save();
                                //产品下架
                                updateOneRecord(original_product_id,0,0); //原产品下架
                            }
                        }
                    }
                }
            }else {  //加上
                System.out.println("Product____"+ bought_product_id + "____Quantity changed");
                stockAvailable.quantity += change_quantity;
                stockAvailable.save();
            }
        }
    }

    public static Result testPizzaAPI(){
        response().setHeader("Connection","close");
        if(if_exceed_pizza()){ //返回值为true，说明卖出的pizza总数还没超过上限
            return ok("Has not exceeded to maximum");
        }else {
            return ok("Exceeded to Maximum quantity");
        }
    }
    /**
     * 开发多次使用voucher功能
     */
    public static synchronized void change_voucher_quantity(int id_customer,int id_order, int id_cart_rule, int id_order_invoice, String name, BigDecimal value, BigDecimal value_tax_excl, int free_shipping){
        System.out.println("++++++++++++++++++++Start to change voucher+++++++++++++++++++++++");
        CartRule cartRule = CartRule.findById(id_cart_rule);//找到这个voucher
        if(cartRule !=null){  //存在这个优惠券的时候才有进一步操作
        BigDecimal voucher_type = cartRule.reduction_amount; //如果voucher是减去多少钱，就要看是不是partial use, 否则直接扣除数量
        if(voucher_type.compareTo(BigDecimal.valueOf(0)) == 1){//reduction_amount > 0 //说明是减去多少金额的折扣券， 接下来要判断是否满足减去数量，生成记录的条件
            int partial_use = cartRule.partial_use;
            if(partial_use == 1){//只有当partial use = 1 的时候才继续判断这张单折扣打了多少钱，原本这个voucher 价值多少钱
                if(voucher_type.compareTo(value) == 1){//这个voucher 可以减免的钱 大于这张单里面 实际的折扣金额
                    //voucher 有余额
                    BigDecimal voucher_amount_left = voucher_type.subtract(value);
                    // 准备生成新的voucher, 让用户再次使用，金额为剩下的金额
                    String last_voucher_name = cartRule.code;  //上一个voucher 编号
                    sendRenewalVoucher(id_customer,voucher_amount_left,last_voucher_name,voucher_type,value); //出发一系列动作，生成新的voucher，发送给用户邮件
                }
            }
        }
        //直接生成 voucher 使用记录，并且减去这个voucher数量
            OrderCartRule new_record = new OrderCartRule(
                    id_order,
                    id_cart_rule,
                    id_order_invoice,
                    name,
                    value,  //打折价值
                    value_tax_excl, //打折价值（不含税）
                    free_shipping
            );
            new_record.save();
            cartRule.quantity -= 1;
            cartRule.save();
            System.out.println("Voucher has been used");
        }else{
            System.out.println("Voucher doesn't exist!");
        }
//        int nbThreads =  Thread.getAllStackTraces().keySet().size(); 查看所有正在用的线程数
//        System.out.println("all threads: "+ nbThreads);
    }
    /**
     * This method is for online-payment transaction, after authentication, some records will be generated
     * in corresponding tables. It will receive the order info from client end through HTTP request
     * @param order_id
     * @see ReturnInformation
     * @see OrderInvoice   create a new object of this class, accordingly add one new record in this table
     * @see OrderInvoicePayment
     * @see OrderInvoiceTax
     * @see OrderDetail
     * @see Order
     * @see OrderHistory
     * @see OrderCarrier
     * */
    @Transactional
    public static Result updateOneOrder(int order_id) throws Exception {
        response().setHeader("Connection","close");
        ReturnInformation returnInformation = renderReturnInformation(request().body().asJson().toString());
        System.out.println("receive time ----" + String.valueOf(System.currentTimeMillis()) + "----------------");
        Integer mobile_type = returnInformation.mobile_type; //手机类型
        if(mobile_type == null){
            mobile_type = 0;
        }
        int id_cart_rule = returnInformation.id_cart_rule;
        String name = returnInformation.name;
        BigDecimal value = returnInformation.value;
        BigDecimal value_tax_excl = returnInformation.value_tax_excl;
        int free_shipping = returnInformation.free_shipping;
        int id_order = returnInformation.id_order;
        int id_customer = returnInformation.id_customer;
        String shop_address = "A2B Living<br />Marina Commercial Park<br />Cork,<br />+353 (0)21 4947888";
        int id_order_payment = returnInformation.id_order_payment;
        Order order = Order.findById(order_id);
        int order_status = order.current_state;
        if (order == null) {
            return status(404, "No such order");
        } else if (order_status == 3) {
            return status(409, "You have already paid");
        } else{
            Date date_add = new Date();
            /* parse the information from http request, then use them to generate records in each table */
            OrderInvoice orderInvoice = new OrderInvoice(               // ps_order_invoice
                    id_order,
                    0,
                    0,
                    date_add,
                    value_tax_excl,
                    value,
                    returnInformation.total_paid_tax_excl,
                    returnInformation.total_paid_tax_incl,
                    returnInformation.total_products,
                    returnInformation.total_products_wt,
                    returnInformation.total_shipping_tax_excl,
                    returnInformation.total_shipping_tax_incl,
                    0,
                    BigDecimal.valueOf(0.000000),
                    BigDecimal.valueOf(0.000000),
                    shop_address,
                    returnInformation.invoice_address,
                    returnInformation.delivery_address,
                    "",
                    date_add
            );
            orderInvoice.save();
            OrderInvoice new_added_invoice = OrderInvoice.findByOrder(id_order);
            int id_order_invoice = new_added_invoice.id_order_invoice;
            new_added_invoice.number = id_order_invoice;
            new_added_invoice.delivery_number = id_order_invoice;
            new_added_invoice.save();
            OrderInvoicePayment orderInvoicePayment = new OrderInvoicePayment( //ps_order_invoice_payment
                    id_order_invoice,
                    id_order_payment,
                    id_order

            );
            orderInvoicePayment.save();
            OrderInvoiceTax orderInvoiceTax = new OrderInvoiceTax(            //ps_order_invoice_tax
                    id_order_invoice,
                    "shipping",
                    1,
                    BigDecimal.valueOf(0.000000)
            );
            orderInvoiceTax.save();
            /* Update the id_order_invoice for each record in the ps_order_details table */
            List<OrderDetail> orderDetails = OrderDetail.findByOrder(id_order);
            int size = orderDetails.size();
            /* Create a new thread for order_details, and order_detail_tax */
            Integer finalMobile_type = mobile_type;
            Runnable forloop_3 = new Runnable() {
                @Override
                public synchronized void run() {
                    for (int i = 0; i < size; i++) {
                        orderDetails.get(i).id_order_invoice = id_order_invoice;
                        orderDetails.get(i).save();
                        //change product quantity in the database
                        int product_id = orderDetails.get(i).product_id;
                        int product_quantity = orderDetails.get(i).product_quantity;
                        //change quantity
                        change_product_quantity(product_id,product_quantity,0);
                    }
                    // promotion code actions
                    if (id_cart_rule !=0){  //如果使用优惠券
                        //参数 int id_order,   int id_cart_rule, int id_order_invoice, String name, BigDecimal value, Bigdecimal value_tax_excl, int free_shipping
                    change_voucher_quantity(id_customer,id_order,id_cart_rule,id_order_invoice,name,value,value_tax_excl,free_shipping);
                    }
                    //准备发送订单notification, 参数 id_customer, mobile_type
                    deviceNotification(id_customer, finalMobile_type);
                    sendEmailToSponsor(id_customer);
                    System.out.println("loop products finished-----" + String.valueOf(System.currentTimeMillis()) + "----------------");
                }
            };
            new Thread(forloop_3).start();
            System.out.println("start to loop products-----" + String.valueOf(System.currentTimeMillis()) + "----------------");
            /* Update the order status after this payment is authenticated by payment organisation */
            order.current_state = 3;                                         // ps_orders
            order.invoice_number = id_order_invoice;
            order.delivery_number = id_order_invoice;
            order.delivery_date = date_add;
            order.invoice_date = date_add;
            order.valid = 1;
            order.save();
            OrderHistory orderHistory = OrderHistory.findById(order_id);     //ps_order_history
            orderHistory.id_order_state = 3;
            orderHistory.save();
            OrderCarrier orderCarrier = OrderCarrier.findByOrder(id_order);  //ps_order_cashier
            orderCarrier.id_order_invoice = id_order_invoice;
            orderCarrier.save();
            System.out.println("updated");
            System.out.println("Payment Finished-----" + String.valueOf(System.currentTimeMillis()) + "----------------");
            return ok("order paid successfully");
        }
    }
    /**
     * Allocate random and unique string to each order, as "order_reference"
     * @return random_String
     * */
    public static String getRandomString() {
        StringBuffer buffer = new StringBuffer("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        StringBuffer reference = new StringBuffer();
        Random random = new Random();
        int range = buffer.length();
        for (int i = 0; i < 8; i++) {
            reference.append(buffer.charAt(random.nextInt(range)));
        }
        return reference.toString();
    }
    /**
     * Return orders within 2 weeks time of a customer, including the details
     * @param id
     * @param secure_key  each user has one unique key for verification
     * @return  ArrayList<returnOrders>
     * @see ReturnOrder
     * @see ReturnProduct
     * @see AddressTemplate
     * */
    public static Result showOrders(int id, String secure_key) {
        response().setHeader("Connection","close");
        System.out.println("start to return");
        List<Order> all_orders = Order.findByCustomer(id, secure_key);  //find all orders of a customer within 14 days
        if (all_orders != null) {
            List<ReturnOrder> returnOrders = new ArrayList<>();         //create a new list of orders using a pre-defined template
            for (Order this_order : all_orders) {
                int order_id = this_order.id_order;
                List<ReturnProduct> bought_products = new ArrayList<>();                // create a template list of products info, which will be returned
                List<OrderDetail> detail_products = OrderDetail.findByOrder(order_id);  // get all the products detail from table ps_order_detail
                for (OrderDetail this_product : detail_products) {
                    ReturnProduct product = new ReturnProduct(
                            this_product.id_order_detail,
                            this_product.product_upc,
                            this_product.product_name,
                            this_product.product_reference,
                            this_product.product_quantity,
                            this_product.total_price_tax_incl
                    );
                    bought_products.add(product);
                }
                int address_id = this_order.id_address_delivery;         //Address information of each order
                AddressTemplate delivery_address;                           //Define an object using AddressTemplate for later return
                if (address_id != 0) {                                      //If there is an address
                    Address address = Address.findById(address_id);         //Find the address in the table of Address
                    String alias = address.alias;
                    String company = address.company;
                    String firstname = address.firstname;
                    String lastname = address.lastname;
                    String address1 = address.address1;
                    String address2 = address.address2;
                    String city = address.city;
                    String phone = address.phone;
                    delivery_address = new AddressTemplate(
                            alias,
                            company,
                            firstname,
                            lastname,
                            address1,
                            address2,
                            city,
                            phone
                    );
                } else {
                    delivery_address = null;
                }
                int id_state = this_order.current_state;                   //find the order status
                String state = OrderState.findById(id_state);
                String payment = this_order.payment;
                String reference = this_order.reference;
                ReturnOrder final_order = new ReturnOrder(
                        order_id,
                        reference,
                        delivery_address,
                        state,
                        payment,
                        this_order.total_paid_real,
                        this_order.total_shipping,
                        this_order.date_add,
                        bought_products
                );
               returnOrders.add(final_order);                              //Keep adding orders into the ReturnOrder list
            }
            System.out.println("orders returned");
            return ok(new JSONSerializer().deepSerialize(returnOrders));
        } else {
            System.out.println("no such user");
            return notFound("Not invalid user");
        }
    }
    /**
     * Get the wholesale price of each product.
     * This method will be called when creating a new order.
     * @param productID
     * @return wholesale_price
     * @see ProductWholesale
     * */
    public static BigDecimal getWholeSale(int productID) {
        String sql =
                "select ps_product.wholesale_price\n" +
                        "\n" +
                        "from ps_product\n" +
                        "\n" +
                        " where ps_product.id_product = \n" + productID;
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_product.wholesale_price", "whole_sale")
                        .create();
        com.avaje.ebean.Query<ProductWholesale> query = Ebean.find(ProductWholesale.class);
        query.setRawSql(rawSql);
        List<ProductWholesale> list = query.findList();
        ArrayList<ProductWholesale> whole_prices = new ArrayList<ProductWholesale>();
        whole_prices.addAll(list);
        BigDecimal wholesale_price = BigDecimal.valueOf(0);
        if(list.size() > 0){
            wholesale_price = whole_prices.get(0).whole_sale;
        }
        return wholesale_price;
    }

    public static Result showAllCart() {
        response().setHeader("Connection","close");
        List<CartRule> all_record = CartRule.findAll();
        return ok(renderCategories(all_record));
    }

    public static Result showAllGroup() {
        response().setHeader("Connection","close");
        List<CartRuleGroup> all_record = CartRuleGroup.findAll();
        return ok(renderCategories(all_record));
    }

    public static int getUsedTimes(int id_cart_rule, int id_customer){
        String sql =
                "select count(*) \n" +
                        "\n" +
                        "from ps_orders, ps_order_cart_rule\n" +
                        "\n" +
                        "where ps_orders.id_order = ps_order_cart_rule.id_order\n" +
                        "\n" +
                        "and ps_order_cart_rule.id_cart_rule =" + id_cart_rule + "\n" +
                        "\n" +
                        "and ps_orders.id_customer =" + id_customer +  "\n" +
                        "and ps_orders.valid = 1";  //只记录有效订单

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("count(*)", "quantity")
                        .create();

        com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
        query.setRawSql(rawSql);
        return query.findUnique().quantity;
    }

    public static Result codeValidate() {
        response().setHeader("Connection","close");
        DynamicForm order_driver = Form.form().bindFromRequest();
        System.out.println("Start to validate");
        int id_customer;
        String promotion_code;
        try {
            id_customer = Integer.parseInt(order_driver.get("id_customer"));
            promotion_code = order_driver.get("promotion_code");
        } catch (Exception e) {
            return badRequest("Wrong request format");
        }
        //第一层条件过滤， 用户是否登录之后再验证
        if (id_customer == 0){
            return  badRequest("Please log in");
        }else {
            int constraint_customer_id;
            try {
                constraint_customer_id = CartRule.getConstraintUser(promotion_code);
            } catch (Exception e) {
                constraint_customer_id = 0;
            }
            //第二层条件过滤， Voucher是否绑定特定用户才能使用
            if(constraint_customer_id !=0 && id_customer != constraint_customer_id){
                return  badRequest("Invalid user for this voucher");
            }else {
                //get the promotion code ID from database
                int id_cart_rule;
                try {
                    id_cart_rule = CartRule.findByCode(promotion_code);
                } catch (Exception e) {
                    id_cart_rule = 0;
                }
                //第三层条件过滤， 是否存在这个vouher
                if (id_cart_rule == 0) {
                    return badRequest("No such promotion code");
                } else {
                    //see if this code is already used up by this user
                    int used_time = getUsedTimes(id_cart_rule, id_customer);
//                if (used_time != 0) {
//                    return badRequest("promotion code already used");
//                } else {
                    ArrayList<Integer> products_list = new ArrayList<Integer>();
                    ArrayList<CodeValidation> total_record = new ArrayList<CodeValidation>();
                    try {
                        String sql =
                                "select ps_cart_rule_lang.name, ps_cart_rule.id_cart_rule, ps_cart_rule.description, ps_cart_rule.quantity_per_user, ps_cart_rule.minimum_amount, ps_cart_rule.minimum_amount_shipping, ps_cart_rule.free_shipping, ps_cart_rule.reduction_percent, ps_cart_rule.reduction_amount,ps_cart_rule.reduction_product, ps_cart_rule.group_restriction,ps_cart_rule_product_rule_group.quantity, ps_cart_rule_product_rule_value.id_item\n" +
                                        "\n" +
                                        "from ps_cart_rule_lang\n" +
                                        "\n" +
                                        "left join ps_cart_rule\n" +
                                        "on ps_cart_rule_lang.id_cart_rule = ps_cart_rule.id_cart_rule\n" +
                                        "\n" +
                                        "left join ps_cart_rule_product_rule_group\n" +
                                        "on ps_cart_rule.id_cart_rule = ps_cart_rule_product_rule_group.id_cart_rule\n" +
                                        "\n" +
                                        "left join ps_cart_rule_product_rule\n" +
                                        "on ps_cart_rule_product_rule_group.id_product_rule_group = ps_cart_rule_product_rule.id_product_rule_group \n" +
                                        "\n" +
                                        "left join ps_cart_rule_product_rule_value\n" +
                                        "on ps_cart_rule_product_rule.id_product_rule = ps_cart_rule_product_rule_value.id_product_rule\n" +
                                        "\n" +
                                        "where ps_cart_rule.id_customer = " + constraint_customer_id+ "\n" +   //加上限制用户
                                        "\n" +
                                        "and ps_cart_rule.date_from < now()\n" +
                                        "\n" +
                                        "and ps_cart_rule.date_to > now()\n" +
                                        "\n" +
                                        "and ps_cart_rule.quantity > 0\n" +
                                        "\n" +
                                        "and ps_cart_rule.active = 1\n" +
                                        "\n" +
                                        "and ps_cart_rule.id_cart_rule =" + id_cart_rule;
                        RawSql rawSql =
                                RawSqlBuilder
                                        .parse(sql)
                                        .columnMapping("ps_cart_rule_lang.name", "promotion_name")
                                        .columnMapping("ps_cart_rule.id_cart_rule", "id_cart_rule")
                                        .columnMapping("ps_cart_rule.description", "description")
                                        .columnMapping("ps_cart_rule.quantity_per_user", "quantity_per_user")
                                        .columnMapping("ps_cart_rule.minimum_amount", "minimum_amount")
                                        .columnMapping("ps_cart_rule.minimum_amount_shipping", "minimum_amount_if_shipping")
                                        .columnMapping("ps_cart_rule.free_shipping", "if_free_shipping")
                                        .columnMapping("ps_cart_rule.reduction_percent", "reduction_percent")
                                        .columnMapping("ps_cart_rule.reduction_amount", "reduction_amount")
                                        .columnMapping("ps_cart_rule.reduction_product", "reduction_product")
                                        .columnMapping("ps_cart_rule.group_restriction", "group_restriction")
                                        .columnMapping("ps_cart_rule_product_rule_group.quantity", "minimum_product_quantity_required")
                                        .columnMapping("ps_cart_rule_product_rule_value.id_item", "if_valid")  //存在产品要求
                                        .create();
                        com.avaje.ebean.Query<CodeValidation> query = Ebean.find(CodeValidation.class);
                        query.setRawSql(rawSql);
                        List<CodeValidation> list = query.findList();
                        total_record.addAll(list);
                    } catch (Exception e) {
                        String sql =
                                "select ps_cart_rule_lang.name, ps_cart_rule.id_cart_rule, ps_cart_rule.description, ps_cart_rule.quantity_per_user, ps_cart_rule.minimum_amount, ps_cart_rule.minimum_amount_shipping, ps_cart_rule.free_shipping, ps_cart_rule.reduction_percent, ps_cart_rule.reduction_amount,ps_cart_rule.reduction_product, ps_cart_rule.group_restriction\n" +
                                        "\n" +
                                        "from ps_cart_rule_lang\n" +
                                        "\n" +
                                        "left join ps_cart_rule\n" +
                                        "on ps_cart_rule_lang.id_cart_rule = ps_cart_rule.id_cart_rule\n" +
                                        "\n" +
                                        "left join ps_cart_rule_product_rule_group\n" +
                                        "on ps_cart_rule.id_cart_rule = ps_cart_rule_product_rule_group.id_cart_rule\n" +
                                        "\n" +
                                        "left join ps_cart_rule_product_rule\n" +
                                        "on ps_cart_rule_product_rule_group.id_product_rule_group = ps_cart_rule_product_rule.id_product_rule_group \n" +
                                        "\n" +
                                        "left join ps_cart_rule_product_rule_value\n" +
                                        "on ps_cart_rule_product_rule.id_product_rule = ps_cart_rule_product_rule_value.id_product_rule\n" +
                                        "\n" +
                                        "where ps_cart_rule.id_customer = " + constraint_customer_id+ "\n" +   //加上限制用户
                                        "\n" +
                                        "and ps_cart_rule.date_from < now()\n" +
                                        "\n" +
                                        "and ps_cart_rule.date_to > now()\n" +
                                        "\n" +
                                        "and ps_cart_rule.quantity > 0\n" +
                                        "\n" +
                                        "and ps_cart_rule.active = 1\n" +
                                        "\n" +
                                        "and ps_cart_rule.id_cart_rule =" + id_cart_rule;
                        RawSql rawSql =
                                RawSqlBuilder
                                        .parse(sql)
                                        .columnMapping("ps_cart_rule_lang.name", "promotion_name")
                                        .columnMapping("ps_cart_rule.id_cart_rule", "id_cart_rule")
                                        .columnMapping("ps_cart_rule.description", "description")
                                        .columnMapping("ps_cart_rule.quantity_per_user", "quantity_per_user")
                                        .columnMapping("ps_cart_rule.minimum_amount", "minimum_amount")
                                        .columnMapping("ps_cart_rule.minimum_amount_shipping", "minimum_amount_if_shipping")
                                        .columnMapping("ps_cart_rule.free_shipping", "if_free_shipping")
                                        .columnMapping("ps_cart_rule.reduction_percent", "reduction_percent")
                                        .columnMapping("ps_cart_rule.reduction_amount", "reduction_amount")
                                        .columnMapping("ps_cart_rule.reduction_product", "reduction_product")
                                        .columnMapping("ps_cart_rule.group_restriction", "group_restriction")
                                        .create();
                        com.avaje.ebean.Query<CodeValidation> query = Ebean.find(CodeValidation.class);
                        query.setRawSql(rawSql);
                        List<CodeValidation> list = query.findList();
                        total_record.addAll(list);
                    }
                    int number_of_products = total_record.size();
                    //第四层条件过滤，voucher在有效时间以内
                    if (number_of_products != 0) { //SQL 返回值
                        CodeValidation sample = total_record.get(0); //拿到第一条记录作为样本Voucher
                        int code_used_time = sample.quantity_per_user;  //times of usage of each person
                        //第五层条件过滤， 没有超过使用上限
                        if (used_time < code_used_time) {
                            if (sample.if_valid != 0) {  //存在限定产品
                                for (CodeValidation aList : total_record) {
                                    int product_id = aList.if_valid;
                                    products_list.add(product_id);
                                }
                            }
                            // get customer group id
                            ArrayList<Integer> customer_group_id = new ArrayList<>();
                            //            int id_group;
                            List<CustomerGroup> all_group = CustomerGroup.findByCustomer(id_customer);
                            if (all_group != null) {
                                for (CustomerGroup this_one : all_group) {
                                    customer_group_id.add(this_one.id_group);
                                }
                            }
                            int if_group_restriction = sample.group_restriction;
                            if (if_group_restriction == 1) {   //user group constraint， 用户群组限定
                                List<CartRuleGroup> all_record = CartRuleGroup.findByCartRule(id_cart_rule); //找到这个打折码的限定用户群
                                List<Integer> group_list = new ArrayList<>();
                                int length = all_record.size();
                                for (int a = 0; a < length; a++) {
                                    group_list.add(all_record.get(a).id_group);
                                }
                                sample.if_valid = 0;
                                for (Integer this_one : group_list) {
                                    for (Integer that_one : customer_group_id) {
                                        if (this_one == that_one) {
                                            sample.if_valid = 1;
                                        }

                                    }
                                }
                            } else {
                                sample.if_valid = 1;
                            }
                            //第六层条件过滤，存在限定用户群组的情况下，用户不在特定群组里面
                            if (sample.if_valid == 0) {
                                return badRequest("Invalid User");
                            } else {
                                sample.promotion_products = products_list;
                                return ok(renderCategories(sample));
                            }
                        } else {
                            return badRequest("Exceeded maximum times of use.");
                            //                            Achieved maximum usage already
                            //                            Sorry, free dishes are sold out!
                        }
                    } else {
                        return badRequest("Voucher expired already.");
//                        Code expired already
                    }
//                }
                }
            }
        }
    }

    public static Result newCodeValidate() {
        response().setHeader("Connection","close");
        DynamicForm order_driver = Form.form().bindFromRequest();
        System.out.println("Start to validate");
        int id_customer;
        String promotion_code;
        try {
            id_customer = Integer.parseInt(order_driver.get("id_customer"));
            promotion_code = order_driver.get("promotion_code");
            promotion_code = promotion_code.replaceAll(" ", ""); //去掉传递过来的空格
        } catch (Exception e) {
            return badRequest("Wrong request format");
        }
        //第一层条件过滤， 用户是否登录之后再验证
        if (id_customer == 0){
            return  badRequest("Please log in");
        }else {
            int constraint_customer_id;
            try {
                constraint_customer_id = CartRule.getConstraintUser(promotion_code);
            } catch (Exception e) {
                constraint_customer_id = 0;
            }
            //第二层条件过滤， Voucher是否绑定特定用户才能使用
            if(constraint_customer_id !=0 && id_customer != constraint_customer_id){
                return  badRequest("Invalid user for this voucher");
            }else {
                //get the promotion code ID from database
                int id_cart_rule;
                try {
                    id_cart_rule = CartRule.findByCode(promotion_code);
                } catch (Exception e) {
                    id_cart_rule = 0;
                }
                //第三层条件过滤， 是否存在这个vouher
                if (id_cart_rule == 0) {
                    return badRequest("No such promotion code");
                } else {
                    //see if this code is already used up by this user
                    int used_time = getUsedTimes(id_cart_rule, id_customer);
//                if (used_time != 0) {
//                    return badRequest("promotion code already used");
//                } else {
                    ArrayList<CodeValidateTool> products_list = new ArrayList<CodeValidateTool>();
                    ArrayList<NewCodeValidation> total_record = new ArrayList<NewCodeValidation>();
                    try {
                        String sql =
                                "select ps_cart_rule_lang.name, ps_cart_rule.id_cart_rule, ps_cart_rule.description, ps_cart_rule.quantity_per_user, ps_cart_rule.minimum_amount, ps_cart_rule.minimum_amount_shipping, ps_cart_rule.free_shipping, ps_cart_rule.reduction_percent, ps_cart_rule.reduction_amount,ps_cart_rule.reduction_product, ps_cart_rule.product_restriction,ps_cart_rule.group_restriction,ps_cart_rule_product_rule_group.quantity, ps_cart_rule_product_rule.type ,ps_cart_rule_product_rule_value.id_item\n" +
                                        "\n" +
                                        "from ps_cart_rule_lang\n" +
                                        "\n" +
                                        "left join ps_cart_rule\n" +
                                        "on ps_cart_rule_lang.id_cart_rule = ps_cart_rule.id_cart_rule\n" +
                                        "\n" +
                                        "left join ps_cart_rule_product_rule_group\n" +
                                        "on ps_cart_rule.id_cart_rule = ps_cart_rule_product_rule_group.id_cart_rule\n" +
                                        "\n" +
                                        "left join ps_cart_rule_product_rule\n" +
                                        "on ps_cart_rule_product_rule_group.id_product_rule_group = ps_cart_rule_product_rule.id_product_rule_group \n" +
                                        "\n" +
                                        "left join ps_cart_rule_product_rule_value\n" +
                                        "on ps_cart_rule_product_rule.id_product_rule = ps_cart_rule_product_rule_value.id_product_rule\n" +
                                        "\n" +
                                        "where ps_cart_rule.id_customer = " + constraint_customer_id+ "\n" +   //加上限制用户
                                        "\n" +
                                        "and ps_cart_rule.date_from < now()\n" +
                                        "\n" +
                                        "and ps_cart_rule.date_to > now()\n" +
                                        "\n" +
                                        "and ps_cart_rule.quantity > 0\n" +
                                        "\n" +
                                        "and ps_cart_rule.active = 1\n" +
                                        "\n" +
                                        "and ps_cart_rule.id_cart_rule =" + id_cart_rule;
                        RawSql rawSql =
                                RawSqlBuilder
                                        .parse(sql)
                                        .columnMapping("ps_cart_rule_lang.name", "promotion_name")
                                        .columnMapping("ps_cart_rule.id_cart_rule", "id_cart_rule")
                                        .columnMapping("ps_cart_rule.description", "description")
                                        .columnMapping("ps_cart_rule.quantity_per_user", "quantity_per_user")
                                        .columnMapping("ps_cart_rule.minimum_amount", "minimum_amount")
                                        .columnMapping("ps_cart_rule.minimum_amount_shipping", "minimum_amount_if_shipping")
                                        .columnMapping("ps_cart_rule.free_shipping", "if_free_shipping")
                                        .columnMapping("ps_cart_rule.reduction_percent", "reduction_percent")
                                        .columnMapping("ps_cart_rule.reduction_amount", "reduction_amount")
                                        .columnMapping("ps_cart_rule.reduction_product", "reduction_product") //0,-1,-2, 或者产品ID
                                        //new
                                        .columnMapping("ps_cart_rule.product_restriction", "product_restriction")
                                        .columnMapping("ps_cart_rule.group_restriction", "group_restriction")
                                        .columnMapping("ps_cart_rule_product_rule_group.quantity", "minimum_product_quantity_required")
                                        //new
                                        .columnMapping("ps_cart_rule_product_rule.type", "type")
                                        .columnMapping("ps_cart_rule_product_rule_value.id_item", "if_valid")  //存在产品要求
                                        .create();
                        com.avaje.ebean.Query<NewCodeValidation> query = Ebean.find(NewCodeValidation.class);
                        query.setRawSql(rawSql);
                        List<NewCodeValidation> list = query.findList();
                        total_record.addAll(list);
                    } catch (Exception e) {
                        String sql =
                                "select ps_cart_rule_lang.name, ps_cart_rule.id_cart_rule, ps_cart_rule.description, ps_cart_rule.quantity_per_user, ps_cart_rule.minimum_amount, ps_cart_rule.minimum_amount_shipping, ps_cart_rule.free_shipping, ps_cart_rule.reduction_percent, ps_cart_rule.reduction_amount,ps_cart_rule.reduction_product, ps_cart_rule.product_restriction,ps_cart_rule.group_restriction\n" +
                                        "\n" +
                                        "from ps_cart_rule_lang\n" +
                                        "\n" +
                                        "left join ps_cart_rule\n" +
                                        "on ps_cart_rule_lang.id_cart_rule = ps_cart_rule.id_cart_rule\n" +
                                        "\n" +
                                        "left join ps_cart_rule_product_rule_group\n" +
                                        "on ps_cart_rule.id_cart_rule = ps_cart_rule_product_rule_group.id_cart_rule\n" +
                                        "\n" +
                                        "left join ps_cart_rule_product_rule\n" +
                                        "on ps_cart_rule_product_rule_group.id_product_rule_group = ps_cart_rule_product_rule.id_product_rule_group \n" +
                                        "\n" +
                                        "left join ps_cart_rule_product_rule_value\n" +
                                        "on ps_cart_rule_product_rule.id_product_rule = ps_cart_rule_product_rule_value.id_product_rule\n" +
                                        "\n" +
                                        "where ps_cart_rule.id_customer = " + constraint_customer_id+ "\n" +   //加上限制用户
                                        "\n" +
                                        "and ps_cart_rule.date_from < now()\n" +
                                        "\n" +
                                        "and ps_cart_rule.date_to > now()\n" +
                                        "\n" +
                                        "and ps_cart_rule.quantity > 0\n" +
                                        "\n" +
                                        "and ps_cart_rule.active = 1\n" +
                                        "\n" +
                                        "and ps_cart_rule.id_cart_rule =" + id_cart_rule;
                        RawSql rawSql =
                                RawSqlBuilder
                                        .parse(sql)
                                        .columnMapping("ps_cart_rule_lang.name", "promotion_name")
                                        .columnMapping("ps_cart_rule.id_cart_rule", "id_cart_rule")
                                        .columnMapping("ps_cart_rule.description", "description")
                                        .columnMapping("ps_cart_rule.quantity_per_user", "quantity_per_user")
                                        .columnMapping("ps_cart_rule.minimum_amount", "minimum_amount")
                                        .columnMapping("ps_cart_rule.minimum_amount_shipping", "minimum_amount_if_shipping")
                                        .columnMapping("ps_cart_rule.free_shipping", "if_free_shipping")
                                        .columnMapping("ps_cart_rule.reduction_percent", "reduction_percent")
                                        .columnMapping("ps_cart_rule.reduction_amount", "reduction_amount")
                                        .columnMapping("ps_cart_rule.reduction_product", "reduction_product")
                                        //todo add product_restriction
                                        .columnMapping("ps_cart_rule.product_restriction", "product_restriction")
                                        .columnMapping("ps_cart_rule.group_restriction", "group_restriction")
                                        .create();
                        com.avaje.ebean.Query<NewCodeValidation> query = Ebean.find(NewCodeValidation.class);
                        query.setRawSql(rawSql);
                        List<NewCodeValidation> list = query.findList();
                        total_record.addAll(list);
                    }
                    int number_of_products = total_record.size();
                    //第四层条件过滤，voucher在有效时间以内
                    if (number_of_products != 0) { //SQL 返回值
                        NewCodeValidation sample = total_record.get(0); //拿到第一条记录作为样本Voucher
                        int code_used_time = sample.quantity_per_user;  //times of usage of each person
                        //第五层条件过滤， 没有超过使用上限
                        if (used_time < code_used_time) {
                            if (sample.if_valid != 0) {  //存在限定产品
                                for (NewCodeValidation aList : total_record) {
                                    String product_type = aList.type;
                                    int product_id = aList.if_valid;
                                    CodeValidateTool new_constraint = new CodeValidateTool(product_type,product_id);
                                    products_list.add(new_constraint);
                                }
                            }
                            // get customer group id
                            ArrayList<Integer> customer_group_id = new ArrayList<>();
                            //            int id_group;
                            List<CustomerGroup> all_group = CustomerGroup.findByCustomer(id_customer);
                            if (all_group != null) {
                                for (CustomerGroup this_one : all_group) {
                                    customer_group_id.add(this_one.id_group);
                                }
                            }
                            int if_group_restriction = sample.group_restriction;
                            if (if_group_restriction == 1) {   //user group constraint， 用户群组限定
                                List<CartRuleGroup> all_record = CartRuleGroup.findByCartRule(id_cart_rule); //找到这个打折码的限定用户群
                                List<Integer> group_list = new ArrayList<>();
                                int length = all_record.size();
                                for (int a = 0; a < length; a++) {
                                    group_list.add(all_record.get(a).id_group);
                                }
                                sample.if_valid = 0;
                                for (Integer this_one : group_list) {
                                    for (Integer that_one : customer_group_id) {
                                        if (this_one == that_one) {
                                            sample.if_valid = 1;
                                        }

                                    }
                                }
                            } else {
                                sample.if_valid = 1;
                            }
                            //第六层条件过滤，存在限定用户群组的情况下，用户不在特定群组里面
                            if (sample.if_valid == 0) {
                                return badRequest("Invalid User");
                            } else {
                                sample.products_constraint = products_list;
                                return ok(renderCategories(sample));
                            }
                        } else {
                            return badRequest("Exceeded maximum times of use.");
                            //                            Achieved maximum usage already
                            //                            Sorry, free dishes are sold out!
                        }
                    } else {
                        return badRequest("Voucher expired already.");
//                        Code expired already
                    }
//                }
                }
            }
        }
    }

    public static Result showAllOrderCart() {
        response().setHeader("Connection","close");
        List<OrderCartRule> all_record = OrderCartRule.findAll();
        return ok(renderCategories(all_record));
    }

    //自动打印订单，返回所有订单 10 秒被访问一次
    @Transactional
    public static Result printOrders() {
        response().setHeader("Connection","close");
        System.out.println("start to return +++++++");
//        ArrayList<ReturnDriverOrders> driver_orders = getDriverOrders(); //数据库调用
        ArrayList<ReturnDriverOrders> driver_orders = new ArrayList<>(); //数据库调用
        List<Order> print_orders = Order.getPrintOrders();
        List<TotalPrintOrder> returnOrders = new ArrayList<>();
        List<Integer> orderID = new ArrayList<>();
        List<OrderPrintHistory> past_record = OrderPrintHistory.getLastHour();
        int size = past_record.size();
        for (int i = 0; i<size;i++){
            orderID.add(past_record.get(i).id_order);
        }
        //create a new list of orders using a pre-defined template
        for (Order order : print_orders) {
            int order_id = order.id_order;
            int customer_id = order.id_customer;
            List<TotalPrintProduct> bought_products = new ArrayList<>();                // create a template list of products info, which will be returned
            int address_id = order.id_address_delivery;              //Address information of each order
            AddressTemplate delivery_address;                           //Define an object using AddressTemplate for later return
            if (address_id != 0) {                                      //If there is an address
                Address address = Address.findById(address_id);         //Find the address in the table of Address
                String alias = address.alias;
                String company = address.company;
                String firstname = address.firstname;
                String lastname = address.lastname;
                String address1 = address.address1;
                String address2 = address.address2;
                String city = address.city;
                String phone = address.phone;
                delivery_address = new AddressTemplate(
                        alias,
                        company,
                        firstname,
                        lastname,
                        address1,
                        address2,
                        city,
                        phone
                );
            } else {
                delivery_address = null;
            }
            String payment = order.payment;
            String reference = order.reference;
            String order_message;
            OrderMessage message = OrderMessage.findByID(order_id);
            if(message != null){
                order_message = message.message;
            }else {
                order_message = "";
            }
            int order_sequence = getSequenceNumber(order_id);
            List<OrderDetail> detail_products = OrderDetail.findByOrder(order_id);
            // get all the products detail from table ps_order_detail
            for (OrderDetail this_product : detail_products) {
                String product_location = "";
                if(this_product.product_upc.equals("")) {  //如果是shop产品
                    ProductLocation this_location = ProductLocation.findByProduct(this_product.product_id);
                    if (this_location != null) {
                        product_location = this_location.zone + "-" + this_location.layer + "-" + this_location.part + this_location.position_number;
                    }
                }
                TotalPrintProduct printProduct = new TotalPrintProduct(
                        this_product.product_id,
                        this_product.id_order_detail,
                        this_product.product_upc,
                        this_product.product_name,
                        this_product.product_reference,
                        product_location, //new
                        this_product.product_quantity,
                        this_product.total_price_tax_incl
                );
                bought_products.add(printProduct);
            }
            TotalPrintOrder final_order = new TotalPrintOrder(
            order_id,
            customer_id,
            order_sequence,
            reference,
            payment,
            order_message,
            order.total_discounts_tax_incl,
            order.total_discounts_tax_excl,
            order.total_paid_real,
            order.total_paid_tax_excl,
            order.total_products,
            order.total_products_wt,
            order.total_shipping,
            order.date_add,
            delivery_address,
            bought_products,
            ""
            );
            if (!orderID.contains(order_id)){
            returnOrders.add(final_order);
            OrderPrintHistory new_record = new OrderPrintHistory(order_id,order_sequence,new Date());
            new_record.save();   //自动打印，生成新记录存入已打印列表
            }                                      //Keep adding orders into the ReturnOrder list
        }
        TotalPrintWithDriverOrders super_total = new TotalPrintWithDriverOrders(
                returnOrders,
                driver_orders
        );
        System.out.println("orders returned"+ "----------"+ returnOrders.size());
        return ok(new JSONSerializer().deepSerialize(super_total));
    }

    public static Result allPrintOrders() {
        response().setHeader("Connection","close");
        List<OrderPrintHistory> print_orders = OrderPrintHistory.findAll();
        return ok(renderCategories(print_orders));
    }
    public static Result getOneHour(){
        response().setHeader("Connection","close");
        List<Integer> orderID = new ArrayList<>();
        List<OrderPrintHistory> past_record = OrderPrintHistory.getLastHour();
        int size = past_record.size();
        for (int i = 0; i<size;i++){
            orderID.add(past_record.get(i).id_order);
        }
        return ok(renderCategories(orderID));
    }

    //手动重新打印一张订单
    public static Result reprint(int orderID){
        response().setHeader("Connection","close");
        System.out.println("start to return +++++++");
        Order new_print = Order.findById(orderID);
        //create a new list of orders using a pre-defined template
            int order_id = new_print.id_order;
            int customer_id = new_print.id_customer;
            List<TotalPrintProduct> bought_products = new ArrayList<>();                // create a template list of products info, which will be returned
            List<OrderDetail> detail_products = OrderDetail.findByOrder(order_id);
            // get all the products detail from table ps_order_detail
            for (OrderDetail this_product : detail_products) {
                String product_location = "";
                if(this_product.product_upc.equals("")) {
                    ProductLocation this_location = ProductLocation.findByProduct(this_product.product_id);
                    if (this_location != null) {
                        product_location = this_location.zone + "-" + this_location.layer + "-" + this_location.part + this_location.position_number;
                    }
                }

                TotalPrintProduct printProduct = new TotalPrintProduct(
                        this_product.product_id,
                        this_product.id_order_detail,
                        this_product.product_upc,
                        this_product.product_name,
                        this_product.product_reference,
                        product_location, //new
                        this_product.product_quantity,
                        this_product.total_price_tax_incl
                );
                bought_products.add(printProduct);
            }
            int address_id = new_print.id_address_delivery;              //Address information of each order
            AddressTemplate delivery_address;                           //Define an object using AddressTemplate for later return
            if (address_id != 0) {                                      //If there is an address
                Address address = Address.findById(address_id);         //Find the address in the table of Address
                String alias = address.alias;
                String company = address.company;
                String firstname = address.firstname;
                String lastname = address.lastname;
                String address1 = address.address1;
                String address2 = address.address2;
                String city = address.city;
                String phone = address.phone;
                delivery_address = new AddressTemplate(
                        alias,
                        company,
                        firstname,
                        lastname,
                        address1,
                        address2,
                        city,
                        phone
                );
            } else {
                delivery_address = null;
            }
            String payment = new_print.payment;
            String reference = new_print.reference;
            String order_message;
            OrderMessage message = OrderMessage.findByID(order_id);
            if(message != null){
                order_message = message.message;
            }else {
                order_message = "";
            }
            int order_sequence = getSequenceNumber(order_id);
            int order_state = new_print.current_state;
            String current_state;
            switch (order_state) {
                case 1:  current_state = "Awaiting check payment";
                    break;
                case 2:  current_state = "Payment accepted";
                    break;
                case 3:  current_state = "Processing in Progress";
                    break;
                case 4:  current_state = "shipped";
                    break;
                case 5:  current_state = "Delivered";
                    break;
                case 6:  current_state = "Cancelled";
                    break;
                case 11:  current_state = "Refunded";
                    break;
                default: current_state = "Processing in Progress";
                    break;
            }
            TotalPrintOrder final_order = new TotalPrintOrder(
                    order_id,
                    customer_id,
                    order_sequence,
                    reference,
                    payment,
                    order_message,
                    new_print.total_discounts_tax_incl,
                    new_print.total_discounts_tax_excl,
                    new_print.total_paid_real,
                    new_print.total_paid_tax_excl,
                    new_print.total_products,
                    new_print.total_products_wt,
                    new_print.total_shipping,
                    new_print.date_add,
                    delivery_address,
                    bought_products,
                    current_state
            );
        System.out.println("orders returned"+ "----------");
        return ok(new JSONSerializer().deepSerialize(final_order));
    }

    /**
     * All the orders of the day
     *一天做的所有的单
     * */
    public static Result reprintAll() throws Exception {
        response().setHeader("Connection","close");
        List<OrderWithSequence> orders_with_sequence =getAllOrders();
        return ok(renderCategories(orders_with_sequence));
    }

    //得到所有订单
    public static List<OrderWithSequence> getAllOrders(){
        String sql =
                "select ps_orders.id_order, ps_orders.reference, ps_orders.id_customer, ps_orders.payment,ps_orders.total_discounts, ps_orders.total_paid, ps_orders.total_shipping, ps_orders.date_add, ps_orders.delivery_date, ps_order_state_lang.name, ps_order_invoice.delivery_address\n" +
                        "from ps_orders, ps_order_state_lang, ps_order_invoice\n" +
                        "where ps_orders.current_state = ps_order_state_lang.id_order_state\n" +
                        "and ps_orders.id_order = ps_order_invoice.id_order\n" +
                        "and ps_orders.valid = 1\n" +
                        "and ps_orders.invoice_date > DATE_SUB(NOW(), INTERVAL 13 HOUR)\n" +
                        "order by ps_orders.id_order desc";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_orders.id_order", "id_order")
                        .columnMapping("ps_orders.reference", "order_reference")
                        .columnMapping("ps_orders.id_customer", "id_customer")
                        .columnMapping("ps_orders.payment", "payment_method")
                        .columnMapping("ps_orders.total_discounts", "total_discounts")
                        .columnMapping("ps_orders.total_paid", "total_paid")
                        .columnMapping("ps_orders.total_shipping", "total_shipping")
                        .columnMapping("ps_orders.date_add", "date_add")
                        .columnMapping("ps_orders.delivery_date", "delivery_date")
                        .columnMapping("ps_order_state_lang.name", "order_state")
                        .columnMapping("ps_order_invoice.delivery_address", "delivery_address")
                        .create();
        com.avaje.ebean.Query<OrderWithSequence> query = Ebean.find(OrderWithSequence.class);
        query.setRawSql(rawSql);
        List<OrderWithSequence> list = query.findList();
        for(OrderWithSequence each_one: list){
            each_one.order_sequence = getSequenceNumber(each_one.id_order);
        }
        return list;
    }


    //手动打单，返回所有订单
    public static Result manualPrint() {
        response().setHeader("Connection","close");
        System.out.println("start to return +++++++");
//        ArrayList<ReturnDriverOrders> driver_orders = getDriverOrders();
        ArrayList<ReturnDriverOrders> driver_orders = new ArrayList<>(); //数据库调用
        List<Order> print_orders = Order.getManualPrintOrders();
        List<TotalPrintOrder> returnOrders = new ArrayList<>();
//        List orderID = OrderPrintHistory.getLastHour();
        List<Integer> orderID = new ArrayList<>();
        List<OrderPrintHistory> past_record = OrderPrintHistory.getLastHour();
        int size = past_record.size();
        for (int i = 0; i<size;i++){
            orderID.add(past_record.get(i).id_order);
        }
        //create a new list of orders using a pre-defined template
        for (Order order : print_orders) {
            int order_id = order.id_order;
            int customer_id = order.id_customer;
            List<TotalPrintProduct> bought_products = new ArrayList<>();                // create a template list of products info, which will be returned
            List<OrderDetail> detail_products = OrderDetail.findByOrder(order_id);
            // get all the products detail from table ps_order_detail
            for (OrderDetail this_product : detail_products) {
                String product_location = "";
                if(this_product.product_upc.equals("")) {
                    ProductLocation this_location = ProductLocation.findByProduct(this_product.product_id);
                    if (this_location != null) {
                    product_location = this_location.zone + "-" + this_location.layer + "-" + this_location.part + this_location.position_number;
                    }
                }

                TotalPrintProduct printProduct = new TotalPrintProduct(
                        this_product.product_id,
                        this_product.id_order_detail,
                        this_product.product_upc,
                        this_product.product_name,
                        this_product.product_reference,
                        product_location,  //new
                        this_product.product_quantity,
                        this_product.total_price_tax_incl
                );
                bought_products.add(printProduct);
            }
            int address_id = order.id_address_delivery;              //Address information of each order
            AddressTemplate delivery_address;                           //Define an object using AddressTemplate for later return
            if (address_id != 0) {                                      //If there is an address
                Address address = Address.findById(address_id);         //Find the address in the table of Address
                String alias = address.alias;
                String company = address.company;
                String firstname = address.firstname;
                String lastname = address.lastname;
                String address1 = address.address1;
                String address2 = address.address2;
                String city = address.city;
                String phone = address.phone;
                delivery_address = new AddressTemplate(
                        alias,
                        company,
                        firstname,
                        lastname,
                        address1,
                        address2,
                        city,
                        phone
                );
            } else {
                delivery_address = null;
            }
            String payment = order.payment;
            String reference = order.reference;
            String order_message;
            OrderMessage message = OrderMessage.findByID(order_id);
            if(message != null){
                order_message = message.message;
            }else {
                order_message = "";
            }
            int order_sequence = getSequenceNumber(order_id);
            int order_state = order.current_state;
            String current_state;
            switch (order_state) {
                case 1:  current_state = "Awaiting check payment";
                    break;
                case 2:  current_state = "Payment accepted";
                    break;
                case 3:  current_state = "Processing in Progress";
                    break;
                case 4:  current_state = "shipped";
                    break;
                case 5:  current_state = "Delivered";
                    break;
                case 6:  current_state = "Cancelled";
                    break;
                case 11:  current_state = "Refunded";
                    break;
                default: current_state = "Processing in Progress";
                    break;
            }
            TotalPrintOrder final_order = new TotalPrintOrder(
                    order_id,
                    customer_id,
                    order_sequence,
                    reference,
                    payment,
                    order_message,
                    order.total_discounts_tax_incl,
                    order.total_discounts_tax_excl,
                    order.total_paid_real,
                    order.total_paid_tax_excl,
                    order.total_products,
                    order.total_products_wt,
                    order.total_shipping,
                    order.date_add,
                    delivery_address,
                    bought_products,
                    current_state
            );
            if (!orderID.contains(order_id)){
                returnOrders.add(final_order);
//                OrderPrintHistory new_record = new OrderPrintHistory(order_id,order_sequence,new Date());
//                new_record.save();
            }                                      //Keep adding orders into the ReturnOrder list
        }
        System.out.println("orders returned"+ "----------"+ returnOrders.size());
        TotalPrintWithDriverOrders all_in_one = new TotalPrintWithDriverOrders(
                returnOrders,
                driver_orders
        );
        return ok(new JSONSerializer().deepSerialize(all_in_one));
    }

    //手动添加打印记录
    public static Result manualAddHistory() throws Exception {
        response().setHeader("Connection","close");
        DynamicForm printForm = Form.form().bindFromRequest();
        int order_id = Integer.valueOf(printForm.get("order_id"));
        int order_sequence = Integer.valueOf(printForm.get("order_sequence"));

        if(OrderPrintHistory.findById(order_id) == null){
        OrderPrintHistory new_record = new OrderPrintHistory(order_id,order_sequence,new Date());
        new_record.save();
        System.out.println("new record ");
        return ok("new record added");}
        else {
            System.out.println("already printed ");
            return  ok("already printed before");
        }
    }

    /** Get a group of order numbers
     */
    public static ArrayList<ProductQuantity> getOrderIDs(String phoneNumber) {
        try{
        String sql =
                "select distinct ps_orders.id_order\n" +
                        "\n" +
                        "from ps_orders, ps_address \n" +
                        "\n" +
                        "where ps_orders.id_address_delivery = ps_address.id_address\n" +
                        "\n" +
                        "and ps_address.phone like" + "'%" + phoneNumber + "%'\n" +
                        "\n" +
                        "and ps_orders.invoice_date > DATE_SUB(NOW(), INTERVAL 12 HOUR)\n" +
                        "\n" +
                        "order by ps_orders.id_order";

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_orders.id_order", "quantity")
                        .create();

        com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
        query.setRawSql(rawSql);
        List<ProductQuantity> list = query.findList();
        ArrayList<ProductQuantity> orderIDs = new ArrayList<ProductQuantity>();
        orderIDs.addAll(list);                  //list to Array
        return orderIDs;

        }catch (Exception e){
            ArrayList<ProductQuantity> orderIDs = new ArrayList<ProductQuantity>();
            return orderIDs;
        }
    }
   //根据电话号码查找订单
    public static Result quickSearchOrders(String phoneNumber) {
        response().setHeader("Connection","close");
        System.out.println("start to return +++++++");
        phoneNumber = phoneNumber.replaceAll(" ",""); //去掉空格
        ArrayList<ProductQuantity> orderIDs = getOrderIDs(phoneNumber);
        List<TotalPrintOrder> returnOrders = new ArrayList<>();
        for(ProductQuantity id: orderIDs){
            int order_id = id.quantity;
            Order order = Order.findById(order_id);
            List<TotalPrintProduct> bought_products = new ArrayList<>();                // create a template list of products info, which will be returned
            List<OrderDetail> detail_products = OrderDetail.findByOrder(order_id);
            // get all the products detail from table ps_order_detail
            for (OrderDetail this_product : detail_products) {
                TotalPrintProduct printProduct = new TotalPrintProduct(
                        this_product.product_id,
                        this_product.id_order_detail,
                        this_product.product_upc,
                        this_product.product_name,
                        this_product.product_reference,
                        "",
                        this_product.product_quantity,
                        this_product.total_price_tax_incl
                );
                bought_products.add(printProduct);
            }
            int address_id = order.id_address_delivery;              //Address information of each order
            AddressTemplate delivery_address;                           //Define an object using AddressTemplate for later return
            if (address_id != 0) {                                      //If there is an address
                Address address = Address.findById(address_id);         //Find the address in the table of Address
                String alias = address.alias;
                String company = address.company;
                String firstname = address.firstname;
                String lastname = address.lastname;
                String address1 = address.address1;
                String address2 = address.address2;
                String city = address.city;
                String phone = address.phone;
                delivery_address = new AddressTemplate(
                        alias,
                        company,
                        firstname,
                        lastname,
                        address1,
                        address2,
                        city,
                        phone
                );
            } else {
                delivery_address = null;
            }
            String payment = order.payment;
            String reference = order.reference;
            String order_message;
            OrderMessage message = OrderMessage.findByID(order_id);
            if(message != null){
                order_message = message.message;
            }else {
                order_message = "";
            }
            int order_sequence = getSequenceNumber(order_id);
            int order_state = order.current_state;
            String current_state;
            switch (order_state) {
                case 1:  current_state = "Awaiting check payment";
                    break;
                case 2:  current_state = "Payment accepted";
                    break;
                case 3:  current_state = "Processing in Progress";
                    break;
                case 4:  current_state = "shipped";
                    break;
                case 5:  current_state = "Delivered";
                    break;
                case 6:  current_state = "Cancelled";
                    break;
                case 11:  current_state = "Refunded";
                    break;
                default: current_state = "Processing in Progress";
                    break;
            }
            TotalPrintOrder final_order = new TotalPrintOrder(
                    order_id,
                    0,
                    order_sequence,
                    reference,
                    payment,
                    order_message,
                    order.total_discounts_tax_incl,
                    order.total_discounts_tax_excl,
                    order.total_paid_real,
                    order.total_paid_tax_excl,
                    order.total_products,
                    order.total_products_wt,
                    order.total_shipping,
                    order.date_add,
                    delivery_address,
                    bought_products,
                    current_state
            );
            returnOrders.add(final_order);
            //Keep adding orders into the ReturnOrder list
        }
        System.out.println("orders returned"+ "----------"+ returnOrders.size());
        return ok(new JSONSerializer().deepSerialize(returnOrders));
    }
    // Ipad 修改订单开始
    //根据电话查找到订单
    public static Result ipadSearchOrder(int order_id) {
        response().setHeader("Connection","close");
        System.out.println("start to return +++++++");
        List<NewCodeValidation> newCodeValidations = new ArrayList<>();
        Order order = Order.findById(order_id);
        String payment = order.payment;
        if(payment.contains("Cash")){ //只能修改 cash 付款的订单
        List<OrderProduct> bought_products = new ArrayList<>();                // create a template list of products info, which will be returned
        List<OrderDetail> detail_products = OrderDetail.findByOrder(order_id);
        // get all the products detail from table ps_order_detail
        for (OrderDetail this_product : detail_products) {  //得到每件产品的详细信息
            int id_order_detail = this_product.id_order_detail;
            //搜索id_manufacturer
            String sql =
                    "select ps_product.id_manufacturer\n" +
                            "from ps_product\n" +
                            "where id_product = " + this_product.product_id;
            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_product.id_manufacturer", "quantity")
                            .create();
            com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
            query.setRawSql(rawSql);
            ProductQuantity productQuantity = query.findUnique();
            int id_manufacturer = productQuantity.quantity;
            OrderDetailTax orderDetailTax =OrderDetailTax.findById(id_order_detail); //ps_order_detail_tax
            OrderProduct orderProduct = new OrderProduct(
                    this_product.product_id,
                    this_product.product_attribute_id,
                    this_product.product_name,
                    this_product.product_quantity,
                    this_product.product_price,
                    this_product.reduction_percent,
                    this_product.reduction_amount,
                    this_product.reduction_amount_tax_incl,
                    this_product.reduction_amount_tax_excl,
                    this_product.product_upc,
                    this_product.product_reference,
                    this_product.id_tax_rules_group,
                    this_product.total_price_tax_incl,
                    this_product.total_price_tax_excl,
                    this_product.unit_price_tax_incl,
                    this_product.unit_price_tax_excl,
                    orderDetailTax.id_tax,
                    orderDetailTax.unit_amount,
                    orderDetailTax.total_amount,
                    id_manufacturer
            );
            bought_products.add(orderProduct);
        }
        OrderInvoice orderInvoice = OrderInvoice.findByOrder(order_id);
        String order_message;
        OrderMessage message = OrderMessage.findByID(order_id);
        if(message != null){
            order_message = message.message;
        }else {
            order_message = "";
        }
            //查找是否使用了打折券
        List<OrderCartRule> if_has_voucher = OrderCartRule.findByOrder(order_id);
        if(if_has_voucher.size()>0){
            for(OrderCartRule this_record: if_has_voucher){
//                if(!this_record.name.contains("MARINA")){ //不用管Marina Park 的voucher, 因为前台可以直接免运费
                    int id_cart_rule = this_record.id_cart_rule;
                    NewCodeValidation newCodeValidation = amendOrderCartRule(id_cart_rule);
                    newCodeValidations.add(newCodeValidation);
//                }
            }
        }
        AmendTotalOrder final_order = new AmendTotalOrder(
                order_id,
                order.id_customer,
                order.id_address_delivery,
                order.id_address_invoice,
                order.total_paid,
                order.total_paid_tax_incl,
                order.total_paid_tax_excl,
                order.total_paid_real,
                order.total_products,
                order.total_products_wt,
                order.total_shipping,
                order.total_shipping_tax_incl,
                order.total_shipping_tax_excl,
                1, //cash
                orderInvoice.invoice_address,
                orderInvoice.delivery_address,
                bought_products,
                "",
                order_message,
                0,
                "",
                BigDecimal.valueOf(0.00),
                BigDecimal.valueOf(0.00),
                0,
                newCodeValidations
        );
            System.out.println("orders returned"+ "----------");
            return ok(renderCategories(final_order));
        }else {
            return badRequest("Sorry, you cannot amend online payment orders");
        }
    }

    public static NewCodeValidation amendOrderCartRule(int id_cart_rule){
        if(id_cart_rule ==0){
            NewCodeValidation newCodeValidation = new NewCodeValidation();
            return newCodeValidation;
        }else {
            ArrayList<CodeValidateTool> products_list = new ArrayList<CodeValidateTool>();
            ArrayList<NewCodeValidation> total_record = new ArrayList<NewCodeValidation>();
            try {
                String sql =
                        "select ps_cart_rule_lang.name, ps_cart_rule.id_cart_rule, ps_cart_rule.description, ps_cart_rule.quantity_per_user, ps_cart_rule.minimum_amount, ps_cart_rule.minimum_amount_shipping, ps_cart_rule.free_shipping, ps_cart_rule.reduction_percent, ps_cart_rule.reduction_amount,ps_cart_rule.reduction_product, ps_cart_rule.product_restriction,ps_cart_rule.group_restriction,ps_cart_rule_product_rule_group.quantity, ps_cart_rule_product_rule.type ,ps_cart_rule_product_rule_value.id_item\n" +
                                "\n" +
                                "from ps_cart_rule_lang\n" +
                                "\n" +
                                "left join ps_cart_rule\n" +
                                "on ps_cart_rule_lang.id_cart_rule = ps_cart_rule.id_cart_rule\n" +
                                "\n" +
                                "left join ps_cart_rule_product_rule_group\n" +
                                "on ps_cart_rule.id_cart_rule = ps_cart_rule_product_rule_group.id_cart_rule\n" +
                                "\n" +
                                "left join ps_cart_rule_product_rule\n" +
                                "on ps_cart_rule_product_rule_group.id_product_rule_group = ps_cart_rule_product_rule.id_product_rule_group \n" +
                                "\n" +
                                "left join ps_cart_rule_product_rule_value\n" +
                                "on ps_cart_rule_product_rule.id_product_rule = ps_cart_rule_product_rule_value.id_product_rule\n" +
                                "\n" +
                                "where ps_cart_rule.id_cart_rule =" + id_cart_rule;
                RawSql rawSql =
                        RawSqlBuilder
                                .parse(sql)
                                .columnMapping("ps_cart_rule_lang.name", "promotion_name")
                                .columnMapping("ps_cart_rule.id_cart_rule", "id_cart_rule")
                                .columnMapping("ps_cart_rule.description", "description")
                                .columnMapping("ps_cart_rule.quantity_per_user", "quantity_per_user")
                                .columnMapping("ps_cart_rule.minimum_amount", "minimum_amount")
                                .columnMapping("ps_cart_rule.minimum_amount_shipping", "minimum_amount_if_shipping")
                                .columnMapping("ps_cart_rule.free_shipping", "if_free_shipping")
                                .columnMapping("ps_cart_rule.reduction_percent", "reduction_percent")
                                .columnMapping("ps_cart_rule.reduction_amount", "reduction_amount")
                                .columnMapping("ps_cart_rule.reduction_product", "reduction_product") //0,-1,-2, 或者产品ID
                                //new
                                .columnMapping("ps_cart_rule.product_restriction", "product_restriction")
                                .columnMapping("ps_cart_rule.group_restriction", "group_restriction")
                                .columnMapping("ps_cart_rule_product_rule_group.quantity", "minimum_product_quantity_required")
                                //new
                                .columnMapping("ps_cart_rule_product_rule.type", "type")
                                .columnMapping("ps_cart_rule_product_rule_value.id_item", "if_valid")  //存在产品要求
                                .create();
                com.avaje.ebean.Query<NewCodeValidation> query = Ebean.find(NewCodeValidation.class);
                query.setRawSql(rawSql);
                List<NewCodeValidation> list = query.findList();
                total_record.addAll(list);
            } catch (Exception e) {
                String sql =
                        "select ps_cart_rule_lang.name, ps_cart_rule.id_cart_rule, ps_cart_rule.description, ps_cart_rule.quantity_per_user, ps_cart_rule.minimum_amount, ps_cart_rule.minimum_amount_shipping, ps_cart_rule.free_shipping, ps_cart_rule.reduction_percent, ps_cart_rule.reduction_amount,ps_cart_rule.reduction_product, ps_cart_rule.product_restriction,ps_cart_rule.group_restriction\n" +
                                "\n" +
                                "from ps_cart_rule_lang\n" +
                                "\n" +
                                "left join ps_cart_rule\n" +
                                "on ps_cart_rule_lang.id_cart_rule = ps_cart_rule.id_cart_rule\n" +
                                "\n" +
                                "left join ps_cart_rule_product_rule_group\n" +
                                "on ps_cart_rule.id_cart_rule = ps_cart_rule_product_rule_group.id_cart_rule\n" +
                                "\n" +
                                "left join ps_cart_rule_product_rule\n" +
                                "on ps_cart_rule_product_rule_group.id_product_rule_group = ps_cart_rule_product_rule.id_product_rule_group \n" +
                                "\n" +
                                "left join ps_cart_rule_product_rule_value\n" +
                                "on ps_cart_rule_product_rule.id_product_rule = ps_cart_rule_product_rule_value.id_product_rule\n" +
                                "\n" +
                                "where ps_cart_rule.id_cart_rule =" + id_cart_rule;
                RawSql rawSql =
                        RawSqlBuilder
                                .parse(sql)
                                .columnMapping("ps_cart_rule_lang.name", "promotion_name")
                                .columnMapping("ps_cart_rule.id_cart_rule", "id_cart_rule")
                                .columnMapping("ps_cart_rule.description", "description")
                                .columnMapping("ps_cart_rule.quantity_per_user", "quantity_per_user")
                                .columnMapping("ps_cart_rule.minimum_amount", "minimum_amount")
                                .columnMapping("ps_cart_rule.minimum_amount_shipping", "minimum_amount_if_shipping")
                                .columnMapping("ps_cart_rule.free_shipping", "if_free_shipping")
                                .columnMapping("ps_cart_rule.reduction_percent", "reduction_percent")
                                .columnMapping("ps_cart_rule.reduction_amount", "reduction_amount")
                                .columnMapping("ps_cart_rule.reduction_product", "reduction_product")
                                //todo add product_restriction
                                .columnMapping("ps_cart_rule.product_restriction", "product_restriction")
                                .columnMapping("ps_cart_rule.group_restriction", "group_restriction")
                                .create();
                com.avaje.ebean.Query<NewCodeValidation> query = Ebean.find(NewCodeValidation.class);
                query.setRawSql(rawSql);
                List<NewCodeValidation> list = query.findList();
                total_record.addAll(list);
            }
            int number_of_products = total_record.size();
            if (number_of_products != 0) {
                NewCodeValidation sample = total_record.get(0); //拿到第一条记录作为样本Voucher
                if (sample.if_valid != 0) {  //存在限定产品
                    for (NewCodeValidation aList : total_record) {
                        String product_type = aList.type;
                        int product_id = aList.if_valid;
                        CodeValidateTool new_constraint = new CodeValidateTool(product_type, product_id);
                        products_list.add(new_constraint);
                    }
                }
                sample.products_constraint = products_list;
                return sample;
            } else {
                NewCodeValidation newCodeValidation = new NewCodeValidation();
                return newCodeValidation;
            }
        }
    }

    //ipad 修改订单，删除前一单的一些使用记录
    @Transactional
    public static synchronized void padEditOrder(int id_order){
        Order old_order  = Order.findById(id_order);
        if(old_order == null){
            System.out.println ("No such record yet"); //404
        }else if(old_order.current_state ==6){
            System.out.println("Already Cancelled before"); //200
        }else{
            List<OrderCartRule> if_has_voucher = OrderCartRule.findByOrder(id_order);
            if(if_has_voucher.size()>0){
                for(OrderCartRule this_record: if_has_voucher){
                    int id_cart_rule = this_record.id_cart_rule;
                    CartRule this_rule = CartRule.findById(id_cart_rule);
                    if(null !=this_rule){
                        this_rule.quantity = this_rule.quantity + 1;   //返回数量
                        this_rule.save();
                    }
                    this_record.delete();  //删除使用记录
                }
            }
            //check if_exist reward record:
            OrderReward if_rewarded = OrderReward.findById(id_order);
            if(if_rewarded != null){ //存在被奖励记录
                int id_cart_rule = if_rewarded.id_cart_rule; //找到id_cart_rule, 并且让此失效
                if(id_cart_rule !=9 && id_cart_rule!=44) {  //Marina和新用户WELCOMEA2B的广泛voucher不能失效
                    CartRule rewarded_cart_rule = CartRule.findById(id_cart_rule);
                    if (rewarded_cart_rule != null) {
                        rewarded_cart_rule.if_active = 0; //直接变成无效
                        rewarded_cart_rule.save();
                    }
                }
            }
            //deal sale products
            // 恢复被购买的meal_deal 产品数量
            String sql =
                    "select ps_deal_sale_table.id_product_shop, ps_deal_sale_table.key_word,sum(ps_order_detail.product_quantity)\n" +
                            "from ps_orders, ps_order_detail, ps_deal_sale_table\n" +
                            "where ps_orders.id_order = ps_order_detail.id_order\n" +
                            "and ps_orders.id_order = " + id_order +"\n" +
                            "and ps_order_detail.product_id = ps_deal_sale_table.deal_product_id\n" +
                            "and  ps_order_detail.product_name like CONCAT('%', ps_deal_sale_table.key_word, '%')\n" +
                            "group by ps_deal_sale_table.id_product_shop";
            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_deal_sale_table.id_product_shop", "id_product_shop")
                            .columnMapping("ps_deal_sale_table.key_word", "key_word")
                            .columnMapping("sum(ps_order_detail.product_quantity)", "purchased_quantity")
                            .create();
            com.avaje.ebean.Query<MealDealProduct> query = Ebean.find(MealDealProduct.class);
            query.setRawSql(rawSql);
            List<MealDealProduct> list = query.findList();
            if(list.size() >0){
                for(MealDealProduct this_one: list){
                    int bought_quantity = this_one.purchased_quantity;    //找到每件meal deal shop 产品的被购买数量
                    change_product_quantity(this_one.id_product_shop,bought_quantity,1);
                }
            }
            //删除已经打印记录
            OrderPrintHistory printHistory = OrderPrintHistory.findById(id_order);
            if(printHistory!=null){
                printHistory.delete();
            }
            //ps_order_detail 返回被买的产品数量
            List<OrderDetail> all_products = OrderDetail.findByOrder(id_order);
            if(all_products.size()>0){
                for(OrderDetail this_product: all_products){
                    int id_product = this_product.product_id; //这件产品的id
                    int quantity = this_product.product_quantity; //这件产品的被购买数量
                    //ps_stock_available
                    change_product_quantity(id_product,quantity,1);
                    int id_order_detail = this_product.id_order_detail;
                    OrderDetailTax this_order_tax = OrderDetailTax.findById(id_order_detail);
                    this_order_tax.delete(); //删除记录
                    //判断是否存在 buy_one_get_one 产品
                    List<BuyOneGetOne> if_exist_list = BuyOneGetOne.findBySaleProductId(id_product);
                    if(if_exist_list.size() > 0){
                        for(BuyOneGetOne this_one: if_exist_list){
                            int real_product_id = this_one.id_real_product;
                            int real_bought_quantity = (1/this_one.basic_unit) * this_one.bundle_number;
                            change_product_quantity(real_product_id,real_bought_quantity,1);  // buy_one_get_one 产品数量恢复
                        }
                    }
                }
            }
            DriverOrders a = DriverOrders.findByOrderId(id_order);
            if(a!=null){
            a.delete();//删除司机分配订单记录
            }
            System.out.println("Order has been edited Successfully"); //200
        }
    }

    //PAD 修改完成后，提交订单
    @Transactional
    public static Result amendEditedOrder() throws Exception {
        response().setHeader("Connection","close");
        //把整个流程方法化
        AmendTotalOrder totalOrder = renderAmendTotalOrder(request().body().asJson().toString());
        int id_order = totalOrder.id_order;
        int customer_id = totalOrder.id_customer;
        padEditOrder(id_order);
        //先得到原订单的单号
        // promotion
        int id_cart_rule = totalOrder.id_cart_rule;
        String name = totalOrder.name;
        BigDecimal value = totalOrder.value;
        BigDecimal value_tax_excl = totalOrder.value_tax_excl;
        int free_shipping = totalOrder.free_shipping;
        String order_message = totalOrder.message;
        List<OrderProduct> new_products = totalOrder.products;
        int number_of_product = new_products.size();
        int order_status = 3;
        Date date_add = new Date();
            /* First table affected, ps_orders, a new record will be generated */
        Order old_order = Order.findById(id_order);
        OrderDetail.deleteAllItems(id_order);//删除所有order_detail
        String order_reference = old_order.reference;
        OrderInvoice old_orderInvoice = OrderInvoice.findByOrder(id_order);
        int id_order_invoice = old_orderInvoice.id_order_invoice;
        //ps_order 第一张表格更改
        old_order.update(
                totalOrder.id_address_delivery,
                totalOrder.id_address_invoice,
                order_status,
                value, //total_discounts
                value, //total_discounts_tax_incl
                value_tax_excl, //total_discounts_tax_excl
                totalOrder.total_paid,
                totalOrder.total_paid_tax_incl,
                totalOrder.total_paid_tax_excl,
                totalOrder.total_paid_real,
                totalOrder.total_products,
                totalOrder.total_products_wt,
                totalOrder.total_shipping,
                totalOrder.total_shipping_tax_incl,
                totalOrder.total_shipping_tax_excl,
                id_order_invoice,
                id_order_invoice,
                date_add //只能更新最后一次更新时间， 不能更新invoice_date时间，否则 order_sequence会受到影响
        );
        old_order.save();
            /*
            The second period, add new records in table
            ps_order_history, ps_order_invoice, ps_order_payment, and ps_message
            */
            OrderHistory orderHistory = new OrderHistory(         //ps_order_history
                    0,
                    id_order,
                    order_status,
                    date_add
            );
            orderHistory.save();
//start
        // update start
        //ps_order_payment 更新记录
        String dml_orderPayment = "UPDATE ps_order_payment\n" +
                "SET amount = :amount,\n" +
                "date_add = :date_add\n" +
                "WHERE order_reference = :order_reference";
        SqlUpdate update_orderPayment = Ebean.createSqlUpdate(dml_orderPayment)
                .setParameter("amount", totalOrder.total_paid)
                .setParameter("date_add", date_add)
                .setParameter("order_reference", order_reference);
        update_orderPayment.execute();
        //ps_order_payment 更新结束

        //ps_order_invoice 更新记录
        String dml_orderInvoice = "UPDATE ps_order_invoice\n" +
                "SET number = :number,\n" +
                "total_discount_tax_excl = :total_discount_tax_excl,\n" +
                "total_discount_tax_incl = :total_discount_tax_incl,\n" +
                "total_paid_tax_excl = :total_paid_tax_excl,\n" +
                "total_paid_tax_incl = :total_paid_tax_incl,\n" +
                "total_products = :total_products,\n" +
                "total_products_wt = :total_products_wt,\n" +
                "total_shipping_tax_excl = :total_shipping_tax_excl,\n" +
                "total_shipping_tax_incl = :total_shipping_tax_incl,\n" +
                "invoice_address = :invoice_address,\n" +
                "delivery_address = :delivery_address\n" +
                "WHERE id_order = :id_order";
        SqlUpdate update_orderInvoice = Ebean.createSqlUpdate(dml_orderInvoice)
                .setParameter("number", id_order_invoice)
                .setParameter("total_discount_tax_excl", value_tax_excl)
                .setParameter("total_discount_tax_incl", value)
                .setParameter("total_paid_tax_excl", totalOrder.total_paid_tax_excl)
                .setParameter("total_paid_tax_incl", totalOrder.total_paid_tax_incl)
                .setParameter("total_products", totalOrder.total_products)
                .setParameter("total_products_wt", totalOrder.total_products_wt)
                .setParameter("total_shipping_tax_excl", totalOrder.total_shipping_tax_excl)
                .setParameter("total_shipping_tax_incl", totalOrder.total_shipping_tax_incl)
                .setParameter("invoice_address", totalOrder.invoice_address)
                .setParameter("delivery_address", totalOrder.delivery_address)
                .setParameter("id_order", id_order);
        update_orderInvoice.execute();
        //ps_order_invoice 更新结束
        //ps_order_carrier  更新
        OrderCarrier last_orderCarrier = OrderCarrier.findByOrder(id_order);
        last_orderCarrier.update(totalOrder.total_shipping_tax_excl,
                totalOrder.total_shipping_tax_incl);
        last_orderCarrier.save();
        //update end
               /* create a thread to process order_details adding action */
                Runnable forloop1 = new Runnable() {
                    @Override
                    public synchronized void run() {
                        for (int i = 0; i < number_of_product; i++) {
                            OrderProduct orderProduct = new_products.get(i);
                            String product_reference = orderProduct.product_reference;
                            if (product_reference == null){
                                product_reference = "";
                            }
                            BigDecimal price = getWholeSale(orderProduct.product_id);
                            OrderDetail orderDetail = new OrderDetail(                       //ps_order_detail
                                    id_order,
                                    id_order_invoice,
                                    0,
                                    1,
                                    orderProduct.product_id,
                                    orderProduct.product_attribute_id,
                                    orderProduct.product_name,
                                    orderProduct.product_quantity,
                                    orderProduct.product_quantity,
                                    0,
                                    0,
                                    0,
                                    orderProduct.product_price,
                                    orderProduct.reduction_percent,
                                    orderProduct.reduction_amount,
                                    orderProduct.reduction_amount_tax_incl,
                                    orderProduct.reduction_amount_tax_excl,
                                    BigDecimal.valueOf(0.00),
                                    BigDecimal.valueOf(0.000000),
                                    "",
                                    orderProduct.product_upc,
                                    product_reference,
                                    "",
                                    BigDecimal.valueOf(0.000000),
                                    orderProduct.id_tax_rules_group,
                                    0,
                                    "",
                                    BigDecimal.valueOf(0.000),
                                    BigDecimal.valueOf(0.000000),
                                    BigDecimal.valueOf(0.000),
                                    0,
                                    "",
                                    0,
                                    new Date(0),
                                    orderProduct.total_price_tax_incl,
                                    orderProduct.total_price_tax_excl,
                                    orderProduct.unit_price_tax_incl,
                                    orderProduct.unit_price_tax_excl,
                                    BigDecimal.valueOf(0.000000),
                                    BigDecimal.valueOf(0.000000),
                                    BigDecimal.valueOf(0.000000),
                                    orderProduct.product_price,
                                    price
                            );
                            orderDetail.save();
                            int id_order_detail = orderDetail.id_order_detail;

                            OrderDetailTax orderDetailTax = new OrderDetailTax(                   //ps_order_detail_tax
                                    id_order_detail,
                                    orderProduct.id_tax,
                                    orderProduct.tax_unit_amount,
                                    orderProduct.tax_total_amount
                            );
                            orderDetailTax.save();
                            //get the current quantity of the product and then reduce the quantity by the number bought in the order
                            change_product_quantity(orderProduct.product_id,orderProduct.product_quantity,0);
                            //判断是否存在 buy_one_get_one 产品
                            List<BuyOneGetOne> if_exist_list = BuyOneGetOne.findBySaleProductId(orderProduct.product_id);
                            if(if_exist_list.size() > 0){
                                for(BuyOneGetOne this_one: if_exist_list){
                                    int real_product_id = this_one.id_real_product;
                                    int real_bought_quantity = (1/this_one.basic_unit) * this_one.bundle_number;
                                    change_product_quantity(real_product_id,real_bought_quantity,0);  // buy_one_get_one 产品数量扣除
                                }
                            }
                        }
                        // promotion code actions
                        if (id_cart_rule !=0){
                        change_voucher_quantity(customer_id,id_order,id_cart_rule,id_order_invoice,name,value,value_tax_excl,free_shipping);
                        }
                        sendEmailToSponsor(customer_id);
                    }
                };
                new Thread(forloop1).start();
        /* Either conditional process will lead to the return of general information of this order(if the order contains a message, add one record in ps_message) */
            if (order_message != null) {
                if (!order_message.equals("")) {
                    OrderMessage last_orderMessage = OrderMessage.findByOrder(id_order,customer_id);
                    if(last_orderMessage!=null){
                        last_orderMessage.update(order_message);
                        last_orderMessage.save();
                    }else{
                        OrderMessage message = new OrderMessage(
                                0,
                                customer_id,
                                0,
                                id_order,
                                order_message,
                                0,  //if_private
                                date_add
                        );
                        message.save();
                    }
                }

            }
        //需要重新减去这张订单的meal_deal 产品， 之前的扫描已经过了，不会再经过扫描
        // 删除被购买的meal_deal 产品数量
        String sql =
                "select ps_deal_sale_table.id_product_shop, ps_deal_sale_table.key_word,sum(ps_order_detail.product_quantity)\n" +
                        "from ps_orders, ps_order_detail, ps_deal_sale_table\n" +
                        "where ps_orders.id_order = ps_order_detail.id_order\n" +
                        "and ps_orders.id_order = " + id_order +"\n" +
                        "and ps_order_detail.product_id = ps_deal_sale_table.deal_product_id\n" +
                        "and  ps_order_detail.product_name like CONCAT('%', ps_deal_sale_table.key_word, '%')\n" +
                        "group by ps_deal_sale_table.id_product_shop";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_deal_sale_table.id_product_shop", "id_product_shop")
                        .columnMapping("ps_deal_sale_table.key_word", "key_word")
                        .columnMapping("sum(ps_order_detail.product_quantity)", "purchased_quantity")
                        .create();
        com.avaje.ebean.Query<MealDealProduct> query = Ebean.find(MealDealProduct.class);
        query.setRawSql(rawSql);
        List<MealDealProduct> list = query.findList();
        if(list.size() >0){
            for(MealDealProduct this_one: list){
                int bought_quantity = this_one.purchased_quantity;    //找到每件meal deal shop 产品的被购买数量
                change_product_quantity(this_one.id_product_shop,bought_quantity,0);
            }
        }
        System.out.println("Successfully amend this order");
        return ok("Successfully amend this order");
    }

    public static ArrayList<ReturnDriverOrders> getDriverOrders(){
        String sql =
                "select ps_drivers.name, ps_drivers.contact_number, ps_drivers_orders.order_number, ps_drivers_orders.start_time, ps_drivers_orders.delivered_time\n" +
                        "from ps_drivers\n" +
                        "LEFT JOIN ps_drivers_orders\n" +
                        "ON ps_drivers_orders.id_driver = ps_drivers.id_driver\n" +
                        "AND ps_drivers_orders.date_add > DATE_SUB(NOW(), INTERVAL 12 HOUR)\n" +
                        "and ps_drivers_orders.if_delivered = 0\n" +
                        "where ps_drivers.id_driver != 8 \n" +
                        "and ps_drivers.if_work = 1\n" +
                        "order by ps_drivers.id_driver";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_drivers.name", "driver_name")
                        .columnMapping("ps_drivers.contact_number", "contact_number")
                        .columnMapping("ps_drivers_orders.order_number", "order_number")
                        .columnMapping("ps_drivers_orders.start_time", "start_time")
                        .columnMapping("ps_drivers_orders.delivered_time", "delivered_time")
                        .create();
        com.avaje.ebean.Query<ReturnDriverOrders> query = Ebean.find(ReturnDriverOrders.class);
        query.setRawSql(rawSql);
        List<ReturnDriverOrders> list = query.findList();
        ArrayList<ReturnDriverOrders> all_driver_order = new ArrayList<>();
        String index_driver_name;  //当前指针
        String contact_number;    //当前指针的电话
        ArrayList<Integer> doing_orders = new ArrayList<>();  //当前指针的数组
        ArrayList<Integer> todo_orders = new ArrayList<>();   //当前指针的数组
        Date original_time = new Date(0);
        int number_of_items = list.size();
        if(number_of_items>0){
            index_driver_name = list.get(0).driver_name;
            contact_number = list.get(0).contact_number;
        }else {
            index_driver_name = "";
            contact_number = "";
        }
        for(int i = 0; i < number_of_items; i++){
            ReturnDriverOrders this_one = list.get(i);
            if(!this_one.driver_name.equals(index_driver_name)){  //跳进下一个司机的送单情况， 处理上一个司机的所有信息
              ReturnDriverOrders last_one = new ReturnDriverOrders();
                last_one.driver_name = index_driver_name;
                last_one.contact_number = contact_number;
                last_one.doing_orders.addAll(doing_orders);
                last_one.todo_orders.addAll(todo_orders);
                if(todo_orders.size() ==0 && doing_orders.size() ==0){
                    last_one.working_status = "Available";
                }else {
                    last_one.working_status = "In delivery";
                }
                all_driver_order.add(last_one);  //把上一个司机的所有情况加入 返回数组
                //更新指针
                index_driver_name = this_one.driver_name;
                contact_number = this_one.contact_number;
                doing_orders.clear();
                todo_orders.clear();
            }
            //处理这个司机的每个订单的情况
          if(this_one.start_time!=null && this_one.delivered_time !=null){
           if(this_one.start_time.after(original_time) && this_one.delivered_time.equals(original_time)){
               doing_orders.add(this_one.order_number);
           }else if(this_one.start_time.equals(original_time) && this_one.delivered_time.equals(original_time)){
               todo_orders.add(this_one.order_number);
           }
          }
           // 最后一个司机的送单情况，加入总数组
            if(i == number_of_items -1){
            ReturnDriverOrders very_last = new ReturnDriverOrders();
            very_last.driver_name = index_driver_name;
            very_last.contact_number = contact_number;
            very_last.doing_orders.addAll(doing_orders);
            very_last.todo_orders.addAll(todo_orders);
            if(todo_orders.size() ==0 && doing_orders.size() ==0){
                very_last.working_status = "Available";
            }else {
                very_last.working_status = "In delivery";
            }
            all_driver_order.add(very_last);
            }

        }
        return all_driver_order;
    }

    @Transactional
    public static void sendEmailToSponsor(int id_customer){ //给推荐人发送50% off 的折扣券， 绑定用户，只运用给food产品
        ReferralProgram if_has_record = ReferralProgram.findBySponsoredUser(id_customer);
        if(if_has_record !=null){
            int id_sponsor = if_has_record.id_sponsor;
            Customer that_sponsor = Customer.findById(id_sponsor);
            if(that_sponsor !=null){
                //可能存在一种情况，发件人邮箱是.facebook@gmail.com
                String sponsor_email = that_sponsor.email;
                if(sponsor_email.contains("facebook")){
                    sponsor_email = sponsor_email.replaceAll(".facebook@","@");//还原出正确的邮箱地址
                }
                String sponsor_firstname = that_sponsor.firstname;
                String sponsor_lastname  = that_sponsor.lastname;
                Customer this_new_user = Customer.findById(id_customer);
                String new_user_email = this_new_user.email;
                String newUser_firstname = this_new_user.firstname;
                String newUser_lastname  = this_new_user.lastname;
                CustomerReferral packed_information = new CustomerReferral(id_sponsor,
                sponsor_firstname,sponsor_lastname,sponsor_email,newUser_firstname,newUser_lastname,new_user_email);
                //生成新voucher, 更新 referralProgram 记录， 发送邮件
                String voucher_code;
                do{
                    voucher_code = getRandomVoucher();
                }while (CartRule.findByVoucher(voucher_code) != null);
                Date today = new Date();
                long nowAdd6Months = System.currentTimeMillis() + Long.parseLong("15768000000");
                Date after_6_month = new Date(nowAdd6Months);
                //start
                //ps_cart_rule
                CartRule new_cart_rule = new CartRule(id_sponsor, today, after_6_month, "", 1, 1, 1, 0, voucher_code, BigDecimal.valueOf(0.00),
                        1, 1, 0, 0, 0, 0, 1, 1, 0, 0, BigDecimal.valueOf(50.00), BigDecimal.valueOf(0.00), 0, 1, -2, 0, 0, 0, 1, today, today); //// TODO: 2017/6/29
                new_cart_rule.save();
                int id_cart_rule = new_cart_rule.id_cart_rule;
                //ps_cart_rule_lang
                CartRuleLang new_cart_lang = new CartRuleLang(id_cart_rule, 1, "50% Off");
                new_cart_lang.save();
                //ps_cart_rule_product_rule_group
                CartRuleProductRuleGroup new_cart_rule_product_rule_group = new CartRuleProductRuleGroup(id_cart_rule, 1);
                new_cart_rule_product_rule_group.save();
                int id_product_rule_group = new_cart_rule_product_rule_group.id_product_rule_group;
                //ps_cart_rule_product_rule
                CartRuleProductRule new_cart_rule_product_rule = new CartRuleProductRule(id_product_rule_group, "manufacturers");
                new_cart_rule_product_rule.save();
                int id_product_rule = new_cart_rule_product_rule.id_product_rule;
                //ps_cart_rule_product_rule_value
                CartRuleProductRuleValue new_cart_rule_product_rule_value = new CartRuleProductRuleValue(id_product_rule, 2);
                new_cart_rule_product_rule_value.save();
                //end
                if_has_record.id_cart_rule_sponsor = id_cart_rule;
                if_has_record.date_upd =today;
                if_has_record.save();
                CongratulationsVoucherEmail.sendCongratulationsToUser(packed_information,voucher_code);
            }
        }
    }

    /**
     * 获取device_token 的同时，检验购物车产品数量
     * 检查数据库库存是否足够
     *1. 生成一个hash map, 存放所有被购买的产品（product_id, bought_quantity）
     *2. 遍历一边传过来的产品数组，然后根据key-value 加入到hash-map 里面
     *3. 检查一般产品（另外一个hash-map, 添加剩余数量）
     *4. 检查买一送一产品（添加进 第二个 hash-map）
     *5. 检查meal_deal 产品()
     * */
    @Transactional
    public static Result saveTokenCheckStock() {
        response().setHeader("Connection","close");
        OrderProductCheck orderProductCheck = renderOrderProduct(request().body().asJson().toString());
        //从网络请求里面提取出 购买产品的信息
        ArrayList<OrderProduct> bought_products = orderProductCheck.products; //准备进行验证的购物车产品
        HashMap<Integer,String>  original_product_bought = new HashMap<>(); //原始产品信息： product_id, name (所有主产品)
        HashMap<Integer,Integer>  hashMap_products_bought = new HashMap<>();
        HashMap<Integer,Integer>  meal_deal_product = new HashMap<>();  //Meal Deal 套餐里面涉及到的shop 产品       product_id, quantity
        HashMap<Integer,Integer>  bundle_sale_product = new HashMap<>(); // Bundle Sales 里面涉及到的 shop 产品
        HashMap<Integer,List<Integer>>  product_combination = new HashMap<>();  //主产品 --> 副产品 组合
        HashMap<Integer,String>  attribtue_product = new HashMap<>();  //副产品 --> 副产品名称
        for(OrderProduct this_bought_product: bought_products){
            original_product_bought.put(this_bought_product.product_id, this_bought_product.product_name); // 记录每件原产品的产品ID， 名字
            if(hashMap_products_bought.containsKey(this_bought_product.product_id)){
                int already_bought = hashMap_products_bought.get(this_bought_product.product_id);
                hashMap_products_bought.put(this_bought_product.product_id, (this_bought_product.product_quantity + already_bought)); // 添加普通产品的购买数量
            }else {
                hashMap_products_bought.put(this_bought_product.product_id, this_bought_product.product_quantity); // 添加普通产品的购买数量
            }
            //判断是否参与 meal Deal 产品
            List<ProductQuantity> if_bought_meal_deal = checkMealDealProduct(this_bought_product.product_id,this_bought_product.product_name);
            if(if_bought_meal_deal.size() > 0){ //如果存在的话，就往 hash-map 里面添加
                for(ProductQuantity this_meal_deal: if_bought_meal_deal){
                    int this_meal_deal_product = this_meal_deal.quantity;   //实际shop 产品的ID
                    if(meal_deal_product.containsKey(this_meal_deal_product)){ //已经存在记录，被购买数量需要累加
                        int original_bought_qty = meal_deal_product.get(this_meal_deal_product);
                        meal_deal_product.put(this_meal_deal_product,(original_bought_qty + this_bought_product.product_quantity));
                    }else{ //不存在记录，生成第一次记录
                        meal_deal_product.put(this_meal_deal_product,this_bought_product.product_quantity);  //被购买的 meal_deal 产品id, 被购买的meal deal 产品数量
                    }
                    if(product_combination.containsKey(this_bought_product.product_id)){
                        List<Integer> original_deal_product = product_combination.get(this_bought_product.product_id);
                        original_deal_product.add(this_meal_deal_product);
                        product_combination.put(this_bought_product.product_id,original_deal_product);
                    }else{
                        List<Integer> deal_product = new ArrayList<>();
                        deal_product.add(this_meal_deal_product);
                        product_combination.put(this_bought_product.product_id,deal_product);//添加索引，  主产品id ——> 套餐产品id List<Integer>
                    }
                }
            }
            //判断是否参与bundle sale 产品
            List<BundleProduct> if_bogo_product = checkBoGoProducts(this_bought_product.product_id);
            if(if_bogo_product.size()>0){
                for(BundleProduct this_bogo_product: if_bogo_product){
                    int real_product_quantity = (this_bought_product.product_quantity* this_bogo_product.bundle_number)/this_bogo_product.basic_unit;
                    int this_bundle_product = this_bogo_product.id_real_product;//实际产品的 ID
                    if(bundle_sale_product.containsKey(this_bundle_product)){
                        int original_bought_qty = bundle_sale_product.get(this_bundle_product);
                        bundle_sale_product.put(this_bundle_product,(real_product_quantity + original_bought_qty));
                    }else{
                        bundle_sale_product.put(this_bundle_product,real_product_quantity);
                    }
                    //该逻辑不支持 ： 买一送一个（Meal Deal）
                    if(product_combination.containsKey(this_bought_product.product_id)){
                        List<Integer> original_bundle_product = product_combination.get(this_bought_product.product_id);
                        original_bundle_product.add(this_bundle_product);
                        product_combination.put(this_bought_product.product_id,original_bundle_product);
                    }else{
                        List<Integer> bundle_product = new ArrayList<>();
                        bundle_product.add(this_bundle_product);
                        product_combination.put(this_bought_product.product_id,bundle_product);//添加索引，  主产品id ——> 套餐产品id List<Integer>
                    }
                }
            }
        }
        for(Integer meal_product: meal_deal_product.keySet()){
            if(hashMap_products_bought.containsKey(meal_product)){
                int meal_deal_bought_qty = meal_deal_product.get(meal_product);
                int product_qty = hashMap_products_bought.get(meal_product);
                hashMap_products_bought.put(meal_product, meal_deal_bought_qty+product_qty);
            }else{
                hashMap_products_bought.put(meal_product,meal_deal_product.get(meal_product));
            }
        }
        for(Integer bundle_product: bundle_sale_product.keySet()){
            if(hashMap_products_bought.containsKey(bundle_product)){
                int bundle_product_bought_qty = bundle_sale_product.get(bundle_product);
                int product_qty = hashMap_products_bought.get(bundle_product);
                hashMap_products_bought.put(bundle_product, bundle_product_bought_qty+product_qty);
            }else {
                hashMap_products_bought.put(bundle_product, bundle_sale_product.get(bundle_product));
            }
        }
         // 开始遍历总 Hash Map, 获取产品实时数量， 并且与HashMap 中 已购买数量进行对比，然后给用户发送回反馈信息
        List<OrderProduct> return_product_result = new ArrayList<OrderProduct>();
        for(Integer each_product: hashMap_products_bought.keySet()){ //总体循环每件产品，还有购买数量
            StockAvailable current_stock = StockAvailable.findByAttribute(each_product,0);
            int current_product_stock;
            if(current_stock == null){
                current_product_stock =0;
            }else {
                current_product_stock = current_stock.quantity;
            }
            //然后判断剩余数量是否大于 被购买数量，如果库存不够，则加入返回数组
            if(current_product_stock < hashMap_products_bought.get(each_product)){  //这件产品库存不够
                //判断这件产品是否属于附属产品
                if(!original_product_bought.containsKey(each_product)){ //存在于总产品ID list, 但是没有在原产品列表里， each_product（int）  就是 附属产品的ID
                    //找到这件附属产品所有的 父类产品
                    for(Integer father_product_id: product_combination.keySet()){
                        List<Integer> child_products = product_combination.get(father_product_id);
                        if(child_products.contains(each_product)){ //说明这是一个父类产品ID
                            OrderProduct product_with_attribute = new OrderProduct();
                            String father_product_name = original_product_bought.get(father_product_id);
                            String attribute_product_name;
                            if(attribtue_product.containsKey(each_product)){ //如果副产品已经出现过
                                attribute_product_name = attribtue_product.get(each_product);
                            }else{ //第一次出现副产品，所以要存进hashMap
                                attribute_product_name = getProductName(each_product); //获得副产品名称
                                attribtue_product.put(each_product,attribute_product_name); //HashMap 里面存入记录，省去数据库第二次检索
                            }
                            String total_name;
                            if(bundle_sale_product.containsKey(each_product)){
                                total_name = attribute_product_name;
                            }else{
                                total_name = attribute_product_name + " in " + father_product_name;
                            }
                            product_with_attribute.product_id = father_product_id; //主产品
                            product_with_attribute.product_name = total_name; //总名称  "Coke in Curry Dish"
                            product_with_attribute.product_quantity = current_product_stock;
                            return_product_result.add(product_with_attribute);
                        }
                    }
                }else {  //不是附属产品，是主产品
                    OrderProduct exceptional_product = new OrderProduct();
                    exceptional_product.product_id = each_product; //主产品
                    exceptional_product.product_name = original_product_bought.get(each_product); //产品名称
                    exceptional_product.product_quantity = current_product_stock;
                    return_product_result.add(exceptional_product);
                }
            }
        }
        // new thread end
        Runnable save_deviceToken = new Runnable() {
            @Override
            public synchronized void  run() {
                Integer customer_id;
                try{
                    customer_id = orderProductCheck.id_customer;
                    String device_token = orderProductCheck.device_token;
                    String device_type = orderProductCheck.device_type;
                    if(customer_id != null && device_token !=null && device_type != null) {
                        Date last_access = new Date();
                        //是否已经存在
                        //New thread start
                        CustomerDeviceToken customerDeviceToken = CustomerDeviceToken.findByToken(customer_id, device_token);
                        if (customerDeviceToken != null) {  //已经存在
                            customerDeviceToken.last_access_time = last_access;
                            customerDeviceToken.save();
                        } else {
                            CustomerDeviceToken new_one = new CustomerDeviceToken(
                                    customer_id,
                                    device_token,
                                    device_type,
                                    last_access
                            );
                            new_one.save();
                        }
                    }
                }catch (Exception e){
                    System.out.println("Something is Wrong here with device token");
                }
            }
        };
        new Thread(save_deviceToken).start();
        if(return_product_result.size() == 0 ){
            return ok("A2B Living has enough products in stock.");
        }else{
            return badRequest(renderCategories(return_product_result));
        }
    }
    //查看一张订单里面是否含有meal Deal 产品
    private static List<ProductQuantity> checkMealDealProduct(int product_id, String product_name) {
        if(product_name.contains("&")){ //如果产品名字里面含有 &， 比如 MM 豆
            product_name = product_name.replaceAll("'s", "");
        }else{
            product_name = product_name.replaceAll("'s", "");
            product_name = product_name.replaceAll("[^A-Za-z0-9 ]", "");
        }
        String sql =
                "select id_product_shop\n" +
                        "from ps_deal_sale_table\n" +
                        "where deal_product_id = " + product_id + "\n" +
                        "and '" + product_name + "' like CONCAT('%', key_word, '%')";

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("id_product_shop", "quantity")
                        .create();
        com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
        query.setRawSql(rawSql);
        List<ProductQuantity> list = query.findList();
        return list;
    }

    //查找一张订单里面是否含有买一送一产品
    private static List<BundleProduct> checkBoGoProducts(int id_onsale_product) {
        String sql =
                "select ps_product_buyone_getone.id_onsale_product, ps_product_buyone_getone.id_real_product, ps_product_buyone_getone.basic_unit, ps_product_buyone_getone.bundle_number\n" +
                        "from ps_product_buyone_getone\n" +
                        "where if_valid = 1 \n" +
                        "and ps_product_buyone_getone.start_date < Now()\n" +
                        "and ps_product_buyone_getone.end_date > Now()\n" +
                        "and id_onsale_product = " + id_onsale_product;
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_product_buyone_getone.id_onsale_product", "id_onsale_product")
                        .columnMapping("ps_product_buyone_getone.id_real_product", "id_real_product")
                        .columnMapping("ps_product_buyone_getone.basic_unit", "basic_unit")
                        .columnMapping("ps_product_buyone_getone.bundle_number", "bundle_number")
                        .create();
        com.avaje.ebean.Query<BundleProduct> query = Ebean.find(BundleProduct.class);
        query.setRawSql(rawSql);
        List<BundleProduct> list = query.findList();
        return list;
    }
}