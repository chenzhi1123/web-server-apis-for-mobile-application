/*
 * Copyright (c) 2017. Zhi Chen
 * WebAPI is for later use, showing the template method to connect Ebean class
 * with Database tables, as well as making use of Ebean Finder and call class methods
 * using controller methods..
 * @see JsonParser
 */

package controllers;

import models.database.BuyOneGetOne;
import models.database.Customer;
import models.database.DealProductsTable;
import play.mvc.Controller;
import play.mvc.Result;
import util.MealDealGenerator;

import java.util.List;

import static parsers.JsonParser.renderCategories;
import static parsers.JsonParser.renderCustomer;

public class WebAPI extends Controller
{
    /**
     * 验证每张订单里面的所有产品实时数量，如果数量不够，则提示顾客缺货
     * */
    public static Result checkRealTimeQuantity(){
        response().setHeader("Connection","close");
        String email = session("email");
        // if in session
        if (email != null) {
        List<Customer> customers = Customer.findAll();
        return ok(renderCustomer(customers));
        }else{
            return ok("not logged in");
        }
    }

    public static Result customer(Long id){
        response().setHeader("Connection","close");
        Customer customer = Customer.findById(id);
        return customer==null? notFound() : ok(renderCustomer(customer));
    }
    
    
    public static Result createCustomer(){
        response().setHeader("Connection","close");
        Customer customer = renderCustomer(request().body().asJson().toString());
        customer.save();
        return ok(renderCustomer(customer));
    }

    public static Result deleteCustomer(Long id){
        response().setHeader("Connection","close");
        Result result = notFound();
        Customer customer = Customer.findById(id);
        if (customer != null)
        {
          customer.delete();
            result = ok();
        }
        return result;
    }

    /**
     * Sample method to execute actions to Database records through Ebean class connection
     * */
    public static Result deleteAllCustomers(){
        response().setHeader("Connection","close");
        Customer.deleteAll();
        return ok();
    }

    public static Result updateCustomer(Long id){
        response().setHeader("Connection","close");
        Result result = notFound();
        Customer customer = Customer.findById(id);
        if (customer != null)
        {
          Customer updatedCustomer = renderCustomer(request().body().asJson().toString());
          customer.update(updatedCustomer);
          customer.save();
          result = ok(renderCustomer(customer));
        }
        return result;
    }

    /**
     * Test Api Features Here
     * */

    public static Result sendEmailToMe() {
        response().setHeader("Connection","close");
//        sendEmailToCustomer("chenzhi","chenzhi1123@gmail.com");
        return ok("email sent successfully");
    }

    public static Result test() {
        response().setHeader("Connection","close");
        String email_a = "chenzhi1123@gmail.com";
        String b = email_a.replaceAll("@",".facebook@");
        return ok(b);
    }

    public static Result generateMealDeal() {
        response().setHeader("Connection","close");
        MealDealGenerator.generate_table_value();
        return ok("email sent successfully");
    }

    public static Result allMealProduct() {
        response().setHeader("Connection","close");
        List<DealProductsTable> a = DealProductsTable.findAll();
        return ok(renderCategories(a));
    }

    public static Result seeAllBoGo() {
        response().setHeader("Connection","close");
        List<BuyOneGetOne> a = BuyOneGetOne.findAll();
        return ok(renderCategories(a));
    }

    public static Result seeOneBoGo(int id_onesale_product) {
        response().setHeader("Connection","close");
        List<BuyOneGetOne> a = BuyOneGetOne.findBySaleProductId(id_onesale_product);
        return ok(renderCategories(a));
    }

    public static Result seeOneGoBo(int id_original_product) {
        response().setHeader("Connection","close");
        List<BuyOneGetOne> a = BuyOneGetOne.findByOriginalProduct(id_original_product);
        return ok(renderCategories(a));
    }

}