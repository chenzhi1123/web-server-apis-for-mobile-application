/*
 * Copyright (c) 2017. Zhi Chen.
 * ProductAPI is for SQL querying all the records related to products.
 * Price, quantity, attribute, image id or button index, etc.
 * Cache must be used on the server or data I/O will be overloading.
 * When adding something into cache, firstly and safely should initiate
 * an object, later that will be easier to retrieve it.
 */

package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import com.avaje.ebean.SqlUpdate;
import models.database.DealProductsTable;
import models.sqlContainer.*;
import play.cache.Cache;
import play.mvc.Controller;
import play.mvc.Result;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static controllers.AttributeAPI.getProductAttributes;
import static controllers.AttributeAPI.setNewCache;
import static models.database.DatabaseProduct.updateOneRecord;
import static parsers.JsonParser.renderCategories;

public class ProductAPI extends Controller {
    /**
     * Get all products of one category, SQL query from database, using Join among multiple tables
     * @param categoryID
     * @see Product
     * @return List<Product>
     * */
    public static Result getProduct(long categoryID) {
        response().setHeader("Connection","close");
        System.out.println("Start-------" + categoryID);
        //check if target data is in the cache
        if (Cache.get("allProductsOfCategory"+ categoryID) == null){
        String sql =
                "SELECT distinct ps_category_product.id_product, ps_product.id_manufacturer, ps_product.id_supplier, ps_product.reference, ps_product.upc, ps_product.id_tax_rules_group, ps_tax.id_tax, ((ps_tax.rate+100)*0.01) AS tax_rate, ps_product.price, ps_product_lang.description_short, ps_product_lang.link_rewrite, ps_product_lang.name, ps_product_lang.available_now, ps_product_lang.available_later, ps_specific_price.reduction, ps_specific_price.reduction_type, ps_specific_price.reduction_tax, ps_image.id_image, ps_stock_available.quantity\n" +
                        "\n" +
                        "FROM ps_category_product\n" +
                        "\n" +
                        "LEFT JOIN ps_product\n" +
                        "ON ps_category_product.id_product = ps_product.id_product\n" +
                        "\n" +
                        "LEFT JOIN ps_stock_available\n" +
                        "ON ps_stock_available.id_product = ps_product.id_product\n" +
                        "\n" +
                        "LEFT JOIN  ps_product_lang\n" +
                        "ON ps_product_lang.id_product = ps_product.id_product\n" +
                        "AND ps_product_lang.id_shop = 1\n" +
                        "\n" +
                        "LEFT JOIN ps_tax_rule\n" +
                        "ON ps_product.id_tax_rules_group = ps_tax_rule.id_tax_rules_group\n" +
                        "\n" +
                        "LEFT JOIN ps_tax\n" +
                        "ON ps_tax_rule.id_tax = ps_tax.id_tax\n" +
                        "\n" +
                        "LEFT JOIN ps_specific_price\n" +
                        "ON ps_product.id_product = ps_specific_price.id_product\n" +
                        " AND NOW() > ps_specific_price.from\n" +
                        " AND NOW() < ps_specific_price.to\n" +
                        "\n" +
                        "LEFT JOIN ps_image\n" +
                        "ON ps_image.id_product = ps_category_product.id_product\n" +
                        " AND ps_image.cover = 1\n" +
                        "\n" +
                        "WHERE ps_category_product.id_category =" + categoryID +
                        " AND ps_product.active = 1\n" +
                        " AND ps_stock_available.quantity > 0\n" +
                        " AND ps_stock_available.id_product_attribute = 0\n" +
                        "\n" +
                        "Order BY ps_category_product.position\n";

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_category_product.id_product", "id_product")
                        .columnMapping("ps_product.id_manufacturer", "id_manufacturer")
                        .columnMapping("ps_product.id_supplier", "id_supplier")
                        .columnMapping("ps_product.reference", "reference")
                        .columnMapping("ps_product.upc", "upc")
                        .columnMapping("ps_product.id_tax_rules_group", "id_tax_rules_group")
                        .columnMapping("ps_tax.id_tax", "tax_id")
                        .columnMapping("((ps_tax.rate+100)*0.01)", "rate")     //transfer the tax rate from integers to float numbers
                        .columnMapping("ps_product.price", "price")
//                        .columnMapping("ps_product_lang.id_shop", "id_shop")
//                        .columnMapping("ps_product_lang.description", "description")     Description data is too big for SQL query along with this query
                        .columnMapping("ps_product_lang.description_short", "description_short")
                        .columnMapping("ps_product_lang.link_rewrite", "link_rewrite")
                        .columnMapping("ps_product_lang.name", "name")
                        .columnMapping("ps_product_lang.available_now", "available_now")
                        .columnMapping("ps_product_lang.available_later", "available_later")
                        .columnMapping("ps_specific_price.reduction", "reduction")
                        .columnMapping("ps_specific_price.reduction_type", "reduction_type")
                        .columnMapping("ps_specific_price.reduction_tax", "reduction_tax")
                        .columnMapping("ps_image.id_image", "id_image")
                        .columnMapping("ps_stock_available.quantity", "quantity") //// TODO: 2017/6/8  
                        .create();

        com.avaje.ebean.Query<Product> query = Ebean.find(Product.class);
        query.setRawSql(rawSql);
        List<Product> list = query.findList();
        ArrayList<Product> total_products = new ArrayList<Product>();
        total_products.addAll(list);
        // Iterate in the List and remove those products of which the quantity is 0
        for (Iterator<Product> products = total_products.iterator();products.hasNext();) {
            Product p =products.next();
            String reduction_type = p.reduction_type;
            BigDecimal reduction_amount = BigDecimal.valueOf(0.0);
            if( reduction_type !=null && reduction_type.contains("percentage")){
                reduction_amount = p.reduction;
            }
            int product_id = p.id_product;
            ArrayList<ProductAttribute> new_attributes = getProductAttributes(product_id,reduction_amount);   //use thread
            p.attributes = new_attributes;
            if(p.rate == null){
                p.rate = BigDecimal.valueOf(1.000);
            }

        }
        Products p = new Products(total_products);                                  //Use an object to contain everything then put it into cache
//      Add the list of products into cache, expiry time is 1 day
//            如果产品不为空，存入缓存
        Cache.set("allProductsOfCategory"+ categoryID, p, 60 * 60 * 24);
        System.out.println("this is not cache-------------" + categoryID);
        return ok(renderCategories(total_products));
        }else {
          Products cache_List = (Products)Cache.get("allProductsOfCategory"+categoryID);  //new line to retrieve records from cache
          ArrayList<Product> all_products = cache_List.products;
            for (Iterator<Product> products = all_products.iterator();products.hasNext();) {
                Product p =products.next();
                String reduction_type = p.reduction_type;
                BigDecimal reduction_amount = BigDecimal.valueOf(0.0);
                if( reduction_type !=null && reduction_type.contains("percentage")){
                    reduction_amount = p.reduction;
                }
                int product_id = p.id_product;
                ArrayList<ProductAttribute> new_attributes = getProductAttributes(product_id, reduction_amount);
                int product_quantity = getQuantity(product_id); //获取当前最新数量
                if (product_quantity >0){               //remove the product of which the quantity is 0
                    p.attributes = new_attributes;
                    p.quantity = product_quantity;
                    if(!(p.rate != null)){
                        p.rate = BigDecimal.valueOf(1.000);
                    }
                }
                else {
                    products.remove();
                }
            }
        System.out.println("Cache used");
        return ok(renderCategories(all_products));
        }
    }


    //版本2， 有关产品信息，把数量为0 的产品照样显示出来
    public static Result getProductV2(long categoryID) {
        response().setHeader("Connection","close");
        System.out.println("Start-------" + categoryID);
        //check if target data is in the cache
        if (Cache.get("allProductsOfCategoryV2"+ categoryID) == null){
            String sql =
                    "SELECT distinct ps_category_product.id_product, ps_product.id_manufacturer, ps_product.id_supplier, ps_product.reference, ps_product.upc, ps_product.id_tax_rules_group, ps_tax.id_tax, ((ps_tax.rate+100)*0.01) AS tax_rate, ps_product.price, ps_product_lang.description_short, ps_product_lang.link_rewrite, ps_product_lang.name, ps_product_lang.available_now, ps_product_lang.available_later, ps_specific_price.reduction, ps_specific_price.reduction_type, ps_specific_price.reduction_tax, ps_image.id_image, ps_stock_available.quantity\n" +
                            "\n" +
                            "FROM ps_category_product\n" +
                            "\n" +
                            "LEFT JOIN ps_product\n" +
                            "ON ps_category_product.id_product = ps_product.id_product\n" +
                            "\n" +
                            "LEFT JOIN ps_stock_available\n" +
                            "ON ps_stock_available.id_product = ps_product.id_product\n" +
                            "\n" +
                            "LEFT JOIN  ps_product_lang\n" +
                            "ON ps_product_lang.id_product = ps_product.id_product\n" +
                            "AND ps_product_lang.id_shop = 1\n" +
                            "\n" +
                            "LEFT JOIN ps_tax_rule\n" +
                            "ON ps_product.id_tax_rules_group = ps_tax_rule.id_tax_rules_group\n" +
                            "\n" +
                            "LEFT JOIN ps_tax\n" +
                            "ON ps_tax_rule.id_tax = ps_tax.id_tax\n" +
                            "\n" +
                            "LEFT JOIN ps_specific_price\n" +
                            "ON ps_product.id_product = ps_specific_price.id_product\n" +
                            " AND NOW() > ps_specific_price.from\n" +
                            " AND NOW() < ps_specific_price.to\n" +
                            "\n" +
                            "LEFT JOIN ps_image\n" +
                            "ON ps_image.id_product = ps_category_product.id_product\n" +
                            " AND ps_image.cover = 1\n" +
                            "\n" +
                            "WHERE ps_category_product.id_category =" + categoryID +
                            " AND ps_product.active = 1\n" +
                            " AND ps_stock_available.id_product_attribute = 0\n" +
                            "\n" +
                            "Order BY ps_category_product.position\n";

            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_category_product.id_product", "id_product")
                            .columnMapping("ps_product.id_manufacturer", "id_manufacturer")
                            .columnMapping("ps_product.id_supplier", "id_supplier")
                            .columnMapping("ps_product.reference", "reference")
                            .columnMapping("ps_product.upc", "upc")
                            .columnMapping("ps_product.id_tax_rules_group", "id_tax_rules_group")
                            .columnMapping("ps_tax.id_tax", "tax_id")
                            .columnMapping("((ps_tax.rate+100)*0.01)", "rate")     //transfer the tax rate from integers to float numbers
                            .columnMapping("ps_product.price", "price")
//                        .columnMapping("ps_product_lang.id_shop", "id_shop")
//                        .columnMapping("ps_product_lang.description", "description")     Description data is too big for SQL query along with this query
                            .columnMapping("ps_product_lang.description_short", "description_short")
                            .columnMapping("ps_product_lang.link_rewrite", "link_rewrite")
                            .columnMapping("ps_product_lang.name", "name")
                            .columnMapping("ps_product_lang.available_now", "available_now")
                            .columnMapping("ps_product_lang.available_later", "available_later")
                            .columnMapping("ps_specific_price.reduction", "reduction")
                            .columnMapping("ps_specific_price.reduction_type", "reduction_type")
                            .columnMapping("ps_specific_price.reduction_tax", "reduction_tax")
                            .columnMapping("ps_image.id_image", "id_image")
                            .columnMapping("ps_stock_available.quantity", "quantity") //// TODO: 2017/6/8
                            .create();
            com.avaje.ebean.Query<Product> query = Ebean.find(Product.class);
            query.setRawSql(rawSql);
            List<Product> list = query.findList();
            ArrayList<Product> total_products = new ArrayList<Product>();
            total_products.addAll(list);
            // Iterate in the List and remove those products of which the quantity is 0
            for (Iterator<Product> products = total_products.iterator();products.hasNext();) {
                Product p =products.next();
                String reduction_type = p.reduction_type;
                BigDecimal reduction_amount = BigDecimal.valueOf(0.0);
                if( reduction_type !=null && reduction_type.contains("percentage")){
                    reduction_amount = p.reduction;
                }
                int product_id = p.id_product;
                ArrayList<ProductAttribute> new_attributes = getProductAttributes(product_id,reduction_amount);   //use thread
                p.attributes = new_attributes;
                if(p.rate == null){
                    p.rate = BigDecimal.valueOf(1.000);
                }
            }
            Products p = new Products(total_products);                                  //Use an object to contain everything then put it into cache
//      Add the list of products into cache, expiry time is 1 day
//            如果产品不为空，存入缓存
            Cache.set("allProductsOfCategoryV2"+ categoryID, p, 60 * 60 * 24);
            System.out.println("this is not cache-------------" + categoryID);
            return ok(renderCategories(total_products));
        }else {
            Products cache_List = (Products)Cache.get("allProductsOfCategoryV2"+categoryID);  //new line to retrieve records from cache
            ArrayList<Product> all_products = cache_List.products;
            for (Iterator<Product> products = all_products.iterator();products.hasNext();) {
                Product p =products.next();
                String reduction_type = p.reduction_type;
                BigDecimal reduction_amount = BigDecimal.valueOf(0.0);
                if( reduction_type !=null && reduction_type.contains("percentage")){
                    reduction_amount = p.reduction;
                }
                int product_id = p.id_product;
                ArrayList<ProductAttribute> new_attributes = getProductAttributes(product_id, reduction_amount);
                int product_quantity = getQuantity(product_id); //获取当前最新数量
                p.attributes = new_attributes;
                p.quantity = product_quantity;
                if(!(p.rate != null)){
                p.rate = BigDecimal.valueOf(1.000);
                }
            }
            System.out.println("Cache used");
            return ok(renderCategories(all_products));
        }
    }

    /**
     * Get descriptions of one product. The result will be "heavy" string data.
     * @param productID
     * @see Description
     * */
    public static Result getDescription(long productID) {
        response().setHeader("Connection","close");
        if (Cache.get("DescriptionOfTheProduct"+ productID) == null){
            String sql =
                    "select ps_product_lang.description\n" +
                            "\n" +
                            "from ps_product_lang\n" +
                            "\n" +
                            "where id_product =" + productID;

            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_product_lang.description", "product_description")
                            .create();

            com.avaje.ebean.Query<Description> query = Ebean.find(Description.class);
            query.setRawSql(rawSql);
//        query.setParameter
            List<Description> list = query.findList();
            ArrayList<Description> descriptions = new ArrayList<Description>();
            descriptions.addAll(list);
            if (descriptions.size() >0){
                Description description_product = descriptions.get(0);

                Cache.set("DescriptionOfTheProduct"+ productID, description_product, 60 * 60 * 24);

                return ok(renderCategories(description_product));

            }else {
                return notFound("no such product");
            }
        }else {
//            ArrayList<Description> descriptions = (ArrayList<Description>) Cache.get("DescriptionOfTheProduct"+ productID);
            Description description = (Description)Cache.get("DescriptionOfTheProduct"+ productID);

            return ok(renderCategories(description));
        }
    }

   /**Get quantity of the product
    * @param productID
    * @return an integer
    * @see ProductQuantity
    * */
    public static int getQuantity(long productID) {
         String sql =
                "select ps_stock_available.quantity\n" +
                        "\n" +
                        "from ps_stock_available\n" +
                        "\n" +
                        " where ps_stock_available.id_product = \n" + productID +
                        " and ps_stock_available.id_product_attribute = 0" ;

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_stock_available.quantity", "quantity")
                        .create();

        com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
        query.setRawSql(rawSql);
        List<ProductQuantity> list = query.findList();
        ArrayList<ProductQuantity> quantities = new ArrayList<ProductQuantity>();
        quantities.addAll(list);
        int quantity = quantities.get(0).quantity;
        return quantity;
    }

    /**Get the wholesale price of a product
     * @param productID
     * @see ProductWholesale
     * */
    public static Result getWholePrice(int productID) {
        response().setHeader("Connection","close");
        String sql =
                "select ps_product.wholesale_price\n" +
                        "\n" +
                        "from ps_product\n" +
                        "\n" +
                        " where ps_product.id_product = \n" + productID;

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_product.wholesale_price", "whole_sale")
                        .create();

        com.avaje.ebean.Query<ProductWholesale> query = Ebean.find(ProductWholesale.class);
        query.setRawSql(rawSql);
        List<ProductWholesale> list = query.findList();
        ArrayList<ProductWholesale> whole_prices = new ArrayList<ProductWholesale>();
        whole_prices.addAll(list);                  //list to Array
        BigDecimal wholesale_price = whole_prices.get(0).whole_sale;
        return ok(renderCategories(wholesale_price));
    }


    /**
     * Shop products search with key words, the minimum word length is 3 letters
     * @param name
     * @return ArrayList<Product>
     * @see Product
     * */
    public static Result searchProduct(String name) {
        response().setHeader("Connection","close");
        if (name.length() < 3) {
            return status(403,"Not enough letters");
        }else {
            if(name.contains("&")){ //如果产品名字里面含有 &， 比如 MM 豆
                name = name.replaceAll("'s", "");
            }else{
                name = name.replaceAll("'s", "");
                name = name.replaceAll("[^A-Za-z0-9 ]", "");
                name = name.replaceAll(" ", "%");
            }
            String sql =
                    "SELECT distinct ps_category_product.id_product, ps_product.id_manufacturer, ps_product.id_supplier, ps_product.reference, ps_product.upc, ps_product.id_tax_rules_group, ps_tax.id_tax, ((ps_tax.rate+100)*0.01) AS tax_rate, ps_product.price, ps_product_lang.description_short,\n" +
                            "ps_product_lang.link_rewrite, ps_product_lang.name, ps_product_lang.available_now, ps_product_lang.available_later, ps_specific_price.reduction, ps_specific_price.reduction_type, ps_specific_price.reduction_tax, ps_image.id_image\n" +
                            "\n" +
                            "FROM ps_category_product\n" +
                            "\n" +
                            "LEFT JOIN ps_product\n" +
                            "ON ps_category_product.id_product = ps_product.id_product\n" +
                            "\n" +
                            "LEFT JOIN ps_stock_available\n" +
                            "ON ps_product.id_product = ps_stock_available.id_product\n" +
                            "\n" +
                            "LEFT JOIN  ps_product_lang\n" +
                            "ON ps_product_lang.id_product = ps_product.id_product\n" +
                            "AND ps_product_lang.id_shop = 1\n" +
                            "\n" +
                            "LEFT JOIN ps_tax_rule\n" +
                            "ON ps_product.id_tax_rules_group = ps_tax_rule.id_tax_rules_group\n" +
                            "\n" +
                            "LEFT JOIN ps_tax\n" +
                            "ON ps_tax_rule.id_tax = ps_tax.id_tax\n" +
                            "\n" +
                            "LEFT JOIN ps_specific_price\n" +
                            "ON ps_product.id_product = ps_specific_price.id_product\n" +
                            " AND NOW() > ps_specific_price.from\n" +
                            " AND NOW() < ps_specific_price.to\n" +
                            "\n" +
                            "LEFT JOIN ps_image\n" +
                            "ON ps_image.id_product = ps_category_product.id_product\n" +
                            "AND ps_image.cover = 1\n" +
                            "\n" +
                            "WHERE ps_product_lang.name like" + "'%" + name + "%'\n" +
                            "AND ps_product.active = 1\n" +
                            "AND ps_product.upc != 1\n" +
                            "AND ps_stock_available.id_product_attribute = 0\n" +
                            "AND ps_stock_available.quantity > 0\n" +
                            "Order BY ps_product.id_product";

            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_category_product.id_product", "id_product")
                            .columnMapping("ps_product.id_manufacturer", "id_manufacturer")
                            .columnMapping("ps_product.id_supplier", "id_supplier")
                            .columnMapping("ps_product.reference", "reference")
                            .columnMapping("ps_product.upc", "upc")
                            .columnMapping("ps_product.id_tax_rules_group", "id_tax_rules_group")
                            .columnMapping("ps_tax.id_tax", "tax_id")
                            .columnMapping("((ps_tax.rate+100)*0.01)", "rate")
                            .columnMapping("ps_product.price", "price")
//                        .columnMapping("ps_product_lang.id_shop", "id_shop")
//                    .    columnMapping("ps_product_lang.description", "description")   // must delete it because of the heavy data
                            .columnMapping("ps_product_lang.description_short", "description_short")
                            .columnMapping("ps_product_lang.link_rewrite", "link_rewrite")
                            .columnMapping("ps_product_lang.name", "name")
                            .columnMapping("ps_product_lang.available_now", "available_now")
                            .columnMapping("ps_product_lang.available_later", "available_later")
                            .columnMapping("ps_specific_price.reduction", "reduction")
                            .columnMapping("ps_specific_price.reduction_type", "reduction_type")
                            .columnMapping("ps_specific_price.reduction_tax", "reduction_tax")
                            .columnMapping("ps_image.id_image", "id_image")
                            .create();

            com.avaje.ebean.Query<Product> query = Ebean.find(Product.class);
            query.setRawSql(rawSql);
            List<Product> list = query.findList();
            Product[] all_products = new Product[list.size()];
            int number = list.size();
            list.toArray(all_products);
            for (int i = 0; i < number; i++) {
                if (all_products[i].rate == null) {
                    all_products[i].rate = BigDecimal.valueOf(1.000);
                }
                int product_id = all_products[i].id_product;
                String reduction_type = all_products[i].reduction_type;
                BigDecimal reduction_amount = BigDecimal.valueOf(0.0);
                if( reduction_type !=null && reduction_type.contains("percentage")){
                    reduction_amount = all_products[i].reduction;
                }
                all_products[i].attributes = getProductAttributes(product_id, reduction_amount);
                all_products[i].quantity = getQuantity(product_id);
            }
            return ok(renderCategories(all_products));
        }
    }

    public static Result updateMealDeals(int deal_shop_product_id, int if_available) {
        response().setHeader("Connection","close");
          int available_number = 0;
          if(if_available !=0){
              available_number =999;
          }
          ArrayList<Integer> id_product_attributes = DealProductsTable.findByShopProduct(deal_shop_product_id);
         if(id_product_attributes.size() !=0){  //循环更新数量
             try {
                 for (Integer this_one : id_product_attributes) {
                     String dml = "UPDATE ps_stock_available\n" +
                             "SET quantity = :quantity\n" +
                             "WHERE id_product_attribute = :id_product_attribute";
                     SqlUpdate update = Ebean.createSqlUpdate(dml)
                             .setParameter("quantity", available_number)
                             .setParameter("id_product_attribute", this_one);
                     update.execute();
                 }
             }catch (Exception e){
                 return badRequest("Something is wrong in the Stock Available Table");
             }
            reGenerateCache(522);  //meal deal
            reGenerateCache(526); //lunch meal
            reGenerateCache(527); //Summer meal
            System.out.println("generate new cache");
            return ok("successfully cleared");
         }else {
             return badRequest("This shop product is not mealDeal product");
         }
    }

    /**
     * 把meal_Deal 里面的产品下架
     * */
    public static synchronized void removeMealDealProduct(int deal_shop_product_id, List<Integer> id_product_attribute) {
            try {
                for (Integer this_one : id_product_attribute) {
                    String dml = "UPDATE ps_stock_available\n" +
                            "SET quantity = :quantity\n" +
                            "WHERE id_product_attribute = :id_product_attribute";
                    SqlUpdate update = Ebean.createSqlUpdate(dml)
                            .setParameter("quantity", 0)
                            .setParameter("id_product_attribute", this_one);
                    update.execute();
                }
                updateOneRecord(deal_shop_product_id,0,0); //把卖完的meal_deal shop 产品给下架
            }catch (Exception e){
                System.out.println("Something is wrong in the Stock Available Table");
            }
            reGenerateCache(522);  //meal deal
            reGenerateCache(526); //lunch meal
//            reGenerateCache(527); //Summer meal
            reGenerateCache(830); //Big Sale
            reGenerateCache(534); //BuyOneGetOne
            System.out.println("generate new cache");
            System.out.println("successfully cleared");
    }

    public static void reGenerateCache(long categoryID) {
        Cache.remove("allProductsOfCategory" + categoryID);
        String sql =
                "SELECT distinct ps_category_product.id_product, ps_product.id_supplier, ps_product.reference, ps_product.upc, ps_product.id_tax_rules_group, ps_tax.id_tax, ((ps_tax.rate+100)*0.01) AS tax_rate, ps_product.price, ps_product_lang.description_short, ps_product_lang.link_rewrite, ps_product_lang.name, ps_product_lang.available_now, ps_product_lang.available_later, ps_specific_price.reduction, ps_specific_price.reduction_type, ps_specific_price.reduction_tax, ps_image.id_image, ps_stock_available.quantity\n" +
                        "\n" +
                        "FROM ps_category_product\n" +
                        "\n" +
                        "LEFT JOIN ps_product\n" +
                        "ON ps_category_product.id_product = ps_product.id_product\n" +
                        "\n" +
                        "LEFT JOIN ps_stock_available\n" +
                        "ON ps_stock_available.id_product = ps_product.id_product\n" +
                        "\n" +
                        "LEFT JOIN  ps_product_lang\n" +
                        "ON ps_product_lang.id_product = ps_product.id_product\n" +
                        "AND ps_product_lang.id_shop = 1\n" +
                        "\n" +
                        "LEFT JOIN ps_tax_rule\n" +
                        "ON ps_product.id_tax_rules_group = ps_tax_rule.id_tax_rules_group\n" +
                        "\n" +
                        "LEFT JOIN ps_tax\n" +
                        "ON ps_tax_rule.id_tax = ps_tax.id_tax\n" +
                        "\n" +
                        "LEFT JOIN ps_specific_price\n" +
                        "ON ps_product.id_product = ps_specific_price.id_product\n" +
                        " AND NOW() > ps_specific_price.from\n" +
                        " AND NOW() < ps_specific_price.to\n" +
                        "\n" +
                        "LEFT JOIN ps_image\n" +
                        "ON ps_image.id_product = ps_category_product.id_product\n" +
                        " AND ps_image.cover = 1\n" +
                        "\n" +
                        "WHERE ps_category_product.id_category = "+ categoryID +"\n"+
                        " AND ps_product.active = 1\n" +
                        " AND ps_stock_available.quantity > 0\n" +
                        " AND ps_stock_available.id_product_attribute = 0\n" +
                        "\n" +
                        "Order BY ps_category_product.position\n";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_category_product.id_product", "id_product")
                        .columnMapping("ps_product.id_supplier", "id_supplier")
                        .columnMapping("ps_product.reference", "reference")
                        .columnMapping("ps_product.upc", "upc")
                        .columnMapping("ps_product.id_tax_rules_group", "id_tax_rules_group")
                        .columnMapping("ps_tax.id_tax", "tax_id")
                        .columnMapping("((ps_tax.rate+100)*0.01)", "rate")     //transfer the tax rate from integers to float numbers
                        .columnMapping("ps_product.price", "price")
//                        .columnMapping("ps_product_lang.id_shop", "id_shop")
//                        .columnMapping("ps_product_lang.description", "description")     Description data is too big for SQL query along with this query
                        .columnMapping("ps_product_lang.description_short", "description_short")
                        .columnMapping("ps_product_lang.link_rewrite", "link_rewrite")
                        .columnMapping("ps_product_lang.name", "name")
                        .columnMapping("ps_product_lang.available_now", "available_now")
                        .columnMapping("ps_product_lang.available_later", "available_later")
                        .columnMapping("ps_specific_price.reduction", "reduction")
                        .columnMapping("ps_specific_price.reduction_type", "reduction_type")
                        .columnMapping("ps_specific_price.reduction_tax", "reduction_tax")
                        .columnMapping("ps_image.id_image", "id_image")
                        .columnMapping("ps_stock_available.quantity", "quantity")
                        .create();
        com.avaje.ebean.Query<Product> query = Ebean.find(Product.class);
        query.setRawSql(rawSql);
        List<Product> list = query.findList();
        ArrayList<Product> total_products = new ArrayList<Product>();
        total_products.addAll(list);
        // Iterate in the List and remove those products of which the quantity is 0
        for (Iterator<Product> products = total_products.iterator(); products.hasNext(); ) {
            Product p = products.next();
            String reduction_type = p.reduction_type;
            BigDecimal reduction_amount = BigDecimal.valueOf(0.0);
            if (reduction_type != null && reduction_type.contains("percentage")) {
                reduction_amount = p.reduction;
            }
            int product_id = p.id_product;
            ArrayList<ProductAttribute> new_attributes = setNewCache(product_id, reduction_amount);   //use thread
                p.attributes = new_attributes;
                if (p.rate == null) {
                    p.rate = BigDecimal.valueOf(1.000);
                }
        }
        Products p = new Products(total_products);                                  //Use an object to contain everything then put it into cache
//      Add the list of products into cache, expiry time is 1 day
        Cache.set("allProductsOfCategory" + categoryID, p, 60 * 60 * 24);
    }

    public static String getProductName(int productID) {
            String sql =
                    "SELECT name \n" +
                            "FROM ps_product_lang\n" +
                            "WHERE id_product = " + productID;

            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("name", "product_description")
                            .create();

            com.avaje.ebean.Query<Description> query = Ebean.find(Description.class);
            query.setRawSql(rawSql);
            Description product_name = query.findUnique();
            String name;
            if(product_name == null){
                name = "One product";
            }else {
                name = product_name.product_description;
            }
            return name;
    }

}
