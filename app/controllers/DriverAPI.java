/*
 * Copyright (c) 2017. Zhi Chen.
 * DriverAPI is used for cashiers to allocate orders to drivers.
 * Then to calculate how much the orders allocated cost, and how
 * much cash drivers should return.Lastly to have a summary of
 * what a driver did in the past whole day(14 hours interval).
 */

package controllers;


import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import com.avaje.ebean.SqlUpdate;
import com.avaje.ebean.annotation.Transactional;
import models.database.*;
import models.sqlContainer.*;
import models.template.AddressTemplate;
import models.template.DriverOrderWithInformation;
import models.template.RequestDriverOrders;
import play.data.DynamicForm;
import play.data.Form;
import play.data.validation.Constraints;
import play.data.validation.Constraints.Required;
import play.mvc.Controller;
import play.mvc.Result;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static controllers.OrderAPI.change_product_quantity;
import static parsers.JsonParser.*;

public class DriverAPI extends Controller {

     /**
     * New Drive Registration class
     * */
    public static class NewDriver{
        @Required
        public String name;

        public String contact_number;

        public NewDriver(String name, String contact_number) {
            this.name = name;
            this.contact_number = contact_number;
        }

        public NewDriver() {
        }
    }

    public static class DriverDailyPayroll{
        @Constraints.Required
        public String till_money;  //driver took money from till
        @Constraints.Required
        public String salary;   //driver salary the day
        @Constraints.Required
        public String extra_cost;   //extra money for Driver
        @Constraints.Required
        public String specification;   //extra money for Driver
        @Constraints.Required
        public String working_hours;   //Driver
    }

    /**
     * Query all the drivers
     * */
    public static Result allDrivers() throws Exception {
        response().setHeader("Connection","close");
        List<Driver> all_drivers = Driver.findAll();
        if(all_drivers ==null){
            return notFound("Invalid request");
        }
        else {
            return ok(renderCategories(all_drivers));
        }
    }

    /**
     * Query drivers according to work status
     * @param if_work
     * @return List<Driver>
     * */
    public static Result checkDrivers(int if_work) throws Exception { //todo: 更改司机名称
        response().setHeader("Connection","close");
        List<Driver> work_drivers = Driver.findByState(if_work);
        if(work_drivers == null){
            return notFound("Invalid request");
        }
        else {
            return ok(renderCategories(work_drivers));
        }
    }

    /**
     * Driver register action. through HTTP POST request
     * */
    public static Result postRegister() throws Exception {
        response().setHeader("Connection","close");
       Form<NewDriver> driverForm = Form.form(NewDriver.class).bindFromRequest();
        if (driverForm != null){
                String driver_name = driverForm.get().name;
                String contact_number = driverForm.get().contact_number;
                if (driver_name == null){
                    driver_name = "";
                }
                if (contact_number == null){
                    contact_number = "";
                }
                if (Driver.findByName(driver_name)!= null || Driver.findByPhone(contact_number)!= null){
                    return status(409,"The driver already exists");
                }else{
                    Date register_time = new Date();
                    Driver driver = new Driver(
                            driver_name,
                            0,
                            register_time,
                            register_time,
                            new Date(0),
                            contact_number,
                            "a2bliving",
                            new Date(),
                            new Date(),
                            0
                           );
                    driver.save();
                    System.out.println("A new driver successfully registered");
                    return status(201,"registered");
                }
        }else {
            return status(404,"bad request");
        }
    }

    /**
     * Driver Login data template
     * */
    public static class DriverLogin {
        @Required
        public int id_driver;
        public Integer car_type;

        public DriverLogin(int id_driver, Integer car_type){
            this.id_driver = id_driver;
            this.car_type = car_type;
        }
        public DriverLogin(){
        }
    }

    /**
     * All the orders of the day for drivers to deliver
     * Cashier will allocate those orders to at-work drivers
     * */
    public static Result OrdersOfTheDay() throws Exception {
        response().setHeader("Connection","close");
        //RawSQL, 得到 todo : order_sequence number
        List<OrderWithSequence> orders_with_sequence = getUnassignedOrders();
        return ok(renderCategories(orders_with_sequence));
    }

    //得到所有没有被分配过司机的订单
    public static List<OrderWithSequence> getUnassignedOrders(){
        String sql =
                "select ps_orders.id_order, ps_orders.reference, ps_orders.id_customer, ps_orders.payment,ps_orders.total_discounts, ps_orders.total_paid, ps_orders.total_shipping, ps_orders.date_add, ps_orders.delivery_date, ps_order_state_lang.name, ps_order_invoice.delivery_address\n" +
                        "from ps_orders, ps_order_state_lang, ps_order_invoice\n" +
                        "where ps_orders.current_state = ps_order_state_lang.id_order_state\n" +
                        "and ps_orders.id_order = ps_order_invoice.id_order\n" +
                        "and ps_orders.valid = 1\n" +
                        "and ps_orders.invoice_date > DATE_SUB(NOW(), INTERVAL 13 HOUR)\n" +
                        "and ps_orders.id_order not in (\n" +
                        "select ps_drivers_orders.id_order \n" +
                        "from  ps_drivers_orders\n" +
                        "where ps_drivers_orders.date_add > DATE_SUB(NOW(), INTERVAL 13 HOUR))\n" +
                        "order by ps_orders.id_order desc";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_orders.id_order", "id_order")
                        .columnMapping("ps_orders.reference", "order_reference")
                        .columnMapping("ps_orders.id_customer", "id_customer")
                        .columnMapping("ps_orders.payment", "payment_method")
                        .columnMapping("ps_orders.total_discounts", "total_discounts")
                        .columnMapping("ps_orders.total_paid", "total_paid")
                        .columnMapping("ps_orders.total_shipping", "total_shipping")
                        .columnMapping("ps_orders.date_add", "date_add")
                        .columnMapping("ps_orders.delivery_date", "delivery_date")
                        .columnMapping("ps_order_state_lang.name", "order_state")
                        .columnMapping("ps_order_invoice.delivery_address", "delivery_address")
                        .create();
        com.avaje.ebean.Query<OrderWithSequence> query = Ebean.find(OrderWithSequence.class);
        query.setRawSql(rawSql);
        List<OrderWithSequence> list = query.findList();
        for(OrderWithSequence each_one: list){
            each_one.order_sequence = getSequenceNumber(each_one.id_order);
        }
        return list;
    }
    /**
     * In the charge of one certain cashier, all the orders of driver delivery will be displayed
     * @param id_driver
     */
    public static Result OrderOfDriver(int id_driver) throws Exception {
        response().setHeader("Connection","close");
        DynamicForm driver_cashier_order = Form.form().bindFromRequest();
        int id_cashier;
        int if_paid;
        try{
        id_cashier = Integer.parseInt(driver_cashier_order.get("id_cashier"));
        if_paid = Integer.parseInt(driver_cashier_order.get("if_paid"));
        }
        catch(Exception e) {
        return status(409,"No valid cashier information");
        }
        List<DriverOrders> driverOrders = DriverOrders.workOfDriver(id_driver,id_cashier,if_paid);
        return ok(renderCategories(driverOrders));
    }
    /**
     * Cashier controls drivers to log in, log in with Driver_ID
     * Change the value of if_work in the Drivers Table from 0 to 1
     * 前台帮司机登陆， 需要记录司机当天是开公司车，还是开自己车
     * if_company_car = 1 或者 2，    1：自己的车； 2： 公司的车
     * */
    public static Result DriverLogin() throws Exception {
         response().setHeader("Connection","close");
         Form<DriverLogin> driverForm = Form.form(DriverLogin.class).bindFromRequest();
         int id = driverForm.get().id_driver;
         Date login_time = new Date();
         Driver driver = Driver.findById(id);
         Integer if_company_car = driverForm.get().car_type; //1 是自己车， 2 是公司车
         if(if_company_car == null){
             if_company_car = 0;
         }
        if (driver != null) {
               driver.if_work = 1;
               driver.last_login = login_time;
               driver.if_company_car = if_company_car;
               driver.save();
            return ok("login successfully");
        }else {
            return notFound("Invalid driver");
        }
     }
    /**
     * Cashier controls driver to log out.
     * Change the if_paid value to 0.
     * 前台帮司机log_out, if_company_car = 0
     * @param id_driver
     * */
    public static Result DriverLogout(int id_driver) throws Exception {
        response().setHeader("Connection","close");
        Date logout_time = new Date();
        Driver driver = Driver.findById(id_driver);
        if (driver != null) {
            driver.update(0,logout_time); //当天退出的时候不改变开什么车的状态，避免先log out 之后 司机再结算，就没法判断开什么车
            driver.save();
            return ok("log out successfully");
        }else {
            return notFound("Invalid driver");
        }
    }
    /**
     * Cashier allocate orders to drivers.
     * In this process, a joint record combined with cashier_id, order_id and driver_id
     * will be created in the database.
     * @param id_driver
     * @param cashier_id
     * */
    @Transactional
    public static Result allocateOrderToDriver(int id_driver, int cashier_id) throws Exception {
        response().setHeader("Connection","close");
//        String body = request().body().asJson().toString();
        ArrayList<RequestDriverOrders> requestDriverOrders = renderRequestDriverTemplate(request().body().asJson().toString());
        int size = requestDriverOrders.size();
        if (size == 0){
        return notFound("Please assign one order first");
        }else {
//            System.out.println(requestDriverOrders);
            ArrayList<DriverOrderWithInformation> error_orders = new ArrayList<>();
            for(RequestDriverOrders driverOrder: requestDriverOrders){
                int id_order = driverOrder.id_order;
                int order_number = driverOrder.order_number;
                String driver_name = driverOrder.driver_name;
                if (driver_name == null) {
                    driver_name = "";
                }
                String order_reference = driverOrder.order_reference;
                Date date_add = new Date();
                DriverOrders a = DriverOrders.findByOrderId(id_order);
                DriverOrders b = DriverOrders.findByReference(order_reference);
                if (a != null || b != null) {
                    error_orders.add(new DriverOrderWithInformation(id_order,order_reference,order_number,"This order was already added"));
                }else {
                    int if_paid = 0;
                    String payment = driverOrder.payment_method;
                    if (payment != null && !payment.contains("Cash")) {
                        if_paid = 1;
                    }
                    Date original_time = new Date(0);
                    DriverOrders driverOrders = new DriverOrders(
                            id_driver,
                            driver_name,
                            id_order,
                            order_reference,
                            1,
                            date_add,
                            order_number,
                            cashier_id,
                            if_paid,
                            0,
                            0,
                            original_time,
                            original_time
                    );
                    driverOrders.save();
//                    changeOrderStatus(id_order,4);
                    Order.updateOneRecord(id_order,4);
                    OrderHistory orderHistory = new OrderHistory(
                            0,
                            id_order,
                            4,
                            date_add
                    );
                    orderHistory.save();
                }
            }
            return ok(renderCategory(error_orders));
        }
    }

    public static void changeOrderStatus(int order_id, int status_number){
        Order this_order = Order.findById(order_id);
        this_order.current_state = status_number;
        this_order.save();
    }

    public static Result deleteOrdersFromDriver(int id_order){
        response().setHeader("Connection","close");
        DriverOrders driverOrder = DriverOrders.findByOrderId(id_order);
        if(driverOrder == null){
            return notFound("No such record yet");
        }else{
            DriverOrders.delete(id_order);
            return ok("Record deleted");
        }
    }
    /**
     * 删除订单： 需要从数据库不同的表格中删除一系列记录
     * */
    @Transactional
    public static Result cancelOneOrder(int id_order, int id_cashier){
        response().setHeader("Connection","close");
        Order old_order  = Order.findById(id_order);
        if(old_order == null){
            return notFound("No such record yet"); //404
        }else if(old_order.current_state ==6){
            return ok("Already Cancelled before"); //200
        }else{
            if(id_cashier == 149 || id_cashier ==102){ //前台两个cashier
                id_cashier = 7;
            }else{
                id_cashier = 0;
            }
            old_order.current_state = 6;  //order cancelled state
            old_order.valid = 0;
            old_order.save();
            OrderHistory orderHistory = new OrderHistory(
                    id_cashier,
                    id_order,
                    6,
                    new Date()
            );
            orderHistory.save();
            List<OrderCartRule> if_has_voucher = OrderCartRule.findByOrder(id_order);
            if(if_has_voucher.size()>0){
                for(OrderCartRule this_record: if_has_voucher){
                 int id_cart_rule = this_record.id_cart_rule;
                    CartRule this_rule = CartRule.findById(id_cart_rule);
                    if(null !=this_rule){
                        this_rule.quantity = this_rule.quantity + 1;   //返回数量
                        this_rule.save();
                    }
                    this_record.delete();  //删除使用记录
                }
            }
            //ps_order_detail 返回被买的产品数量
            List<OrderDetail> all_products = OrderDetail.findByOrder(id_order);
            if(all_products.size()>0){
                for(OrderDetail this_product: all_products){
                    int id_product = this_product.product_id; //这件产品的id
                    int quantity = this_product.product_quantity; //这件产品的被购买数量
                    //ps_stock_available
                    change_product_quantity(id_product,quantity,1);
                    //判断是否存在 buy_one_get_one 产品
                    List<BuyOneGetOne> if_exist_list = BuyOneGetOne.findBySaleProductId(id_product);
                    if(if_exist_list.size() > 0){
                      for(BuyOneGetOne this_one: if_exist_list){
                          int real_product_id = this_one.id_real_product;
                          int real_bought_quantity = (1/this_one.basic_unit) * this_one.bundle_number;
                          change_product_quantity(real_product_id,real_bought_quantity,1);  // buy_one_get_one 产品数量恢复
                      }
                    }
                }
            }
            //check if_exist reward record:
            OrderReward if_rewarded = OrderReward.findById(id_order);
            if(if_rewarded != null){ //存在被奖励记录
                int id_cart_rule = if_rewarded.id_cart_rule; //找到id_cart_rule, 并且让此失效
                if(id_cart_rule !=9 && id_cart_rule!=44) {  //Marina和新用户的广泛voucher不能失效
                    CartRule rewarded_cart_rule = CartRule.findById(id_cart_rule);
                    if (rewarded_cart_rule != null) {
                        rewarded_cart_rule.if_active = 0; //直接变成无效
                        rewarded_cart_rule.save();
                    }
                }
            }
            //deal sale products
            // 恢复被购买的meal_deal 产品数量
            String sql =
                    "select ps_deal_sale_table.id_product_shop, ps_deal_sale_table.key_word,sum(ps_order_detail.product_quantity)\n" +
                            "from ps_orders, ps_order_detail, ps_deal_sale_table\n" +
                            "where ps_orders.id_order = ps_order_detail.id_order\n" +
                            "and ps_orders.id_order = " + id_order +"\n" +
                            "and ps_order_detail.product_id = ps_deal_sale_table.deal_product_id\n" +
                            "and  ps_order_detail.product_name like CONCAT('%', ps_deal_sale_table.key_word, '%')\n" +
                            "group by ps_deal_sale_table.id_product_shop";
            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_deal_sale_table.id_product_shop", "id_product_shop")
                            .columnMapping("ps_deal_sale_table.key_word", "key_word")
                            .columnMapping("sum(ps_order_detail.product_quantity)", "purchased_quantity")
                            .create();
            com.avaje.ebean.Query<MealDealProduct> query = Ebean.find(MealDealProduct.class);
            query.setRawSql(rawSql);
            List<MealDealProduct> list = query.findList();
            if(list.size() >0){
                for(MealDealProduct this_one: list){
                    int bought_quantity = this_one.purchased_quantity;
                    StockAvailable current_quantity = StockAvailable.findByAttribute(this_one.id_product_shop,0);
                    current_quantity.quantity = current_quantity.quantity + bought_quantity; //还原被购买的deal产品
                    current_quantity.save();
                }
            }
            return ok("Order Cancelled Successfully"); //200
        }
    }

    /**
     * Cashier creates a new record when the driver delivered an order and paid back the order sales(delivery fee exclusive)
     * The record consists of cashier_id, order_id as well as driver_id
     * @param id_order
     * */
     public static Result driverBackPay(int id_order) throws Exception {
         response().setHeader("Connection","close");
        DynamicForm driverPay = Form.form().bindFromRequest();
//        int order_number = Integer.parseInt(driverPay.get("order_number"));
        int id_cashier = Integer.parseInt(driverPay.get("id_cashier")); //new
        DriverOrders newOrder = DriverOrders.findByOrderId(id_order);
        if (newOrder == null){
            return status(409,"Please allocate a driver first");
        }else{
            if(newOrder.if_paid ==1){
                return status(404,"Already paid to cashier");
            }else {
                newOrder.if_paid = 1;
                newOrder.paid_cashier = id_cashier;
                newOrder.save();
                return ok("successfully paid to cashier");
            }
        }
     }

    /**
     * All delivered orders of a driver
     * @param id_driver
     * */
     public static Result orderHistories(int id_driver) throws Exception {
         response().setHeader("Connection","close");
        Driver driver = Driver.findById(id_driver);
        if (driver == null){
          return status(409,"Not a valid driver");
        }else{
            List<DriverOrders> all_history = DriverOrders.findByDriver(id_driver);
            return ok(renderCategories(all_history));
        }
     }

    /**
     * Generate the order sequence number for each one, for the convenience of
     * calculating order numbers and counting orders of a day.
     * */
    public static int getSequenceNumber(int id_order) {
    //SQL get the sequential number of each order among all the orders of a day
    String sql = "SELECT count(*) FROM ps_orders\n" +
                    " WHERE invoice_date >= cast((SELECT invoice_date FROM ps_orders\n" +
                    " WHERE id_order =" + id_order + ") AS date) \n" +
                    " AND invoice_date < (SELECT invoice_date FROM ps_orders\n" +
                    " WHERE  id_order =" + id_order+ ")";
    RawSql rawSql =
            RawSqlBuilder
                    .parse(sql)
                    .columnMapping("count(*)", "order_sequence")
                    .create();

    com.avaje.ebean.Query<OrderSequence> query = Ebean.find(OrderSequence.class);
    query.setRawSql(rawSql);
    OrderSequence sequence = query.findUnique();
        //order sequential number starts from 101.
    return sequence.order_sequence + 101;
    }
    /**
     * Get a total summary of overall orders that a drivers did in one day.
     * The summary includes explicit quantity of online-payment orders and
     * cash-payment orders,as well as the total price amount of both payment
     * methods, and also total cash in the driver's hand after the whole day's work..
     * @param id_driver
     * @see TotalReturnDriverOrder
     * */
    public static ArrayList<TotalReturnDriverOrder> getOrdersOfDriver(int id_driver) {  //司机晚上回来付完钱才知道结果  if_paid= 1
        String sql =
                "select ps_drivers_orders.driver_name, ps_orders.payment, sum(total_paid_real) as total_amount_sale, sum(total_shipping) as total_shipping_fee, count(payment) as number_of_order\n" +
                        "\n" +
                        "from ps_orders, ps_drivers_orders\n" +
                        "\n" +
                        "where ps_orders.id_order = ps_drivers_orders.id_order\n" +
                        "\n" +
                        "and ps_orders.reference = ps_drivers_orders.order_reference\n" +
                        "\n" +
                        "and ps_orders.valid = 1\n" +  //valid orders only
                        "\n" +
                        "and ps_drivers_orders.id_driver =" + id_driver + "\n" +
                        "\n" +
                        "and ps_drivers_orders.if_paid = 1\n" +
                        "\n" +
                        "and ps_drivers_orders.date_add > DATE_SUB(NOW(), INTERVAL 12 HOUR)\n" +  //12小时之内的订单
                        "\n" +
                        "group by payment\n";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_drivers_orders.driver_name", "driver_name")
                        .columnMapping("ps_orders.payment", "payment_method")
                        .columnMapping("sum(total_paid_real)", "total_sale_online")      //total_paid_real        total_paid_real - total_shipping
                        .columnMapping("sum(total_shipping)", "total_shipping_online")
                        .columnMapping("count(payment)", "total_numberof_online_order")
                        .create();

        com.avaje.ebean.Query<TotalReturnDriverOrder> query = Ebean.find(TotalReturnDriverOrder.class);
        query.setRawSql(rawSql);
//        query.setParameter
        List<TotalReturnDriverOrder> list = query.findList();
        int size = list.size();
        for (int i =0; i <size; i++){
            list.get(i).total_sale_online = list.get(i).total_sale_online.subtract(list.get(i).total_shipping_online);
        }
        ArrayList<TotalReturnDriverOrder> totalReturnDriverOrders = new ArrayList<>();
        totalReturnDriverOrders.addAll(list);
        return totalReturnDriverOrders;
    }

    public static ArrayList<TotalReturnDriverOrder> getAllOrdersOfDriver(int id_driver) {  //在司机付钱之前就算出所有数据  MySQL 筛选条件删除if_paid= 1
        String sql =
                "select ps_drivers_orders.driver_name, ps_orders.payment, sum(total_paid_real) as total_amount_sale, sum(total_shipping) as total_shipping_fee, count(payment) as number_of_order\n" +
                        "\n" +
                        "from ps_orders, ps_drivers_orders\n" +
                        "\n" +
                        "where ps_orders.id_order = ps_drivers_orders.id_order\n" +
                        "\n" +
                        "and ps_orders.reference = ps_drivers_orders.order_reference\n" +
                        "\n" +
                        "and ps_orders.valid = 1\n" +  //valid orders only
                        "\n" +
                        "and ps_drivers_orders.id_driver =" + id_driver + "\n" +
                        "\n" +
                        "and ps_drivers_orders.date_add > DATE_SUB(NOW(), INTERVAL 12 HOUR)\n" + //12小时之内
                        "\n" +
                        "group by payment\n";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_drivers_orders.driver_name", "driver_name")
                        .columnMapping("ps_orders.payment", "payment_method")
                        .columnMapping("sum(total_paid_real)", "total_sale_online")      //total_paid_real        total_paid_real - total_shipping
                        .columnMapping("sum(total_shipping)", "total_shipping_online")
                        .columnMapping("count(payment)", "total_numberof_online_order")
                        .create();

        com.avaje.ebean.Query<TotalReturnDriverOrder> query = Ebean.find(TotalReturnDriverOrder.class);
        query.setRawSql(rawSql);
//        query.setParameter
        List<TotalReturnDriverOrder> list = query.findList();
        int size = list.size();
        for (int i =0; i <size; i++){
            list.get(i).total_sale_online = list.get(i).total_sale_online.subtract(list.get(i).total_shipping_online);
        }
        ArrayList<TotalReturnDriverOrder> totalReturnDriverOrders = new ArrayList<>();
        totalReturnDriverOrders.addAll(list);
        return totalReturnDriverOrders;
    }


    public static ArrayList<TotalReturnDriverOrder> getExceptionOrders(int id_driver) {  //算出网上使用 marina voucher 的订单数量
        String sql =
                "select ps_orders.payment, sum(total_shipping) as total_shipping_fee,  count(*) as number_of_order\n" +
                        "\n" +
                        "from ps_orders, ps_drivers_orders\n" +
                        "\n" +
                        "where ps_orders.id_order = ps_drivers_orders.id_order\n" +
                        "\n" +
                        "and ps_orders.reference = ps_drivers_orders.order_reference\n" +
                        "\n" +
                        "and ps_orders.valid = 1\n" +  //valid orders only
                        "\n" +
                        "and ps_orders.total_discounts = 2.000000\n" +  //网站使用voucher，数据库
                        "\n" +
                        "and ps_orders.total_shipping = 2.000000\n" +   //网站付钱
                        "\n" +
                        "and ps_drivers_orders.id_driver =" + id_driver + "\n" +
                        "\n" +
                        "and ps_drivers_orders.if_paid = 1\n" +
                        "\n" +
                        "and ps_drivers_orders.date_add > DATE_SUB(NOW(), INTERVAL 12 HOUR)\n" +
                        "\n" +
                        "group by payment\n";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_orders.payment", "payment_method")
                        .columnMapping("sum(total_shipping)", "total_shipping_online")
                        .columnMapping("count(*)", "marina_orders")      //获得特殊情况数量
                        .create();

        com.avaje.ebean.Query<TotalReturnDriverOrder> query = Ebean.find(TotalReturnDriverOrder.class);
        query.setRawSql(rawSql);
//        query.setParameter
        List<TotalReturnDriverOrder> list = query.findList();
        ArrayList<TotalReturnDriverOrder> totalReturnDriverOrders = new ArrayList<>();
        if(list !=null){
        totalReturnDriverOrders.addAll(list);
        }
        return totalReturnDriverOrders;
    }

    /**
     * After cashier allocates orders to a driver, and even after the driver comes back and pays back some
     * order sales, calculate how much unpaid money the driver needs to pay back. (Only with regard to cash-payment
     * orders)
     * @param id_driver
     * @see TotalReturnDriverOrder
     * */
    private static TotalReturnDriverOrder getUnpaidOrders(int id_driver) {
        String sql =
                "select sum(total_paid_real) as total_amount_sale, sum(total_shipping) as total_shipping_fee, count(*) as number_of_order\n" +
                        "\n" +
                        "from ps_orders, ps_drivers_orders\n" +
                        "\n" +
                        "where ps_orders.id_order = ps_drivers_orders.id_order\n" +
                        "\n" +
                        "and ps_orders.reference = ps_drivers_orders.order_reference\n" +
                        "\n" +
                        "and ps_orders.valid = 1\n" +  //valid orders only
                        "\n" +
                        "and ps_drivers_orders.id_driver =" + id_driver + "\n" +
                        "\n" +
                        "and ps_drivers_orders.if_paid = 0\n" +
                        "\n" +
                        "and ps_drivers_orders.paid_cashier = 0\n" +
                        "\n" +
                        "and ps_drivers_orders.date_add > DATE_SUB(NOW(), INTERVAL 12 HOUR)\n" +
                        "\n" +
                        "group by ps_orders.payment";   // 只有一种可能， 未付款的账单，是cash 支付

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("sum(total_paid_real)", "unpaid_amount")
                        .columnMapping("sum(total_shipping)", "total_shipping_online")
                        .columnMapping("count(*)", "unpaid_number")
                        .create();

        com.avaje.ebean.Query<TotalReturnDriverOrder> query = Ebean.find(TotalReturnDriverOrder.class);
        query.setRawSql(rawSql);
        TotalReturnDriverOrder unpaid_orders = query.findUnique();
        String sqlException =
                "select sum(total_shipping) as total_shipping_fee\n" +
                        "\n" +
                        "from ps_orders, ps_drivers_orders\n" +
                        "\n" +
                        "where ps_orders.id_order = ps_drivers_orders.id_order\n" +
                        "\n" +
                        "and ps_orders.reference = ps_drivers_orders.order_reference\n" +
                        "\n" +
                        "and ps_orders.valid = 1\n" +  //valid orders only
                        "\n" +
                        "and ps_orders.total_discounts = 2.000000\n" +
                        "\n" +
                        "and ps_orders.total_shipping = 2.000000\n" +
                        "\n" +
                        "and ps_drivers_orders.id_driver =" + id_driver + "\n" +
                        "\n" +
                        "and ps_drivers_orders.if_paid = 0\n" +
                        "\n" +
                        "and ps_drivers_orders.paid_cashier = 0\n" +
                        "\n" +
                        "and ps_drivers_orders.date_add > DATE_SUB(NOW(), INTERVAL 12 HOUR)\n" +
                        "\n" +
                        "group by ps_orders.payment";  //防止数据库返回值为空
        RawSql rawSqlException =
                RawSqlBuilder
                        .parse(sqlException)
                        .columnMapping("sum(total_shipping)", "total_shipping_online")
                        .create();
        com.avaje.ebean.Query<TotalReturnDriverOrder> queryException = Ebean.find(TotalReturnDriverOrder.class);
        queryException.setRawSql(rawSqlException);
        TotalReturnDriverOrder unpaidException = queryException.findUnique();
        if(unpaidException == null){
            unpaidException = new TotalReturnDriverOrder();
            unpaidException.total_shipping_online = BigDecimal.valueOf(0);
        }else {
            if(unpaidException.total_shipping_online ==null){
                unpaidException.total_shipping_online = BigDecimal.valueOf(0);
            }
        }
        if(unpaid_orders != null){
        unpaid_orders.unpaid_amount = unpaid_orders.unpaid_amount.subtract(unpaid_orders.total_shipping_online).add(unpaidException.total_shipping_online);
        }else{
          unpaid_orders = new TotalReturnDriverOrder();
        }
        return unpaid_orders;
    }

    /**
     * Total delivery details and orders of a driver in one day
     * Summary about: online-payment quantity, cash-payment quantity,
     * online-payment total amount, cash-payment total amount,
     * and all the orders he delivered including
     * */
    public static Result totalOrdersOfDriver(int id_driver) {
        response().setHeader("Connection","close");
        ArrayList<TotalReturnDriverOrder> totalReturnDriverOrders;
        totalReturnDriverOrders = getOrdersOfDriver(id_driver);
        /**create two empty object for later use. One will be assigned the value as "Cash payment summary"
         *the other will be assigned the value of "Online payment summary"
         * */
        TotalReturnDriverOrder a; //cash
        TotalReturnDriverOrder b;  //online
        TotalReturnDriverOrder c;  //online
        TotalReturnDriverOrder exception_a = new TotalReturnDriverOrder();  //cash
        TotalReturnDriverOrder exception_b = new TotalReturnDriverOrder();   //online
        TotalReturnDriverOrder exception_c = new TotalReturnDriverOrder();   //online
        TotalReturnDriverOrder total = new TotalReturnDriverOrder();
        TotalReturnDriverOrder summary = getUnpaidOrders(id_driver);
        if (totalReturnDriverOrders != null){
            if (totalReturnDriverOrders.size() == 1 ){
                    a = totalReturnDriverOrders.get(0);
                if(a.payment_method.contains("Cash")){
                    total.total_numberof_cash_order = a.total_numberof_online_order;
                    total.total_sale_cash = a.total_sale_online;
                    total.total_shipping_cash = a.total_shipping_online;
                    total.driver_name = a.driver_name;
                }else{
                    total.total_numberof_online_order= a.total_numberof_online_order;
                    total.total_sale_online= a.total_sale_online;
                    total.total_shipping_online = a.total_shipping_online;
                    total.driver_name = a.driver_name;
                }
            }else if (totalReturnDriverOrders.size() == 2){
                if (totalReturnDriverOrders.get(0).payment_method.contains("Ingenico") && totalReturnDriverOrders.get(1).payment_method.contains("Online")){
                    b = totalReturnDriverOrders.get(0); //online
                    c = totalReturnDriverOrders.get(1); //online
                    total.total_numberof_online_order= b.total_numberof_online_order + c.total_numberof_online_order;
                    total.total_sale_online= b.total_sale_online.add(c.total_sale_online);
                    total.total_shipping_online = b.total_shipping_online.add(c.total_shipping_online);
                    total.driver_name = b.driver_name;
                }
                else{
                    a = totalReturnDriverOrders.get(1); //online
                    b = totalReturnDriverOrders.get(0); //cash
                    total.total_numberof_online_order = a.total_numberof_online_order;
                    total.total_sale_online = a.total_sale_online;
                    total.total_shipping_online = a.total_shipping_online;
                    total.total_numberof_cash_order = b.total_numberof_online_order;
                    total.total_sale_cash = b.total_sale_online;
                    total.total_shipping_cash = b.total_shipping_online;
                    total.driver_name = a.driver_name;
                }

            }else if (totalReturnDriverOrders.size() == 3){
                a = totalReturnDriverOrders.get(0);  //代表 cash
                b = totalReturnDriverOrders.get(1);  //代表 online
                c = totalReturnDriverOrders.get(2);  //代表 online

                total.total_numberof_cash_order = a.total_numberof_online_order;
                total.total_sale_cash = a.total_sale_online;
                total.total_shipping_cash = a.total_shipping_online;

                total.total_numberof_online_order = b.total_numberof_online_order + c.total_numberof_online_order;
                total.total_sale_online = b.total_sale_online.add(c.total_sale_online);
                total.total_shipping_online = b.total_shipping_online.add(c.total_shipping_online);
                total.driver_name = a.driver_name;
            }
            if (summary != null){
                total.unpaid_amount = summary.unpaid_amount;
                total.unpaid_number = summary.unpaid_number;
            }
            else {
                total.unpaid_amount = BigDecimal.valueOf(0);
                total.unpaid_number = 0;
            }
            int marinaExceptionOrders = 0;
            int marinaNormalOrders = 0;
            ArrayList<TotalReturnDriverOrder> totalExceptionOrders;
            totalExceptionOrders = getExceptionOrders(id_driver);
            if(totalExceptionOrders.size() > 0 ){
                if (totalExceptionOrders.size() ==1){
                    if(totalExceptionOrders.get(0).payment_method.contains("Cash")){
                        exception_a = totalExceptionOrders.get(0);
                    }else {
                        exception_b = totalExceptionOrders.get(0);
                    }
                }else if (totalExceptionOrders.size() ==2){
                    if(totalExceptionOrders.get(0).payment_method.contains("Cash")){
                        exception_a = totalExceptionOrders.get(0);
                        exception_b = totalExceptionOrders.get(1);
                    }else if(totalExceptionOrders.get(0).payment_method.contains("Ingenico") && totalExceptionOrders.get(1).payment_method.contains("Online")){
                        exception_b = totalExceptionOrders.get(0);
                        exception_c = totalExceptionOrders.get(1);
                    }else{
                        exception_a = totalExceptionOrders.get(1);
                        exception_b = totalExceptionOrders.get(0);
                    }
                }else if (totalExceptionOrders.size() ==3){
                        exception_a = totalExceptionOrders.get(0);
                        exception_b = totalExceptionOrders.get(1);
                        exception_c = totalExceptionOrders.get(2);
                }

                total.total_sale_cash = total.total_sale_cash.add(exception_a.total_shipping_online);
                total.total_sale_online = total.total_sale_online.add(exception_b.total_shipping_online).add(exception_c.total_shipping_online);
                total.total_shipping_cash = total.total_shipping_cash.subtract(exception_a.total_shipping_online);
                total.total_shipping_online = total.total_shipping_online.subtract(exception_b.total_shipping_online).subtract(exception_c.total_shipping_online);
                marinaExceptionOrders = exception_a.marina_orders + exception_b.marina_orders + exception_c.marina_orders;  //网站 使用 marina voucher
            }
            /**
             * Query all the orders delivered by this certain driver, return a list of orders
             * @see DriverOrders
             * */
            String sql =
                    "select ps_drivers_orders.id_order, ps_drivers_orders.driver_name, ps_drivers_orders.order_reference, ps_drivers_orders.order_number, ps_drivers_orders.if_paid, ps_orders.id_address_delivery, ps_orders.payment, ps_orders.total_paid_real, ps_orders.total_discounts, ps_orders.total_shipping, ps_order_invoice.delivery_address\n" +
                            "\n" +
                            "from ps_orders, ps_drivers_orders, ps_order_invoice\n" +
                            "\n" +
                            "where ps_orders.id_order = ps_drivers_orders.id_order\n" +
                            "\n" +
                            "and ps_orders.id_order = ps_order_invoice.id_order\n" +
                            "\n" +
                            "and ps_orders.reference = ps_drivers_orders.order_reference\n" +
                            "\n" +
                            "and ps_orders.valid = 1\n" +  //valid orders only
                            "\n" +
                            "and ps_drivers_orders.id_driver =" + id_driver + "\n" +
                            "\n" +
                            "and ps_drivers_orders.date_add > DATE_SUB(NOW(), INTERVAL 12 HOUR)\n" +
                            "\n" +
                            "order by id_order";
            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_drivers_orders.id_order", "id_order")
                            .columnMapping("ps_drivers_orders.driver_name", "driver_name")
                            .columnMapping("ps_drivers_orders.order_reference", "order_reference")
                            .columnMapping("ps_drivers_orders.order_number", "order_number")
                            .columnMapping("ps_drivers_orders.if_paid", "if_paid")
                            .columnMapping("ps_orders.id_address_delivery", "id_address_delivery")
                            .columnMapping("ps_orders.payment", "payment")
                            .columnMapping("ps_orders.total_paid_real", "total_paid_real")  // calculate only the products prices
                            .columnMapping("ps_orders.total_discounts", "total_discounts")
                            .columnMapping("ps_orders.total_shipping", "total_shipping")
                            .columnMapping("ps_order_invoice.delivery_address", "delivery_address")
                            .create();
            com.avaje.ebean.Query<ReturnDriverOrder> query = Ebean.find(ReturnDriverOrder.class);
            query.setRawSql(rawSql);
            List<ReturnDriverOrder> list = query.findList();
            int size = list.size();
            for (int i = 0; i<size; i++){
                BigDecimal discount_amount = list.get(i).total_discounts;
                BigDecimal shipping_amount = list.get(i).total_shipping;

                if (shipping_amount.doubleValue() == 0.0){
                    marinaNormalOrders +=1;
                }

                if(discount_amount.equals(shipping_amount) && discount_amount.doubleValue() == 2.0 ){ //特殊订单
                list.get(i).total_paid_real = list.get(i).total_paid_real;
                list.get(i).total_shipping = BigDecimal.valueOf(0.0);
                }else{
                list.get(i).total_paid_real = list.get(i).total_paid_real.subtract(list.get(i).total_shipping);
                }
            }
            ArrayList<ReturnDriverOrder> totalOrders = new ArrayList<>();
            totalOrders.addAll(list);
            total.driverOrders = totalOrders;
            total.marina_orders = marinaNormalOrders + marinaExceptionOrders;  //总共Marina 订单
            return ok(renderCategories(total));
        } else {
            return notFound("No such driver");
        }
    }

    public static Result undeliveredOrders(int id_driver) {   //司机没有送的订单
        response().setHeader("Connection","close");
            /**
             * Query all the orders delivered by this certain driver, return a list of orders
             * @see DriverOrders
             * */
            String sql =
                    "select ps_drivers_orders.id_order, ps_drivers_orders.driver_name, ps_drivers_orders.order_reference, ps_drivers_orders.order_number, ps_drivers_orders.if_paid, ps_drivers_orders.start_time, ps_orders.id_address_delivery, ps_orders.payment, ps_orders.total_paid_real, ps_orders.total_discounts, ps_orders.total_shipping, ps_order_invoice.delivery_address\n" +
                            "\n" +
                            "from ps_orders, ps_drivers_orders, ps_order_invoice\n" +
                            "\n" +
                            "where ps_orders.id_order = ps_drivers_orders.id_order\n" +
                            "\n" +
                            "and ps_orders.id_order = ps_order_invoice.id_order\n" +
                            "\n" +
                            "and ps_orders.reference = ps_drivers_orders.order_reference\n" +
                            "\n" +
                            "and ps_orders.valid = 1\n" +  //valid orders only
                            "\n" +
                            "and ps_drivers_orders.id_driver =" + id_driver + "\n" +
                            "\n" +
                            "and ps_drivers_orders.if_delivered =0\n" +
                            "\n" +
                            "and ps_drivers_orders.date_add > DATE_SUB(NOW(), INTERVAL 12 HOUR)\n" +
                            "\n" +
                            "order by id_order";
            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_drivers_orders.id_order", "id_order")
                            .columnMapping("ps_drivers_orders.driver_name", "driver_name")
                            .columnMapping("ps_drivers_orders.order_reference", "order_reference")
                            .columnMapping("ps_drivers_orders.order_number", "order_number")
                            .columnMapping("ps_drivers_orders.if_paid", "if_paid")
                            .columnMapping("ps_drivers_orders.start_time", "start_time")
                            .columnMapping("ps_orders.id_address_delivery", "id_address_delivery")
                            .columnMapping("ps_orders.payment", "payment")
                            .columnMapping("ps_orders.total_paid_real", "total_paid_real")  // calculate only the products prices
                            .columnMapping("ps_orders.total_discounts", "total_discounts")
                            .columnMapping("ps_orders.total_shipping", "total_shipping")
                            .columnMapping("ps_order_invoice.delivery_address", "delivery_address")
                            .create();
            com.avaje.ebean.Query<ReturnDriverOrder> query = Ebean.find(ReturnDriverOrder.class);
            query.setRawSql(rawSql);
            List<ReturnDriverOrder> list = query.findList();
            int size = list.size();
           if(size > 0 ) {
               for (int i = 0; i < size; i++) {
                   BigDecimal discount_amount = list.get(i).total_discounts;
                   BigDecimal shipping_amount = list.get(i).total_shipping;
                   if (discount_amount.equals(shipping_amount) && discount_amount.doubleValue() == 2.0) { //特殊订单
                       list.get(i).total_paid_real = list.get(i).total_paid_real;
                       list.get(i).total_shipping = BigDecimal.valueOf(0.0);
                   } else {
                       list.get(i).total_paid_real = list.get(i).total_paid_real.subtract(list.get(i).total_shipping);
                   }
                   int id_address_delivery = list.get(i).id_address_delivery;
                   AddressTemplate delivery_address = new AddressTemplate();                           //Define an object using AddressTemplate for later return
                   if (id_address_delivery != 0) {                                      //If there is an address
                       Address address = Address.findById(id_address_delivery);         //Find the address in the table of Address
                       String alias = address.alias;
                       String company = address.company;
                       String firstname = address.firstname;
                       String lastname = address.lastname;
                       String address1 = address.address1;
                       String address2 = address.address2;
                       String city = address.city;
                       String phone = address.phone;
                       delivery_address = new AddressTemplate(
                               alias,
                               company,
                               firstname,
                               lastname,
                               address1,
                               address2,
                               city,
                               phone
                       );
                   }
                   list.get(i).customer_address = delivery_address;
               }
           }
            ArrayList<ReturnDriverOrder> totalOrders = new ArrayList<>();
            totalOrders.addAll(list);
            return ok(renderCategories(totalOrders));
    }

    @Transactional
    public static Result addToDelivered(int id_driver,int id_order) {   //司机发送已经送达请求
        response().setHeader("Connection","close");
        DriverOrders this_order = DriverOrders.findByBoth(id_driver, id_order);
        if (this_order == null) {
            return badRequest("Invalid driver-order combination");
        } else {
            this_order.update(1,new Date());
            this_order.save();
//            changeOrderStatus(id_order,5);
            Order.updateOneRecord(id_order,5);
            OrderHistory orderHistory = new OrderHistory(
                    0,
                    id_order,
                    5,
                    new Date()
            );
            orderHistory.save(); //增加状态更改记录
            return ok("Order delivered successfully!");
        }
    }
  /**
   * 司机向服务器发送开始送单这个动作
   * */
    public static Result addStartTime(int id_driver,int id_order) {   //司机发送开始配送请求
        response().setHeader("Connection","close");
        DriverOrders this_order = DriverOrders.findByBoth(id_driver, id_order);
        if (this_order == null) {
            return badRequest("Invalid driver-order combination");
        } else {
            Date new_time = new Date();
            String dml = "UPDATE ps_drivers_orders\n" +
                    "SET start_time = :start_time\n" +
                    "WHERE id_driver = :id_driver\n"+
                    "AND id_order = :id_order\n";
            SqlUpdate update = Ebean.createSqlUpdate(dml)
                    .setParameter("start_time", new_time)
                    .setParameter("id_driver", id_driver)
                    .setParameter("id_order", id_order);
            update.execute();
            //给用户发送司机出发的推送提示 todo: 暂时不发送打开地图的推送
//            int id_customer = Order.findById(id_order).id_customer; //找到订单用户ID， 149 和 102 是前台
//            //添加另外一个线程，用来发送notification 给用户
//            Runnable sendNotification = new Runnable() {
//                @Override
//                public void run() {
//                    sendNotificationForDelivery(id_customer,id_order);
//                }
//            };
//            if(id_customer!=149 && id_customer!=102){ //如果不是前台点单，就发送推送
//                new Thread(sendNotification).start();
//            }
            return ok("Order started successfully!");
        }
    }

    /**
     * Cashier controls drivers to log in, log in with Driver_ID
     * Change the value of if_work in the Drivers Table from 0 to 1
     * */
    public static Result AppDriverLogin() throws Exception {
        response().setHeader("Connection","close");
        DynamicForm driverForm = Form.form().bindFromRequest();
        String phone_number = driverForm.get("phone_number");
        if(phone_number == null){
            phone_number = "";
        }
        String password = driverForm.get("password");
        if(password == null){
            password = "";
        }
        Date login_time = new Date();
        Driver driver = Driver.findByPhone(phone_number);
        if (driver != null) {
            if(driver.password.equals(password)){
                int id_driver = driver.id_driver;
//            driver.app_last_login = login_time;
//            driver.save();
                //With Update rawSQL clause
                String dml = "UPDATE ps_drivers\n" +
                        "SET app_last_login = :app_last_login\n" +
                        "WHERE id_driver = :id_driver";
                SqlUpdate update = Ebean.createSqlUpdate(dml)
                        .setParameter("app_last_login", login_time)
                        .setParameter("id_driver", id_driver);
                update.execute();
            return ok(renderCategory(driver.id_driver));
            }
            else {
                return badRequest("Wrong password");
            }
        }else {
            return notFound("Invalid driver");
        }
    }

    /**
     * Cashier controls drivers to log in, log in with Driver_ID
     * Change the value of if_work in the Drivers Table from 0 to 1
     * */
    public static Result AppDriverLogout(int id_driver) throws Exception {
        response().setHeader("Connection","close");
        Date logout_time = new Date();
        Driver driver = Driver.findById(id_driver);
        if (driver != null) {
            driver.app_last_logout = logout_time;
            driver.save();
        return ok("Log out successfully");
        }else {
        return notFound("Invalid driver");
        }
    }

}
