/*
 * Copyright (c) 2017. Zhi Chen.
 */

package util.http;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.database.CustomerBrowserToken;
import models.sqlContainer.DeviceTokenTemplate;
import play.libs.Json;
import play.libs.WS;

import java.util.List;
public class SendHttp {
    static String public_key = ""; //Key
    static String private_key = ""; 
    public static void browserNotification(String data) {  //通过第三方，给浏览器客户发送通知
        List<CustomerBrowserToken> all_token = CustomerBrowserToken.findAll();
        for(CustomerBrowserToken this_one: all_token){  //循环发送给所有用户，单线程
        String endpoint = this_one.endpoint;
        String p256dh = this_one.p256dh;
        String auth = this_one.auth;
        ObjectNode application_keys = Json.newObject();
        application_keys.put("public",public_key);
        application_keys.put("private",private_key);
        ObjectNode keys_object = Json.newObject();
        keys_object.put("p256dh",p256dh);
        keys_object.put("auth",auth);
        ObjectNode subscription_Object = Json.newObject();
        subscription_Object.put("endpoint",endpoint);
        subscription_Object.put("keys",keys_object);
        ObjectNode post_content = Json.newObject();
        post_content.put("subscription",subscription_Object);
        post_content.put("data",data);
        post_content.put("applicationKeys",application_keys);
//        Promise<WS.Response> result = WS.url("https://web-push-codelab.glitch.me/api/send-push-msg")//该死的谷歌一直在换域名
//                .setContentType("application/json")
//                .post(post_content);
        WS.url("https://web-push-codelab.glitch.me/api/send-push-msg")
//                .setHeader("Content-Type","application/json")
                    .setContentType("application/json")
                    .post(post_content);   //不等待返回值
        System.out.println("Sent Http Post Request!");
        }
    }
    public static void deviceNotification(int customer_id, int mobile_type) {  //通过firebase API, 给用户设备发送推送通知
        String server_key = "";
        String to = "";//数据库传送过来的字符,目标用户的device_token
        if(mobile_type == 1){ //安卓用户
         //使用安卓 server_key
            server_key = "###";
            String sql ="select device_token, device_type\n" +
                    "from ps_customer_device_token\n" +
                    "where id_customer =" + customer_id+"\n" +
                    "and device_type = 'android'\n" +
                    "order by last_access_time desc\n" +
                    "limit 1";
            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("device_token", "device_token")
                            .columnMapping("device_type", "device_type")
                            .create();
            com.avaje.ebean.Query<DeviceTokenTemplate> query = Ebean.find(DeviceTokenTemplate.class);
            query.setRawSql(rawSql);
            DeviceTokenTemplate recent_one = query.findUnique();
            if(recent_one !=null){ //存在最新device_token记录
               to = recent_one.device_token;
            }
        }else if(mobile_type == 2){//苹果用户
         //使用苹果 server_key
            server_key = "###";
            String sql ="select device_token, device_type\n" +
                    "from ps_customer_device_token\n" +
                    "where id_customer =" + customer_id+ "\n" +
                    "and device_type = 'IOS'\n" +
                    "order by last_access_time desc\n" +
                    "limit 1";
            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("device_token", "device_token")
                            .columnMapping("device_type", "device_type")
                            .create();
            com.avaje.ebean.Query<DeviceTokenTemplate> query = Ebean.find(DeviceTokenTemplate.class);
            query.setRawSql(rawSql);
            DeviceTokenTemplate recent_one = query.findUnique();
            if(recent_one !=null){ //存在最新device_token记录
              to = recent_one.device_token;
            }
        }else if(mobile_type ==0){ //待定用户
            String sql ="select device_token, device_type\n" +
                    "from ps_customer_device_token\n" +
                    "where id_customer =" + customer_id+"\n" +
                    "order by last_access_time desc\n" +
                    "limit 1";
            RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("device_token", "device_token")
                        .columnMapping("device_type", "device_type")
                        .create();
            com.avaje.ebean.Query<DeviceTokenTemplate> query = Ebean.find(DeviceTokenTemplate.class);
            query.setRawSql(rawSql);
            DeviceTokenTemplate recent_one = query.findUnique();
            if(recent_one !=null){ //存在最新device_token记录
              String device_type = recent_one.device_type;
              if(device_type.contains("IOS")){
                  //使用苹果 server_key
                  server_key = "###";
              }else {
                  //使用安卓 server_key
                  server_key = "###";
              }
              to = recent_one.device_token;
            }
        }
        //获取值以后，拼接JsonNode, 作为每一个对象
        //先检验是否存在device
        if(!to.equals("")){
        String title = "Order Confirmed!";
        String body = "Thanks for ordering with A2B Living.";
            ObjectNode notification_body = Json.newObject();
            notification_body.put("title",title);
            notification_body.put("body",body);
            ObjectNode overall_body = Json.newObject();
            overall_body.put("notification",notification_body);
            overall_body.put("to",to);
            WS.url("https://fcm.googleapis.com/fcm/send")//谷歌发送notification API
                    .setHeader("Authorization","key="+ server_key)
                    .setContentType("application/json")
                    .post(overall_body); //直接发送推送，不等待返回值
//            Promise<WS.Response> result = WS.url("https://fcm.googleapis.com/fcm/send")//谷歌发送notification API
//                    .setHeader("Authorization","key="+ server_key)
//                    .setContentType("application/json")
//                    .post(overall_body);
            System.out.println("Sent Http Post Request!");
        }else{
            System.out.println("This Customer has no device token!");
        }
    }
    /**
     * 给用户notification告诉他们司机在配送订单
     * */
    public static void sendNotificationForDelivery(int customer_id, int id_order){  //通过firebase API, 给用户设备发送推送通知
        String server_key = "";
        String to = "";//数据库传送过来的字符,目标用户的device_token
        String sql ="select device_token, device_type\n" +
                "from ps_customer_device_token\n" +
                "where id_customer =" + customer_id+"\n" +
                "order by last_access_time desc\n" +
                "limit 1";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("device_token", "device_token")
                        .columnMapping("device_type", "device_type")
                        .create();
        com.avaje.ebean.Query<DeviceTokenTemplate> query = Ebean.find(DeviceTokenTemplate.class);
        query.setRawSql(rawSql);
        DeviceTokenTemplate recent_one = query.findUnique();
        if(recent_one !=null){ //存在最新device_token记录
            String device_type = recent_one.device_type;
            if(device_type.contains("IOS")){
                //使用苹果 server_key
                server_key = "###";
            }else {
                //使用安卓 server_key
                server_key = "###";
            }
            to = recent_one.device_token;
        }
        //获取值以后，拼接JsonNode, 作为每一个对象
        //先检验是否存在device
        if(!to.equals("")){
            String title = "Your order is on it’s way!";
            String body = "You can now find out where your drive is.";
            ObjectNode notification_body = Json.newObject();
            notification_body.put("title",title);
            notification_body.put("body",body);
            notification_body.put("type","driver");  //添加特征值，说明是司机系统的notification
            notification_body.put("url","target_url");  //添加特征值，用来拼接打开地图的连接
            notification_body.put("id_order",id_order);
            ObjectNode overall_body = Json.newObject();
            overall_body.put("notification",notification_body);
            overall_body.put("to",to);
//            Promise<WS.Response> result = WS.url("https://fcm.googleapis.com/fcm/send")//谷歌发送notification API
//                    .setHeader("Authorization","key="+ server_key)
//                    .setContentType("application/json")
//                    .post(overall_body);
            WS.url("https://fcm.googleapis.com/fcm/send")//谷歌发送notification API
                    .setHeader("Authorization","key="+ server_key)
                    .setContentType("application/json")
                    .post(overall_body);  //直接发送，不等待接收值
            System.out.println("Sent Http Post Request!");
        }else{
            System.out.println("This Customer has no device token!");
        }
    }
}
