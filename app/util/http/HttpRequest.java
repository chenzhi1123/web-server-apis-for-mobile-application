/*
 * Copyright (c) 2017. Zhi Chen.
 */

package util.http;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;

public class HttpRequest {
    /**
     * Get Request to certain URL
     *
     * @param param: data form should be: name1=value1&name2=value2
     * @return URL, feedback result
     */
    public static void sendPost(String param) {
//        String result = "";
        String url = "target url";
        BufferedReader in = null;
        try {
            String urlNameString = url + "?" + param;
            URL realUrl = new URL(urlNameString);
            // Open links between sender and target URL
            URLConnection connection = realUrl.openConnection();
            // Set general request parameters
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            // Establish real connections
            connection.connect();
            // Get all responding headers
//            Map<String, List<String>> map = connection.getHeaderFields();
//            // forlooop all headers
//            for (String key : map.keySet()) {
//                System.out.println(key + "--->" + map.get(key));
//            }
            // Define BufferedReader input stream to read URL Responds
            in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
//            String line;
//            while ((line = in.readLine()) != null) {
//                result += line;
//            }
        } catch (Exception e) {
//            System.out.println("hTTP Exception: "+new Date());
            System.out.println("GET Request has error!" + e);
//            e.printStackTrace();
        }
        // Use finally to close the data stream
        finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        System.out.println("Update Last Login time");
    }

    /**
     * Send Post Request to certain URL
     * @param param
     * @return
     */
    public static void sendOnePost(String param) {
        PrintWriter out = null;
        BufferedReader in = null;
        String url = "Target url";
        try {
            URL realUrl = new URL(url);
            // Open connections to certain URL
            URLConnection conn = realUrl.openConnection();
            // Set General request parameters
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            // POST requests must execute the following two lines
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // Get URLConnection object output stream
            out = new PrintWriter(conn.getOutputStream());
            // Send request params
            out.print(param);
            // Flush buffers the output stream
            out.flush();
            // Define BufferReader input stream to read URL respond contents
            in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
        } catch (Exception e) {
            System.out.println("exceptions happened!"+e);
            e.printStackTrace();
        }
        //Use finally block to shut the input and output streams
        finally{
            try{
                if(out!=null){
                    out.close();
                }
                if(in!=null){
                    in.close();
                }
            }
            catch(IOException ex){
                ex.printStackTrace();
            }
        }

    }


    public static String getReferralUrl(String plaintext) { //获取推荐人的url拼接
        String result = "";
        String url = "http://www.a2bliving.ie/api/Rijndael.php";
        BufferedReader in = null;
        try {
            String urlNameString = url + "?" + "plaintext=" + plaintext;
            URL realUrl = new URL(urlNameString);
            // Open links between sender and target URL
            URLConnection connection = realUrl.openConnection();
            // Set general request parameters
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            // Establish real connections
            connection.connect();
            // Get all responding headers
//            Map<String, List<String>> map = connection.getHeaderFields();
//            // forlooop all headers
//            for (String key : map.keySet()) {
//                System.out.println(key + "--->" + map.get(key));
//            }
            // Define BufferedReader input stream to read URL Responds
            in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
//            System.out.println("hTTP Exception: "+new Date());
            System.out.println("GET Request has error!" + e);
//            e.printStackTrace();
        }
        // Use finally to close the data stream
        finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return result;
    }
}
