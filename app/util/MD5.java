/*
 * Copyright (c) 2017. Zhi Chen.
 */

package util;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;

public class MD5 {
    public static String getMD5(String input) {
        byte[] source;
        try {
            //Get byte according by specified coding.
            source = input.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            source = input.getBytes();
        }
        String result = null;
        char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7',
                '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(source);
            //The result should be one 128 integer
            byte temp[] = md.digest();
            char str[] = new char[16 * 2];
            int k = 0;
            for (int i = 0; i < 16; i++) {
                byte byte0 = temp[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            result = new String(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
 
    public static String encrypt(String password) {
      String cookie_key = "JMGT8HxNufQnkGqVn89Nnr8vvxpWj2iQLDxga7rYiBDOuIqbXJW7SkH0";
      String newpassword = cookie_key+password;
      return getMD5(newpassword);
    }

    public static String getSecureKey() {
        Long h = System.currentTimeMillis();//get currently millisecond number
        String str = h.toString();         // transform it into a string
        return getMD5(str);}
      
//   public static String (String passwd){
//     String secure_key = passwd;
//     return getMD5(secure_key);
//     
//   }

    
//    public static void main(String[] args){
//     System.out.println(encrypt("911220"));     
//   }
}
