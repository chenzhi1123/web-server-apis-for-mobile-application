/*
 * Copyright (c) 2017. Zhi Chen.
 */

package util.sendEmail;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.Version;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class EmailSender {

    Properties props;
    Session session;

    public EmailSender( Properties props,Session session) {
        this.props=props;
        this.session=session;
    }

    public static void sendEmailToCustomer(String firstName, String emailAddress, String voucher_code){
        Properties props = new Properties();
//sample connection from Google
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("noreply.a2b@gmail.com",
                                "###");
                    }
                });
        new EmailSender(props,session).startPrint(emailAddress,firstName,voucher_code);
    }


    public void startPrint(String email,String userName,String voucher_code){
        String c= this.generateCounterHtmlString(userName, voucher_code);
//        System.out.println(new EmailSender().generateCounterHtmlString());
        sendEmails(c,email);
    }

    private String generateCounterHtmlString(String firstName, String voucher_code) {
        String contents = "";
        Configuration cfg = new Configuration(new Version("2.3.25-incubating"));
        try {
            // String workingDir = System.getProperty("user.dir");
            cfg.setClassForTemplateLoading(this.getClass(),"/templates");
            cfg.setDefaultEncoding("UTF-8");
            Template template = cfg.getTemplate("/a2b-email_template.ftl");
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("user_name", firstName);
            data.put("voucher_code", voucher_code);
            Writer out = new StringWriter();
            template.process(data, out);
            contents = out.toString();
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
        }
        return contents;
    }


    public void sendEmails(String content, String email){
        //put this in one of your methods:
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("info@a2bliving.ie"));
//            InternetAddress[]to = {new InternetAddress("chenzhi1123@gmail.com"),
//                    new InternetAddress("info@a2bliving.ie"),
//                    new InternetAddress("saiwingho0824@gmail.com"),
//                    new InternetAddress("markmavambu@gmail.com")};
            message.addRecipients(Message.RecipientType.TO,InternetAddress.parse(email));
            message.setSubject("[A2B Living] Welcome!");
            message.setContent(content,"text/html");
            Transport.send(message);
        } catch (MessagingException e) {
            System.out.println("unable to send email: " + e);
        }
    }
}
