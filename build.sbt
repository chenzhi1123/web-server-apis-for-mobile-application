name := "webapi"

version := "1.0-SNAPSHOT"

scalaVersion := "2.10.3"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
   "net.sf.flexjson" % "flexjson" % "3.1",
   "mysql" % "mysql-connector-java" % "5.1.18",
   "org.json"%"org.json"%"chargebee-1.0",
   "org.freemarker"%"freemarker-gae"%"2.3.25-incubating",
   "javax.mail"%"mail"%"1.4.7"
)

play.Project.playJavaSettings

//scalacOptions ++= Seq(
//  "-encoding",
//  "UTF-8",
//  "-deprecation",
//  "-unchecked",
//  "-feature",
//  "-language:postfixOps",
//  "-language:implicitConversions"
//)
//
//play.Project.playScalaSettings